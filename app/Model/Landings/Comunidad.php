<?php

namespace App\Model\Landings;

use Illuminate\Database\Eloquent\Model;

class Comunidad extends Model
{
    protected $table = 'landings_comunidad';

    protected $fillable = [
        'name',
        'lastname',
        'gender_id',
        'email',
        'birthdate',
        'accept_email',
        'age_verification',
        'phone',
        'document',
        'operator_id',
    ];
}
