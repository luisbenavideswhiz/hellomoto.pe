<?php

namespace App\Model\Landings;

use Illuminate\Database\Eloquent\Model;

class DimitriVegas extends Model
{
    protected $table = 'landings_dimitrivegas';

    protected $fillable = [
        'name',
        'lastname',
        'gender_id',
        'email',
        'birthdate',
        'accept_email',
        'age_verification',
        'phone',
        'document',
        'operator_id',
    ];
}
