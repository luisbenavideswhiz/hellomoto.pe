<?php

namespace App\Model\Landings;

use Illuminate\Database\Eloquent\Model;

class Subtable extends Model
{
    protected $table = 'landings_subtables';
}
