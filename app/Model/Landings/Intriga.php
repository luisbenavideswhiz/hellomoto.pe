<?php

namespace App\Model\Landings;

use Illuminate\Database\Eloquent\Model;

class Intriga extends Model
{
    protected $table = 'landings_intriga';
    protected $fillable = ['name', 'email'];
}
