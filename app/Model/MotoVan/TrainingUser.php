<?php

namespace App\Model\MotoVan;

use Illuminate\Database\Eloquent\Model;

class TrainingUser extends Model
{
    protected $table = 'motovan_training_user';
    protected $fillable = ['training_id', 'user_id'];
}
