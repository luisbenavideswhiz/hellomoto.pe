<?php

namespace App\Model\MotoVan;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'motovan_users';
    protected $fillable = ['name','last_name','area','birthdate','document_number','email','phone','terms_conditions'];

    public function trainings()
    {
        return $this->belongsToMany('App\Model\MotoVan\Training', 'motovan_training_user');
    }

    public function hasTraining($id)
    {
        return $this->trainings()->where('training_id','=',$id)->count()>0;
    }
}
