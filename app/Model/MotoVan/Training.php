<?php

namespace App\Model\MotoVan;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    protected $table = 'motovan_trainings';
    protected $dates = ['date'];
    protected $fillable = ['date', 'total_coupons', 'remaining_coupons', 'location', 'status'];

    public function users()
    {
        return $this->belongsToMany('App\Model\MotoVan\User', 'motovan_training_user');
    }

    public function getDateFormattedAttribute()
    {
        setlocale(LC_TIME, 'es_PE.utf8');
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->date)
            ->formatLocalized('%B %d, %A');
    }

    public function getTimeFormattedAttribute()
    {
        return $this->date->format('h:i a');
    }
}
