<?php

namespace App\Model\Trivia;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Option extends Model
{
    use SoftDeletes;

    protected $table = "trivia_options";
    protected $fillable = ['question_id', 'answer', 'correct'];

    public function scopeQuestion($sql, $question)
    {
        return $sql->where('question_id', $question);
    }
}