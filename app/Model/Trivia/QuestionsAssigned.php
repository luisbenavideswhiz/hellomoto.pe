<?php

namespace App\Model\Trivia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionsAssigned extends Model
{
    use SoftDeletes;

    protected $table = "trivia_questions_assigned";
    protected $fillable = ['question_id', 'user_id', 'count'];

    public function scopeActive($sql)
    {
        return $sql->where('active', 1);
    }

    public function scopeuserCount($sql, $user, $count)
    {
        return $sql->where('user_id', $user)
            ->where('count', $count);
    }

    public function scopeUserQuestion($sql, $user, $question)
    {
        return $sql->where('user_id', $user)
                    ->where('question_id', $question);
    }

    public function scopeUser($sql, $user)
    {
        return $sql->where('user_id', $user);
    }

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id', 'id');
    }
}