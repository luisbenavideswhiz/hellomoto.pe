<?php

namespace App\Model\Trivia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;

    protected $table = 'trivia_question';
    protected $fillable = ['question', 'answer', 'correct_description', 'error_description', 'image'];

    public function options()
    {
        return $this->hasMany(Option::class);
    }
}