<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;

class WebColor extends Model
{

    use CommonsTrait;

    /**
     * Generated
     */

    protected $table = 'web_colors';
    protected $fillable = ['id', 'name', 'hex', 'status', 'deleted_at'];


    public function webAmbassadors()
    {
        return $this->hasMany(WebAmbassador::class, 'web_colors_id', 'id');
    }


}
