<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;

class WebPromotionsHasWebCategory extends Model
{

    use CommonsTrait;

    /**
     * Generated
     */

    protected $table = 'web_promotions_has_web_categories';
    protected $fillable = ['web_promotions_id', 'web_categories_id', 'status', 'deleted_at'];


    public function webCategory()
    {
        return $this->belongsTo(WebCategory::class, 'web_categories_id', 'id');
    }

    public function webPromotion()
    {
        return $this->belongsTo(WebLanding::class, 'web_promotions_id', 'id');
    }


}
