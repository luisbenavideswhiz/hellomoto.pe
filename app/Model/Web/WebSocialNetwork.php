<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebSocialNetwork extends Model
{

    use CommonsTrait, SoftDeletes;

    /**
     * Generated
     */

    protected $table = 'web_social_networks';
    protected $fillable = ['id', 'web_social_networks_types_id', 'url', 'status', 'deleted_at'];


    public function webSocialNetworksType()
    {
        return $this->belongsTo(WebSocialNetworksType::class, 'web_social_networks_types_id', 'id');
    }

    public function webAmbassadors()
    {
        return $this->belongsToMany(WebAmbassador::class, 'web_ambassadors_has_web_social_networks', 'web_social_networks_id', 'web_ambassadors_id');
    }

    public function webAmbassadorsHasWebSocialNetworks()
    {
        return $this->hasMany(WebAmbassadorsHasWebSocialNetwork::class, 'web_social_networks_id', 'id');
    }


}
