<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;

class WebPostsHasWebGallery extends Model
{

    use CommonsTrait;

    /**
     * Generated
     */

    protected $table = 'web_posts_has_web_galleries';
    protected $fillable = ['web_posts_id', 'web_galleries_id', 'status', 'deleted_at'];


    public function webGallery()
    {
        return $this->belongsTo(WebGallery::class, 'web_galleries_id', 'id');
    }

    public function webPost()
    {
        return $this->belongsTo(WebPost::class, 'web_posts_id', 'id');
    }


}
