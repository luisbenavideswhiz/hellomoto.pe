<?php

namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class WebUser extends Authenticatable
{

    use CommonsTrait, SoftDeletes;

    /**
     * Generated
     */

    protected $table = 'web_users';
    protected $fillable = ['id', 'web_users_type_id', 'name', 'lastname', 'document_type', 'document_number', 'sex',
        'email', 'accept_emails', 'birthdate', 'age_verify', 'phone', 'mobile_operator', 'brand_phone', 'model_phone', 'profile_img', 'password',
        'fbid', 'twid', 'gid', 'status'];
    protected $dates = ['birthdate', 'created_at'];


    public function webLandings()
    {
        return $this->belongsToMany(WebLanding::class, 'web_users_has_web_landings', 'web_user_id', 'web_landing_id');
    }

    public function webAmbassadors()
    {
        return $this->hasMany(WebAmbassador::class, 'web_users_id', 'id');
    }

    public function webPosts()
    {
        return $this->hasMany(WebPost::class, 'web_users_id', 'id');
    }

    public function webUsersActions()
    {
        return $this->hasMany(WebUsersAction::class, 'web_users_id', 'id');
    }

    public function webUsersHasWebLandings()
    {
        return $this->hasMany(WebUsersHasWebLanding::class, 'web_users_id', 'id');
    }

    public function getCreatedAtByAmbassador($ambassador)
    {
        $action = $this->webUsersActions()->where('tablename', '=', 'ambassador')
            ->where('referal_id', '=', $ambassador)->first();
        return is_null($action)?'':$action->created_at;
    }

    public function getCreatedAtByBenefit($benefit)
    {
        $action = $this->WebLandings()->where('web_landing_id', '=', $benefit)
            ->where('web_users_has_web_landings.status', '=', '1')->first();
        return is_null($action)?'':$action->created_at;
    }
}
