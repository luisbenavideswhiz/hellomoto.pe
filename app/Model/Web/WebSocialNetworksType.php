<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;

class WebSocialNetworksType extends Model
{

    use CommonsTrait;

    /**
     * Generated
     */

    protected $table = 'web_social_networks_types';
    protected $fillable = ['id', 'name', 'icon_html', 'status', 'deleted_at'];


    public function webSocialNetworks()
    {
        return $this->hasMany(WebSocialNetwork::class, 'web_social_networks_types_id', 'id');
    }


}
