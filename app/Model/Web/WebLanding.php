<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebLanding extends Model
{

    use CommonsTrait, SoftDeletes;

    /**
     * Generated
     */

    protected $table = 'web_landings';
    protected $fillable = ['id', 'title', 'image', 'path', 'status', 'deleted_at'];


    public function webUsers()
    {
        return $this->belongsToMany(WebUser::class, 'web_users_has_web_landings', 'web_landing_id', 'web_user_id');
    }

    public function webUsersHasWebLandings()
    {
        return $this->hasMany(WebUsersHasWebLanding::class, 'web_landing_id', 'id');
    }

    public function webUserHasLanding($user)
    {
        return $this->webUsersHasWebLandings()->where('web_user_id','=', $user)->count()!=0;
    }

}
