<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;

class WebCareer extends Model
{

    use CommonsTrait;

    /**
     * Generated
     */

    protected $table = 'web_careers';
    protected $fillable = ['id', 'name', 'status', 'deleted_at'];


    public function webAmbassadors()
    {
        return $this->hasMany(WebAmbassador::class, 'web_career_id', 'id');
    }


}
