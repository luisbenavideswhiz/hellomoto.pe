<?php

namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;

class WebPromotion extends Model
{

    use CommonsTrait;

    protected $table = 'web_promotions';
    protected $fillable = ['title', 'image', 'link', 'status', 'position'];


    public function webCategories()
    {
        return $this->belongsToMany(WebCategory::class, 'web_promotions_has_web_categories', 'web_promotion_id', 'web_category_id');
    }

    public function webPromotionsHasWebCategories()
    {
        return $this->hasMany(WebPromotionsHasWebCategory::class, 'web_promotion_id', 'id');
    }

}
