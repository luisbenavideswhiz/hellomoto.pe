<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class WebGallery extends Model
{

    use CommonsTrait;

    /**
     * Generated
     */

    protected $table = 'web_galleries';
    protected $fillable = ['id', 'title', 'alt', 'filetype', 'path', 'status', 'deleted_at'];


    public function webAmbassadors()
    {
        return $this->belongsToMany(WebAmbassador::class, 'web_ambassadors_has_web_galleries', 'web_galleries_id', 'web_ambassadors_id');
    }

    public function webPosts()
    {
        return $this->belongsToMany(WebPost::class, 'web_posts_has_web_galleries', 'web_galleries_id', 'web_posts_id');
    }

    public function webAmbassadorsHasWebGalleries()
    {
        return $this->hasMany(WebAmbassadorsHasWebGallery::class, 'web_galleries_id', 'id');
    }

    public function webPostsHasWebGalleries()
    {
        return $this->hasMany(WebPostsHasWebGallery::class, 'web_galleries_id', 'id');
    }

    public function getThumbnailAttribute()
    {
        $dirname = dirname($this->path);
        $name = File::name($this->path);
        $filename = $dirname.'/'.$name.'_80x50.jpg';
        if(!file_exists(public_path($filename))){
            $img = Image::make(public_path($this->path));
            $img->resize(80, 50);
            $img->save(public_path($filename));
        }
        if(is_null($this->title)){
            $this->title = $name.'.'.File::extension($this->path);
            $this->save();
        }
        return url($filename);
    }

    public function getSizeAttribute()
    {
        try{
            $size = File::size(public_path($this->path));
            if ($size > 0) {
                $size = (int) $size;
                $base = log($size) / log(1024);
                $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');
                $size = round(pow(1024, $base - floor($base)), 2) . $suffixes[floor($base)];
            } else {
                $size = 0;
            }
        }catch (\Exception $exception){
            $size = 0;
        }
        return $size;
    }

}
