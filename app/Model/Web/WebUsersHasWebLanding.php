<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;

class WebUsersHasWebLanding extends Model
{

    use CommonsTrait;

    /**
     * Generated
     */

    protected $table = 'web_users_has_web_landings';
    protected $fillable = ['web_user_id', 'web_landing_id', 'status', 'deleted_at'];


    public function webLanding()
    {
        return $this->belongsTo(WebLanding::class, 'web_landing_id', 'id');
    }

    public function webUser()
    {
        return $this->belongsTo(WebUser::class, 'web_user_id', 'id');
    }


}
