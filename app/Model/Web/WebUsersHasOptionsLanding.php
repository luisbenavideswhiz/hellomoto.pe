<?php

namespace App\Model\Web;

use Illuminate\Database\Eloquent\Model;

class WebUsersHasOptionsLanding extends Model
{
    protected $table = 'web_users_has_options_landings';
    protected $fillable = ['key', 'value', 'web_user_id', 'web_landing_id'];
}
