<?php

namespace App\Model\Web;

use Illuminate\Database\Eloquent\Model;

class WebUbigeo extends Model
{
    protected $table = 'web_ubigeo';
}
