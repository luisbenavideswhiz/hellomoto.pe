<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;

class WebAmbassadorsHasWebSocialNetwork extends Model
{

    use CommonsTrait;

    /**
     * Generated
     */

    protected $table = 'web_ambassadors_has_web_social_networks';
    protected $fillable = ['web_ambassadors_id', 'web_social_networks_id', 'status', 'deleted_at'];


    public function webAmbassador()
    {
        return $this->belongsTo(WebAmbassador::class, 'web_ambassadors_id', 'id');
    }

    public function webSocialNetwork()
    {
        return $this->belongsTo(WebSocialNetwork::class, 'web_social_networks_id', 'id');
    }


}
