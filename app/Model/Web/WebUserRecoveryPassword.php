<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebUserRecoveryPassword extends Model
{
    use SoftDeletes;
    use CommonsTrait;

    /**
     * Generated
     */

    protected $table = 'web_users_recovery_password';
    protected $fillable = ['id', 'web_users_id', 'code', 'status', 'daleted_at'];

    public function webUser()
    {
        return $this->belongsTo(WebUser::class, 'web_users_id', 'id');
    }

}
