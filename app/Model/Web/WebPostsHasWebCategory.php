<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;

class WebPostsHasWebCategory extends Model
{

    use CommonsTrait;

    /**
     * Generated
     */

    protected $table = 'web_posts_has_web_categories';
    protected $fillable = ['web_posts_id', 'web_categories_id', 'status', 'deleted_at'];


    public function webCategory()
    {
        return $this->belongsTo(WebCategory::class, 'web_categories_id', 'id');
    }

    public function webPost()
    {
        return $this->belongsTo(WebPost::class, 'web_posts_id', 'id');
    }


}
