<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;

class WebPostsHasWebAmbassador extends Model
{

    use CommonsTrait;

    /**
     * Generated
     */

    protected $table = 'web_posts_has_web_ambassadors';
    protected $fillable = ['web_posts_id', 'web_ambassadors_id', 'status', 'crated_at', 'deleted_at'];


    public function webAmbassador()
    {
        return $this->belongsTo(WebAmbassador::class, 'web_ambassadors_id', 'id');
    }

    public function webPost()
    {
        return $this->belongsTo(WebPost::class, 'web_posts_id', 'id');
    }


}
