<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebPost extends Model
{

    use CommonsTrait, SoftDeletes;

    /**
     * Generated
     */

    protected $table = 'web_posts';
    protected $fillable = ['id', 'web_users_id', 'title', 'slug', 'description', 'after_description', 'tags', 'banner_img', 'thumb_img', 'meta_title', 'meta_description', 'meta_keywords', 'status', 'deleted_at'];


    public function webUser()
    {
        return $this->belongsTo(WebUser::class, 'web_users_id', 'id');
    }

    public function webAmbassadors()
    {
        return $this->belongsToMany(WebAmbassador::class, 'web_posts_has_web_ambassadors', 'web_posts_id', 'web_ambassadors_id');
    }

    public function webCategories()
    {
        return $this->belongsToMany(WebCategory::class, 'web_posts_has_web_categories', 'web_posts_id', 'web_categories_id');
    }

    public function webGalleries()
    {
        return $this->belongsToMany(WebGallery::class, 'web_posts_has_web_galleries', 'web_posts_id', 'web_galleries_id');
    }

    public function webPostsHasWebAmbassadors()
    {
        return $this->hasMany(WebPostsHasWebAmbassador::class, 'web_posts_id', 'id');
    }

    public function webPostsHasWebCategories()
    {
        return $this->hasMany(WebPostsHasWebCategory::class, 'web_posts_id', 'id');
    }

    public function webPostsHasWebGalleries()
    {
        return $this->hasMany(WebPostsHasWebGallery::class, 'web_posts_id', 'id');
    }
}
