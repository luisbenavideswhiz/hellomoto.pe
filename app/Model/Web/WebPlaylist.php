<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebPlaylist extends Model
{

    use CommonsTrait, SoftDeletes;

    /**
     * Generated
     */

    protected $table = 'web_playlists';
    protected $fillable = ['id', 'slug', 'web_ambassadors_id', 'title', 'image', 'url', 'status', 'deleted_at'];


    public function webAmbassador()
    {
        return $this->belongsTo(WebAmbassador::class, 'web_ambassadors_id', 'id');
    }


}
