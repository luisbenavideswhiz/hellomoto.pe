<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;

class WebUsersAction extends Model
{

    use CommonsTrait;

    /**
     * Generated
     */

    protected $table = 'web_users_actions';
    protected $fillable = ['id', 'web_users_id', 'action_type', 'tablename', 'referal_id', 'status', 'deleted_at'];


    public function webAmbassador()
    {
        return $this->belongsTo(WebAmbassador::class, 'referal_id', 'id');
    }

    public function webPlaylists()
    {
        return $this->hasMany(WebPlaylist::class, 'id', 'referal_id')
            ->orderBy('id', 'DESC');
    }

}
