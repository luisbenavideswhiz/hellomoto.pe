<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;

class WebAmbassadorsHasWebGallery extends Model
{

    use CommonsTrait;

    /**
     * Generated
     */

    protected $table = 'web_ambassadors_has_web_galleries';
    protected $fillable = ['web_ambassadors_id', 'web_galleries_id', 'status', 'deleted_at'];


    public function webAmbassador()
    {
        return $this->belongsTo(WebAmbassador::class, 'web_ambassadors_id', 'id');
    }

    public function webGallery()
    {
        return $this->belongsTo(WebGallery::class, 'web_galleries_id', 'id');
    }


}
