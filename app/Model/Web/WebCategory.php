<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebCategory extends Model
{

    use CommonsTrait, SoftDeletes;

    /**
     * Generated
     */

    protected $table = 'web_categories';
    protected $fillable = ['id', 'category_id', 'category_type', 'name', 'slug', 'status', 'deleted_at'];


    public function webPosts()
    {
        return $this->belongsToMany(WebPost::class, 'web_posts_has_web_categories', 'web_categories_id', 'web_posts_id');
    }

    public function webPromotions()
    {
        return $this->belongsToMany(WebLanding::class, 'web_promotions_has_web_categories', 'web_categories_id', 'web_promotions_id');
    }

    public function webPostsHasWebCategories()
    {
        return $this->hasMany(WebPostsHasWebCategory::class, 'web_categories_id', 'id');
    }

    public function webPostsHasWebCategoriesParent()
    {
        return $this->hasMany(WebPostsHasWebCategory::class, 'web_categories_id_parent', 'id');
    }

    public function webCategoryParent()
    {
        return $this->belongsTo(WebCategory::class, 'category_id');
    }

    public function webPromotionsHasWebCategories()
    {
        return $this->hasMany(WebPromotionsHasWebCategory::class, 'web_categories_id', 'id');
    }


}
