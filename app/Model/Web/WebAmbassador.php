<?php namespace App\Model\Web;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebAmbassador extends Model
{

    use CommonsTrait, SoftDeletes;

    /**
     * Generated
     */

    protected $table = 'web_ambassadors';
    protected $fillable = ['id', 'web_users_id', 'city', 'nickname', 'slug', 'web_career_id', 'bio', 'profile_img', 'profile_video', 'web_colors_id', 'tags', 'position', 'status', 'deleted_at', 'meta_title','meta_description','meta_keywords'];


    public function webUser()
    {
        return $this->belongsTo(WebUser::class, 'web_users_id', 'id');
    }

    public function webCareer()
    {
        return $this->belongsTo(WebCareer::class, 'web_career_id', 'id');
    }

    public function webColor()
    {
        return $this->belongsTo(WebColor::class, 'web_colors_id', 'id');
    }

    public function webGalleries()
    {
        return $this->belongsToMany(WebGallery::class, 'web_ambassadors_has_web_galleries', 'web_ambassadors_id', 'web_galleries_id');
    }

    public function webSocialNetworks()
    {
        return $this->belongsToMany(WebSocialNetwork::class, 'web_ambassadors_has_web_social_networks', 'web_ambassadors_id', 'web_social_networks_id');
    }

    public function webPosts()
    {
        return $this->belongsToMany(WebPost::class, 'web_posts_has_web_ambassadors', 'web_ambassadors_id', 'web_posts_id');
    }

    public function webAmbassadorsHasWebGalleries()
    {
        return $this->hasMany(WebAmbassadorsHasWebGallery::class, 'web_ambassadors_id', 'id');
    }

    public function webAmbassadorsHasWebSocialNetworks()
    {
        return $this->hasMany(WebAmbassadorsHasWebSocialNetwork::class, 'web_ambassadors_id', 'id');
    }

    public function webPlaylists()
    {
        return $this->hasMany(WebPlaylist::class, 'web_ambassadors_id', 'id')
            ->orderBy('id', 'DESC');
    }

    public function webPostsHasWebAmbassadors()
    {
        return $this->hasMany(WebPostsHasWebAmbassador::class, 'web_ambassadors_id', 'id');
    }

    public function webUsersAction()
    {
        return $this->hasMany(WebUsersAction::class, 'referal_id', 'id');
    }
}
