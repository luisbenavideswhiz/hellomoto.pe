<?php

namespace App\Model\Workarea;

use App\Http\Traits\CommonsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebSeo extends Model
{
    use CommonsTrait;
    use SoftDeletes;

    protected $table = 'web_seo';
    protected $fillable = ['name', 'title', 'description', 'image', 'alt', 'url', 'keywords', 'status'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
