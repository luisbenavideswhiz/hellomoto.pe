<?php

namespace App\Model\Workarea;

use Illuminate\Database\Eloquent\Model;

class RecoverPassword extends Model
{
    protected $table = 'workarea_recover_password';
    protected $fillable = ['user_id', 'code', 'status'];
    protected $dates = ['created_at', 'updated_at'];

}
