<?php

namespace App\Model\Workarea;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'workarea_users';
    protected $fillable = ['name', 'email', 'password', 'remember_token', 'role_id', 'status'];
    protected $hidden = ['password'];
    protected $dates = ['created_at', 'updated_at'];

    /**
     * @param $nameRole
     * @return bool
     */
    public function hasRole($nameRole)
    {
        switch ($nameRole)
        {
            case 'admin': $response = $this->role_id == 1;
                break;
            case 'motovan': $response = $this->role_id == 2 || $this->role_id == 1;
                break;
            default:
                dd($nameRole);
                break;
        }
        return $response;
    }
}
