<?php

namespace App\Transformers\Trivia;


use App\Model\Trivia\Question;
use League\Fractal\TransformerAbstract;

class QuestionTransformer extends TransformerAbstract
{
    protected $availableIncludes = [

    ];

    protected $defaultIncludes = [

    ];

    private function splitWords($str, $orientation)
    {
        $word = explode(' ', $str);
        $length = count($word);
        $left = [];
        $right = [];
        for ($i = 0; $i < $length; $i++) {
            if ($i < $length / 2) {
                array_push($left, $word[$i]);
            } else {
                array_push($right, $word[$i]);
            }
        }
        return $orientation == 'left' ? implode(' ', $left) : implode(' ', $right);
    }

    public function transform(Question $question)
    {
        $response = [
            'id' => $question->id,
            'question_right' => $this->splitWords($question->question, 'right'),
            'question_left' => $this->splitWords($question->question, 'left'),
            'image' => $question->image,
        ];
        return $response;
    }
}