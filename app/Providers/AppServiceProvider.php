<?php

namespace App\Providers;

use App\Helpers\Fragment;
use App\Helpers\Instagram;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Agent\Agent;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!app()->runningInConsole()) {
            /**
             * URL
             **/
            View::share('url_current', url()->current());

            /*
             * Detect Device
             * */
            $agent = new Agent();
            if ($agent->isDesktop()) {
                View::share('device', 'desktop');
            } else {
                View::share('device', 'responsive');
            }

            /*
             * Menu
             * */
            $menu = Fragment::get_menu([
                'hellociudades' => 1,
                'motostyle' => 3
            ]);

            /*
             * Instagram
             * */
            $instagram = Instagram::get_posts();

            View::share('menu', $menu);
            View::share('instagram', $instagram);

            /*
             * Validation extends
             * */
            Validator::extend('alpha_spaces', function ($attribute, $value) {
                return preg_match('/^[\pL-\s]+$/u', $value);
            });

            Validator::extend('alpha_no_spaces', function ($attribute, $value) {
                return preg_match('/^[\pL]+$/u', $value);
            });

            Validator::extend('only_numbers', function ($attribute, $value) {
                return preg_match('/^[0-9]+$/u', $value);
            });

            Validator::extend('email_format', function ($attribute, $value) {
                return preg_match('/^[a-zA-Z0-9@_.-]+$/u', $value);
            });

            Validator::extend('phone_verify', function ($attribute, $value) {
                $client = new Client(['cookies' => true]);
                $client->request('GET', 'https://freecarrierlookup.com/');
                $cookie = $client->getConfig('cookies');
                $response = $client->request('POST', 'https://freecarrierlookup.com/getcarrier.php', [
                    'cookies' => $cookie,
                    'form_params' => ['test' => '456', 'phonenum' => $value, 'cc' => '51']
                ]);
                print_r(json_decode((string)$response->getBody()));
                exit;
            });
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }
}
