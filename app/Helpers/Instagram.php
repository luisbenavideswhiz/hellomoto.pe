<?php

namespace App\Helpers;

class Instagram
{
    public static function get_posts()
    {
        $access_token = env('INSTAGRAM_ACCESS_TOKEN');
        $count = env('INSTAGRAM_COUNT');

        $url = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' . $access_token . '&count=' . $count;
        $cache = './' . sha1($url) . '.json';
        if (file_exists($cache) && filemtime($cache) > time() - 60 * 60) {
            $jsonData = json_decode(@file_get_contents($cache));
        } else {
            $jsonData = json_decode((@file_get_contents($url)));
            @file_put_contents($cache, json_encode($jsonData));
        }
        if (isset($jsonData->data)) {
            return $jsonData->data;
        } else {
            return [];
        }
    }
}