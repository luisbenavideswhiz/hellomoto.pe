<?php

namespace App\Helpers;

use App\Model\Web\WebAmbassador;
use App\Model\Web\WebCategory;
use App\Model\Web\WebPlaylist;
use App\Model\Web\WebPost;
use App\Model\Web\WebPostsHasWebCategory;

class Fragment
{
    public static function get_menu($categories)
    {
        $rows = [];
        foreach ($categories as $key => $value) {
            $rows[$key] = Fragment::get_subcategories_by($value);
        }
        return $rows;
    }

    public static function get_slider($options)
    {
        if (!isset($options['take']) && is_null($options['take'])) {
            $options['take'] = 6;
        }
        $rows = WebPost::where('status', 1)
            ->distinct('id')
            ->orderBy('created_at', 'DESC')
            ->skip(0)
            ->take($options['take']);
        return $rows->get();
    }

    public static function get_subcategories_by($category)
    {
        $subcategories = WebCategory::where('status', 1)
            ->where('category_id', $category);
        return $subcategories->get();
    }

    public static function get_post($options)
    {
        $rows = new \stdClass();
        $post = WebPost::where('status', 1);

        if (!isset($options['prev'])) {
            $options['prev'] = true;
        }

        if (!isset($options['next'])) {
            $options['next'] = true;
        }

        if (isset($options['slug']) && !is_null($options['slug'])) {
            $current = $post->where('slug', $options['slug'])->first();

            if (is_null($current)) {
                return redirect('404');
            }

            $rows->current = $current;

            if ($options['prev']) {
                $previous = WebPost::where('status', 1)
                    ->where('id', '<', $current->id)
                    ->orderby('id', 'DESC')
                    ->first();

                if (is_null($previous)) {
                    $previous = WebPost::where('status', 1)
                        ->orderby('id', 'DESC')
                        ->limit(1)
                        ->first();
                }

                $rows->prev = $previous;
            }

            if ($options['next']) {
                $next = WebPost::where('status', 1)
                    ->where('id', '>', $current->id)
                    ->orderby('id', 'ASC')
                    ->first();

                if (is_null($next)) {
                    $next = WebPost::where('status', 1)
                        ->orderby('id', 'ASC')
                        ->limit(1)
                        ->first();
                }

                $rows->next = $next;
            }

        } else {
            $posts = WebPost::where('status', 1)
                ->orderby('id', 'DESC')
                ->limit(3)
                ->get();
            $rows->prev = $posts[0];
            $rows->current = $posts[1];
            $rows->next = $posts[2];
        }

        return $rows;

    }

    public static function get_paginate_posts($options)
    {
        if (!isset($options['take']) || is_null($options['take'])) {
            $options['take'] = 6;
        }

        if (!isset($options['page']) || is_null($options['page'])) {
            $options['page'] = 1;
        }
        $skipped = ($options['page'] - 1) * $options['take'];

        $rows = WebPostsHasWebCategory::whereHas('webPost', function($query){
            $query->where('status', '=', 1);
        })->distinct();

        if (isset($options['category']) && !is_null($options['category'])) {
            $rows->where('web_categories_id_parent', $options['category']);
        }

        if (isset($options['subcategory']) && !is_null($options['subcategory'])) {
            if ($options['subcategory'] != 'todos') {

                $subcategory_id = WebCategory::where('status', 1)
                    ->where('category_id', $options['category'])
                    ->where('slug', $options['subcategory'])
                    ->first()
                    ->id;

                $rows->where('web_categories_id', $subcategory_id);
            }
        }

        if (isset($options['where']) && is_array($options['where'])) {
            foreach ($options['where'] as $key => $value) {
                $rows->where($key, $value);
            }
        }

        if (isset($options['except_ids']) && is_array($options['except_ids'])) {
            $rows->whereNotIn('web_posts_id', $options['except_ids']);
        }

        $rows = $rows->skip($skipped)
            ->take($options['take'])
            ->orderBy('web_posts_id', 'DESC')
            ->get(['web_posts_id']);

        if ($rows->count() != 0) {
            $post_ids = Fragment::extract_ids($rows, 'web_posts_id');
            $posts = WebPost::where('status','=', 1)
                ->whereIn('id', $post_ids);

            if (isset($options['orderby']) && ($options['orderby'] == 'menos-recientes')) {
                $posts->orderBy('id', 'ASC');
            } else {
                $posts->orderBy('id', 'DESC');
            }

            return $posts->get();
        }

        return false;
    }

    public static function get_paginate_ambassadors($options)
    {
        if (!isset($options['take']) || is_null($options['take'])) {
            $options['take'] = 12;
        }

        if (!isset($options['page']) || is_null($options['page'])) {
            $options['page'] = 1;
        }
        $skipped = ($options['page'] - 1) * $options['take'];

        $rows = WebAmbassador::where('status', 1)
            ->skip($skipped)
            ->take($options['take'])
            ->orderBy('position', 'ASC')
            ->get();

        if ($rows->count() != 0) {
            return $rows;
        }

        return false;
    }

    public static function get_paginate_playlists($options)
    {
        if (!isset($options['take']) || is_null($options['take'])) {
            $options['take'] = 8;
        }

        if (!isset($options['page']) || is_null($options['page'])) {
            $options['page'] = 1;
        }
        $skipped = ($options['page'] - 1) * $options['take'];

        $rows = WebPlaylist::where('status', 1)
            ->whereHas('webAmbassador', function($query){
                $query->where('status', '=', 1);
                $query->whereNull('deleted_at');
            })
            ->skip($skipped)
            ->take($options['take'])
            ->orderBy('id', 'DESC')
            ->get();

        if ($rows->count() != 0) {

            return $rows;
        }

        return false;
    }

    public static function extract_ids($rows, $fieldname = 'id')
    {
        $response = [];
        foreach ($rows as $key => $value) {
            $response[] = $value->{$fieldname};
        }
        return $response;
    }

}