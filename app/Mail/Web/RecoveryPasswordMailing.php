<?php

namespace App\Mail\Web;

use App\Model\Web\WebUser;
use App\Model\Web\WebUserRecoveryPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecoveryPasswordMailing extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $recovery;
    public $campaign = 'web.mailings';

    /**
     * RecoveryPasswordMailing constructor.
     * @param WebUser $user
     * @param WebUserRecoveryPassword $recovery
     */
    public function __construct(WebUser $user, WebUserRecoveryPassword $recovery)
    {
        $this->user = $user;
        $this->recovery = $recovery;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(getenv('MAIL_USERNAME'), 'Motorola')
            ->subject('Motorola - Recuperación de contraseña')
            ->view($this->campaign.'.recovery-mailing');
    }
}
