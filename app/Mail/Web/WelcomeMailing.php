<?php

namespace App\Mail\Web;

use App\Model\Web\WebUser;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeMailing extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $campaign = 'web.mailings';

    /**
     * Create a new message instance.
     *
     * @param WebUser $user
     */
    public function __construct(WebUser $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(getenv('MAIL_USERNAME'), 'Motorola')
            ->subject('Motorola - Bienvenido a la comunidad Hellomoto')
            ->view($this->campaign.'.welcome-mailing');
    }
}
