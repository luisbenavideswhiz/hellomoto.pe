<?php

namespace App\Mail\Landings;

use App\Model\Landings\DimitriVegas;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DimitriVegasMailingConfirmation extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $campaign = 'landings.dimitrivegas';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(DimitriVegas $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(getenv('MAIL_USERNAME'), utf8_encode('Motorola - Confirmación de registro'))
            ->subject(utf8_encode('Motorola | Dimitri Vegas & Like Mike en Lima'))
            ->view($this->campaign.'.emails.mail-confirmation');
    }
}
