<?php

namespace App\Mail\Landings;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Model\Landings\Comunidad;

class ComunidadMailingConfirmation extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $campaign = 'landings.comunidad';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Comunidad $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(getenv('MAIL_USERNAME'), utf8_encode('Motorola'))
            ->subject(utf8_encode('Motorola - Confirmación de registro'))
            ->view($this->campaign.'.mailing.confirmacion');
    }
}
