<?php

namespace App\Mail\Workarea;


use App\Model\Workarea\RecoverPassword;
use App\Model\Workarea\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecoveryPasswordMailing extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $recovery;
    public $campaign = 'workarea.mailings';

    /**
     * RecoveryPasswordMailing constructor.
     * @param User $user
     * @param RecoverPassword $recovery
     */
    public function __construct(User $user, RecoverPassword $recovery)
    {
        $this->user = $user;
        $this->recovery = $recovery;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(getenv('MAIL_USERNAME'), 'Motorola')
            ->subject('Motorola - Recuperación de contraseña')
            ->view($this->campaign.'.recover-mailing');
    }
}
