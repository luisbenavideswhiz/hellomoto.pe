<?php

namespace App\Http\Traits;


use App\Model\Web\WebUser;

trait SocialiteTrait
{

    function checkEmail($email)
    {
        $row = WebUser::where('status', 1)
            ->where('email', $email);

        if ($row->count() == 0) {
            return false;
        }
        return $row->first();
    }

    function checkSocialId($options, $driver)
    {
        $field = null;
        if ($driver == 'facebook') {
            $field = 'fbid';
        }
        if ($driver == 'twitter') {
            $field = 'twid';
        }
        if ($driver == 'google') {
            $field = 'gid';
        }
        $row = WebUser::where('status', '=', 1)
            ->where($field, $options['id'])
            ->count();

        if ($row == 0) {
            WebUser::where('status', '=', 1)
                ->where('email', $options['email'])
                ->update([
                    $field => $options['id']
                ]);
        }
    }

}