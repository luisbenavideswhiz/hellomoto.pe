<?php

namespace App\Http\Traits;

use App\Model\Web\WebCategory;
use Carbon\Carbon;

trait CommonsTrait
{

    protected $prefix = '';

    public function getTitleShortAttribute()
    {
        return substr($this->title, 0, 40);
    }

    public function getDescriptionShortAttribute()
    {
        return substr($this->description, 0, 150);
    }

    public function getUrlPostAttribute()
    {
        return $this->prefix . '/post/' . $this->slug;
    }

    public function getTagsHtmlAttribute()
    {
        $response = [];
        $tags = explode(',', $this->tags);
        foreach ($tags as $key => $value) {
            $text = strtolower(htmlentities($value));
            $response[] = '<a href="/buscar/' . $text . '" rel="tag">' . ucwords($text) . '</a>';
        }
        $response = join(' &bull; ', $response);
        return $response;
    }

    public function getCreatedAtReadableAttribute()
    {
        list($date, $time) = explode(' ', $this->created_at);
        list($year, $month, $day) = explode('-', $date);
        setlocale(LC_TIME, 'es_PE.utf8');
        return $response = Carbon::create($year, $month, $day, 0, 0, 0, 'America/Lima')
            ->formatLocalized('%d de %B del %Y');
    }

    public function getUserFullnameAttribute()
    {
        return $this->webUser->name . ' ' . $this->webUser->lastname;
    }

    public function getAmbassadorFullnameAttribute()
    {
        if (!is_null($this->nickname)) {
            $response = $this->nickname;
        } else {
            $response = $this->webUser->name . ' ' . $this->webUser->lastname;
        }
        return $response;
    }

    public function getAmbassadorNicknameAttribute()
    {
        if ($this->nickname != '') {
            return $this->nickname;
        } else {
            return $this->webUser->name;
        }
    }

    public function getAmbassadorSocialnetworksAttribute()
    {
        $response = '';
        if (isset($this->webAmbassadorsHasWebSocialNetworks)) {
            foreach ($this->webAmbassadorsHasWebSocialNetworks as $key => $value) {
                $response .= '<a href="' . $value->webSocialNetwork->url . '" title="' . $value->webSocialNetwork->webSocialNetworksType->name . '" target="_blank">
                ' . $value->webSocialNetwork->webSocialNetworksType->icon_html . '
                </a>';
            }
        }
        return $response;
    }

    public function getTypeClassAttribute()
    {
        $name = $this->getParentCategoryAttribute()->name;

        switch ($name) {
            case "hellociudades":
                $response = "type1";
                break;
            case "experienciasmoto":
                $response = "type4";
                break;
            case "motostyle":
                $response = "type3";
                break;
            case "motoembajadores":
                $response = "type4";
                break;
            case "hellotips":
                $response = "type2";
                break;
            default:
                $response = "default";
                break;
        }
        return $response;
    }

    public function getCategoryHtmlAttribute()
    {
        $name = $this->getParentCategoryAttribute()->name;

        switch ($name) {
            case "hellociudades":
                $response = "<span>hello<span>ciudades</span></span>";
                break;
            case "experienciasmoto":
                $response = "<span>experiencias<span>moto</span></span>";
                break;
            case "motostyle":
                $response = "<span>moto<span>style</span></span>";
                break;
            case "motoembajadores":
                $response = "<span>moto<span>embajadores</span></span>";
                break;
            case "hellotips":
                $response = "<span>hello<span>tips</span></span>";
                break;
            default:
                $response = "default";
                break;
        }
        return $response;
    }

    public function getCategoryNameAttribute()
    {
        $response = $this->webPostsHasWebCategories[0]->webCategory->name;
        return $response;
    }

    public function getCategorySlugAttribute()
    {
        return $this->webPostsHasWebCategories[0]->webCategory->slug;
    }

    public function getParentCategoryAttribute()
    {
        $category = $this->webPostsHasWebCategories[0]->webCategory->category_id;
        $response = WebCategory::where('status', 1)
            ->where('id', $category)
            ->first();
        return $response;
    }

}