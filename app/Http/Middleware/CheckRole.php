<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        \Log::info('role',['result' => $request->user()->hasRole($role), 'role'=>$role, 'user'=>$request->user()]);
        if(!$request->user()->hasRole($role)){
            return redirect()->route('workarea.dashboard.index');
        }
        return $next($request);
    }
}
    