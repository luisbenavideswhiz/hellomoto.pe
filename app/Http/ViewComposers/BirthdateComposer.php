<?php

namespace App\Http\ViewComposers;

use Carbon\Carbon;
use Illuminate\View\View;

class BirthdateComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $today = Carbon::now();
        $date = $today->subYears(18);
        $view->with('year', $date->format('Y'));
    }
}