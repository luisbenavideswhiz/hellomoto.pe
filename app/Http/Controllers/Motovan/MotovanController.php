<?php

namespace App\Http\Controllers\Motovan;

use App\Http\Requests\Motovan\LoginRequest;
use App\Http\Requests\Motovan\UserCreateRequest;
use App\Model\MotoVan\Training;
use App\Model\MotoVan\TrainingUser;
use App\Model\MotoVan\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MotovanController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRegister()
    {
        return view('motovan/registrate');
    }

    /**
     * @param UserCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postRegister(UserCreateRequest $request)
    {
        $motoUser = $request->only(['name','last_name','area','birthdate','email','document_number','phone','terms_conditions']);
        $motoUser['terms_conditions'] = $request->has('terms_conditions');
        $user = User::create($motoUser);
        session()->flush();
        Auth::guard('motovan')->loginUsingId($user->id);
        return response()->json(['status'=>true, 'message'=> 'Registrado con éxito!','redirect'=>'/motovan/inscribete']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEnroll()
    {
        $data['trainings'] = Training::where('status','=',1)
            ->where('date','>=', date('Y-m-d'))
            ->orderBy('date','DESC')->paginate(5);
        return view('motovan/inscribete', $data);
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    public function postEnroll($id)
    {
        Training::find($id)->decrement('remaining_coupons');
        TrainingUser::updateOrCreate([
            'user_id' => Auth::guard('motovan')->id(),
            'training_id' => $id
        ]);
        return response()->json([
            'status'=>true,
            'message'=>'Se ha registrado con éxito a la capacitación',
            'redirect'=>url('/motovan/exito/'.$id)
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSuccess($id)
    {
        if(Auth::user()->trainings()->where('training_id','=',$id)->count()==0){
            return redirect()->to('/motovan/inscribete');
        }
        $data['training'] = Training::find($id);
        return view('motovan/success',$data);
    }


    public function postLogin(LoginRequest $request)
    {
        try{
            $user = User::where('email', '=', $request->email)
                ->where('document_number','=', $request->document_number)->firstOrFail();
            Auth::guard('motovan')->loginUsingId($user->id);
        }catch (\Exception $exception){
            return response()->json(['status'=>false, 'message'=> 'No se encontro usuario']);
        }
        return response()->json(['status'=>true, 'message'=> 'Inicio sesión con éxito!','redirect'=>'/motovan/inscribete']);
    }

}
