<?php

namespace App\Http\Controllers\LandingG6;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MotoG6Controller extends Controller
{
    public function index()
    {
        return view('landing-g6.index');
    }
}
