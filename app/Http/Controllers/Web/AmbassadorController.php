<?php

namespace App\Http\Controllers\Web;

use App\Helpers\Fragment;
use App\Http\Requests\Web\Ambassador\FollowRequest;
use App\Http\Requests\Web\Ambassador\UnfollowRequest;
use App\Http\Traits\ResponseTrait;
use App\Model\Web\WebUsersAction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AmbassadorController extends Controller
{
    use ResponseTrait;

    public function get_rows(Request $request)
    {
        if (!isset($request->response_type)) {
            return $this->responseError('',['error' => 'Response type is undefined']);
        } else {
            $rows = Fragment::get_paginate_ambassadors([
                'page' => $request->page
            ]);
            if ($rows) {
                if ($request->response_type == 'json') {
                    return $this->responseSuccess('',[
                        'rows' => $rows
                    ]);
                }
                if ($request->response_type == 'html') {
                    $html = view('web.templates.ambassador-item',
                        [
                            'rows' => $rows
                        ]
                    )->render();
                    return $this->responseSuccess('',[
                        'html' => $html
                    ]);
                }
            } else {
                $html = view('web.partials.no-rows')->render();
                return $this->responseError('',[
                    'html' => $html
                ]);
            }
        }
    }

    public function get_playlists(Request $request)
    {
        if (!isset($request->response_type)) {
            return $this->responseError('',[
                'error' => 'Response type is undefined'
            ]);
        } else {
            $rows = Fragment::get_paginate_playlists([
                'page' => $request->page
            ]);
            if ($rows) {
                if ($request->response_type == 'json') {
                    return $this->responseSuccess('',['rows' => $rows]);
                }
                if ($request->response_type == 'html') {
                    $html = view('web.templates.playlist-item',
                        [
                            'rows' => $rows
                        ]
                    )->render();
                    return $this->responseSuccess('',[
                        'html' => $html
                    ]);
                }
            } else {
                $html = view('web.partials.no-rows')->render();
                return $this->responseError('', [
                    'html' => $html
                ]);
            }
        }
    }

    public function follow(FollowRequest $request)
    {
        $data = $request->except('_token');

        $data['web_users_id'] = Session::get('userdata.id');
        $data['action_type'] = 'follow';
        $data['tablename'] = 'ambassador';

        try {
            $row = WebUsersAction::updateOrCreate($data, ['status' => 1]);
            if ($row) {
                $response = [
                    'success' => true,
                    'text' => 'Dejar de seguir embajador',
                    'url' => '/ambassadors/unfollow',
                ];
            } else {
                $response = [
                    'success' => false,
                    'errors' => [
                        'name' => ["No hemos podido seguir al embajador, por favor, intenta nuevamente"]
                    ]
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'errors' => [
                    'name' => ["No hemos podido seguir al embajador, por favor, intenta nuevamente"]
                ],
                'catch' => $e,
            ];
        }
        return response()->json($response);
    }

    public function unfollow(UnfollowRequest $request)
    {
        $data = $request->except('_token');
        $data['web_users_id'] = Session::get('userdata.id');
        $data['action_type'] = 'follow';
        $data['tablename'] = 'ambassador';
        try {
            $row = WebUsersAction::updateOrCreate($data, ['status' => 0]);
            if ($row) {
                $response = [
                    'success' => true,
                    'text' => 'Seguir embajador',
                    'url' => '/ambassadors/follow',
                ];
            } else {
                $response = [
                    'success' => false,
                    'errors' => [
                        'name' => ["No hemos podido seguir al embajador, por favor, intenta nuevamente"]
                    ]
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'errors' => [
                    'name' => ["No hemos podido seguir al embajador, por favor, intenta nuevamente"]
                ],
                'catch' => $e,
            ];
        }
        return response()->json($response);
    }

    public function playlist_like(FollowRequest $request)
    {
        $data = $request->except('_token');

        $data['web_users_id'] = Session::get('userdata.id');
        $data['action_type'] = 'like';
        $data['tablename'] = 'playlist';

        try {
            $row = WebUsersAction::updateOrCreate($data, ['status' => 1]);
            if ($row) {
                $response = [
                    'success' => true,
                    'text' => 'Eliminar playlist',
                    'url' => '/ambassadors/playlists/unlike',
                ];
            } else {
                $response = [
                    'success' => false,
                    'errors' => [
                        'name' => ["No hemos podido guardar la playlist, por favor, intenta nuevamente"]
                    ]
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'errors' => [
                    'name' => ["No hemos podido guardar la playlist, por favor, intenta nuevamente"]
                ],
                'catch' => $e,
            ];
        }
        return response()->json($response);
    }

    public function playlist_unlike(UnfollowRequest $request)
    {
        $data = $request->except('_token');

        $data['web_users_id'] = Session::get('userdata.id');
        $data['action_type'] = 'like';
        $data['tablename'] = 'playlist';

        try {
            $row = WebUsersAction::updateOrCreate($data, ['status' => 0]);
            if ($row) {
                $response = [
                    'success' => true,
                    'text' => 'Guardar playlist',
                    'url' => '/ambassadors/playlists/like',
                ];
            } else {
                $response = [
                    'success' => false,
                    'errors' => [
                        'name' => ["No hemos podido guardar la playlist, por favor, intenta nuevamente"]
                    ]
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'errors' => [
                    'name' => ["No hemos podido guardar la playlist, por favor, intenta nuevamente"]
                ],
                'catch' => $e,
            ];
        }
        return response()->json($response);
    }
}
