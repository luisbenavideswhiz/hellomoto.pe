<?php

namespace App\Http\Controllers\Web;

use App\Helpers\Fragment;
use App\Http\Traits\ResponseTrait;
use App\Model\Web\WebPost;
use App\Model\Web\WebPostsHasWebCategory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    use ResponseTrait;

    public function get_rows(Request $request)
    {
        $html = '';
        if (!isset($request->response_type)) {
            return $this->responseError('', [
                'error' => 'Response type is undefined'
            ]);
        } else {
            $data = $request->all();
            $slider = Fragment::get_slider([
                'take' => 5
            ]);
            $slider_ids = Fragment::extract_ids($slider);
            $options = [
                'category' => (isset($data['category'])) ? $data['category'] : null,
                'subcategory' => (isset($data['subcategory'])) ? $data['subcategory'] : null,
                'where' => (isset($data['where'])) ? $data['where'] : null,
                'page' => (isset($data['page'])) ? $data['page'] : 1,
                'take' => (isset($data['take'])) ? $data['take'] : null,
                'orderby' => (isset($data['orderby'])) ? $data['orderby'] : null,
                'except_ids' => $slider_ids,
            ];
            $rows = Fragment::get_paginate_posts($options);
            if ($rows) {
                if ($request->response_type == 'json') {
                    return $this->responseSuccess('',[
                        'rows' => $rows
                    ]);
                }
                if ($request->response_type == 'html') {
                    $html = view('web.templates.post-item', [
                            'rows' => $rows
                        ])->render();
                    return $this->responseSuccess('', [ 'html' => $html ]);
                }
            } else {
                $html = view('web.partials.no-rows')->render();
                return $this->responseError('', [ 'html' => $html ]);
            }

        }
    }

    public function detail_post($slug, Request $request)
    {
        $post = Fragment::get_post([
            'slug' => $slug
        ]);

        if (! $post instanceOf RedirectResponse) {
            $data['post'] = $post;
            $data['related'] = Fragment::get_paginate_posts([
                'orderby' => 'mas-recientes',
                'except_ids' => [$post->current->id]
            ]);
            return view('web.detalle', $data);
        } else {
            return redirect('/404');
        }
    }
}
