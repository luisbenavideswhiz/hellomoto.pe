<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Model\Workarea\WebSeo;
use App\Model\Web\WebPromotion;
use App\Http\Controllers\Controller;

class PromotionController extends Controller
{
    public function index()
    {
        $data['promotions'] = WebPromotion::where('status','<>',0)->orderBy('position', 'ASC')->get();
        $data['seo'] = WebSeo::where('name', '=', 'promociones')->first();
        return view('web/promociones/index', $data);
    }

    public function terms_conditions()
    {
        return view('web/promociones/tyc');
    }
}
