<?php

namespace App\Http\Controllers\Web;

use App\Helpers\Fragment;
use App\Model\Landings\Unete;
use App\Model\Web\WebAmbassador;
use App\Model\Web\WebPlaylist;
use App\Model\Web\WebPost;
use App\Model\Web\WebPostsHasWebAmbassador;
use App\Model\Web\WebLanding;
use App\Model\Web\WebUsersAction;
use App\Model\Workarea\WebSeo;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    public function index()
    {
        $data['slider'] = Fragment::get_slider([
            'take' => 5
        ]);
        $slider_ids = Fragment::extract_ids($data['slider']);
        $options = [
            'orderby' => 'mas-recientes'
        ];
        $data['rows'] = Fragment::get_paginate_posts($options);
        $data['seo'] = WebSeo::where('name', '=', 'home')->first();
        return view('web/index', $data);
    }

    public function hellociudades(Request $request)
    {
        $category = 1;
        $data['rows'] = Fragment::get_paginate_posts([
            'category' => $category,
            'subcategory' => $request->subcategory,
            'orderby' => $request->orderby,
        ]);
        $data['subcategories'] = Fragment::get_subcategories_by($category);
        $data['filter_subcategory'] = $request->subcategory;
        $data['filter_orderby'] = $request->orderby;
        $data['seo'] = WebSeo::where('name', '=', 'hellociudades')->first();
        return view('web/hellociudades/index', $data);
    }

    public function experienciasmoto(Request $request)
    {
        $category = 2;
        $data['rows'] = Fragment::get_paginate_posts([
            'category' => $category,
            'subcategory' => $request->subcategory,
            'orderby' => $request->orderby,
        ]);

        $data['subcategories'] = Fragment::get_subcategories_by($category);
        $data['filter_subcategory'] = $request->subcategory;
        $data['filter_orderby'] = $request->orderby;
        $data['seo'] = WebSeo::where('name', '=', 'experienciasmoto')->first();
        return view('web/experienciasmoto/index', $data);
    }

    public function motostyle(Request $request)
    {
        $category = 3;
        $data['rows'] = Fragment::get_paginate_posts([
            'category' => $category,
            'subcategory' => $request->subcategory,
            'orderby' => $request->orderby,
        ]);
        $data['subcategories'] = Fragment::get_subcategories_by($category);
        $data['filter_subcategory'] = $request->subcategory;
        $data['filter_orderby'] = $request->orderby;
        $data['seo'] = WebSeo::where('name', '=', 'motostyle')->first();
        return view('web/motostyle/index', $data);
    }


    public function motoembajadores()
    {
        $data['rows'] = Fragment::get_paginate_ambassadors([]);
        $data['seo'] = WebSeo::where('name', '=', 'motoembajadores')->first();
        return view('web/motoembajadores/index', $data);
    }

    public function motoembajadoresDetalle(Request $request)
    {
        $category = 5;
        $data['row'] = WebAmbassador::where('status', 1)
            ->where('slug', $request->ambassador)
            ->first();

        if (is_null($data['row'])) {
            return redirect('404');
        }


        if (Auth::check()) {
            $data['follow'] = WebUsersAction::where('status', 1)
                ->where('tablename', 'ambassador')
                ->where('action_type', 'follow')
                ->where('web_users_id', Session::get('userdata.id'))
                ->where('referal_id', $data['row']->id)
                ->count();
        } else {
            $data['follow'] = 0;
        }

        if (Auth::check() && (count($data['row']->webPlaylists) != 0)) {
            $data['like'] = WebUsersAction::where('status', 1)
                ->where('tablename', 'playlist')
                ->where('action_type', 'like')
                ->where('web_users_id', Session::get('userdata.id'))
                ->where('referal_id', $data['row']->webPlaylists[0]->id)
                ->count();
        } else {
            $data['like'] = 0;
        }

        $data['related_ambassador'] = WebPostsHasWebAmbassador::where('status', 1)
            ->where('web_ambassadors_id', $data['row']->id)
            ->whereHas('webPost', function($query){
                $query->whereNull('deleted_at');
            })
            ->get();

        $data['post'] = Fragment::get_post([]);

        $data['related'] = Fragment::get_paginate_posts([
            'orderby' => 'mas-recientes'
        ]);

        $data['url'] = url('/motoembajadores/embajador/' . $request->ambassador);

        return view('web/motoembajadores/detalle', $data);
    }

    public function motoembajadoresPlaylist(Request $request)
    {
        $data['rows'] = Fragment::get_paginate_playlists([
            'take' => 8
        ]);

        $category = 5;
        $data['subcategories'] = Fragment::get_subcategories_by($category);
        $data['filter_subcategory'] = $request->subcategory;
        $data['filter_orderby'] = $request->orderby;

        return view('web/motoembajadores/playlists/index', $data);
    }

    public function motoembajadoresPlaylistDetalle(Request $request)
    {
        $data['row'] = WebPlaylist::where('status', 1)
            ->where('slug', $request->playlist)
            ->first();

        if (Session::get('userdata.id')) {
            $data['follow'] = WebUsersAction::where('status', 1)
                ->where('tablename', 'ambassador')
                ->where('action_type', 'follow')
                ->where('web_users_id', Session::get('userdata.id'))
                ->where('referal_id', $data['row']->id)
                ->count();
        } else {
            $data['follow'] = 0;
        }

        if (Session::get('userdata.id')) {
            $data['like'] = WebUsersAction::where('status', 1)
                ->where('tablename', 'playlist')
                ->where('action_type', 'like')
                ->where('web_users_id', Session::get('userdata.id'))
                ->where('referal_id', $data['row']->id)
                ->count();
        } else {
            $data['like'] = 0;
        }

        return view('web/motoembajadores/playlists/detalle', $data);
    }

    public function hellotips(Request $request)
    {
        $category = 5;
        $data['rows'] = Fragment::get_paginate_posts([
            'category' => $category,
            'subcategory' => $request->subcategory,
            'orderby' => $request->orderby
        ]);
        $data['subcategories'] = Fragment::get_subcategories_by($category);
        $data['filter_subcategory'] = $request->subcategory;
        $data['filter_orderby'] = $request->orderby;
        $data['seo'] = WebSeo::where('name', '=', 'hellotips')->first();
        return view('web/hellotips/index', $data);
    }

    public function search(Request $request)
    {
        $data['word'] = $request->word;
        $rows = WebPost::where('status', 1)
            ->where('description', 'like', '%' . $request->word . '%');

        if ($rows->count() > 0) {
            $data['rows'] = $rows->get();
        } else {
            $data['rows'] = false;
        }

        return view('web/busqueda/index', $data);
    }

    public function error_404()
    {
        return view('errors/404');
    }

    public function error_505()
    {
        return view('web/505');
    }

    public function usuario_mis_embajadores()
    {
        $data['rows'] = WebUsersAction::where('status', 1)
            ->where('web_users_id', Session::get('userdata.id'))
            ->where('tablename', 'ambassador')
            ->where('action_type', 'follow')
            ->get();
        return view('web/usuarios/mis-embajadores', $data);
    }

    public function usuario_mis_playlist()
    {
        $data['rows'] = WebUsersAction::where('status', 1)
            ->where('web_users_id', Session::get('userdata.id'))
            ->where('tablename', 'playlist')
            ->where('action_type', 'like')
            ->get();
        return view('web/usuarios/mis-playlist', $data);
    }

    public function usuario_resultados()
    {
        return view('web/resultados');
    }

    public function usuario_loultimo()
    {
        $data['landings'] = WebLanding::orderBy('id', 'desc')->get();
        $data['user'] = Auth::user();

        return view('web/usuarios/lo-ultimo', $data);
    }

    public function usuario_canjear()
    {
        return view('web/usuarios/canjear');
    }

    public function usuario_perfil()
    {
        $data['user'] = Auth::user();
        return view('web/usuarios/perfil', $data);
    }

    public function usuario_hellomoto()
    {
        return view('web/usuarios/hellomoto');
    }
}
