<?php

namespace App\Http\Controllers\Web;

use App\Http\Traits\SocialiteTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\Request;

class FacebookController extends Controller
{
    use SocialiteTrait;

    protected $driver = 'facebook';

    public function redirect(Request $request)
    {
        return Socialite::driver($this->driver)
            ->with(['state' => $request->action_type])
            ->redirect();
    }

    public function callback(Request $request)
    {
        $action = $request->input('state');
        $user = Socialite::driver($this->driver)->stateless()->user();

        if (isset($user->email) && !is_null($user->email)) {
            $login = $this->checkEmail($user->email);
            if($login){
                $this->checkSocialId([
                    'id' => $user->id,
                    'email' => $user->email
                ], $this->driver);
                Auth::guard('web')->loginUsingId($login->id);
                return redirect('/usuario/beneficios');
            }else{
                $fullname = explode(' ', $user->name);
                if (count($fullname) > 2) {
                    $counter = count($fullname) - 2;
                } else {
                    $counter = 1;
                }
                $data['firstname'] = $fullname[0];
                $data['lastname'] = $fullname[count($fullname) - $counter];
                $data['email'] = $user->email;

                return redirect('/?modal=register')->with($data);
            }
        }else{
            return redirect('/?error-'.$action.'=' . $this->driver);
        }
    }
}