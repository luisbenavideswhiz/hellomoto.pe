<?php

namespace App\Http\Controllers\Web;

use App\Model\Web\WebCategory;
use App\Model\Web\WebPost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SitemapController extends Controller
{
    /**
     * @return $this
     */
    public function sitemapXml()
    {
        $data['categories'] = WebCategory::where('status','=', 1)
                ->where('category_id', '<>', 0)
                ->has('webPostsHasWebCategories')
                ->get();
        return response()
            ->view('web/sitemap/xml', $data)
            ->header('Content-Type', 'text/xml');
    }

    public function sitemapXmlCategory($categoryId, $subCategoryId=null)
    {
        $data['category'] = WebCategory::find($categoryId);
        $data['subCategory'] = WebCategory::where('id', '=', $subCategoryId)
            ->where('category_id', '=', $categoryId)->firstOrFail();

        return response()
            ->view('web/sitemap/types', $data)
            ->header('Content-Type', 'text/xml');
    }

    public function sitemapXmlTypes($categoryId, $subCategoryId, $type='web')
    {
        $data['posts'] =  WebPost::whereHas('webCategories', function ($query) use ($categoryId, $subCategoryId){
                    $query->where('web_categories_id_parent', '=', $categoryId);
                    $query->where('web_categories_id', '=', $subCategoryId);
                })
                ->where('status', '=', 1)
                ->select(DB::raw('title, updated_at, created_at, tags, CONCAT("/post/",slug) as slug'))
                ->orderBy('created_at', 'DESC')
                ->limit(50000)
                ->get();

        if($type == 'web'){
            $view = 'web/sitemap/category';
        }
        if($type == 'news'){
            $view = 'web/sitemap/category-news';
        }
        return response()
            ->view($view, $data)
            ->header('Content-Type', 'text/xml');
    }
}
