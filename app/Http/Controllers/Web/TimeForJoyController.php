<?php

namespace App\Http\Controllers\Web;

use App\Services\TimeForJoy;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class TimeForJoyController extends Controller
{
    public function index(TimeForJoy $timeForJoy)
    {
        if(!Cache::has('count')){
            $data['count'] = $timeForJoy->count();
            Cache::put('count', $data['count'], 5);
        }else{
            $data['count'] = Cache::get('count');
        }
        return view('mototimeforjoy.index', $data);
    }

    /**
     * @param TimeForJoy $timeForJoy
     * @return mixed
     */
    public function count(TimeForJoy $timeForJoy)
    {
        if(!Cache::has('count')){
            $data = $timeForJoy->count();
            Cache::put('count', $data, 5);
        }else{
            $data = Cache::get('count');
        }
        return $this->responseSuccess('',$data);
    }
}
