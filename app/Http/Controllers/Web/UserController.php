<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\ChangePasswordRequest;
use App\Http\Requests\Web\CreateRequest;
use App\Http\Requests\Web\LoginRequest;
use App\Http\Requests\Web\RecoveryPasswordRequest;
use App\Http\Requests\Web\UpdateRequest;
use App\Http\Requests\Web\UploadRequest;
use App\Http\Traits\ResponseTrait;
use App\Mail\Web\RecoveryPasswordMailing;
use App\Mail\Web\WelcomeMailing;
use App\Model\Web\WebUser;
use App\Model\Web\WebUserRecoveryPassword;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    use ResponseTrait;

    /**
     * @param CreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(CreateRequest $request)
    {
        $data = $request->except('_token');
        $data['web_users_type_id'] = 'usuario';
        $data['status'] = '1';

        $date = Carbon::now();
        if ($date->subYears(18)->format('Y-m-d') < $data['birthdate']) {
            return $this->responseError('', ['errors' => ['birthdate' => ["Para registrarte es necesario tener 18 años"]]
            ]);
        }

        try {
            $data['password'] = bcrypt($data['password']);
            $create = WebUser::create($data);
            Auth::guard('web')->loginUsingId($create->id);
            Mail::to($create->email)->send(new WelcomeMailing($create));
            $response = $this->responseSuccess('', ['redirect' => '/usuario/beneficios']);
        } catch (\Exception $e) {
            $response = $this->responseError('', [
                'errors' => [
                    'name' => ["No hemos podido registrarte, por favor, intenta nuevamente"]
                ],
                'catch' => $e->getMessage()
            ]);
        }
        return $response;
    }

    public function update(UpdateRequest $request)
    {
        $data = $request->except('_token');
        try {
            WebUser::where('status', 1)
                ->where('web_users_type_id', 'usuario')
                ->where('id', Auth::id())
                ->update($data);
            $response = $this->responseSuccess('', ['redirect' => '/usuario/perfil']);
        } catch (\Exception $e) {
            $response = $this->responseError('', [
                'errors' => [
                    'name' => ["No hemos podido actualizar tu perfil, por favor, intenta nuevamente"]
                ],
                'catch' => $e,
            ]);
        }
        return $response;
    }

    public function login(LoginRequest $request)
    {
        if (Auth::guard('web')->attempt($request->only(['email', 'password']), $request->has('remember'))) {
            $response = $this->responseSuccess('', ['redirect' => '/usuario/beneficios']);
        } else {
            $response = $this->responseError('', [
                'errors' => [
                    'username' => ["Usuario o password no v&aacute;lido"]
                ]
            ]);
        }
        return $response;
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function recovery_password(RecoveryPasswordRequest $request)
    {
        $user = WebUser::where('email', $request->email);

        if ($user->count() != 0) {
            $user = $user->first();
            WebUser::where('status', 1)
                ->where('email', $request->email)
                ->update(['status' => 0]);

            $exist = WebUserRecoveryPassword::where('status', 1)
                ->where('web_users_id', $user->id);

            if ($exist->count() != 0) {
                $exist->delete();
            }

            $recovery = new WebUserRecoveryPassword();
            $recovery->code = str_pad(mt_rand(1, 99999999), 8, '0', STR_PAD_LEFT);
            $recovery->web_users_id = $user->id;
            $recovery->status = 1;
            $recovery->save();

            Mail::to($user->email)->send(new RecoveryPasswordMailing($user, $recovery));
            $response = $this->responseSuccess();
        } else {
            $response = $this->responseError('', [
                'errors' => [
                    'email' => ["Email no existe"]
                ]
            ]);
        }
        return $response;
    }

    public function change_password(ChangePasswordRequest $request)
    {
        $recovery = WebUserRecoveryPassword::where('status', 1)
            ->where('code', $request->code);

        if ($recovery->count() != 0) {
            $row = $recovery->first();
            WebUser::where('status', 0)
                ->where('email', $row->webUser->email)
                ->update([
                    'status' => 1,
                    'password' => bcrypt($request->password)
                ]);
            $recovery->delete();
            $response = $this->responseSuccess('Tu contrase&ntilde;a ha sido cambiada<br>ya puedes logearte y disfrutar de la comunidad hellomoto');
        } else {
            $response = $this->responseError('', [
                'errors' => [
                    'code' => ["C&oacute;digo inv&aacute;lido"]
                ]
            ]);
        }
        return $response;
    }

    public function upload_image(UploadRequest $request)
    {
        $id = Auth::id();
        $path = $request->profile_img->storeAs('/public/users', $id . '.' . $request->profile_img->getClientOriginalExtension());

        if ($path) {
            WebUser::where('status', 1)
                ->where('id', $id)
                ->update([
                    'profile_img' => '/storage/users/' . $id . '.' . $request->profile_img->getClientOriginalExtension()
                ]);
            $user = WebUser::where('status', 1)
                ->where('id', $id)
                ->first();

            $response = $this->responseSuccess('', [
                'redirect' => '/usuario/perfil'
            ]);
        } else {
            $response = $this->responseError('', [
                'errors' => [
                    'profile_img' => ["No hemos podido subir la imagen, por favor, intenta nuevamente"]
                ]
            ]);
        }
        return $response;
    }
}
