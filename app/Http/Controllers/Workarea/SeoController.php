<?php

namespace App\Http\Controllers\Workarea;

use App\Http\Requests\Workarea\Seo\SeoCreateRequest;
use App\Http\Requests\Workarea\Seo\SeoUpdateRequest;
use App\Http\Traits\ResponseTrait;
use App\Model\Web\WebCategory;
use App\Model\Workarea\WebSeo;
use Exception;
use Illuminate\Http\Request;

class SeoController extends Controller
{
    use ResponseTrait;

    public function index(Request $request)
    {
        $data['category'] = WebCategory::where('category_id', '=', 0)
            ->where('status', '=', 1)->get();
        $seo = WebSeo::where('status', '!=', 0);
        if ($request->has('query')) {
            $seo = $seo->where('name', 'like', '%' . $request->get('query') . '%');
            $data['query'] = $request->get('query');
        }
        $data['seo'] = $seo->orderBy('created_at', 'desc')->paginate(20);
        return view('workarea.seo.index', $data);
    }

    /**
     * @param SeoCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SeoCreateRequest $request)
    {
        try {
            $data = $request->only(['name', 'title', 'description', 'alt', 'url', 'keywords']);
            $data['image'] = $request->hasFile('image') ? '/storage/' . $request->image->store('seo', 'public') : '';
            $data['status'] = 1;
            WebSeo::create($data);
            $response = $this->responseSuccess('Se creo con exito', ['redirect' => route('workarea.seo.index')]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $data = WebSeo::find($id);
        return $this->responseSuccess('', $data);
    }

    /**
     * @param $id
     * @param SeoUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, SeoUpdateRequest $request)
    {
        $seo = WebSeo::find($id);
        $data = $request->only(['title', 'description', 'alt', 'url', 'keywords']);
        if ($request->hasFile('image')) {
            $data['image'] = '/storage/' . $request->image->store('seo', 'public');
        }
        $seo->update($data);
        return $this->responseSuccess('Se editó con exito', ['redirect' => route('workarea.seo.index')]);
    }

    public function delete($id)
    {
        try {
            WebSeo::find($id)->delete();
        } catch (Exception $e) {
            return $this->responseError($e->getMessage(), ['redirect' => route('workarea.seo.index')]);
        }
        return $this->responseSuccess();
    }
}
