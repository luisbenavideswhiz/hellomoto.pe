<?php

namespace App\Http\Controllers\Workarea;

use App\Http\Requests\Workarea\Trivia\TriviaCreateRequest;
use App\Http\Requests\Workarea\Trivia\TriviaEditRequest;
use App\Model\Trivia\Option;
use App\Model\Trivia\Question;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TriviaController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $questions = Question::orderBy('created_at','asc');
        if($request->has('query')){
            $questions = $questions->where('question','like', '%'.$request->get('query').'%')
            ->orWhere('answer','like', '%'.$request->get('query').'%');
            $data['query'] = $request->get('query');
        }
        $questions = $questions->paginate(20);
        $data['questions'] = $questions;
        return view('workarea.trivia.index', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        return view('workarea.trivia.create');
    }

    public function postCreate(TriviaCreateRequest $request)
    {
        $questionData = $request->only(['question','correct_description','error_description','answer']);
        $question = Question::create($questionData);

        $options = $request->get('options', []);
        $answers = isset($options['answer'])?$options['answer']:[];
        $corrects = isset($options['correct'])?$options['correct']:[];
        foreach($answers as $k => $answer){
            Option::create([
                'question_id'=> $question->id,
                'answer' => $answer,
                'correct' => in_array($k+1, $corrects)?1:0
            ]);
        }
        return response()->json(['status' => true, 'message' => 'Se registro con exito!', 'data'=> ['redirect' => route('workarea.trivia.edit', ['id'=>$question->id])]]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getEdit($id)
    {
        try{
            $question = Question::find($id);
            $data['question'] = $question;
            $data['options'] = $question->options;
        }catch (Exception $exception){
            return redirect()->route('workarea.dashboard.index');
        }
        return view('workarea.trivia.edit', $data);
    }

    /**
     * @param $id
     * @param TriviaEditRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function postEdit($id, TriviaEditRequest $request)
    {
        $questionData = $request->only(['question','correct_description','error_description','answer']);
        $question = Question::find($id);
        $question->update($questionData);

        if($request->has('options.answer')){
            $delete = [];
            $options = $request->get('options', []);
            $answers = isset($options['answer'])?$options['answer']:[];
            $corrects = isset($options['correct'])?$options['correct']:[];
            $ids = isset($options['id'])?$options['id']:[];

            foreach($question->options as $option){
                if(!in_array($option->id, $ids)){
                    array_push($delete, $option->id);
                }
            }
            Option::whereIn('id', $delete)->delete();

            foreach($answers as $k => $answer){
                Option::updateOrCreate([
                    'question_id'=> $question->id,
                    'id' => isset($ids[$k])?$ids[$k]:null
                ],[
                    'answer' => $answer,
                        'correct' => in_array($k+1, $corrects)?1:0
                ]);
            }
        }else{
            Option::where('question_id', '=', $question->id)->delete();
        }
        return response()->json(['status' => true, 'message' => 'Se actualizo con exito!', 'data'=> []]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function delete($id)
    {
        try{
            $question = Question::find($id);
            $question->delete();
        }catch (Exception $exception){
            throw new Exception('no se encontro la pregunta');
        }
        return response()->json(['status' => true, 'message' => 'Se elimino con exito!']);
    }
}
