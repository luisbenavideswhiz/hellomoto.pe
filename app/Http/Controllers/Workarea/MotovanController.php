<?php

namespace App\Http\Controllers\Workarea;

use App\Http\Requests\Motovan\TrainingCreateRequest;
use App\Http\Requests\Motovan\TrainingEditRequest;
use App\Model\MotoVan\Training;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class MotovanController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $trainings = Training::where('status', '<>', 0);
        if($request->has('query')){
            $trainings = $trainings->where('location', 'like', '%'.$request->get('query').'%');
            $data['query'] = $request->get('query');
        }
        $data['trainings'] =  $trainings->orderBy('date', 'desc')->paginate(20);
        return view('workarea.motovan.index', $data);
    }

    /**
     * @param TrainingCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreate(TrainingCreateRequest $request)
    {
        $data = $request->only(['total_coupons', 'location', 'date']);
        $data['date'] = Carbon::createFromFormat('d/m/Y H:i:s', $data['date']);
        $data['remaining_coupons'] = $data['total_coupons'];
        $data['status'] = 1;
        Training::create($data);
        return response()->json(['status'=>true, 'message' => 'Se creo con exito', 'data'=> ['redirect' => route('workarea.motovan.index')]]);
    }

    /**
     * @param $id
     * @param TrainingEditRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postEdit($id, TrainingEditRequest $request)
    {
        $training = Training::find($id);
        $data = $request->only(['total_coupons', 'location', 'date']);
        $data['date'] = Carbon::createFromFormat('d/m/Y H:i:s', $data['date']);
        if($data['total_coupons'] < $training->remaining_coupons){
            $data['remaining_coupons'] = $data['total_coupons'];
        }
        $training->update($data);
        return  response()->json(['status'=>true, 'message' => 'Se editó con exito', 'data'=> ['redirect' => route('workarea.motovan.index')]]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEdit($id)
    {
        $data = Training::find($id);
        $newData = $data->toArray();
        $newData['date'] =  $data->date->format('d/m/Y H:i:s');
        return response()->json(['status'=> true, 'data' => $newData]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getUnpublish($id)
    {
        Training::where('id', '=',$id)->where('status','=',1)->update(['status' => 2]);
        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getPublish($id)
    {
        Training::where('id', '=',$id)->where('status','=',2)->update(['status' => 1]);
        return redirect()->back();
    }

    /**
     * @param $id
     */
    public function getDownload($id)
    {
        $training = Training::find($id);
        $data = $training->users()->select('name as nombre',
            'last_name as apellido',
            'area',
            'birthdate as fecha_nacimiento',
            'document_number as dni',
            'email',
            'phone as celular',
            'motovan_training_user.created_at as fecha_registro'
        )->get();
        Excel::create('motovan-'.str_slug(strtolower($training->location)), function($excel) use($data) {
            $excel->sheet('users', function($sheet) use($data) {
                $sheet->fromArray($data);
            });
        })->download('xlsx');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteTraining($id)
    {
        $training = Training::find($id);
        if($training->users()->count()>0){
            return response()->json(['status'=>false, 'message'=> 'No se puede eliminar capacitacion porque tiene personas registradas']);
        }else{
            $training->delete();
        }
        return response()->json(['status'=>true, 'message'=> 'Se elimino con exito!']);

    }
}
