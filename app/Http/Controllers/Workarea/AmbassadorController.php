<?php

namespace App\Http\Controllers\Workarea;

use App\Http\Requests\Workarea\Ambassador\AmbassadorCreateRequest;
use App\Http\Requests\Workarea\Ambassador\AmbassadorEditRequest;
use App\Model\Web\WebAmbassador;
use App\Model\Web\WebCareer;
use App\Model\Web\WebColor;
use App\Model\Web\WebGallery;
use App\Model\Web\WebPlaylist;
use App\Model\Web\WebSocialNetwork;
use App\Model\Web\WebSocialNetworksType;
use App\Model\Web\WebUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use PHPHtmlParser\Dom;

class AmbassadorController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $ambassadors = WebAmbassador::where('status','<>', 0)
            ->where('slug','like', '%'.$request->q.'%')
            ->select(\DB::raw('slug as text, id'))->get();
        return response()->json(['results'=>$ambassadors]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $ambassadors = WebAmbassador::where('status','<>', 0);
        if($request->has('query')){
            $ambassadors = $ambassadors->where('slug','like', '%'.$request->get('query').'%')
                ->orWhere('nickname','like', '%'.$request->get('query').'%')
                ->orWhereHas('webUser',function($query) use ($request){
                    $query->where('name','like', '%'.$request->get('query').'%');
                });
            $data['query'] = $request->get('query');
        }
        $ambassadors = $ambassadors->orderBy('created_at','desc')->paginate(20);
        $data['ambassadors'] = $ambassadors;
        return view('workarea.ambassadors.index',$data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data['colors'] = WebColor::all();
        $data['networks'] = WebSocialNetworksType::all();
        $data['careers'] = WebCareer::all();
        return view('workarea.ambassadors.create', $data);
    }

    /**
     * @param AmbassadorCreateRequest $request
     * @param Dom $dom
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreate(AmbassadorCreateRequest $request, Dom $dom)
    {
        $dataUser = $request->only('name');
        $dataUser['document_type'] = 'dni';
        $dataUser['web_users_type_id'] = 'embajador';
        $dataUser['mobile_operator'] = 'claro';
        $dataUser['status'] = 1;
        $user = WebUser::create($dataUser);

        $dataAmbassador = $request->only(['nickname','web_colors_id', 'city', 'web_career_id', 'bio', 'position', 'status','profile_video','tags','meta_title','meta_description','meta_keywords']);        $dataAmbassador['profile_img'] = $request->hasFile('image')?'/storage/'.$request->image->store('ambassadors', 'public'):'';
        $dataAmbassador['web_users_id'] = $user->id;
        $dataAmbassador['slug'] = str_slug($dataUser['name']);
        if(!is_numeric($dataAmbassador['web_career_id'])){
            $dataAmbassador['web_career_id'] = WebCareer::create(['status'=>1, 'name'=>$dataAmbassador['web_career_id']])->id;
        }
        $ambassador = WebAmbassador::create($dataAmbassador);

        if($request->has('playlist_url')){
            foreach($request->playlist_url as $key => $url){
                $dom->load($url);
                $contents = file_get_contents($dom->find("meta[property='og:image']")->content);
                $title = $request->get('playlist_title')[$key];
                Storage::put('public/ambassadors/playlists/'.str_slug($title).'.png',$contents);
                WebPlaylist::create([
                    'url'=> $url,
                    'web_ambassadors_id'=>$ambassador->id,
                    'title' => $title,
                    'status' => 1,
                    'slug' => str_slug($title),
                    'image' => '/storage/ambassadors/playlists/'.str_slug($title).'.png'
                ]);
            }
        }

        $socialNetworks = $request->get('social_network',[]);
        foreach($request->get('social_network_type',[]) as $key => $socialNetworkType){
            $socialNetwork = WebSocialNetwork::create([
                'url' => $socialNetworks[$key],
                'web_social_networks_types_id' => $socialNetworkType,
                'status' => 1
            ]);
            $ambassador->webSocialNetworks()->attach($socialNetwork->id,  ['status'=>1]);
        }

        return response()->json([
            'status'=>true,
            'message'=>'Se ha creado con exito!',
            'data'=>[
                'redirect'=> route('workarea.ambassadors.edit', ['id'=>$ambassador->id])
            ]
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {
        $data['ambassador'] = WebAmbassador::find($id);
        $data['colors'] = WebColor::all();
        $data['networks'] = WebSocialNetworksType::all();
        $data['careers'] = WebCareer::all();
        return view('workarea.ambassadors.edit', $data);
    }

    /**
     * @param $id
     * @param AmbassadorEditRequest $request
     * @param Dom $dom
     * @return \Illuminate\Http\JsonResponse
     */
    public function postEdit($id, AmbassadorEditRequest $request, DOM $dom)
    {
        $ambassador = WebAmbassador::find($id);

        if($request->has('playlist_url')){
            foreach ($ambassador->webPlaylists as $playlist){
                if(!in_array($playlist->url, $request->playlist_url)){
                    $playlist->delete();
                }
            }
            try{
                foreach($request->get('playlist_url', []) as $key => $url){
                    $dom->load($url);
                    $contents = file_get_contents($dom->find("meta[property='og:image']")->content);
                    $title = $request->get('playlist_title')[$key];
                    Storage::put('public/ambassadors/playlists/'.str_slug($title).'.png',$contents);
                    WebPlaylist::updateOrCreate([
                        'url'=> $url,
                        'web_ambassadors_id'=>$ambassador->id
                    ],[
                        'title' => $title,
                        'status' => 1,
                        'slug' => str_slug($title),
                        'image' => '/storage/ambassadors/playlists/'.str_slug($title).'.png'
                    ]);
                }
            }catch (\Exception $exception){
                Log::error('error dom scrapper', ['message' =>$exception->getMessage(), 'trace'=> $exception->getTrace()]);
            }
        }

        $ambassador->webUser->update($request->only('name'));
        $dataAmbassador = $request->only(['nickname','web_colors_id', 'city', 'web_career_id', 'bio', 'position', 'status','profile_video','tags','meta_title','meta_description','meta_keywords']);
        $dataAmbassador['slug'] = str_slug($request->get('name'));
        if($request->hasFile('image')){
            $dataAmbassador['profile_img'] = '/storage/'.$request->image->store('ambassadors', 'public');
        }
        if(!is_numeric($dataAmbassador['web_career_id'])){
            $dataAmbassador['web_career_id'] = WebCareer::create(['status'=>1, 'name'=>$dataAmbassador['web_career_id']])->id;
        }
        $ambassador->update($dataAmbassador);

        if($request->has('social_network') && count($request->get('social_network',[]))>0){
            $socialNetworks = $request->get('social_network',[]);
            $socialNetworkTypes = $request->get('social_network_type',[]);

            foreach ($ambassador->webSocialNetworks as $socialNetwork) {
                if(!in_array($socialNetwork->web_social_networks_types_id, $socialNetworkTypes)){
                    $ambassador->webSocialNetworks()->detach($socialNetwork->id);
                    $socialNetwork->delete();
                }
            }
            $socialNetworkTypesId = array_map(function($network){ return $network['web_social_networks_types_id']; },$ambassador->webSocialNetworks->toArray());
            foreach($socialNetworkTypes as $key => $socialNetworkType){
                if(in_array($socialNetworkType, $socialNetworkTypesId)){
                    WebSocialNetwork::whereHas('webAmbassadors', function ($query) use ($ambassador){
                        $query->where('id', '=', $ambassador->id);
                    })->where('web_social_networks_types_id', '=', $socialNetworkType)
                        ->where('status','=', 1)
                        ->update([
                        'url' => $socialNetworks[$key]
                    ]);
                }else{
                    $socialNetwork = WebSocialNetwork::create([
                        'web_social_networks_types_id' => $socialNetworkType,
                        'status' => 1,
                        'url' => $socialNetworks[$key]
                    ]);
                    $ambassador->webSocialNetworks()->attach($socialNetwork->id, ['status'=>1]);
                }
            }
        }
        return response()->json([
            'status'=>true,
            'message'=>'Se ha editado con exito!',
            'data'=>[]
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $ambassador = WebAmbassador::find($id);
        if($ambassador->webPosts()->whereNotNull('web_posts.deleted_at')->count()>0){
            return response()->json(['status'=>false, 'message' => 'No se ha podido eliminar hay post relacionados con el embajador']);
        }
        if($ambassador->webPlaylists()->whereNotNull('web_playlists.deleted_at')->count()>0){
            return response()->json(['status'=>false, 'message' => 'No se ha podido eliminar hay playlist relacionados con el embajador']);
        }
        $ambassador->delete();
        return response()->json(['status'=>true, 'message' => 'Se ha eliminado con exito!']);
    }

    /**
     * @param $ambassadorId
     * @param $galleryId
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteGallery($ambassadorId, $galleryId)
    {
        WebAmbassador::find($ambassadorId)->webGalleries()->detach($galleryId);
        $gallery = WebGallery::find($galleryId);
        Storage::delete($gallery->path);
        $gallery->delete();
        return response()->json([]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postGallery(Request $request)
    {
        $ambassador = WebAmbassador::find($request->id);
        $files = [];
        foreach ($request->gallery as $file){

            $path = $file->store('gallery/ambassadors/'.$request->id, 'public');
            $gallery = WebGallery::create([
                'status' => 1,
                'filetype' => 'image',
                'title' => File::name('storage/'.$path),
                'path' => 'storage/' . $path
            ]);

            array_push($files,[
                'deleteType' => 'DELETE',
                'deleteUrl' => route('workarea.ambassadors.gallery.delete', ['ambassador'=> $ambassador->id,'gallery' => $gallery->id]),
                'name' => File::name('storage/'.$path),
                'size' => File::size('storage/'.$path),
                'type' => File::mimeType('storage/'.$path),
                'url'=> url('storage/'.$path),
                'thumbnailUrl' => $gallery->thumbnail
            ]);
            $ambassador->webGalleries()->attach($gallery->id,['status'=>1]);
        }
        return response()->json(['files' => $files]);
    }

    /**
     * @param $id
     */
    public function deletePlaylist($id)
    {
        WebPlaylist::find($id)->delete();
        response()->json(['status' => true]);
    }
}
