<?php

namespace App\Http\Controllers\Workarea;

use App\Model\Workarea\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Workarea\Profile\UpdateRequest;
use App\Http\Requests\Workarea\User\UpdatePasswordRequest;

class ProfileController extends Controller
{
    public function index()
    {
        return view('workarea.profile.index');
    }

    public function update(UpdateRequest $request)
    {
        $data = User::find($request->id);
        $data->name = $request->name;
        $data->save();

        if ($data) {
            return back()->with('status', 'Se actualizo correctamente.');
        } else {
            return back()->with('status', 'Error al actualizar.');
        }
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $data = User::find($request->id);
        $data->password = bcrypt($request->password);
        $data->save();

        if ($data) {
            return back()->with('status', 'Se actualizo correctamente su contraseña.');
        } else {
            return back()->with('status', 'Error al actualizar.');
        }
    }
}
