<?php

namespace App\Http\Controllers\Workarea;

use App\Http\Requests\Workarea\Promotion\PromotionCreateRequest;
use App\Http\Requests\Workarea\Promotion\PromotionEditRequest;
use App\Model\Web\WebPromotion;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $promotions = WebPromotion::where('status','<>',0);

        if($request->has('query')){
            $promotions = $promotions->where('title','like', '%'.$request->get('query').'%')
                ->orWhere('created_at','like', '%'.$request->get('query').'%');
            $data['query'] = $request->get('query');
        }
        $data['promotions'] =  $promotions->orderBy('position', 'ASC')->paginate(20);
        return view('workarea.promotions.index', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        return view('workarea.promotions.create');
    }

    /**
     * @param PromotionCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreate(PromotionCreateRequest $request)
    {
        $data = $request->only(['title','link','status', 'position']);
        $data['image'] = $request->hasFile('image')?'/storage/'.$request->image->store('promotions', 'public'):'';
        $data['status'] = 1;
        WebPromotion::create($data);
        return response()->json(['status'=>true, 'message' => 'Se creo con exito', 'data'=> ['redirect' => route('workarea.promotions.index')]]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getEdit($id)
    {
        $promotion = WebPromotion::find($id);
        if(is_null($promotion)){
            return response()->json(['status'=>false, 'data'=> ['redirect'=>route('workarea.promotions.index')]]);
        }
        return  response()->json(['status'=>true, 'data'=> $promotion]);
    }

    /**
     * @param $id
     * @param PromotionEditRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postEdit($id, PromotionEditRequest $request)
    {
        $promotion = WebPromotion::find($id);
        $data = $request->only(['title','link','status', 'position']);
        if($request->hasFile('image')){
            $data['image'] = '/storage/'.$request->image->store('promotions', 'public');
        }
        $promotion->update($data);
        return  response()->json(['status'=>true, 'message' => 'Se editó con exito', 'data'=> ['redirect' => route('workarea.promotions.index')]]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try{
            WebPromotion::find($id)->delete();
        }catch (\Exception $exception){
            return response()->json(['status'=>false, 'message'=> $exception->getMessage(), 'data'=> ['redirect'=> route('workarea.promotions.index')]]);
        }
        return response()->json(['status'=>true]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getPublish($id)
    {
        WebPromotion::where('id', '=',$id)->where('status','=',2)->update(['status' => 1]);
        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getUnpublish($id)
    {
        WebPromotion::where('id', '=',$id)->where('status','=',1)->update(['status' => 2]);
        return redirect()->back();
    }
}
