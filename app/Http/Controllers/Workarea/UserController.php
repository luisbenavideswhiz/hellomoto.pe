<?php

namespace App\Http\Controllers\Workarea;

use App\Model\Web\WebUser;
use App\Exports\UsersExport;
use App\Model\Workarea\User;
use Illuminate\Http\Request;
use App\Model\Web\WebLanding;
use App\Model\Web\WebAmbassador;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use App\Model\Workarea\RecoverPassword;
use App\Http\Requests\Workarea\User\LoginRequest;
use App\Mail\Workarea\RecoveryPasswordMailing;
use App\Http\Requests\Workarea\User\ChangePasswordRequest;
use App\Http\Requests\Workarea\User\RecoverPasswordRequest;

class UserController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $data = $this->getUsers($request->get('query'), $request->filter, $request->dates, 100);
        $data['filters'] = (object)[
            (object)['id' => 0, 'name' => 'Todos los campos'],
            (object)['id' => 1, 'name' => 'Embajadores'],
            (object)['id' => 2, 'name' => 'Beneficios']
        ];
        $data['appends'] = ['query' => $request->get('query', []), 'filter' => $data['filter']];
        return view('workarea.users.index', $data);
    }

    public function getDownload(Request $request)
    {
        $data = $this->getUsers($request->get('query'), $request->filter, $request->dates);
        /*if(count($data)>2000){

        }else{*/
        return Excel::download(new UsersExport($data['users']), 'users.xlsx');
        //}
    }

    /**
     * @param null $query
     * @param null $limit
     * @return WebUser
     */
    private function getUsers($query = null, $filter = 0, $dates = null, $limit = null)
    {
        $users = WebUser::where('web_users.status', '=', 1);
        $dates = is_null($dates) ? null : explode(" ", $dates);

        if (!is_null($query)) {
            switch ($filter) {
                case 1:
                    $search = $query;
                    $ambassador = WebAmbassador::where('slug', 'like', '%' . $search . '%')
                        ->orWhere('nickname', 'like', '%' . $search . '%')
                        ->first();
                    if (is_null($ambassador)) {
                        $ambassador = (object)['id' => 0];
                    }
                    $users = $users->join('web_users_actions', 'web_users_actions.web_users_id', '=', 'web_users.id')
                        ->where('tablename', '=', 'ambassador')
                        ->where('referal_id', '=', $ambassador->id)
                        ->select(['name', 'lastname', 'document_type', 'document_number', 'sex', 'email', 'birthdate', 'web_users_actions.created_at']);
                    if (!is_null($dates)) {
                        $dates[0] = $dates[0] . ' 00:00:00';
                        $dates[1] = $dates[1] . ' 23:59:59';
                        $users = $users->whereBetween('web_users_actions.created_at', $dates);
                    }
                    break;
                case 2:
                    $search = $query;
                    $benefit = WebLanding::where('title', 'like', '%' . $search . '%')->first();
                    if (is_null($benefit)) {
                        $benefit = (object)['id' => 0];
                    }
                    $users = $users->join('web_users_has_web_landings', 'web_users_has_web_landings.web_user_id', '=', 'web_users.id')
                        ->where('web_landing_id', '=', $benefit->id)
                        ->select(['name', 'lastname', 'document_type', 'document_number', 'sex', 'email', 'birthdate', 'web_users_has_web_landings.created_at']);
                    if (!is_null($dates)) {
                        $dates[0] = $dates[0] . ' 00:00:00';
                        $dates[1] = $dates[1] . ' 23:59:59';
                        $users = $users->whereBetween('web_users_has_web_landings.created_at', $dates);
                    }
                    break;
                default:
                    $field = $query;
                    $users = $users->where(function ($query) use ($field) {
                        $query->orWhere('name', 'like', '%' . $field . '%')
                            ->orWhere('lastname', 'like', '%' . $field . '%')
                            ->orWhere('document_number', 'like', '%' . $field . '%')
                            ->orWhere('document_type', 'like', '%' . $field . '%')
                            ->orWhere('sex', 'like', '%' . $field . '%')
                            ->orWhere('email', 'like', '%' . $field . '%')
                            ->orWhere('created_at', 'like', '%' . $field . '%');
                    });
                    if (!is_null($dates)) {
                        $dates[0] = $dates[0] . ' 00:00:00';
                        $dates[1] = $dates[1] . ' 23:59:59';
                        $users = $users->whereBetween('created_at', $dates);
                    }
                    break;
            }
        } else {
            if (!is_null($dates)) {
                $dates[0] = $dates[0] . ' 00:00:00';
                $dates[1] = $dates[1] . ' 23:59:59';
                $users = $users->whereBetween('created_at', $dates);
            }
        }
        $data['filter'] = is_null($filter) ? 0 : $filter;
        $data['query'] = isset($data['filter']) ? $query : null;
        $users = $users->orderBy('created_at', 'DESC');
        if (!is_null($limit)) {
            $data['users'] = $users->paginate($limit);
        } else {
            $data['users'] = $users->get();
        }
        return $data;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLogin()
    {
        return view('workarea.users.login');
    }

    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postLogin(LoginRequest $request)
    {
        if (Auth::guard('workarea')->attempt($request->only(['email', 'password']), $request->has('remember'))) {
            $response = redirect()->route('workarea.dashboard.index');
        } else {
            $response = redirect()->back()
                ->withInput($request->only(['email']), 'remember')
                ->withErrors(['email' => 'No se encontro el usuario']);
        }
        return $response;
    }

    /**
     * @param RecoverPasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postRecoverPassword(RecoverPasswordRequest $request)
    {
        $user = User::where('email', '=', $request->email)->where('status', '=', 1)
            ->firstOrFail();
        RecoverPassword::where('user_id', '=', $user->id)->update(['status' => 0]);
        $recovery = RecoverPassword::create(['user_id' => $user->id, 'code' => str_random(16), 'status' => 1]);

        Mail::to($request->email)->send(new RecoveryPasswordMailing($user, $recovery));
        return redirect()->route('workarea.users.login');
    }


    /**
     * @param $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getVerifyCode($code)
    {
        try {
            $recovery = RecoverPassword::where('code', '=', $code)
                ->where('status', '=', 1)->firstOrFail();
        } catch (\Exception $exception) {
            return redirect()->route('workarea.users.login');
        }
        return redirect()->route('workarea.users.password.change')->with('id', $recovery->user_id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getChangePassword(Request $request)
    {
        $user = User::findOrFail($request->session()->get('id'));
        return view('workarea.users.password', ['user' => $user]);
    }


    /**
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postChangePassword(ChangePasswordRequest $request)
    {
        User::where('email', '=', $request->email)->where('id', '=', $request->user)
            ->update(['password' => bcrypt($request->password)]);
        RecoverPassword::where('user_id', '=', $request->user)->update(['status' => 0]);
        return redirect()->route('workarea.users.login');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogout()
    {
        Auth::guard('workarea')->logout();
        return redirect()->route('workarea.users.login');
    }

}
