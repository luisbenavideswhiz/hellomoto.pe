<?php

namespace App\Http\Controllers\Workarea;

class DashboardController extends Controller
{
    public function getIndex()
    {
        return view('workarea.dashboard.index');
    }
}
