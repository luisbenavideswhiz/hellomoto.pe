<?php

namespace App\Http\Controllers\Workarea;

use App\Model\Web\WebCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function getIndex(Request $request)
    {
        $categories = WebCategory::where('category_id', '<>', 0)
            ->where('name','like', '%'.$request->q.'%')
            ->select(\DB::raw('name as text, id'))->get();
        return response()->json(['results'=>$categories]);
    }
}
