<?php

namespace App\Http\Controllers\Workarea;

use App\Http\Requests\Workarea\Post\PostCreateRequest;
use App\Http\Requests\Workarea\Post\PostEditRequest;
use App\Model\Web\WebCategory;
use App\Model\Web\WebGallery;
use App\Model\Web\WebPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $posts = WebPost::where('status','<>',0);
        if($request->has('query')){
            $posts = $posts->where('title','like', '%'.$request->get('query').'%');
            $data['query'] = $request->get('query');
        }
        $posts = $posts->orderBy('created_at','desc')->paginate(20);
        $data['posts'] = $posts;
        $data['appends'] = isset($data['query'])?['query'=>$data['query']]:[];
        return view('workarea.posts.index', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        return view('workarea.posts.create');
    }

    /**
     * @param PostCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(PostCreateRequest $request)
    {
        $dataPost = $request->only(['title','description','tags', 'meta_title', 'meta_description', 'meta_keywords', 'status', 'after_description']);
        $dataPost['slug'] = str_slug($dataPost['title']);
        $dataPost['web_users_id'] = 1;
        $dataPost['banner_img'] = $request->hasFile('banner')?'/storage/'.$request->banner->store('posts/banners', 'public'):'';
        $dataPost['thumb_img'] = $request->hasFile('thumbnail')?'/storage/'.$request->thumbnail->store('posts/thumbs', 'public'):'';
        $dataPost['status'] = 1;
        $post = WebPost::create($dataPost);

        foreach ($request->get('subcategories',[]) as $key => $subCategory){
            $category = WebCategory::find($subCategory);
            $post->webCategories()->attach($subCategory, ['status'=>1, 'web_categories_id_parent'=>$category->category_id]);
        }
        foreach ($request->get('ambassadors',[]) as $ambassador){
            $post->webAmbassadors()->attach($ambassador, ['status'=>1]);
        }
        return response()->json(['status'=>true, 'message' => 'Se creo con exito', 'data'=> ['redirect' => route('workarea.posts.edit', ['id' => $post->id])]]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {
        $post = WebPost::find($id);
        if(is_null($post)){
           return redirect()->route('workarea.posts.index');
        }
        $data = ['post' => $post];
        return view('workarea.posts.edit', $data);
    }

    /**
     * @param $id
     * @param PostEditRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit($id, PostEditRequest $request)
    {
        $dataPost = $request->only(['title','description','tags', 'meta_title', 'meta_description', 'meta_keywords', 'status', 'after_description']);
        if($request->hasFile('banner')){
            $dataPost['banner_img'] = '/storage/'.$request->banner->store('posts/banners', 'public');
        }
        if($request->hasFile('thumbnail')){
            $dataPost['thumb_img'] = '/storage/'.$request->thumbnail->store('posts/thumbs', 'public');
        }
        $post = WebPost::find($id);
        $post->fill($dataPost)->save();

        $post->webCategories()->detach();
        foreach ($request->get('subcategories',[]) as $key => $subCategory){
            $category = WebCategory::find($subCategory);
            $post->webCategories()->attach($subCategory, ['status'=>1, 'web_categories_id_parent'=>$category->category_id]);
        }
        $post->webAmbassadors()->detach();
        foreach ($request->get('ambassadors',[]) as $ambassador){
            $post->webAmbassadors()->attach($ambassador, ['status'=>1]);
        }

        return response()->json(['status'=>true, 'message' => 'Se edito con exito','data'=>[]]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $post = WebPost::find($id);
        $post->delete();
        return response()->json(['status'=>true, 'message' => 'Se ha eliminado con exito!']);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getPublish($id)
    {
        WebPost::where('id', '=',$id)->where('status','=',2)->update(['status' => 1]);
        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getUnpublish($id)
    {
        WebPost::where('id', '=',$id)->where('status','=',1)->update(['status' => 2]);
        return redirect()->back();
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postGallery(Request $request)
    {
        $post = WebPost::find($request->id);
        $files = [];
        foreach ($request->gallery as $file){

            $path = $file->store('gallery/posts/'.$request->id, 'public');
            $gallery = WebGallery::create([
                'status' => 1,
                'filetype' => 'image',
                'title' => File::name('storage/'.$path),
                'path' => 'storage/' . $path
            ]);

            array_push($files,[
                'deleteType' => 'DELETE',
                'deleteUrl' => route('workarea.posts.gallery.delete', ['post'=> $post->id,'gallery' => $gallery->id]),
                'name' => File::name('storage/'.$path),
                'size' => File::size('storage/'.$path),
                'type' => File::mimeType('storage/'.$path),
                'url'=> url('storage/'.$path),
                'thumbnailUrl' => $gallery->thumbnail
            ]);
            $post->webGalleries()->attach($gallery->id,['status'=>1]);
        }
        return response()->json(['files' => $files]);
    }

    /**
     * @param $postId
     * @param $galleryId
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteGallery($postId, $galleryId)
    {
        WebPost::find($postId)->webGalleries()->detach($galleryId);
        $gallery = WebGallery::find($galleryId);
        Storage::delete($gallery->path);
        $gallery->delete();
        return response()->json([]);
    }
}
