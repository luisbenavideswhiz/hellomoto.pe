<?php

namespace App\Http\Controllers\Workarea;

use App\Http\Requests\Workarea\Image\ImageUploaderRequest;
use App\Http\Requests\Workarea\Video\VideoUploaderRequest;
use App\Model\Web\WebGallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class ImageController extends Controller
{
    public function getImageBrowser(Request $request)
    {
        $page = $request->has('page')?$request->page:1;
        $images = WebGallery::where('filetype','=','image')->where('status','=',1)
            ->paginate(100, ['*'], 'page', $page);
        return view('workarea.partials.ckeditor_browser', [
            'images' => $images
        ]);
    }

    public function postImageUploader(ImageUploaderRequest $request)
    {
        $path = $request->hasFile('upload')?'/storage/'.$request->upload->store('posts/content', 'public'):'';
        $gallery = WebGallery::create([
            'status' => 1,
            'filetype' => 'image',
            'title' => File::name($path),
            'path' => $path
        ]);
        return response()->json([
            'uploaded' => $gallery->status,
            'fileName' => $gallery->title,
            'url' => url($gallery->path)
        ]);
    }


    public function postVideoUploader(VideoUploaderRequest $request)
    {
        $path = $request->hasFile('upload')?'/storage/'.$request->upload->store('posts/content', 'public'):'';
        $gallery = WebGallery::create([
            'status' => 1,
            'filetype' => 'video',
            'title' => File::name($path),
            'path' => $path
        ]);

        return response()->json([
            'uploaded' => $gallery->status,
            'fileName' => $gallery->title,
            'url' => url($gallery->path)
        ]);
    }
}
