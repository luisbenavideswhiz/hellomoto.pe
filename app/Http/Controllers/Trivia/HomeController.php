<?php

namespace App\Http\Controllers\Trivia;

use App\Model\Trivia\Option;
use App\Model\Trivia\Question;
use App\Model\Trivia\QuestionsAssigned;
use App\Model\Web\WebUsersHasWebLanding;
use App\Serializers\CustomSerializer;
use App\Transformers\Trivia\QuestionTransformer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        return redirect()->to('/', 301);
        !Auth::check() ?: QuestionsAssigned::user(Auth::id())->delete();
        $data['user'] = Auth::check() ? Auth::id() : 0;
        return view('trivia.index', $data);
    }

    public function question($count)
    {
        $data['assigned'] = QuestionsAssigned::active()->userCount(Auth::id(), $count)->first();
        if (is_null($data['assigned'])) {
            $questions = Question::inRandomOrder()->take(3)->get();
            foreach ($questions as $k => $question) {
                QuestionsAssigned::create(['question_id' => $question->id, 'user_id' => Auth::id(), 'count' => $k + 1]);
            }
            $data['assigned'] = QuestionsAssigned::active()->userCount(Auth::id(), $count)->first();
        }
        $question = $data['assigned']->question;
        $data['question'] = fractal()->item($question, new QuestionTransformer(), false)
            ->serializeWith(new CustomSerializer())->toArray();
        $data['options'] = Option::question($question->id)->get();

        return view('trivia.question', $data);
    }

    public function result()
    {
        $data['questions'] = QuestionsAssigned::with('question')->user(Auth::id())->active()->get();
        $data['hit'] = $data['questions']->sum('hit');

        $isPlay = WebUsersHasWebLanding::where('web_landing_id', '=', 6)
            ->where('web_user_id', '=', Auth::id())->first();

        if(is_null($isPlay)){
            WebUsersHasWebLanding::create([
                'web_landing_id' => 6,
                'web_user_id' => Auth::id(),
                'status'=> 2
            ]);
        }else{
            WebUsersHasWebLanding::where([
                'web_landing_id' => 6,
                'web_user_id' => Auth::id()
            ])->update(['status'=> 2]);
        }
        return view('trivia.result', $data);
    }

    public function finish()
    {
        WebUsersHasWebLanding::updateOrCreate([
            'web_landing_id' => 6,
            'web_user_id' => Auth::id()
        ], ['status'=> 1]);
        QuestionsAssigned::user(Auth::guard('web')->id())->update(['active' => 0]);
        return view('trivia.finish');
    }
}
