<?php

namespace App\Http\Controllers\Trivia;

use App\Http\Requests\Trivia\RegisterRequest;
use App\Http\Requests\Trivia\LoginRequest;
use App\Model\Web\WebUser;
use App\Http\Controllers\Controller;
use App\Http\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    use ResponseTrait;

    public function register(RegisterRequest $request)
    {
        $data = $request->only(['name', 'lastname', 'document_type', 'document_number', 'sex', 'email', 'accept_emails',
            'birthdate', 'phone', 'mobile_operator', 'accept_emails']);
        $data['password'] = bcrypt($request->password);
        $data['age_verify'] = !$request->has('full_age') ?: 1;
        $data['web_users_type_id'] = 'usuario';
        $data['status'] = 1;
        $user = WebUser::create($data);
        Auth::guard('web')->loginUsingId($user->id);

        return $this->responseSuccess('', ['redirect' => url('trivia/pregunta/1')]);
    }

    public function login(LoginRequest $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->has('remember'))) {
            $response = $this->responseSuccess('', ['redirect' => url('trivia/pregunta/1')]);
        } else {
            $response = $this->responseError('', ['errors' => ['email' => trans('validation.login.error')]]);
        }
        return $response;
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/trivia');
    }
}
