<?php

namespace App\Http\Controllers\Trivia;

use App\Model\Trivia\Option;
use App\Model\Trivia\Question;
use App\Model\Trivia\QuestionsAssigned;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OptionController extends Controller
{
    public function toCorrect(Request $request)
    {
        $response = array();

        $option = Option::find($request->resp);
        $response['answer'] = Question::find($option->question_id);

        if($option->correct == 1)
        {
            QuestionsAssigned::active()->userQuestion(Auth::id(), $option->question_id)->update([ 'hit' => 1 ]);
            $response['status'] = true;
        }else
        {
            QuestionsAssigned::active()->userQuestion(Auth::id(), $option->question_id)->update([ 'hit' => 0 ]);
            $response['status'] = false;
        }

        return response()->json($response);
    }
}
