<?php

namespace App\Http\Controllers\Landings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Psy\Exception\ErrorException;

use App\Http\Requests\Landings\ComunidadCreateRequest;
use App\Mail\Landings\ComunidadMailingConfirmation;
use App\Model\Landings\Comunidad;
use App\Model\Landings\Subtable;

class ComunidadController extends Controller
{
    public $campaign = 'landings/comunidad';

    public function index()
    {
        $data['operator'] = Subtable::where('status', 1)
            ->where('tablename', 'OPERATOR')
            ->get();

        return view($this->campaign . '/index', $data);
    }

    public function register(ComunidadCreateRequest $request)
    {
        $data = $request->except(['_token']);

        $data['birthdate'] = $data['year'] . '-' . $data['month'] . '-' . $data['day'];
        unset($data['year']);
        unset($data['month']);
        unset($data['day']);

        $user = new Comunidad($data);
        $user->status = 0;
        $user->confirmation_code = base64_encode($user->email);

        try {
            $user->save();
            Mail::to($user->email)->send(new ComunidadMailingConfirmation($user));
        } catch (ErrorException $e) {
            return response()->json([
                'success' => true,
                'name' => $user->name,
                'email' => $user->email,
                'redirect' => '/comunidad/gracias/' . $user->name,
                'error' => $e->getMessage(),
            ]);
        }

        return response()->json([
            'success' => true,
            'name' => $user->name,
            'email' => $user->email,
            'redirect' => '/comunidad/gracias/' . $user->name,
        ]);
    }

    public function confirmation(Request $request)
    {
        $code = $request->confirmation_code;
        $email = base64_decode($request->confirmation_code);

        $exist = Comunidad::where('email', $email)
            ->where('confirmation_code', $code);

        if ($exist->count() > 0) {

            $user = $exist->first();

            $confirm = 0;
            if ($user->status == 0) {
                $confirm = 1;
                Comunidad::where('email', $email)
                    ->where('status', 0)
                    ->update([
                        'status' => 1
                    ]);
            }

            $data['exist'] = true;

            $row = new \stdClass();
            $row->name = $user->name;
            $row->lastname = $user->lastname;
            $row->confirm = $confirm;

            $data['user'] = $row;

        } else {
            $data['exist'] = false;
        }

        return view($this->campaign . '/confirmation', $data);
    }

    public function gracias(Request $request)
    {
        $data['name'] = $request->name;
        return view($this->campaign . '/gracias', $data);
    }

    public function bienvenido()
    {
        return view($this->campaign . '/mailing/bienvenido');
    }

    public function confirmacion()
    {
        return view($this->campaign . '/mailing/confirmacion');
    }

    public function validate_dni(Request $request)
    {
        $data = $request->all();
        if (Session::token() == $data['_token']) {
            $exist = Comunidad::where('status', 1)
                ->where('document', $data['dni'])
                ->count();
            if ($exist > 0) {
                return response()->json([
                    'success' => false,
                    'error' => [
                        'document' => 'El documento ya ha sido registrado'
                    ]
                ]);
            }else{
                return response()->json([
                    'success' => true,
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'error' => [
                    'document' => 'El token no existe'
                ]
            ]);
        }
    }

    public function validate_email(Request $request)
    {
        $data = $request->all();
        if (Session::token() == $data['_token']) {
            $exist = Comunidad::where('status', 1)
                ->where('email', $data['email'])
                ->count();
            if ($exist > 0) {
                return response()->json([
                    'success' => false,
                    'error' => [
                        'email' => 'El Email ya ha sido registrado'
                    ]
                ]);
            }else{
                return response()->json([
                    'success' => true,
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'error' => [
                    'email' => 'El token no existe'
                ]
            ]);
        }
    }

    public function fin()
    {
        return view('landings/comunidad/fin');
    }
    public function finalizacion()
    {
        return view($this->campaign . '/mailing/finalizacion');
    }
}
