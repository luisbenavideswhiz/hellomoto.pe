<?php

namespace App\Http\Controllers\Landings;

use App\Http\Requests\Landings\ExtraOptionsRequest;
use App\Http\Requests\Landings\UneteRegisterRequest;
use App\Model\Web\WebLanding;
use App\Model\Web\WebUbigeo;
use App\Model\Web\WebUser;
use App\Model\Web\WebUsersHasOptionsLanding;
use App\Model\Web\WebUsersHasWebLanding;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MotogiftController extends Controller
{

    const MOTOGIFT_ID = 3;

    public function index()
    {
        $data['departments'] = WebUbigeo::select('department')->groupBy('department')->get();
        $data['landing'] = WebLanding::find(self::MOTOGIFT_ID);

        if($data['landing']->status==0){
            $view = view('landings/motogift/finish');
        }else{
            if(session()->get('logged',false)){
                $data['user'] = session()->get('userdata');
                $view = view('landings/motogift/logged', $data);
            }else{
                $view = view('landings/motogift/not-logged', $data);
            }
        }
        return $view;
    }

    /**
     * @param UneteRegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(UneteRegisterRequest $request)
    {
        $data = $request->only(['name', 'lastname', 'document_number', 'sex',
            'birthdate', 'email', 'phone', 'mobile_operator','password', 'accept_emails']);
        if($request->has('full_age')){
            $data['age_verify'] = 1;
        }
        $data['web_users_type_id'] = 'usuario';
        $data['status'] = 1;
        $user = WebUser::create($data);
        WebUsersHasWebLanding::create(['web_user_id' => $user->id, 'web_landing_id'=>self::MOTOGIFT_ID, 'status'=>1]);
        session()->put('logged',true);

        session()->put('userdata', $user->toArray());
        return response()->json(['success'=> true, 'redirect'=> url('/sorteos/motogift')]);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function welcome()
    {
        return view('landings/motogift/success');
    }

    /**
     * @param ExtraOptionsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveExtraOptions(ExtraOptionsRequest $request)
    {
        if(session()->get('logged',false)){
            $user = session()->get('userdata');

            WebUsersHasWebLanding::updateOrCreate([
                'web_landing_id' => self::MOTOGIFT_ID,
                'web_user_id' => $user['id']
            ], ['status'=> 1]);

            WebUsersHasOptionsLanding::updateOrCreate([
                'web_landing_id' => self::MOTOGIFT_ID,
                'web_user_id' => $user['id'],
                'key' => 'ticket'
            ], [
                'value' => $request->ticket
            ]);

            WebUsersHasOptionsLanding::updateOrCreate([
                'web_landing_id' => self::MOTOGIFT_ID,
                'web_user_id' => $user['id'],
                'key' => 'master_class'
            ], [
                'value' => $request->master_class
            ]);
            $redirect = url('/sorteos/motogift/bienvenido');
        }else{
            $redirect = url('/');
        }
        return response()->json(['success'=> true, 'redirect'=> $redirect]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function disclaimer()
    {
        return view('landings/motogift/disclaimer');
    }
}
