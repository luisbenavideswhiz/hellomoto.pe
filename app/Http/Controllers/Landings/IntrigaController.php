<?php

namespace App\Http\Controllers\Landings;

use App\Http\Requests\Landings\IntrigaRegisterRequest;
use App\Http\Controllers\Controller;
use App\Model\Landings\Intriga;

class IntrigaController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('landings/intriga/index');
    }

    /**
     * @param IntrigaRegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postRegister(IntrigaRegisterRequest $request)
    {
        Intriga::create($request->only(['email','name']));
        return response()->json(['status' => true, 'message' => 'Se ha registrado con exito!']);
    }
}
