<?php

namespace App\Http\Controllers\Landings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Psy\Exception\ErrorException;

use App\Model\Landings\DimitriVegas;
use App\Http\Requests\Landings\DimitriVegasCreateRequest;
use App\Mail\Landings\DimitriVegasMailingConfirmation;
use App\Model\Landings\Subtable;

class DimitriVegasController extends Controller
{
    public $campaign = 'landings/dimitrivegas';

    public function index()
    {
        $data['operator'] = Subtable::where('status', 1)
            ->where('tablename', 'OPERATOR')
            ->get();

        return view($this->campaign . '/index', $data);
    }

    public function register(DimitriVegasCreateRequest $request)
    {
        $data = $request->except(['_token']);

        $data['birthdate'] = $data['year'] . '-' . $data['month'] . '-' . $data['day'];
        unset($data['year']);
        unset($data['month']);
        unset($data['day']);

        $user = new DimitriVegas($data);
        $user->status = 0;
        $user->confirmation_code = base64_encode($user->email);

        try {
            $user->save();
            Mail::to($user->email)->send(new DimitriVegasMailingConfirmation($user));
        } catch (ErrorException $e) {
            return response()->json([
                'success' => true,
                'name' => $user->name,
                'email' => $user->email,
                'error' => $e->getMessage(),
            ]);
        }

        return response()->json([
            'success' => true,
            'name' => $user->name,
            'email' => $user->email,
        ]);
    }

    public function confirmation(Request $request)
    {
        $code = $request->confirmation_code;
        $email = base64_decode($request->confirmation_code);

        $exist = DimitriVegas::where('email', $email)
            ->where('confirmation_code', $code);

        if ($exist->count() > 0) {

            $user = $exist->first();

            $confirm = 0;
            if ($user->status == 0) {
                $confirm = 1;
                DimitriVegas::where('email', $email)
                    ->where('status', 0)
                    ->update([
                        'status' => 1
                    ]);
            }

            $data['exist'] = true;

            $row = new \stdClass();
            $row->name = $user->name;
            $row->lastname = $user->lastname;
            $row->confirm = $confirm;

            $data['user'] = $row;

        } else {
            $data['exist'] = false;
        }

        return view($this->campaign . '/confirmation', $data);
    }

    public function finish()
    {
        return view($this->campaign . '/finish');
    }
    public function mailing_finish()
    {
        return view($this->campaign . '/emails/mail-finish');
    }
}
