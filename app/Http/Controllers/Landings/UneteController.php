<?php

namespace App\Http\Controllers\Landings;

use App\Http\Requests\Landings\UneteRegisterRequest;
use App\Model\Landings\Unete;
use App\Model\Web\WebLanding;
use App\Model\Web\WebUbigeo;
use App\Model\Web\WebUser;
use App\Model\Web\WebUsersHasWebLanding;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\RequestMatcher;

class UneteController extends Controller
{
    public function index()
    {
        $data['departments'] = WebUbigeo::select('department')->groupBy('department')->get();
        $data['landing'] = WebLanding::find(1);
        if(session()->get('logged',false)){
            $data['user'] = session()->get('userdata');
            $view = view('landings/unete/logged', $data);
        }else{
            $view = view('landings/unete/index', $data);
        }
        //$view = view('landings/unete/finish');
        return $view;
    }

    public function welcome()
    {
        if(session()->get('logged',false)){
            $user = session()->get('userdata');
            WebUsersHasWebLanding::updateOrCreate(['web_landing_id'=>1, 'web_user_id' => $user['id']],['status'=>1]);
        }else{
            $user = null;
        }
        return view('landings/unete/success',['user'=> $user ]);
    }

    public function register(UneteRegisterRequest $request)
    {
        $data = $request->only(['name', 'lastname', 'document_number', 'sex',
            'birthdate', 'email', 'phone', 'mobile_operator','password', 'accept_emails']);
        if($request->has('full_age')){
            $data['age_verify'] = 1;
        }
        $data['web_users_type_id'] = 'usuario';
        $data['status'] = '1';
        $user = WebUser::create($data);
        WebUsersHasWebLanding::create(['web_user_id' => $user->id, 'web_landing_id'=>1, 'status'=>1]);
        return response()->json(['success'=>true, 'redirect'=>url('/sorteos/moto-z-play-motomods-jbl-soundboost/bienvenido')]);
    }

    public function ubigeo(Request $request)
    {
        if($request->has('department')){
           $data = WebUbigeo::where('department','=', $request->department)
               ->groupBy('province')
               ->select('province as data')
               ->get();
        }
        if($request->has('province')){
            $data = WebUbigeo::where('province','=', $request->province)
                ->groupBy('district')
                ->select('district as data')
                ->get();
        }
        $response = is_null($data)?[]:array_map(function($data){ return $data['data']; }, $data->toArray());
        return response()->json(['data'=>$response]);
    }
}
