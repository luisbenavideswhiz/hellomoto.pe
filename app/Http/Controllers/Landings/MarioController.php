<?php

namespace App\Http\Controllers\Landings;

use App\Http\Controllers\Controller;
use App\Http\Requests\Landings\ExtraOptionsRequest;
use App\Http\Requests\Landings\UneteRegisterRequest;
use App\Http\Traits\ResponseTrait;
use App\Model\Web\WebLanding;
use App\Model\Web\WebUbigeo;
use App\Model\Web\WebUser;
use App\Model\Web\WebUsersHasOptionsLanding;
use App\Model\Web\WebUsersHasWebLanding;
use Illuminate\Support\Facades\Auth;

class MarioController extends Controller
{
    use ResponseTrait;
    const MARIO_ID = 5;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex() {
        $data['landing'] = WebLanding::find(self::MARIO_ID);
        if($data['landing']->status==0){
            $view = view('landings.mario.finish');
        }else {
            if(Auth::guard('web')->check()){
                $view =  view('landings.mario.logged', $data);
            }else{
                $view = view('landings.mario.index', $data);
            }
            $data['departments'] = WebUbigeo::select('department')
                ->groupBy('department')->get();
        }
        return $view;
    }

    /**
     * @param UneteRegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postRegister(UneteRegisterRequest $request){
        $data = $request->only(['name', 'lastname', 'document_number', 'sex',
            'birthdate', 'email', 'phone', 'mobile_operator', 'accept_emails']);
        $data['password'] = bcrypt($request->password);
        if($request->has('full_age')){
            $data['age_verify'] = 1;
        }
        $data['web_users_type_id'] = 'usuario';
        $data['status'] = 1;
        $user = WebUser::create($data);
        WebUsersHasWebLanding::create(['web_user_id' => $user->id, 'web_landing_id'=>self::MARIO_ID, 'status'=>1]);
        Auth::guard('web')->loginUsingId($user->id);
        return $this->responseSuccess('',['redirect'=> url('/sorteos/mario')]);
    }

    /**
     * @param ExtraOptionsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postWelcome(ExtraOptionsRequest $request)
    {
        if(Auth::guard('web')->check()){
            WebUsersHasWebLanding::updateOrCreate([
                'web_landing_id' => self::MARIO_ID,
                'web_user_id' => Auth::guard('web')->id()
            ], ['status'=> 1]);

            WebUsersHasOptionsLanding::updateOrCreate([
                'web_landing_id' => self::MARIO_ID,
                'web_user_id' => Auth::guard('web')->id(),
                'key' => 'ticket'
            ], [
                'value' => $request->ticket
            ]);

            $redirect = url('sorteos/mario/exito');
        }else{
            $redirect = url('/');
        }
        return $this->responseSuccess('', ['redirect'=> $redirect]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSuccess()
    {
        return view('landings.mario.success');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDisclaimer()
    {
        return view('landings.mario.disclaimer');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getClosed() {
        return view('landings.mario.finish');
    }
}
