<?php

namespace App\Http\Controllers\Seeker;

use App\Http\Requests\Seeker\SearchForRequest;
use App\Model\Landings\Comunidad;
use App\Model\Landings\DimitriVegas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class IndexController extends Controller
{
    public function home()
    {
        return view('seeker/index');
    }

    public function search_for(SearchForRequest $request)
    {
        $data = $request->except('_token');

        switch ($data['landing']) {
            case 1:
                $row = DimitriVegas::where('status', 1)
                    ->where('name', 'like', $data['name'] . '%')
                    ->where('lastname', 'like', $data['lastname'] . '%');
                break;
            case 2:
                $row = Comunidad::where('status', 1)
                    ->where('name', 'like', $data['name'] . '%')
                    ->where('lastname', 'like', $data['lastname'] . '%');
                break;
            default:
                $response = [
                    'html' => '
                    <div class="alert alert-danger" role="alert">
                        Opci�n no encontrada
                    </div>
                '];
                return response()->json($response);
                break;
        }

        if ($row->count() == 1) {
            $user = $row->first();
            $response = [
                'html' => '
                <div class="alert alert-success" role="alert">
                    Usuario encontrado, <a class="alert-link">' . $user->name . ' ' . $user->lastname . '</a>
                </div>
            '];
        } else if ($row->count() > 1) {
            $html = '';
            $users = $row->get();
            foreach ($users as $key => $value) {
                $html = $html . '
                    <div class="alert alert-success" role="alert">
                        Usuario encontrado, <a class="alert-link">' . $value->name . ' ' . $value->lastname . '</a>
                    </div>
                ';
            }
            $response = [
                'html' => $html
            ];
        } else {
            $response = [
                'html' => '
                <div class="alert alert-warning" role="alert">
                    Usuario no encontrado, <a class="alert-link">' . $data['name'] . ' ' . $data['lastname'] . '</a>
                </div>
            '];
        }
        return response()->json($response);
    }
}
