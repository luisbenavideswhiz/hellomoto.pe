<?php

namespace App\Http\Requests\Workarea\Trivia;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class TriviaCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question' => 'required|string',
            'correct_description' => 'required',
            'error_description' => 'required',
            'answer' => 'required',
            'options' => 'required|array',
            'options.answer' => 'required|array|min:1',
            'options.answer.*' => 'required'
        ];
    }

    public function response(array $errors)
    {
        if ($this->expectsJson()) {
            return new JsonResponse([
                'status' => false,
                'data' => $errors
            ], 200);
        }
    }
}
