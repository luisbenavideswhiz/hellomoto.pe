<?php

namespace App\Http\Requests\Workarea\Post;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class PostCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'tags' => 'required',
            'banner' => 'image',
            'thumbnail' => 'image',
            'ambassadors' => 'array',
            'subcategories' => 'required|array|min:1',
        ];
    }


    public function attributes()
    {
        return [
            'title' => 'Título',
            'description' => 'Descripción',
            'tags' => 'Etiquetas',
            'banner' => 'Imagen banner',
            'thumbnail' => 'Imagen miniatura',
            'ambassadors' => 'Embajadores',
            'subcategories' => 'Subcategorias'
        ];
    }

    public function response(array $errors)
    {
        if ($this->expectsJson()) {
            return new JsonResponse([
                'status' => false,
                'data' => $errors
            ], 200);
        }
    }
}
