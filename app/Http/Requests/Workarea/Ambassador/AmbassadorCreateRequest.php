<?php

namespace App\Http\Requests\Workarea\Ambassador;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class AmbassadorCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'city' => 'required',
            'image' => 'required|image',
            'profile_video' => 'string|nullable',
            'tags' => 'string',
            'status' => 'in:1,2',
            'web_colors_id' => 'required|exists:web_colors,id',
            'web_career_id' => 'required',
            'playlist_url' => 'required|array|min:1',
            'playlist_url.*' => 'url',
            'playlist_title' => 'required|array|min:1',
            'playlist_title.*' => 'string',
            'social_network_type' => 'required|array|min:1',
            'social_network_type.*' => 'numeric',
            'position' => 'required|numeric'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nombre',
            'city' => 'ciudad',
            'image' => 'imagen',
            'profile_video' => 'codigo video',
            'tags' => 'etiquetas',
            'status' => 'estado',
            'web_colors_id' => 'color',
            'web_career_id' => 'profesion/oficio',
            'playlist_url.*' => 'url de playlist',
            'playlist_title.*' => 'titulo de playlist',
            'social_network_type.*' => 'red social',
            'position' => 'posición'
        ];
    }

    public function response(array $errors)
    {
        if ($this->expectsJson()) {
            return new JsonResponse([
                'status' => false,
                'data' => $errors
            ], 200);
        }
    }
}
