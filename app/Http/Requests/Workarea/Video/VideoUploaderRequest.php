<?php

namespace App\Http\Requests\Workarea\Video;

use App\Http\Traits\ResponseTrait;
use Illuminate\Foundation\Http\FormRequest;

class VideoUploaderRequest extends FormRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'upload' => 'required|file|max:50000|mimes:mp4,ogx,oga,ogv,ogg,webm'
        ];
    }

    public function attributes()
    {
        return [
            'upload' => 'Video',
        ];
    }

    public function response(array $errors)
    {
        return $this->responseError('', $errors);
    }
}
