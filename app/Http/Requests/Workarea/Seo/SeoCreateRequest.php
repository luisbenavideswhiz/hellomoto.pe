<?php

namespace App\Http\Requests\Workarea\Seo;

use Illuminate\Foundation\Http\FormRequest;

class SeoCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:web_seo,name',
            'title' => 'required|min:2',
            'alt' => '',
            'description' => 'required|min:2',
            'image' => '',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Sección de la web es requerido',
            'name.unique' => 'Esta sección de la web ya está en uso',
            'title.required' => 'Título es requerido',
            'title.min' => 'Título inválido',
            'alt.required' => 'alt es requerido',
            'description.required' => 'Descripción es requerido',
            'description.min' => 'Descripción inválida',
            'image' => '',
        ];
    }
}
