<?php

namespace App\Http\Requests\Trivia;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|alpha_spaces',
            'lastname' => 'required|alpha_spaces',
            'document_number' => 'required|digits:8',
            'sex' => 'required|alpha_no_spaces|in:hombre,mujer',
            'email' => 'required|email|unique:web_users',
            'birthdate' => 'required|date',
            'full_age' => 'required|accepted',
            'phone' => 'required|unique:web_users|digits:9',
            'mobile_operator' => 'required|alpha_no_spaces',
            'accept_emails' => 'boolean',
            'password' => 'required|same:password_confirm',
            'terms_conditions' => 'required|accepted'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nombre',
            'lastname' => 'apellido',
            'sex' => 'g&eacute;nero',
            'email' => 'email',
            'birthdate' => 'fecha de nacimiento',
            'accept_email' => 'Aceptar correos electronicos',
            'full_age' => 'Verificaci&oacute;n de edad',
            'phone' => 'celular',
            'document_number' => 'nº de documento',
            'mobile_operator' => 'operador',
            'terms_conditions' => 'términos y condiciones',
            'password' => 'contrase&ntilde;a',
            'password_confirm' => 'confirmar contrase&ntilde;a',
        ];
    }
}
