<?php

namespace App\Http\Requests\Landings;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\JsonResponse;

class DimitriVegasCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'lastname' => 'required',
            'gender_id' => 'required',
            'email' => 'required|email|unique:landings_dimitrivegas',
            'day' => 'required',
            'month' => 'required',
            'year' => 'required',
            'accept_email' => 'required',
            'age_verification' => 'required',
            'phone' => 'required|min:6|max:9',
            'document' => 'required|unique:landings_dimitrivegas|min:8|max:8',
            'operator_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute es requerido',

            'lastname.required' => ':attribute es requerido',

            'gender_id.required' => ':attribute es requerido',

            'email.required' => ':attribute es requerido',
            'email.email' => ':attribute no v&aacute;lido',
            'email.unique' => ':attribute ya registrado',

            'day.required' => ':attribute es requerido',
            'month.required' => ':attribute es requerido',
            'year.required' => ':attribute es requerido',

            'accept_email.required' => ':attribute es requerido',

            'age_verification.required' => ':attribute es requerido',

            'phone.required' => ':attribute es requerido',
            'phone.min' => 'El :attribute debe tener m&iacute;nimo 6 caract&eacute;res',
            'phone.max' => 'El :attribute debe tener m&aacute;ximo 9 caract&eacute;res',

            'document.required' => ':attribute es requerido',
            'document.min' => 'El :attribute debe tener 8 caract&eacute;res',
            'document.max' => 'El :attribute debe tener 8 caract&eacute;res',
            'document.unique' => 'El :attribute ya ha sido registrado',

            'operator_id.required' => ':attribute es requerido',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'lastname' => 'Apellido',
            'gender_id' => 'G&eacute;nero',
            'email' => 'Email',
            'day' => 'D&iacute;a',
            'month' => 'Mes',
            'year' => 'A&ntilde;o',
            'accept_email' => 'Aceptar correos electronicos',
            'age_verification' => 'Verificaci&oacute;n de edad',
            'phone' => 'Celular',
            'document' => 'DNI',
            'operator_id' => 'Operador',
        ];
    }

}
