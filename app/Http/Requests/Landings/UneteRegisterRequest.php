<?php

namespace App\Http\Requests\Landings;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class UneteRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|alpha_spaces',
            'lastname' => 'required|alpha_spaces',
            'document_number' => 'required|digits:8',
            'sex' => 'required|alpha_no_spaces|in:hombre,mujer',
            'email' => 'required|email|unique:web_users',
            'department' => 'required',
            'province' => 'required',
            'district' => 'required',
            'birthdate' => 'required|date',
            'full_age' => 'required|accepted',
            'phone' => 'required|unique:web_users|digits:9',
            'mobile_operator' => 'required|alpha_no_spaces',
            'accept_emails' => 'boolean',
            'password' => 'required|same:repassword',
            'terms_conditions' => 'required|accepted'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'lastname' => 'Apellido',
            'sex' => 'G&eacute;nero',
            'email' => 'Email',
            'birthdate' => 'Fecha de nacimiento',
            'accept_email' => 'Aceptar correos electronicos',
            'full_age' => 'Verificaci&oacute;n de edad',
            'phone' => 'Celular',
            'document_number' => 'DNI',
            'mobile_operator' => 'Operador',
            'terms_conditions' => 'Términos y condiciones'
        ];
    }

}
