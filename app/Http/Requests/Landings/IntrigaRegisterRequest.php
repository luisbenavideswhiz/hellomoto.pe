<?php

namespace App\Http\Requests\Landings;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class IntrigaRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3',
            'email' => 'required|email|unique:landings_intriga'
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => 'nombre',
            'email' => 'email'
        ];
    }


}
