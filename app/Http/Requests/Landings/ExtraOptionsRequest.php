<?php

namespace App\Http\Requests\Landings;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ExtraOptionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'landing' => 'required|numeric',
            'ticket' => [
                'required',
                Rule::unique('web_users_has_options_landings', 'value')->where(function($query) use ($request){
                   return $query->where('web_landing_id','=',$request->landing)->where('key','=', 'ticket');
                })
            ]
        ];
    }

}
