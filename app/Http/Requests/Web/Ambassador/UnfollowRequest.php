<?php

namespace App\Http\Requests\Web\Ambassador;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class UnfollowRequest extends FormRequest
{
    protected $alpha_no_spaces = '/^[a-zA-Z]+$/u';
    protected $alpha_spaces = '/^[a-zA-Z ]+$/u';
    protected $only_numbers = '/^[0-9]+$/u';
    protected $email = '/^[a-zA-Z0-9@_.-]+$/u';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'referal_id' => 'required|regex:' . $this->only_numbers,
        ];
    }

    public function messages()
    {
        return [
            'referal_id.required' => ':attribute es requerido',
            'referal_id.regex' => ':attribute debe contener solo n&uacute;meros',
        ];
    }

    public function attributes()
    {
        return [
            'referal_id' => 'ID de Embajador',
        ];
    }
}
