<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class CreateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|alpha_spaces',
            'lastname' => 'required|alpha_spaces',
            'document_type' => 'required|alpha_no_spaces',
            'document_number' => 'required|digits:8|only_numbers',
            'sex' => 'required|alpha_no_spaces',
            'email' => 'required|email|unique:web_users|email_format',
            'accept_emails' => 'required|only_numbers',
            'birthdate' => 'required|date_format:Y-m-d',
            'phone' => 'required|unique:web_users|only_numbers',
            'mobile_operator' => 'required|alpha_no_spaces',
            'password' => 'required|same:password_confirmation',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'lastname' => 'Apellido',
            'document_type' => 'Tipo de documento',
            'document_number' => 'N&uacute;mero de documento',
            'sex' => 'Sexo',
            'email' => 'Email',
            'accept_emails' => 'Aceptar correos electr&oacute;nicos',
            'phone' => 'N&uacute;mero de celular',
            'mobile_operator' => 'Operador',
            'password' => 'Contrase&ntilde;a',
            'birthdate' => 'Fecha de nacimiento'
        ];
    }

}
