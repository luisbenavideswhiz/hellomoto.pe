<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class UploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profile_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'profile_img.required' => ':attribute es requerido',
            'profile_img.image' => ':attribute debe ser imagen',
            'profile_img.mimes' => ':attribute debe ser de tipo jpeg, png, jpg, gif o svg',
            'profile_img.max' => ':attribute debe tener máximo 2mb',
        ];
    }

    public function attributes()
    {
        return [
            'profile_img' => 'Imagen',
        ];
    }

}
