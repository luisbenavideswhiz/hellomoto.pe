<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class RecoveryPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|email_format',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => ':attribute es requerido',
            'email.email' => ':attribute debe ser válido',
            'email.email_format' => ':attribute debe ser válido',
        ];
    }

    public function attributes()
    {
        return [
            'email' => 'Email',
        ];
    }

}
