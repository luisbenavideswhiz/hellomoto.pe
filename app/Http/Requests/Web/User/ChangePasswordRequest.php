<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|same:password_confirmation',
            'code' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'password.required' => ':attribute es requerido',
            'password.same' => ':attribute no coinciden',

            'code.required' => ':attribute es requerido',
        ];
    }

    public function attributes()
    {
        return [
            'password' => utf8_encode('Contraseña'),
            'code' => utf8_encode('Código'),
        ];
    }


}
