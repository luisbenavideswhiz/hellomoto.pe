<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class UpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|alpha_spaces',
            'lastname' => 'required|alpha_spaces',
//            'document_type' => 'required|alpha_no_spaces',
            'document_number' => 'required|digits:8|only_numbers',
            'sex' => 'required|alpha_no_spaces',
            'email' => 'required|email|email_format',
//            'accept_emails' => 'required|only_numbers',
            'birthdate' => 'required',
//            'day' => 'required|only_numbers',
//            'month' => 'required|only_numbers',
//            'year' => 'required|only_numbers',
//            'age_verify' => 'required|only_numbers',
            'phone' => 'required|only_numbers',
            'mobile_operator' => 'required|alpha_no_spaces',
//            'password' => 'required|same:password_confirmation',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute es requerido',
            'name.alpha_spaces' => ':attribute debe contener solo letras',

            'lastname.required' => ':attribute es requerido',
            'lastname.alpha_spaces' => ':attribute debe contener solo letras',

            'document_type.required' => ':attribute es requerido',
            'document_type.alpha_no_spaces' => ':attribute debe contener solo letras',

            'document_number.required' => ':attribute es requerido',
            'document_number.digits' => ':attribute debe tener 8 digitos',
            'document_number.only_numbers' => ':attribute debe contener solo n&uacute;meros',

            'sex.required' => ':attribute es requerido',
            'sex.alpha_no_spaces' => ':attribute debe contener solo letras',

            'email.required' => ':attribute es requerido',
            'email.unique' => ':attribute ya esta registrado',
            'email.email' => ':attribute debe ser un email v&aacute;lido',
            'email.email_format' => ':attribute debe ser un email v&aacute;lido',

            'accept_emails.required' => ':attribute es requerido',
            'accept_emails.only_numbers' => ':attribute debe contener solo n&uacute;meros',

            'birthdate.required' => ':attribute es requerido',

            'day.required' => ':attribute es requerido',
            'day.only_numbers' => ':attribute debe contener solo n&uacute;meros',

            'month.required' => ':attribute es requerido',
            'month.only_numbers' => ':attribute debe contener solo n&uacute;meros',

            'year.required' => ':attribute es requerido',
            'year.only_numbers' => ':attribute debe contener solo n&uacute;meros',

            'age_verify.required' => ':attribute es requerido',
            'age_verify.only_numbers' => ':attribute debe contener solo n&uacute;meros',

            'phone.required' => ':attribute es requerido',
            'phone.unique' => ':attribute ya esta regitrado',
            'phone.only_numbers' => ':attribute debe contener solo n&uacute;meros',

            'mobile_operator.required' => ':attribute es requerido',
            'mobile_operator.alpha_no_spaces' => ':attribute debe contener letras',

            'password.required' => ':attribute es requerido',
            'password.same' => ':attribute no coinciden',

        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'lastname' => 'Apellido',
            'document_type' => 'Tipo de documento',
            'document_number' => 'N&uacute;mero de documento',
            'sex' => 'Sexo',
            'email' => 'Email',
            'accept_emails' => 'Aceptar correos electr&oacute;nicos',
            'birthdate' => 'Cumplea&ntilde;os',
            'day' => 'D&iacute;a',
            'month' => 'Mes',
            'year' => 'A&ntilde;o',
            'age_verify' => 'Verificacion de edad',
            'phone' => 'N&uacute;mero de celular',
            'mobile_operator' => 'Operador',
            'password' => 'Contrase&ntilde;a',
        ];
    }

}
