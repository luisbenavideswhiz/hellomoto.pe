<?php

namespace App\Http\Requests\Motovan;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:motovan_users,email',
            'document_number' => 'required|string|digits:8|exists:motovan_users,document_number'
        ];
    }

    public function attributes()
    {
        return [
            'email' => 'Email',
            'document_number' => 'Numero de DNI'
        ];
    }
}
