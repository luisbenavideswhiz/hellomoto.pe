<?php

namespace App\Http\Requests\Motovan;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:motovan_users,email',
            'name' => 'required|string',
            'last_name' => 'required|string',
            'document_number' => 'required|digits:8|unique:motovan_users,document_number',
            'area' => 'required|string',
            'phone' => 'required|numeric|min:7',
            'birthdate' => 'required|date_format:"Y-m-d"',
            'terms_conditions' => 'required|boolean'
        ];
    }


    public function attributes()
    {
        return [
            'email' => 'correo electronico',
            'name' => 'nombre',
            'last_name' => 'apellidos',
            'document_number' => 'DNI',
            'area' => 'area',
            'phone' => 'celular',
            'birthdate' => 'fecha de nacimiento',
            'terms_conditions' => 'términos y condiciones'
        ];
    }
}
