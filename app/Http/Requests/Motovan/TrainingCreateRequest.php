<?php

namespace App\Http\Requests\Motovan;

use Illuminate\Foundation\Http\FormRequest;

class TrainingCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'total_coupons' => 'required|numeric',
            'location' => 'required|string',
            'date' => 'required|date_format:d/m/Y H:i:s'
        ];
    }

    public function attributes()
    {
        return [
            'total_coupons' => 'Total de Cupones',
            'location' => 'Ubicación',
            'date' => 'Fecha'
        ];
    }
}
