<?php

namespace App\Http\Requests\Seeker;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class SearchForRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'lastname' => 'required',
            'landing' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute es requerido',
            'lastname.required' => ':attribute es requerido',
            'landing.required' => ':attribute es requerido',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'lastname' => 'Apellido',
            'landing' => 'Landing',
        ];
    }

    public function response(array $errors)
    {
        $errors['success'] = false;
        if ($this->expectsJson()) {
            return new JsonResponse($errors, 200);
        }
    }
}
