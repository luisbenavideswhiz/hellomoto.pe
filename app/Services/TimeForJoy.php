<?php

namespace App\Services;

use PHPHtmlParser\Dom;

class TimeForJoy
{
    private $url;

    public function __construct()
    {
        $this->dom = app()->make(DOM::class);
        $this->url = 'https://www.mototimeforjoy.com';
    }

    public function count()
    {
        $this->dom->load($this->url);
        return $this->dom->find('.startnumber')->value;
    }

}
