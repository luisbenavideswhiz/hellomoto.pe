<?php

namespace App\Exceptions;

use App\Http\Traits\ResponseTrait;
use Exception;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    use ResponseTrait;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * @param Exception $exception
     * @return mixed|void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->expectsJson() || $request->ajax()) {
            switch (get_class($exception)) {
                case ValidationException::class:
                    if ($request->ajax()) {
                        $response = $this->responseError('validation_error', ['errors'=>$exception->validator->errors()], $exception, false);
                    } else {
                        $response = $this->responseError('validation_error', ['errors'=>$exception->validator->errors()], $exception, false, 422);
                    }
                    break;
                case AuthenticationException::class:
                    $response = $this->responseError(trans('auth.unauthenticated'), null, $exception);
                    break;
                case RequestException::class:
                    $response = $this->responseError($exception->getMessage(), null, $exception, true);
                    break;
                default:
                    $response = $this->responseError($exception->getMessage(), null, $exception);
                    break;
            }
        } else {
            $response = parent::render($request, $exception);
        }
        return $response;
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Illuminate\Auth\AuthenticationException $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        $route = '/';
        foreach ($request->route()->computedMiddleware as $auth) {
            if ($auth == 'auth:web' || $auth == 'auth') {
                $route = '/?modal=login';
            }
            if ($auth == 'auth:motovan') {
                $route = '/motovan';
            }
            if ($auth == 'auth:workarea') {
                $route = route('workarea.users.login');
            }
        }
        if ($request->expectsJson()) {
            return $this->responseError('Unauthenticated.', ['redirect' => $route]);
        }
        return redirect()->guest($route);
    }
}
