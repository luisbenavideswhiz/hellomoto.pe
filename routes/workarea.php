<?php

Route::group(['middleware' => 'auth:workarea'], function () {

    Route::get('/dashboard', 'Workarea\DashboardController@getIndex')->name('dashboard.index');

    Route::group(['middleware' => 'role:admin'], function () {

        Route::get('profile', 'Workarea\ProfileController@index');
        Route::post('profile/update', 'Workarea\ProfileController@update');
        Route::post('password/update', 'Workarea\ProfileController@updatePassword');

        Route::post('posts/gallery', 'Workarea\PostController@postGallery');
        Route::delete('posts/{post}/gallery/{gallery}', 'Workarea\PostController@deleteGallery')->name('posts.gallery.delete');

        Route::get('posts', 'Workarea\PostController@getIndex')->name('posts.index');
        Route::get('posts/create', 'Workarea\PostController@getCreate')->name('posts.create');
        Route::post('posts/create', 'Workarea\PostController@postCreate');
        Route::get('posts/{id}', 'Workarea\PostController@getEdit')->name('posts.edit');
        Route::patch('posts/{id}', 'Workarea\PostController@postEdit');
        Route::delete('posts/{id}', 'Workarea\PostController@delete')->name('posts.delete');
        Route::get('posts/{id}/unpublish', 'Workarea\PostController@getUnpublish')->name('posts.unpublish');
        Route::get('posts/{id}/publish', 'Workarea\PostController@getPublish')->name('posts.publish');

        Route::post('ambassadors/gallery', 'Workarea\AmbassadorController@postGallery');
        Route::delete('ambassadors/{ambassador}/gallery/{gallery}', 'Workarea\AmbassadorController@deleteGallery')->name('ambassadors.gallery.delete');

        Route::get('ambassadors', 'Workarea\AmbassadorController@getIndex')->name('ambassadors.index');
        Route::get('ambassadors/list', 'Workarea\AmbassadorController@getList')->name('ambassadors.list');
        Route::get('ambassadors/create', 'Workarea\AmbassadorController@getCreate')->name('ambassadors.create');
        Route::post('ambassadors/create', 'Workarea\AmbassadorController@postCreate');
        Route::get('ambassadors/{id}', 'Workarea\AmbassadorController@getEdit')->name('ambassadors.edit');
        Route::patch('ambassadors/{id}', 'Workarea\AmbassadorController@postEdit');
        Route::delete('ambassadors/{id}', 'Workarea\AmbassadorController@delete')->name('ambassadors.delete');

        Route::delete('playlists/{id}', 'Workarea\AmbassadorController@deletePlaylist')->name('playlists.delete');

        Route::get('promotions', 'Workarea\PromotionController@getIndex')->name('promotions.index');
        Route::get('promotions/create', 'Workarea\PromotionController@getCreate')->name('promotions.create');
        Route::post('promotions/create', 'Workarea\PromotionController@postCreate');
        Route::get('promotions/{id}', 'Workarea\PromotionController@getEdit')->name('promotions.edit');
        Route::patch('promotions/{id}', 'Workarea\PromotionController@postEdit');
        Route::delete('promotions/{id}', 'Workarea\PromotionController@delete')->name('promotions.delete');
        Route::get('promotions/{id}/unpublish', 'Workarea\PromotionController@getUnpublish')->name('promotions.unpublish');
        Route::get('promotions/{id}/publish', 'Workarea\PromotionController@getPublish')->name('promotions.publish');

        Route::group(['prefix' => 'trivia'], function () {
            Route::get('/', 'Workarea\TriviaController@getIndex')->name('trivia.index');
            Route::get('/create', 'Workarea\TriviaController@getCreate')->name('trivia.create');
            Route::post('/create', 'Workarea\TriviaController@postCreate');
            Route::get('/{id}', 'Workarea\TriviaController@getEdit')->name('trivia.edit');
            Route::patch('/{id}', 'Workarea\TriviaController@postEdit');
            Route::delete('/{id}', 'Workarea\TriviaController@delete')->name('trivia.delete');
        });

        Route::get('subcategories', 'Workarea\CategoryController@getIndex')->name('subcategories.index');

        Route::get('images/browser', 'Workarea\ImageController@getImageBrowser')->name('images.browser');
        Route::post('images/uploader', 'Workarea\ImageController@postImageUploader')->name('images.uploader');

        Route::get('videos/browser', 'Workarea\ImageController@getVideoBrowser')->name('videos.browser');
        Route::post('videos/uploader', 'Workarea\ImageController@postVideoUploader')->name('videos.uploader');

        Route::get('users', 'Workarea\UserController@getIndex')->name('users.index');

        Route::group(['prefix' => 'seo'], function () {
            Route::get('/', 'Workarea\SeoController@index')->name('seo.index');
            Route::get('/create', 'Workarea\SeoController@getCreate')->name('seo.create');
            Route::post('/create', 'Workarea\SeoController@store');
            Route::get('/{id}', 'Workarea\SeoController@show')->name('seo.edit');
            Route::patch('/{id}', 'Workarea\SeoController@update');
            Route::delete('/{id}', 'Workarea\SeoController@delete')->name('seo.delete');
        });

        Route::get('users/download', 'Workarea\UserController@getDownload')->name('users.download');
    });

    Route::group(['middleware' => ['role:motovan'], ['prefix' => 'motovan']], function () {
        Route::get('/', 'Workarea\MotovanController@getIndex')->name('motovan.index');
        Route::get('/create', 'Workarea\MotovanController@getCreate')->name('motovan.create');
        Route::post('/create', 'Workarea\MotovanController@postCreate');
        Route::get('/{id}', 'Workarea\MotovanController@getEdit')->name('motovan.edit');
        Route::patch('/{id}', 'Workarea\MotovanController@postEdit');
        Route::delete('/{id}', 'Workarea\MotovanController@deleteTraining')->name('motovan.delete');
        Route::get('/{id}/unpublish', 'Workarea\MotovanController@getUnpublish')->name('motovan.unpublish');
        Route::get('/{id}/publish', 'Workarea\MotovanController@getPublish')->name('motovan.publish');
        Route::get('/{id}/download', 'Workarea\MotovanController@getDownload')->name('motovan.download');
    });
});

Route::group(['prefix' => 'users'], function () {
    Route::get('/login', 'Workarea\UserController@getLogin')->name('users.login');
    Route::post('/login', 'Workarea\UserController@postLogin');

    Route::post('/password/recover', 'Workarea\UserController@postRecoverPassword')->name('users.password.recover');
    Route::get('/password/change', 'Workarea\UserController@getChangePassword')->name('users.password.change');
    Route::post('/password/change', 'Workarea\UserController@postChangePassword');
    Route::get('/password/{code}', 'Workarea\UserController@getVerifyCode')->name('users.password.verify');

    Route::get('/logout', 'Workarea\UserController@getLogout')->name('users.logout');
});
