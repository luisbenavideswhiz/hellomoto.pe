<?php

Route::group(['prefix' => 'comunidad'], function () {
    //Route::get('/', 'Landings\ComunidadController@index');
    Route::get('/', 'Landings\ComunidadController@fin');
    Route::post('/register', 'Landings\ComunidadController@register');
    Route::get('/gracias/{name?}', 'Landings\ComunidadController@gracias');
    Route::get('/confirmation/{confirmation_code?}', 'Landings\ComunidadController@confirmation');

    Route::post('/validate/dni', 'Landings\ComunidadController@validate_dni');
    Route::post('/validate/email', 'Landings\ComunidadController@validate_email');

    Route::get('/bienvenido', 'Landings\ComunidadController@bienvenido');
    Route::get('/confirmacion', 'Landings\ComunidadController@confirmacion');

    Route::get('/fin', 'Landings\ComunidadController@fin');
    Route::get('/finalizacion', 'Landings\ComunidadController@finalizacion');
});

Route::group(['prefix' => 'dimitrivegas'], function () {
//    Route::get('/', 'Landings\DimitriVegasController@index');
    Route::get('/', 'Landings\DimitriVegasController@finish');
    Route::post('/register', 'Landings\DimitriVegasController@register');
    Route::get('/confirmation/{confirmation_code?}', 'Landings\DimitriVegasController@confirmation');

    Route::get('/mailing-finish', 'Landings\DimitriVegasController@mailing_finish');
});

Route::group(['prefix' => 'moto-z-play-motomods-jbl-soundboost'], function(){
    Route::get('/', 'Landings\UneteController@index');
    Route::post('/', 'Landings\UneteController@register');
    Route::get('/ubigeo', 'Landings\UneteController@ubigeo');
    Route::get('/bienvenido', 'Landings\UneteController@welcome');
    Route::get('/terminos-condiciones', function(){
        return view('landings/unete/tyc');
    });
    Route::get('/emails', function () {
        return view('landings/unete/emails/finalizacion');
    });
});

Route::group(['prefix' => 'motogift'], function () {
    Route::get('/', 'Landings\MotogiftController@index');
    Route::post('/', 'Landings\MotogiftController@register');
    Route::get('/ubigeo', 'Landings\UneteController@ubigeo');
    Route::post('/confirma-datos', 'Landings\MotogiftController@saveExtraOptions');
    Route::get('/bienvenido', 'Landings\MotogiftController@welcome');
    Route::get('/terminos-condiciones', 'Landings\MotogiftController@disclaimer');
});

Route::group(['prefix' => 'copa-motorola'], function () {
    Route::get('/', 'Landings\CopaMotorolaController@index');
    Route::post('/', 'Landings\CopaMotorolaController@register');
    Route::get('/ubigeo', 'Landings\UneteController@ubigeo');
    Route::get('/bienvenido', 'Landings\CopaMotorolaController@welcome');
    Route::post('/bienvenido', 'Landings\CopaMotorolaController@welcome');
    Route::get('/terminos-condiciones', 'Landings\CopaMotorolaController@terms_conditions');
    Route::get('/emails', function () {
        return view('landings/unete/emails/finalizacion');
    });
});

Route::group(['prefix' => 'g6'], function () {
    Route::get('/', function () {
    return view('landings/intriga/index');
    });
});

Route::group(['prefix' => 'mario'], function () {
    Route::get('/', 'Landings\MarioController@getIndex');
    Route::post('/', 'Landings\MarioController@postRegister');
    Route::post('/bienvenido', 'Landings\MarioController@postWelcome');
    Route::get('/terminos-condiciones', 'Landings\MarioController@getDisclaimer');
    Route::get('/exito', 'Landings\MarioController@getSuccess');
    Route::get('/finalizado', 'Landings\MarioController@getClosed');

});

Route::group(['prefix' => 'tolinchi'], function () {
    Route::get('/', 'Landings\TolinchiController@getIndex');
    Route::post('/', 'Landings\TolinchiController@postRegister');
    Route::post('/bienvenido', 'Landings\TolinchiController@postWelcome');
    Route::get('/terminos-condiciones', 'Landings\TolinchiController@getDisclaimer');
    Route::get('/exito', 'Landings\TolinchiController@getSuccess');
    Route::get('/finalizado', 'Landings\TolinchiController@getClosed');
});