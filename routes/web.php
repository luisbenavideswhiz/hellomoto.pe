<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Http\Request;

Route::get('/', 'Web\HomeController@index');

Route::get('/amp', function () {
    return view('web/experienciasmoto/amp');
});

Route::group(['prefix' => 'web'], function () {
    Route::get('/{path1?}/{path2?}/{path3?}', function (Request $request) {
        return redirect(str_replace('web', '', $request->path()), 301);
    });
});

Route::get('/motostyle/gastronomia-y-restaurantes', function () {
    return redirect('/motostyle/hellococina', 301);
});

Route::get('/{category}/post/{slug}', function ($category, $slug) {
    return redirect('/post/' . $slug, 301);
});

Route::get('post/{slug}', 'Web\PostController@detail_post');

Route::get('/buscar/{word?}', 'Web\HomeController@search');

Route::get('/hellociudades/{subcategory?}/{orderby?}', 'Web\HomeController@hellociudades');
Route::get('/experienciasmoto/{subcategory?}/{orderby?}', 'Web\HomeController@experienciasmoto');
Route::get('/motostyle/{subcategory?}/{orderby?}', 'Web\HomeController@motostyle');
Route::get('/hellotips/{subcategory?}/{orderby?}', 'Web\HomeController@hellotips');

Route::group(['prefix' => 'motoembajadores'], function () {
    Route::get('/playlists', 'Web\HomeController@motoembajadoresPlaylist');
    Route::get('/playlist/{playlist?}', 'Web\HomeController@motoembajadoresPlaylistDetalle');
    Route::get('/embajador/{ambassador?}', 'Web\HomeController@motoembajadoresDetalle');
    Route::get('/', 'Web\HomeController@motoembajadores');
});

Route::get('/promociones', 'Web\PromotionController@index');
Route::get('/promociones/terminos-condiciones', 'Web\PromotionController@terms_conditions');

Route::get('/phone-life-balance', function () {
    return view('web/phonelifebalance/index');
});

Route::post('/posts/rows', 'Web\PostController@get_rows');

Route::post('/ambassadors/rows', 'Web\AmbassadorController@get_rows');
Route::post('/ambassadors/follow', 'Web\AmbassadorController@follow');
Route::post('/ambassadors/unfollow', 'Web\AmbassadorController@unfollow');
Route::post('/ambassadors/playlists', 'Web\AmbassadorController@get_playlists');
Route::post('/ambassadors/playlists/like', 'Web\AmbassadorController@playlist_like');
Route::post('/ambassadors/playlists/unlike', 'Web\AmbassadorController@playlist_unlike');

Route::get('/sitemap.xml', 'Web\SitemapController@sitemapXml');
Route::get('/sitemap/{category}-{subcategory}.xml', 'Web\SitemapController@sitemapXmlCategory');
Route::get('/sitemap/{category}-{subcategory}/{type}.xml', 'Web\SitemapController@sitemapXmlTypes');

Route::group(['prefix' => 'usuario'], function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/mis-playlist', 'Web\HomeController@usuario_mis_playlist');
        Route::get('/mis-embajadores', 'Web\HomeController@usuario_mis_embajadores');
        Route::get('/beneficios', 'Web\HomeController@usuario_loultimo');
        Route::get('/canjear', 'Web\HomeController@usuario_canjear');
        Route::get('/perfil', 'Web\HomeController@usuario_perfil');
        Route::get('/hellomoto', 'Web\HomeController@usuario_hellomoto');
        Route::get('/resultados', 'Web\HomeController@usuario_resultados');
        Route::get('/logout', 'Web\UserController@logout');
    });
    Route::post('/registro', 'Web\UserController@register');
    Route::post('/actualizar', 'Web\UserController@update');
    Route::post('/login', 'Web\UserController@login');
    Route::post('/recuperar-password', 'Web\UserController@recovery_password');
    Route::post('/cambiar-password', 'Web\UserController@change_password');
    Route::post('/subir-imagen', 'Web\UserController@upload_image');
});

Route::get('/facebook', 'Web\FacebookController@redirect');
Route::get('/facebook/processing', 'Web\FacebookController@callback');

Route::get('/twitter', 'Web\TwitterController@redirect');
Route::get('/twitter/processing', 'Web\TwitterController@callback');

Route::get('/google/', 'Web\GooglePlusController@redirect');
Route::get('/google/processing', 'Web\GooglePlusController@callback');

Route::get('/404', 'Web\HomeController@error_404');
Route::get('/505', 'Web\HomeController@error_505');

Route::group(['prefix' => 'seeker'], function () {
    Route::get('/', 'Seeker\IndexController@home');
    Route::post('/search-for', 'Seeker\IndexController@search_for');
});

Route::group(['prefix' => 'motovan'], function () {
    Route::get('/', 'Motovan\MotovanController@getRegister');
    Route::post('/', 'Motovan\MotovanController@postRegister');
    Route::post('motovan/login', 'Motovan\MotovanController@postLogin')->name('motovan.login');

    Route::group(['middleware' => 'auth:motovan'], function () {
        Route::get('/inscribete', 'Motovan\MotovanController@getEnroll');
        Route::post('/inscribete/{id}', 'Motovan\MotovanController@postEnroll');
        Route::get('/exito/{id}', 'Motovan\MotovanController@getSuccess');
    });
});

Route::group(['prefix' => 'trivia', 'namespace' => 'Trivia'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('logout', 'UserController@logout');
    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@login');
    Route::group(['middleware' => 'auth'], function () {
        Route::get('pregunta/{count}', 'HomeController@question');
        Route::get('resultado', 'HomeController@result');
        Route::get('finalizo', 'HomeController@finish');
        Route::get('corregir', 'OptionController@toCorrect')->name('trivia.answer.tocorrect');
    });
    Route::get('/terminos-condiciones', function (){
        return view('trivia.disclaimer');
    });
});
Route::get('g6', function () {
    return redirect('/moto-g6', 301);
});

Route::group(['prefix' => 'moto-g6'], function () {
    Route::get('/', 'LandingG6\MotoG6Controller@index');
});

Route::get('timeforjoy', 'Web\TimeForJoyController@index');
Route::get('timeforjoy/count', 'Web\TimeForJoyController@count');
