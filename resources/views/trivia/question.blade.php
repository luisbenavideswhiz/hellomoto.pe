@extends('trivia.layout.index')

@section('content')
    <div class="wrapper">
        <div class="container">
            <div class="questions">
                <div class="row">
                    <div class="col">
                        <h3>
                            {{ $assigned->count }}. {{ $question['question_left'] }}
                            <span class="text-a">{{ $question['question_right'] }}</span>
                        </h3>
                        <div class="row">
                            {{ csrf_field() }}
                            @php $i = 1; @endphp
                            @foreach($options as $row)
                                @if($i % 2 == 0)
                                    <div class="col-md-6 mb-4 col-xs-12">
                                        <button type="button" class="btn animated slideInRight hvr-underline-from-left">
                                            <input type="hidden" value="{{ $row->id }}">
                                            <span class="text-white">{{ $row->answer }}</span>
                                        </button>
                                    </div>
                                @else
                                    <div class="col-md-6 mb-4 col-xs-12">
                                        <button type="button" class="btn animated slideInLeft hvr-underline-from-left">
                                            <input type="hidden" value="{{$row->id}}">
                                            <span class="text-white">{{ $row->answer }}</span>
                                        </button>
                                    </div>
                                @endif
                                @php $i++ @endphp
                            @endforeach
                        </div>
                    </div>
                </div>
                <div id="this_answer">
                    <div class="section-answer d-none">
                    <h3 class="resp">
                        <i class="text-r-icon"></i>
                        <span class="resp-1"></span><span class="resp-2"></span>
                    </h3>
                    <div class="row">
                        <div class="answer-text">
                            <h4 class="rpt"></h4>
                            <p id="rpt-1"><span id="rpt-2"></span></p>
                            <div class="img-res"><img src="{{ url($question['image']) }}" class="img-fluid"></div>
                            <div class="action">
                                @if($assigned->count != 3)
                                    <a href="/trivia/pregunta/{{ $assigned->count+1 }}"
                                       class="btn btn-red">siguiente</a>
                                @else
                                    <a href="/trivia/resultado" class="btn btn-red">finalizar</a>
                                @endif
                            </div>
                        </div>
                        <div class="answer-img"><img src="{{ url($question['image']) }}" class="img-fluid"></div>
                    </div>
                </div>
                </div>
                <div class="progr">
                    <div class="progress">
                        <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="99" aria-valuemax="99"
                             style="width: {!! (33.3*$assigned->count)+0.1 !!}%; background-color: #fee533">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection