@extends('trivia.layout.index')

@section('content')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-6 left mb-75">
                    <h1>Trivia <br><span>motorola</span></h1>
                    <div class="d-block d-sm-none">
                        <img src="{{ url('img/trivia/motog_g6.png') }}" alt="">
                    </div>
                    <p>
                        ¡Responde correctamente estas 3 preguntas aleatorias y participa por uno de los<br>
                        <span>equipos g6 que tenemos para ti!</span>
                    </p>
                </div>
                <div class="col-md-6 col-lg-6 right">
                    <img src="{{ url('img/trivia/motog_g6.png') }}">
                </div>
            </div>
        </div>
    </div>
@stop