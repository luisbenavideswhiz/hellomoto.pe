<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('js/popper.js') }}"></script>
<script src="{{ asset('js/trivia/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/trivia/combodate.js') }}"></script>
<script src="{{ asset('js/trivia/moment.js') }}"></script>
<script src="{{ asset('js/moment-locale-es.js') }}"></script>
<script src="{{ asset('js/trivia/jquery.validate.js') }}"></script>
<script src="{{ asset('js/trivia/app.js') }}"></script>
