<!DOCTYPE html>
<html>
<head>
    <title>Trivia Motorola</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta property="og:url" content="{{ url('trivia') }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Trivia Motorola">
    <meta property="og:description" content="¡Responde 3 preguntas mundialistas y participa por ganar uno de los equipos g6 que tenemos para ti!">
    <meta property="og:image" content="{{ asset('/img/trivia/thumb.png') }}">

    <meta name="author" content="WHIZ - Agencia Digital">
    <meta name="title" content="Trivia Motorola">
    <meta name="description" content="¡Responde 3 preguntas mundialistas y participa por ganar uno de los equipos g6 que tenemos para ti!">
    <meta name="keywords" content="trivia, Motorola, mundial, Rusia, 2018, fútbol, motog6, polla, perú, concurso, premio.">

    @include('trivia.layout.css.index')
    @include('web.partials.seo.analytics')
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5JC6WBV"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

@include('trivia.partials.header')

@yield('content')

@include('trivia.partials.footer')
@include('trivia.layout.js.index')
</body>
</html>