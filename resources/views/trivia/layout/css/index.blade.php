<link rel="stylesheet" href="{{ asset('css/trivia/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('css/trivia/animate.css') }}">
<link rel="stylesheet" href="{{ asset('css/trivia/hover.css') }}">
<link rel="stylesheet" href="{{ asset('css/trivia/style.css') }}">
<link rel="stylesheet" href="{{ asset('css/trivia/responsive.css') }}">
<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">