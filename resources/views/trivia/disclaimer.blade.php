@extends('trivia.layout.index')

@section('content')
    <div class="wrapper this_not">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-8 left">
                    <div class="disclaimer">
                       <h2>Términos y Condiciones</h2>
                        <p>
                            Solo podrán participar en esta promoción personas mayores de edad con DNI (Peruanos), Carné de Extranjería o Pasaporte (Extranjeros), residentes en Lima. Los documentos de identidad deberán estar vigentes. 
                        </p>
                        <p>
                            Los participantes tienen desde el 15/06/2018 hasta el 15/07/2018 para registrarse en la web Hellomoto y responder las tres preguntas indicadas de la trivia Motorola. 
                        </p>
                        <p>
                            Los participantes que cumplan con registrarse y responder correctamente las preguntas, ingresarán automáticamente al sistema informático que determinará aleatoriamente a los tres (3) ganadores del concurso.
                        </p>
                        <p>
                            Los premios son 3 equipos moto g6: 1 moto g6 Plus, 1 moto g6 Play y 1 moto g6.
                        </p>
                        <p>
                            El anuncio de los ganadores se realizará el 17/07/2018 mediante una publicación en el Facebook de Motorola.
                        </p>
                        <p>
                            Motorola Perú se comunicará vía telefónica o correo con los ganadores para coordinar la entrega del premio.
                        </p>
                        <p>
                            Solo se tomará en cuenta 1 participación por usuario (según el Documento de Identidad ingresado).
                        </p>
                        <p>
                            El concurso no es válido para trabajadores de la agencia Wunderman Phantasia, Whiz o de la empresa de dispositivos móviles Motorola.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>footer{position:relative;opacity:0;}</style>
    @include('trivia.partials.modals.login')
@endsection