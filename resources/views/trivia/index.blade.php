@extends('trivia.layout.index')

@section('content')
    <div class="home wrapper">
        <div class="container">
            <div class="row clearfix">
                <div class="col-sm-12 col-lg-6 left">
                    <h1>Trivia <br><span>motorola</span></h1>
                    <p>
                        ¡Demuéstrale a Michael Succar qué tan hincha eres de la selección y participa por un equipo de la familia moto g<sup>6</sup>:
                         <span>1 moto g6 play, 1 moto g<sup>6</sup> o 1 moto g<sup>6</sup> plus</span>
                    </p>
                    <div class="action">
                        <a href="#!" id="btn-start" class="btn btn-red">¡comenzar!</a>
                        <input type="hidden" id="user" value="{{ $user }}">
                    </div>
                </div>
                <div class="home-img col-xs-12 col-lg-6 right">
                    <img src="{{ url('img/trivia/motog_g6.png') }}">
                    {{--<div class="action">--}}
                    {{--<a href="https://www.instagram.com/motorolape/" id="btn-start" class="btn btn-red">--}}
                    {{--¡Vívelo de cerca!</a>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
    @include('trivia.partials.modals.login')
@endsection