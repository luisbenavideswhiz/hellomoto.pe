@extends('trivia.layout.index')

@section('content')
    <div class="wrapper this_not">
        <div class="container finish">
            <div class="row">
                <div class="result">
                    @if($hit == 3)
                        <i class="fa fa-check-circle-o yellow"></i>
                        <h2>¡Felicidades! Has respondido correctamente todas las preguntas, de esta manera <br>
                            <span>ya estás participando por uno de los moto g6 que tenemos para ti.</span></h2>
                    @else
                        <i class="fa fa-times-circle-o red"></i>
                        <h2>¡Oh, lo sentimos! Sabiamos que no era facil acertarlas todas. <br>
                            <span>Puedes hacer de nuevo la Trivia, o finalizar.</span></h2>
                    @endif
                </div>
                <div class="response">
                    @php $i = 1; @endphp
                    @foreach($questions as $row)
                        <div class="col-12">
                            <i class="fa @if($row->hit == 1) fa-check amarillo @else fa-times rojo @endif "></i>
                            <div class="answer">
                                <p>{{ $i }}. {{ $row->question->question }}</p>
                                <div class="correct-answer">{{ $row->question->answer }}</div>
                            </div>
                        </div>
                        @php $i++ @endphp
                    @endforeach
                    <div class="action @if($hit === 3)center @endif">
                        <a href="{{ url('/') }}" class="btn btn-red">FINALIZAR</a>
                        @if($hit != 3)<a href="/trivia" class="btn btn-yellow">INTENTAR DE NUEVO</a>@endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection