<form id="login" method="post" action="trivia/login">
    <div class="logo"><img src="img/trivia/logo_negro.png" class="mt-4"></div>
    {{ csrf_field() }}
    <div class="mt-5">
        <div class="col-12 pl-5 pr-5">
            <div class="form-group">
                <label>Usuario</label>
                <input type="text" id="login_username" class="form-control popover-validate" name="email"
                       autocomplete="off">
            </div>
        </div>
        <div class="col-12 pl-5 pr-5">
            <div class="form-group">
                <label>Contraseña</label>
                <input type="password" id="login_password" class="form-control popover-validate"
                       name="password" autocomplete="off">
            </div>
        </div>
        <div class="col-12">
            <div class="form-group text-center">
                <button type="submit" class="btn btn-outline-primary">INGRESAR</button>
            </div>
        </div>
        <div class="col-12 pl-5">
            <div class="input-field">
                <label><input type="checkbox"> Recordar contrase&ntilde;a</label>
            </div>
        </div>
    </div>
</form>