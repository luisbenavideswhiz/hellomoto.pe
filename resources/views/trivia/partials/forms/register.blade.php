<form id="register" method="post" action="trivia/register">
    <div class="logo"><img src="{{ asset('img/trivia/logo_negro.png') }}" class="mt-4"></div>
    {{ csrf_field() }}
    <div class="row mt-4 pl-5 pr-5">
        <div class="col-lg-6 col-xs-12">
            <div class="form-group">
                <label for="name">Nombres</label>
                <input type="text" class="form-control popover-validate" name="name" id="name">
            </div>
        </div>
        <div class="col-lg-6 col-xs-12">
            <div class="form-group">
                <label for="lastname">Apellidos</label>
                <input type="text" class="form-control popover-validate" name="lastname" id="lastname">
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <label>Tipo de documento (*)</label>
                    <div class="form-group">
                        <select class="form-control popover-validate" name="document_type" id="document_type">
                            <option value="dni">DNI</option>
                            <option value="pasaporte">Pasaporte</option>
                            <option value="extranjeria">Carnet de extranjeria</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <label>N de documento(*)</label>
                    <div class="form-group">
                        <input type="text" class="form-control popover-validate" id="document_number"
                               name="document_number">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12">
            <label>Sexo</label>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label> Hombre
                            <input type="radio" name="sex" id="sex_hombre" checked="true" value="hombre">
                        </label>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label> Mujer<input type="radio" name="sex" id="sex_mujer" value="mujer"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label>Correo electr&oacute;nico: </label>
                <input type="email" class="form-control popover-validate" name="email" id="email">
            </div>
        </div>
        <div class="col-12">
            <label>Aceptar correos electronicos: </label>
            <label class="ml-3"><input type="radio" value="1" name="accept_emails" id="accept_emails_si" checked> Si
            </label>
            <label class="ml-3"><input type="radio" value="0" name="accept_emails" id="accept_emails_no">No</label>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label>Fecha de Nacimiento</label><br>
                <input type="text" class="form-control popover-validate" name="birthdate" id="birthdate">
            </div>
        </div>
        <div class="col-12">
            <label>Verificar fecha de nacimiento: </label>
            <label class="ml-3"><input type="radio" value="1" name="full_age" id="full_age_yes" checked>Si </label>
            <label class="ml-3"><input type="radio" value="0" name="full_age" id="full_age_no">No</label>
        </div>

        <div class="col-12">
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <label>N de celular(*)</label>
                    <div class="form-group">
                        <input type="text" class="form-control popover-validate" name="phone" id="phone">
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <label>Operador (*)</label>
                    <div class="form-group">
                        <select class="form-control popover-validate" name="mobile_operator" id="mobile_operator">
                            <option value="claro">Claro</option>
                            <option value="movistar">Movistar</option>
                            <option value="entel">Entel</option>
                            <option value="bitel">Bitel</option>
                            <option value="virgin">Virgin Mobile</option>
                            <option value="tuenti">Tuenti</option>
                            <option value="cuy">Cuy Móvil</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="row">
                <div class="col-md-6">
                    <label>Contraseña(*)</label>
                    <div class="form-group">
                        <input type="password" class="form-control popover-validate" id="password" name="password">
                    </div>
                </div>
                <div class="col-md-6">
                    <label>Confirmar Contraseña(*)</label>
                    <div class="form-group">
                        <input type="password" class="form-control popover-validate" id="password_confirm"
                               name="password_confirm">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group text-center">
                <label>Acepto Terminos y Condiciones
                    <input type="radio" class="popover-validate ml-3" name="terms_conditions" id="terms_conditions">
                </label>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-outline-primary">REGISTRARME</button>
            </div>
            {{--<div class="form-group text-center">--}}
            {{-------------- o registrate con --------------}}
            {{--</div>--}}
            {{--<div class="form-group text-center">--}}
            {{--<a href="/facebook?action_type=redirect"><img src="img/trivia/ico-facbook.png"></a>--}}
            {{--<a href="/google?action_type=redirect"><img src="img/trivia/ico-google.png"></a>--}}
            {{--<a href="/twitter?action_type=redirect"><img src="img/trivia/ico-twitter.png"></a>--}}
            {{--</div>--}}
        </div>
    </div>
</form>