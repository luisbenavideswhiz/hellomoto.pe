<div class="modal fade" id="mod-log" style="" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <ul class="nav nav-pills flex-sm-row">
                    <li class="active" style="width: 50%">
                        <a href="#1a" data-toggle="tab" class="nav-link active text-center">Tengo una cuenta</a>
                    </li>
                    <li style="width: 50%">
                        <a href="#2a" data-toggle="tab" class="nav-link text-center ">Registrate</a>
                    </li>
                </ul>
                <div class="tab-content clearfix">
                    <div class="tab-pane active" id="1a">
                        @include('trivia.partials.forms.login')
                    </div>
                    <div class="tab-pane" id="2a">
                        @include('trivia.partials.forms.register')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>