<!DOCTYPE html>
<html lang="es">
<head>
    <title>Motorola - Seeker</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/seeker/app.js') }}" type="text/javascript"></script>
</head>
<body>
<br>
<br>
<section class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Motorola Seeker</h3>
                </div>
                <div class="panel-body">
                    <form>
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control validate" id="name" name="name" placeholder="Nombre">
                        </div>
                        <div class="form-group">
                            <label for="lastname">Apellido</label>
                            <input type="text" class="form-control validate" id="lastname" name="lastname"
                                   placeholder="Apellido">
                        </div>
                        <div class="form-group">
                            <label for="lastname">Landing</label>
                            <select id="landing" name="landing" class="form-control validate">
                                <option value="">Seleccione</option>
                                <option value="1">Dimitri Vegas</option>
                                <option value="2">Comunidad</option>
                            </select>
                        </div>
                        <a id="register-btn" href="#" class="btn btn-primary" data-url="/seeker/search-for">Buscar</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="container">
    <div class="row">
        <div id="response" class="col-lg-8 col-lg-offset-2"></div>
    </div>
</section>

</body>
</html>
