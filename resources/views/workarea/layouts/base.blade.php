<!DOCTYPE html>
<html lang="es">
<head>
    <title>{{ getenv('APP_TITLE') }}</title>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0, minimum-scale=1.0">

    @section('metas')
    @show

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-reset.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/workarea/vendor/new-style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/workarea/vendor/new-responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('css/workarea/vendor/slidebars.css') }}" rel="stylesheet">

@section('css')
@show

<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/workarea/vendor/html5shiv.js') }}"></script>
    <script src="{{ asset('js/workarea/vendor/respond.min.js') }}"></script>
    <![endif]-->
</head>
<body>
<section id="container">
    @include('workarea.partials.header')
    @include('workarea.partials.aside')
    @yield('content')
    @include('workarea.partials.sidebar')
    @include('workarea.partials.footer')
</section>

<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/workarea/vendor/jquery.dcjqaccordion.2.7.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/workarea/vendor/jquery.sparkline.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/workarea/vendor/jquery.nicescroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/workarea/vendor/slidebars.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/workarea/scripts/app.js') }}"></script>

@section('js')
@show

<script>
    if(typeof CKEDITOR != 'undefined'){
        CKEDITOR.tools.getCsrfToken = function(){
            return $('meta[name="csrf-token"]').attr('content');
        };
    }
</script>
</body>
</html>
