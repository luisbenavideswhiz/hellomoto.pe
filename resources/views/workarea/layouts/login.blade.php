<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Workarea hellomoto</title>
    @section('css')
    @show
</head>

<body class="login-body">

    <div class="container">
    @yield('content')
    </div>

    @section('js')
    @show
</body>
</html>
