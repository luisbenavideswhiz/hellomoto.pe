@extends('workarea.layouts.base')

@section('metas')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/bootstrap-fileupload.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/toastr.min.css') }}" type="text/css" />
@endsection

@section('content')
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Motovan / Capacitaciones</header>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="dt-content">
                            <div class="left">
                                <form action="{{ route('workarea.motovan.index') }}" method="GET">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                                        </span>
                                        <input type="text" name="query" @if(!empty($query)) value="{{ $query }}" @endif class="form-control" placeholder="Buscar capacitación">
                                    </div>
                                </form>
                            </div>
                            <div class="right">
                                <a href="#!" class="btn-create btn btn-success">
                                    Crear nueva capacitación
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="dt-content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Ubicación</th>
                                    <th>Cupos totales</th>
                                    <th>Cupos disponibles</th>
                                    <th>Fecha de la capacitación</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($trainings as $training)
                                    <tr>
                                        <td>{{ $training->location }}</td>
                                        <td>{{ $training->total_coupons }}</td>
                                        <td>{{ $training->remaining_coupons }}</td>
                                        <td>{{ $training->date->format('d/m/Y g:i a') }}</td>
                                        <td>
                                            <a href="{{ route('workarea.motovan.download', ['id' => $training->id])  }}" class="btn btn-primary"><i class="fa fa-download"></i> </a>

                                            @if($training->status==1)
                                                <a href="{{ route('workarea.motovan.unpublish', ['id' => $training->id])  }}" class="btn btn-success btn-post-action"><i class="fa fa-eye"></i> </a>
                                            @elseif($training->status==2)
                                                <a href="{{ route('workarea.motovan.publish', ['id' => $training->id]) }}" class="btn btn-warning btn-post-action"><i class="fa fa-eye-slash"></i></a>
                                            @endif

                                            <a href="#!" class="btn btn-info btn-update" data-action="{{ route('workarea.motovan.edit', ['id' => $training->id]) }}"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>

                                            <button data-action="{{ route('workarea.motovan.delete', ['id' => $training->id]) }}" class="btn btn-danger btn-delete">
                                                <i class="fa fa-times i-action-d" aria-hidden="true"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @if($trainings->lastPage()>1)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="dt-content">
                                {!! $trainings->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </section>
    </section>

    @component('workarea.modals.delete_item_table')
        @slot('title')
            Eliminar capacitación
        @endslot
        @slot('message')
            Desea eliminar la capacitación seleccionada?
        @endslot
    @endcomponent

    @include('workarea.motovan.modals.create')
    @include('workarea.motovan.modals.update')
@endsection


@section('js')
    <script type="text/javascript" src="{{ asset('js/workarea/vendor/bootstrap-fileupload.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/vendor/bootstrap-inputmask.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/scripts/form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/scripts/list.js') }}"></script>
    <script>
        $(function () {
            List.deleteButton('.btn-delete', '#modal');
            List.createModal('.btn-create', '#mCreate', '#form-create');
            List.updateModal('.btn-update', '#mUpdate', '#form-update', '/workarea/motovan/');
        });
    </script>
@endsection