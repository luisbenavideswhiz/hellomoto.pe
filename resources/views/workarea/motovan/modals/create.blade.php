<div class="modal fade modal-dialog-center in" id="mCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Nuevo</h4>
                </div>
                <form action="{{ route('workarea.motovan.create') }}" id="form-create" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="location_c">Ubicación : </label>
                            <input type="text" id="location_c" name="location" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="total_coupons_c">Total Cupos: </label>
                            <input type="text" id="total_coupons_c" name="total_coupons" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="date_c">Fecha de la capacitación: </label>
                            <input type="text" id="date_c" name="date" data-mask="99/99/9999 99:99:99" class="form-control">
                            <span class="help-inline">dd/mm/yyyy hh:mm:ss</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" class="btn btn-default" style="width: 20%">Cerrar</a>
                        <button class="btn btn-success" style="width: 20%"> Crear</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>