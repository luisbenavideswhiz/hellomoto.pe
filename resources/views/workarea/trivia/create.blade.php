@extends('workarea.layouts.base')

@section('metas')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/toastr.min.css') }}" type="text/css"/>
@endsection

@section('content')
    <section id="main-content">
        <section class="wrapper">
            <form class="form" role="form" action="{{ route('workarea.trivia.create') }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <header class="panel-heading">Crear Pregunta</header>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label for="">Pregunta</label>
                                                <input class="form-control m-bot15" type="text" name="question">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 m-bot15">
                                                <label for="">Descripción Correcta</label>
                                                <textarea class="form-control ckeditor" id="description" name="correct_description"
                                                          rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 m-bot15">
                                                <label for="">Descripción Error</label>
                                                <textarea class="form-control ckeditor" id="description" name="error_description"
                                                          rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label for="">Respuesta</label>
                                                <input class="form-control m-bot15" type="text" name="answer">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <header class="panel-heading">Alternativas</header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary" id="add-option">
                                                <i class="fa fa-plus"></i> Agregar alternativa
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="content-options">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group m-bot15">
                                                <input type="text" name="options[answer][]" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" class="check-correct" name="options[correct][]" value="1"> Respuesta correcta
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-danger delete-option">
                                                <i class="glyphicon glyphicon-trash"></i> <span>Eliminar</span>
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-4">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">Guardar</button>
                                            <a href="{{ route('workarea.trivia.index') }}"
                                               class="btn btn-danger">Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </section>
    </section>
@endsection

@section('js')
    <script class="include" type="text/javascript"
            src="{{ asset('js/workarea/vendor/ckeditor/ckeditor.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/toastr.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/workarea/scripts/form.js') }}"></script>

    <script>
        $(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').prop('content')
                }
            });

            toastr.options = {
                "closeButton": true,
                "positionClass": "toast-top-right",
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            Form.form('.form');
            Form.addQuestion('#add-option', '.content-options', '.row');
            Form.deleteQuestion('.delete-option', '.dropdown-toggle');
        });
    </script>
@endsection



