@extends('workarea.layouts.base')

@section('metas')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/toastr.min.css') }}" type="text/css">
@endsection

@section('content')
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">MotoTrivia</header>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="dt-content">
                            <div class="left">
                                <form action="{{ route('workarea.trivia.index') }}" method="GET">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-white"><i
                                                        class="fa fa-search"></i></button>
                                        </span>
                                        <input type="text" class="form-control" name="query" placeholder="Buscar pregunta"
                                               @if(!empty($query)) value="{{ $query }}" @endif>
                                    </div>
                                </form>
                            </div>
                            <div class="right">
                                <a href="{{ route('workarea.trivia.create') }}" class="btn btn-success">
                                    Crear pregunta
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="dt-content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Pregunta</th>
                                    <th>Respuesta</th>
                                    <th style="width: 200px">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($questions as $question)
                                    <tr>
                                        <td>{{ $question->question }}</td>
                                        <td>{{ $question->answer }}</td>
                                        <td>
                                            <a href="{{ route('workarea.trivia.edit', ['id' => $question->id])  }}"
                                               class="btn btn-info">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                            <button data-action="{{ route('workarea.trivia.delete', ['id' => $question->id]) }}"
                                                    class="btn btn-danger btn-delete">
                                                <i class="fa fa-times i-action-d" aria-hidden="true"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @if($questions->lastPage()>1)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="dt-content">
                                {!! $questions->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </section>
    </section>

    @component('workarea.modals.delete_item_table')
        @slot('title')
            Eliminar pregunta
        @endslot
        @slot('message')
            Desea eliminar la pregunta seleccionada?
        @endslot
    @endcomponent
@endsection

@section('js')
    <script src="{{ asset('js/workarea/vendor/toastr.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/scripts/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/scripts/list.js') }}"></script>
    <script>
        $(function () {
            List.deleteButton('.btn-delete', '#modal');
        });
    </script>
@endsection



