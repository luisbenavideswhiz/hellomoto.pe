<form action="" id="fileupload">
    <div class="panel">
        <header class="panel-heading">
            Galería
            <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-chevron-down"></a>
        </span>
        </header>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                    <div class="row fileupload-buttonbar">
                        <div class="col-lg-12">
                            <!-- The fileinput-button span is used to style the file input field as button -->
                            <span class="btn btn-success fileinput-button">
                                                <i class="glyphicon glyphicon-plus"></i>
                                                <span>Agregar archivos...</span>
                                                <input type="file" name="gallery[]" multiple>
                                                </span>
                            <button type="submit" class="btn btn-primary start">
                                <i class="glyphicon glyphicon-upload"></i>
                                <span>Subir</span>
                            </button>
                            <button type="reset" class="btn btn-warning cancel">
                                <i class="glyphicon glyphicon-ban-circle"></i>
                                <span>Cancelar</span>
                            </button>
                            <button type="button" class="btn btn-danger delete">
                                <i class="glyphicon glyphicon-trash"></i>
                                <span>Eliminar</span>
                            </button>
                            <input type="checkbox" class="toggle">
                            <!-- The global file processing state -->
                            <span class="fileupload-process"></span>
                        </div>
                        <!-- The global progress state -->
                        <div class="col-lg-12 fileupload-progress fade">
                            <!-- The global progress bar -->
                            <div class="progress progress-striped active" role="progressbar"
                                 aria-valuemin="0" aria-valuemax="100">
                                <div class="progress-bar progress-bar-success" style="width:0%;">
                                </div>
                            </div>
                            <!-- The extended global progress state -->
                            <div class="progress-extended">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <!-- The table listing the files available for upload/download -->
                    <table role="presentation" class="table table-striped">
                        <tbody class="files">
                        @foreach($ambassador->webGalleries as $file)
                            <tr class="template-download fade in">
                                <td>
                                <span class="preview">
                                    @if ($file->thumbnail)
                                        <a href="{{ url($file->path) }}" title="" download="" data-gallery><img src="{{ $file->thumbnail }}"></a>
                                    @endif
                                </span>
                                </td>
                                <td>
                                    <p class="name">
                                        <a href="{{ url($file->path) }}" title="{{ $file->title }}" download="{{ $file->title }}" data-gallery> {{ $file->title }}</a>
                                    </p>
                                </td>
                                <td>
                                    <span class="size">{{ $file->size }}</span>
                                </td>
                                <td>
                                    <button class="btn btn-danger delete" data-type="DELETE" data-url="{{ route('workarea.ambassadors.gallery.delete',['ambassador'=>$ambassador->id, 'gallery'=>$file->id]) }}">
                                        <i class="glyphicon glyphicon-trash"></i>
                                        <span>Eliminar</span>
                                    </button>
                                    <input type="checkbox" name="delete" value="1" class="toggle">
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>