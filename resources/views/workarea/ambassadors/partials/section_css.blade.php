
<link rel="stylesheet" href="{{ asset('css/workarea/select2.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/workarea/bootstrap-fileupload.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/workarea/toastr.css') }}" type="text/css" />

<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="{{ asset('css/workarea/file-uploader/blueimp-gallery.min.css') }}">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="{{ asset('css/workarea/file-uploader/jquery.fileupload.css') }}">
<link rel="stylesheet" href="{{ asset('css/workarea/file-uploader/jquery.fileupload-ui.css') }}">

<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript>
    <link rel="stylesheet" href="{{ asset('css/workarea/jquery.fileupload-noscript.css') }}">
</noscript>
<noscript>
    <link rel="stylesheet" href="{{ asset('css/workarea/jquery.fileupload-ui-noscript.css') }}">
</noscript>