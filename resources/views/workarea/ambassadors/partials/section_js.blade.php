
<script class="include" type="text/javascript" src="{{ asset('js/workarea/select2.min.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/workarea/jquery.tagsinput.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/workarea/ckeditor/ckeditor.js') }}"></script>



<!--Multiuple File Uploader-->
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="{{ asset('js/workarea/file-uploader/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('js/workarea/file-uploader/tmpl.min.js') }}"></script>
<script src="{{ asset('js/workarea/file-uploader/load-image.all.min.js') }}"></script>
<script src="{{ asset('js/workarea/file-uploader/canvas-to-blob.min.js') }}"></script>


<!-- blueimp Gallery script -->
<script src="{{ asset('js/workarea/file-uploader/jquery.blueimp-gallery.min.js') }}"></script>

<script src="{{ asset('js/workarea/file-uploader/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('js/workarea/file-uploader/jquery.fileupload.js') }}"></script>
<script src="{{ asset('js/workarea/file-uploader/jquery.fileupload-process.js') }}"></script>
<script src="{{ asset('js/workarea/file-uploader/jquery.fileupload-image.js') }}"></script>
<script src="{{ asset('js/workarea/file-uploader/jquery.fileupload-audio.js') }}"></script>
<script src="{{ asset('js/workarea/file-uploader/jquery.fileupload-video.js') }}"></script>
<script src="{{ asset('js/workarea/file-uploader/jquery.fileupload-validate.js') }}"></script>
<script src="{{ asset('js/workarea/file-uploader/jquery.fileupload-ui.js') }}"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="{{ asset('js/workarea/file-uploader/jquery.xdr-transport.js') }}"></script>
<![endif]-->

<script class="include" type="text/javascript" src="{{ asset('js/workarea/bootstrap-fileupload.js') }}"></script>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
      {% for (var i=0, file; file=o.files[i]; i++) { %}
      <tr class="template-upload fade">
          <td>
              <span class="preview"></span>
          </td>
          <td>
              <p class="name">{%=file.name%}</p>
              <strong class="error text-danger"></strong>
          </td>
          <td>
              <p class="size">Processing...</p>
              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
          </td>
          <td>
              {% if (!i && !o.options.autoUpload) { %}
              <button class="btn btn-primary start" disabled>
                  <i class="glyphicon glyphicon-upload"></i>
                  <span>Start</span>
              </button>
              {% } %}
              {% if (!i) { %}
              <button class="btn btn-warning cancel">
                  <i class="glyphicon glyphicon-ban-circle"></i>
                  <span>Cancel</span>
              </button>
              {% } %}
          </td>
      </tr>
      {% } %}
  </script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
      {% for (var i=0, file; file=o.files[i]; i++) { %}
      <tr class="template-download fade">
          <td>
            <span class="preview">
                {% if (file.thumbnail_url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnail_url%}"></a>
                {% } %}
            </span>
          </td>
          <td>
              <p class="name">
                  {% if (file.url) { %}
                  <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnail_url?'data-gallery':''%}>{%=file.name%}</a>
                  {% } else { %}
                  <span>{%=file.name%}</span>
                  {% } %}
              </p>
              {% if (file.error) { %}
              <div><span class="label label-danger">Error</span> {%=file.error%}</div>
              {% } %}
          </td>
          <td>
              <span class="size">{%=o.formatFileSize(file.size)%}</span>
          </td>
          <td>
              {% if (file.deleteUrl) { %}
              <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}" {% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
              <i class="glyphicon glyphicon-trash"></i>
              <span>Eliminar</span>
              </button>
              <input type="checkbox" name="delete" value="1" class="toggle">
              {% } else { %}
              <button class="btn btn-warning cancel">
                  <i class="glyphicon glyphicon-ban-circle"></i>
                  <span>Cancelar</span>
              </button>
              {% } %}
          </td>
      </tr>
      {% } %}
  </script>



<script type="text/javascript" src="{{ asset('js/workarea/app.js') }}"></script>

<script>
    window.errors = {!! json_encode($errors->all()) !!};
</script>
