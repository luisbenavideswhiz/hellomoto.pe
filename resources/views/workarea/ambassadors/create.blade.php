@extends('workarea.layouts.base')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/select2.min.css') }}" type="text/css">

    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/bootstrap-fileupload.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/toastr.min.css') }}" type="text/css"/>

    <!-- blueimp Gallery styles -->
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/file-uploader/blueimp-gallery.min.css') }}">
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/file-uploader/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/file-uploader/jquery.fileupload-ui.css') }}">

    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript>
        <link rel="stylesheet" href="{{ asset('css/workarea/vendor/jquery.fileupload-noscript.css') }}">
    </noscript>
    <noscript>
        <link rel="stylesheet" href="{{ asset('css/workarea/vendor/jquery.fileupload-ui-noscript.css') }}">
    </noscript>

    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/bootstrap-colorselector.min.css') }}">
@endsection

@section('content')
    <section id="main-content">
        <section class="wrapper">
            <form class="form" role="form" action="{{ route('workarea.ambassadors.create') }}" method="POST"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <header class="panel-heading">Nuevo Embajador</header>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-6">
                                                        <label for="">Nombre del embajador</label>
                                                        <input class="form-control m-bot15" type="text" name="name">
                                                    </div>
                                                    <div class="col-xs-12 col-md-6">
                                                        <label for="">Alias del embajador</label>
                                                        <input class="form-control m-bot15" type="text" name="nickname">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-6">
                                                        <label for="">Ciudad</label>
                                                        <select name="city" id="city" class="form-control m-bot15">
                                                            <option value="">Seleccionar</option>
                                                            <option value="lima">Lima</option>
                                                            <option value="piura">Piura</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-md-6">
                                                        <label for="">Profesion / oficio</label>
                                                        <select name="web_career_id" id="web_career_id" class="form-control">
                                                            @foreach($careers as $career)
                                                                <option value="{{ $career->id }}">{{ $career->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 m-bot15">
                                                <label for="">Biografía</label>
                                                <textarea class="form-control ckeditor" id="bio" name="bio"
                                                          rows="10"></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label for="">Etiquetas</label>
                                                    <input name="tags" id="tagsinput" class="tagsinput"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <header class="panel-heading">Redes sociales</header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary" id="add-network"><i
                                                        class="fa fa-plus"></i> Agregar red social
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="content-network">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="input-group m-bot15">
                                                    <div class="input-group-btn">
                                                        <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
                                                            Seleccione <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            @foreach($networks as $network)
                                                                <li>
                                                                    <a href="#" data-id="{{ $network->id }}">{!! $network->icon_html !!} {{ $network->name }}</a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                        <input type="hidden" name="social_network_type[]">
                                                    </div>
                                                    <input type="text" name="social_network[]" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <button type="button" class="btn btn-danger delete-network">
                                                    <i class="glyphicon glyphicon-trash"></i> <span>Eliminar</span>
                                                </button>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <header class="panel-heading">
                                Playlists <span class="tools pull-right"><a href="javascript:;" class="fa fa-chevron-down"></a></span>
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary" id="add-playlist"><i
                                                        class="fa fa-plus"></i> Agregar playlist
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="content-playlist">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="">Nombre</label>
                                                <div class="form-group">
                                                    <input class="form-control m-bot15" type="text" name="playlist_title[]">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="">URL Spotify</label>
                                                <div class="form-group">
                                                    <input class="form-control m-bot15" type="text" name="playlist_url[]">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label for=""></label>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-danger delete-playlist">
                                                        <i class="glyphicon glyphicon-trash"></i> <span>Eliminar</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        @component('workarea.posts.partials.seo')
                        @endcomponent
                    </div>

                    <div class="col-sm-4">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">Guardar</button>
                                            <a href="{{ route('workarea.ambassadors.index') }}"
                                               class="btn btn-danger">Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="">Estado</label>
                                            <select name="status" class="form-control">
                                                <option value="2">Borrador</option>
                                                <option value="1">Publicado</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Posicion del Embajador</label>
                                            <input type="text" name="position" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Asignar color</label>
                                            <select name="web_colors_id" id="color" class="form-control">
                                                @foreach($colors as $color)
                                                    <option value="{{ $color->id }}" data-color="{{ $color->hex }}">{{ $color->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        @component('workarea.partials.upload_image')
                                            @slot('title')
                                                Foto de perfil
                                            @endslot
                                            @slot('inputName')
                                                image
                                            @endslot
                                        @endcomponent
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label class="">Video destacado</label>
                                        <div class="form-group">
                                            <input class="form-control m-bot15" type="text" name="profile_video"
                                                   placeholder="Código URL de YouTube">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>

        </section>
    </section>
@endsection

@section('js')
    <script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/select2.min.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/jquery.tagsinput.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/bootstrap-colorselector.min.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/ckeditor/ckeditor.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/toastr.min.js') }}"></script>

    <script src="{{ asset('js/workarea/vendor/file-uploader/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('js/workarea/vendor/file-uploader/tmpl.min.js') }}"></script>
    <script src="{{ asset('js/workarea/vendor/file-uploader/load-image.all.min.js') }}"></script>
    <script src="{{ asset('js/workarea/vendor/file-uploader/canvas-to-blob.min.js') }}"></script>

    <script src="{{ asset('js/workarea/vendor/file-uploader/jquery.blueimp-gallery.min.js') }}"></script>

    <script src="{{ asset('js/workarea/vendor/file-uploader/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('js/workarea/vendor/file-uploader/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('js/workarea/vendor/file-uploader/jquery.fileupload-process.js') }}"></script>
    <script src="{{ asset('js/workarea/vendor/file-uploader/jquery.fileupload-image.js') }}"></script>
    <script src="{{ asset('js/workarea/vendor/file-uploader/jquery.fileupload-audio.js') }}"></script>
    <script src="{{ asset('js/workarea/vendor/file-uploader/jquery.fileupload-video.js') }}"></script>
    <script src="{{ asset('js/workarea/vendor/file-uploader/jquery.fileupload-validate.js') }}"></script>
    <script src="{{ asset('js/workarea/vendor/file-uploader/jquery.fileupload-ui.js') }}"></script>

    <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
    <!--[if (gte IE 8)&(lt IE 10)]>
    <script src="{{ asset('js/workarea/vendor/file-uploader/jquery.xdr-transport.js') }}"></script>
    <![endif]-->

    <script class="include" type="text/javascript"
            src="{{ asset('js/workarea/vendor/bootstrap-fileupload.js') }}"></script>

    <script id="template-upload" type="text/x-tmpl">
        @include('workarea.templates.multiple_upload')
    </script>
    <script id="template-download" type="text/x-tmpl">
        @include('workarea.templates.multiple_display')
    </script>
    <script id="template-playlist" type="text/x-tmpl">
        @include('workarea.templates.item_playlist')
    </script>
    <script id="template-social-network" type="text/x-tmpl">
        @include('workarea.templates.social_network')
    </script>

    <script type="text/javascript" src="{{ asset('js/workarea/scripts/form.js') }}"></script>

    <script>
        $(function () {
            $("#web_career_id").select2({
                tags: true
            });

            $(".tagsinput").tagsInput();

            toastr.options = {
                "closeButton": true,
                "positionClass": "toast-top-right",
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            $('#fileupload').fileupload({
                url: '/workarea/ambassadors/gallery',
                disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
                maxFileSize: 5000000,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                formData: {
                    id: $('#ambassador-id').val()
                }
            });

            $('#color').colorselector();
            Form.form('.form', true);
            Form.selectInputGroup('.input-group-btn');
            Form.addNetwork('#add-network', '.content-network', '.row');
            Form.deleteNetwork('.delete-network', '.dropdown-toggle');
            Form.deletePlaylist('.delete-playlist');
            Form.addPlaylist('#add-playlist', '.content-playlist', '.row');

        });
    </script>
@endsection



