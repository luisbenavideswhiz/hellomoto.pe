@extends('workarea.layouts.base')

@section('metas')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/toastr.min.css') }}" type="text/css">
@endsection

@section('content')
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Embajadores</header>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="dt-content">
                            <form action="{{ route('workarea.ambassadors.index') }}" method="GET">
                                <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-white"><i
                                                        class="fa fa-search"></i></button>
                                        </span>
                                    <input type="text" class="form-control" name="query" placeholder="Buscar publicaciones"
                                           @if(!empty($query)) value="{{ $query }}" @endif>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="dt-content">
                            <div class="pull-left">
                                Mostrando {{ $ambassadors->count() }} de {{ $ambassadors->total() }} embajadores
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-primary download-excel disabled"><i class="fa fa-download"></i> Descargar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <a href="{{ route('workarea.ambassadors.create') }}" class="btn btn-success">
                                        Crear nuevo embajador
                                    </a>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col-sm-12">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Título</th>
                                            <th>Ocupación</th>
                                            <th>Fecha de ingreso</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($ambassadors as $ambassador)
                                            <tr>
                                                <td>{{ $ambassador->ambassador_fullname }}</td>
                                                <td>{{ $ambassador->webCareer->name }}</td>
                                                <td>{{ $ambassador->created_at->format('d/m/Y H:i A') }}</td>
                                                <td>
                                                    <a href="{{ route('workarea.ambassadors.edit', ['id' => $ambassador->id])  }}"
                                                       class="btn btn-info">
                                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                                    </a>
                                                    <button data-action="{{ route('workarea.ambassadors.delete', ['id' => $ambassador->id]) }}"
                                                            class="btn btn-danger btn-delete">
                                                        <i class="fa fa-times i-action-d" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($ambassadors->lastPage()>1)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="dt-content">
                                {!! $ambassadors->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </section>
    </section>

    @component('workarea.modals.delete_item_table')
        @slot('title')
            Eliminar embajador
        @endslot
        @slot('message')
            Desea eliminar el embajador seleccionado?
        @endslot
    @endcomponent
@endsection

@section('js')
    <script src="{{ asset('js/workarea/vendor/toastr.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/scripts/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/scripts/list.js') }}"></script>
    <script>
        $(function () {
            List.deleteButton('.btn-delete', '#modal');
        });
    </script>
@endsection



