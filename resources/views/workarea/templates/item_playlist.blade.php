<div class="row">
    <div class="col-md-3">
        <label class="">Nombre</label>
        <div class="form-group">
            <input class="form-control m-bot15" type="text" name="playlist_title[]">
        </div>
    </div>
    <div class="col-md-6">
        <label class="">URL Spotify</label>
        <div class="form-group">
            <input class="form-control m-bot15" type="text" name="playlist_url[]">
        </div>
    </div>
    <div class="col-md-3">
        <label for=""></label>
        <div class="form-group">
            <button type="button" class="btn btn-danger delete-playlist">
                <i class="glyphicon glyphicon-trash"></i> <span>Eliminar</span>
            </button>
        </div>
    </div>
</div>
