<div class="row">
    <div class="col-md-9">
        <div class="input-group m-bot15">
            <div class="input-group-btn">
                <button type="button" class="btn btn-white dropdown-toggle"
                        data-toggle="dropdown">
                    Seleccione <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    @foreach($networks as $network)
                        <li>
                            <a href="#"
                               data-id="{{ $network->id }}">{!! $network->icon_html !!} {{ $network->name }}</a>
                        </li>
                    @endforeach
                </ul>
                <input type="hidden" name="social_network_type[]">
            </div>
            <input type="text" name="social_network[]" class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <button type="button" class="btn btn-danger delete-network">
            <i class="glyphicon glyphicon-trash"></i> <span>Eliminar</span>
        </button>
    </div>
</div>
