@extends('workarea.layouts.base')

@section('metas')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/toastr.min.css') }}" type="text/css">
@endsection

@section('content')
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Publicación</header>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="dt-content">
                            <form action="{{ route('workarea.posts.index') }}" method="GET">
                                <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-white"><i
                                                        class="fa fa-search"></i></button>
                                        </span>
                                    <input type="text" class="form-control" name="query" placeholder="Buscar publicaciones"
                                           @if(!empty($query)) value="{{ $query }}" @endif>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="dt-content">
                            <div class="pull-left">
                                Mostrando {{ $posts->count() }} de {{ $posts->total() }} publicaciones
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-primary download-excel disabled"><i class="fa fa-download"></i> Descargar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <a href="{{ route('workarea.posts.create') }}" class="btn btn-success">
                                        Crear nueva publicación
                                    </a>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col-sm-12">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Título</th>
                                            <th></th>
                                            <th>Fecha de Publicación</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($posts as $post)
                                            <tr>
                                                <td>{{ $post->title }}</td>
                                                <td></td>
                                                <td>{{ $post->created_at->format('d/m/Y g:i a') }}</td>
                                                <td>
                                                    @if($post->status==1)
                                                        <a href="{{ route('workarea.posts.unpublish', ['id' => $post->id])  }}" class="btn btn-success btn-post-action"><i class="fa fa-eye"></i> </a>
                                                    @elseif($post->status==2)
                                                        <a href="{{ route('workarea.posts.publish', ['id' => $post->id]) }}" class="btn btn-warning btn-post-action"><i class="fa fa-eye-slash"></i></a>
                                                    @endif

                                                    <a href="{{ route('workarea.posts.edit', ['id' => $post->id])  }}" class="btn btn-info"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>

                                                    <button data-action="{{ route('workarea.posts.delete', ['id' => $post->id]) }}" class="btn btn-danger btn-delete">
                                                        <i class="fa fa-times i-action-d" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($posts->lastPage()>1)
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="dt-content">
                            {!! $posts->appends($appends)->render() !!}
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </section>
    </section>

    @component('workarea.modals.delete_item_table')
        @slot('title')
            Eliminar post
        @endslot
        @slot('message')
            Desea eliminar el post seleccionado?
        @endslot
    @endcomponent
@endsection


@section('js')
    <script src="{{ asset('js/workarea/vendor/toastr.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/scripts/list.js') }}"></script>
    <script>
        $(function () {
            List.deleteButton('.btn-delete', '#modal');
        });
    </script>
@endsection


