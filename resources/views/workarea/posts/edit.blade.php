@extends('workarea.layouts.base')

@section('metas')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('css')
    @include('workarea.posts.partials.section_css')
@endsection

@section('content')
    <section id="main-content">
        <section class="wrapper">
            <form class="form" role="form" action="{{ route('workarea.posts.edit', ['id'=>$post->id]) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PATCH">
                <input type="hidden" id="post-id" value="{{ $post->id }}">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <header class="panel-heading">Edición de Entrada</header>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label for="">Título</label>
                                                <input class="form-control m-bot15" type="text" name="title" value="{{ $post->title }}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 m-bot15">
                                                <label for="">Descripción</label>
                                                <textarea class="form-control ckeditor" name="description"
                                                          rows="10">{!! $post->description !!}</textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 m-bot15">
                                                <label for="">Texto despues de galerias</label>
                                                <textarea class="form-control ckeditor" name="after_description"
                                                          rows="10">{!! $post->after_description !!}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <section class="panel">
                            <div class="panel-body">
                                <label for="">Etiquetas</label>
                                <div class="form-group">
                                    <input name="tags" id="tagsinput" class="tagsinput" value="{{ $post->tags }}"/>
                                </div>
                            </div>
                        </section>

                        @component('workarea.posts.partials.seo', [
                        'metaTitle'=>$post->meta_title,
                        'metaDescription' => $post->meta_description,
                        'metaKeywords' => $post->meta_keywords
                        ])
                        @endcomponent

                    </div>
                    <div class="col-sm-4">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Editar</button>
                                            <a href="{{ route('workarea.posts.index') }}"
                                               class="btn btn-danger">Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="">Estado</label>
                                            <select name="status" class="form-control">
                                                <option value="2" @if($post->status==2) selected @endif>Borrador</option>
                                                <option value="1" @if($post->status==1) selected @endif>Publicado</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        @component('workarea.partials.upload_image',['path'=> $post->banner_img])
                                            @slot('title')
                                                Banner
                                            @endslot
                                            @slot('inputName')
                                                banner
                                            @endslot
                                        @endcomponent
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        @component('workarea.partials.upload_image',['path'=> $post->thumb_img])
                                            @slot('title')
                                                Miniatura
                                            @endslot
                                            @slot('inputName')
                                                thumbnail
                                            @endslot
                                        @endcomponent
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label class="">Subcategorias (Mínimo 1) </label>
                                        <div class="form-group">
                                            <select class="select2-subcategories" name="subcategories[]" multiple="multiple">
                                                @foreach($post->webCategories as $category)
                                                <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label class="">Embajadores relacionados (Opcional)</label>
                                        <div class="form-group">
                                            <select class="select2-ambassadors" name="ambassadors[]" multiple="multiple">
                                                @foreach($post->webAmbassadors as $ambassador)
                                                    <option value="{{ $ambassador->id }}" selected>{{ $ambassador->slug }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            @component('workarea.posts.partials.gallery', ['post'=> $post])
            @endcomponent

        </section>
    </section>
@endsection

@section('js')
    @include('workarea.posts.partials.section_js')
@endsection