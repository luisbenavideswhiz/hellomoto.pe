<script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/select2.min.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/jquery.tagsinput.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/ckeditor/ckeditor.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/toastr.min.js') }}"></script>

<script src="{{ asset('js/workarea/vendor/file-uploader/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('js/workarea/vendor/file-uploader/tmpl.min.js') }}"></script>
<script src="{{ asset('js/workarea/vendor/file-uploader/load-image.all.min.js') }}"></script>
<script src="{{ asset('js/workarea/vendor/file-uploader/canvas-to-blob.min.js') }}"></script>

<script src="{{ asset('js/workarea/vendor/file-uploader/jquery.blueimp-gallery.min.js') }}"></script>

<script src="{{ asset('js/workarea/vendor/file-uploader/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('js/workarea/vendor/file-uploader/jquery.fileupload.js') }}"></script>
<script src="{{ asset('js/workarea/vendor/file-uploader/jquery.fileupload-process.js') }}"></script>
<script src="{{ asset('js/workarea/vendor/file-uploader/jquery.fileupload-image.js') }}"></script>
<script src="{{ asset('js/workarea/vendor/file-uploader/jquery.fileupload-audio.js') }}"></script>
<script src="{{ asset('js/workarea/vendor/file-uploader/jquery.fileupload-video.js') }}"></script>
<script src="{{ asset('js/workarea/vendor/file-uploader/jquery.fileupload-validate.js') }}"></script>
<script src="{{ asset('js/workarea/vendor/file-uploader/jquery.fileupload-ui.js') }}"></script>

<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="{{ asset('js/workarea/vendor/file-uploader/jquery.xdr-transport.js') }}"></script>
<![endif]-->

<script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/bootstrap-fileupload.js') }}"></script>

<script id="template-upload" type="text/x-tmpl">
    @include('workarea.templates.multiple_upload')
</script>
<script id="template-download" type="text/x-tmpl">
    @include('workarea.templates.multiple_display')
</script>

<script type="text/javascript" src="{{ asset('js/workarea/scripts/form.js') }}"></script>

<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').prop('content')
            }
        });

        $(".tagsinput").tagsInput();
        $(".select2-subcategories").select2({
            ajax: {
                url: '/workarea/subcategories',
                dataType: 'json'
            }
        });

        $(".select2-ambassadors").select2({
            ajax: {
                url: '/workarea/ambassadors/list',
                dataType: 'json'
            }
        });

        toastr.options = {
            "closeButton": true,
            "positionClass": "toast-top-right",
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        $('#fileupload').fileupload({
            url: '/workarea/posts/gallery',
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            formData: {
                id: $('#post-id').val()
            }
        });

        Form.form('.form', true);
    });
</script>
