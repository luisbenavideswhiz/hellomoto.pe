<div class="panel">
    <header class="panel-heading">
        SEO
        <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-chevron-up"></a>
        </span>
    </header>
    <div class="panel-body" style="display: none">
        <div class="row">
            <div class="col-md-12">
                <label for="">MetaTag Título</label>
                <input class="form-control m-bot15" type="text" name="meta_title" @if(isset($metaTitle)) value="{{ $metaTitle }}" @endif>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="">MetaTag Descripción</label>
                <input class="form-control m-bot15" type="text" name="meta_description" @if(isset($metaDescription)) value="{{ $metaDescription }}" @endif>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="">Palabras clave</label>
                <input id="tagsinput2" class="tagsinput" name="meta_keywords" @if(isset($metaKeywords)) value="{{ $metaKeywords }}" @endif/>
            </div>
        </div>
    </div>
</div>