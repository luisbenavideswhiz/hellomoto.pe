<link rel="stylesheet" href="{{ asset('css/workarea/vendor/select2.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/workarea/vendor/bootstrap-fileupload.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/workarea/vendor/toastr.min.css') }}" type="text/css" />

<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="{{ asset('css/workarea/vendor/file-uploader/blueimp-gallery.min.css') }}">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="{{ asset('css/workarea/vendor/file-uploader/jquery.fileupload.css') }}">
<link rel="stylesheet" href="{{ asset('css/workarea/vendor/file-uploader/jquery.fileupload-ui.css') }}">

<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript>
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/jquery.fileupload-noscript.css') }}">
</noscript>
<noscript>
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/jquery.fileupload-ui-noscript.css') }}">
</noscript>
