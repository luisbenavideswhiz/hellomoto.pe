<div class="modal fade modal-dialog-center in" id="mCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Nuevo</h4>
                </div>
                <form action="{{ route('workarea.promotions.create') }}" id="form-create" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="type_c">Título : </label>
                            <input type="text" id="title_c" name="title" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="title_c">Link : </label>
                            <input type="text" id="link_c" name="link" class="form-control">
                        </div>
                        <div class="form-group">
                            @component('workarea.partials.upload_image', ['id'=>'image_c'])
                                @slot('title')
                                    Imagen
                                @endslot
                                @slot('inputName')
                                    image
                                @endslot
                            @endcomponent
                        </div>
                        <div class="form-group">
                            <label for="status">Estado : </label>
                            <select name="status" id="status_c" class="form-control">
                                <option value="1">Publicado</option>
                                <option value="2">Borrador</option>
                                <option value="3">Slider</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="position">Posición : </label>
                            <input type="text" name="position" id="position_c" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" class="btn btn-default" style="width: 20%">Cerrar</a>
                        <button class="btn btn-success" style="width: 20%"> Crear</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
