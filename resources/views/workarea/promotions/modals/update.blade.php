<div class="modal fade modal-dialog-center in" id="mUpdate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Editar</h4>
                </div>
                <form action="{{ route('workarea.promotions.edit', ['id'=>null]) }}" id="form-update" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" id="id_u">
                    <input type="hidden" name="_method" value="PATCH">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="type_u">Título : </label>
                            <input type="text" id="title_u" name="title" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="title_u">Link : </label>
                            <input type="text" id="link_u" name="link" class="form-control">
                        </div>
                        <div class="form-group">
                            @component('workarea.partials.upload_image', ['id'=>'image_u'])
                                @slot('title')
                                    Imagen
                                @endslot
                                @slot('inputName')
                                    image
                                @endslot
                            @endcomponent
                        </div>
                        <div class="form-group">
                            <label for="status">Estado : </label>
                            <select name="status" id="status_u" class="form-control">
                                <option value="1">Publicado</option>
                                <option value="2">Borrador</option>
                                <option value="3">Slider</option>
                            </select>
                        </div>
                        <div class="form-group" style="display: none;">
                            <label for="position">Posición : </label>
                            <input type="text" name="position" id="position_u" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" class="btn btn-default" style="width: 20%">Cerrar</a>
                        <button class="btn btn-success" style="width: 20%"> Confirmar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
