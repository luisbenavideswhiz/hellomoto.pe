@extends('workarea.layouts.base')

@section('metas')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/bootstrap-fileupload.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/toastr.min.css') }}" type="text/css" />
@endsection

@section('content')
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Promociones</header>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="dt-content">
                            <div class="left">
                                <form action="{{ route('workarea.promotions.index') }}" method="GET">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                                        </span>
                                        <input type="text" name="query" @if(!empty($query)) value="{{ $query }}" @endif class="form-control" placeholder="Buscar promoción">
                                    </div>
                                </form>
                            </div>
                            <div class="right">
                                <a href="#!" class="btn-create btn btn-success">
                                    Crear nueva promoción
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="dt-content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Título</th>
                                    <th></th>
                                    <th>Fecha de Publicación</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($promotions as $promotion)
                                    <tr>
                                        <td>{{ $promotion->title }}</td>
                                        <td></td>
                                        <td>{{ $promotion->created_at->format('d/m/Y g:i a') }}</td>
                                        <td>
                                            @if($promotion->status==1)
                                                <a href="{{ route('workarea.promotions.unpublish', ['id' => $promotion->id])  }}" class="btn btn-success btn-post-action"><i class="fa fa-eye"></i> </a>
                                            @elseif($promotion->status==2)
                                                <a href="{{ route('workarea.promotions.publish', ['id' => $promotion->id]) }}" class="btn btn-warning btn-post-action"><i class="fa fa-eye-slash"></i></a>
                                            @endif

                                            <a href="#!" class="btn btn-info btn-update" data-action="{{ route('workarea.promotions.edit', ['id' => $promotion->id]) }}"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>

                                            <button data-action="{{ route('workarea.promotions.delete', ['id' => $promotion->id]) }}" class="btn btn-danger btn-delete">
                                                <i class="fa fa-times i-action-d" aria-hidden="true"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @if($promotions->lastPage()>1)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="dt-content">
                                {!! $promotions->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </section>
    </section>

    @component('workarea.modals.delete_item_table')
        @slot('title')
            Eliminar post
        @endslot
        @slot('message')
            Desea eliminar el post seleccionado?
        @endslot
    @endcomponent

    @include('workarea.promotions.modals.create')
    @include('workarea.promotions.modals.update')
@endsection


@section('js')
    <script type="text/javascript" src="{{ asset('js/workarea/vendor/bootstrap-fileupload.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/scripts/form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/scripts/list.js') }}"></script>
    <script>
        $(function () {
            List.deleteButton('.btn-delete', '#modal');
            List.createModal('.btn-create', '#mCreate', '#form-create');
            List.updateModal('.btn-update', '#mUpdate', '#form-update', '/workarea/promotions/');
        });
    </script>
@endsection