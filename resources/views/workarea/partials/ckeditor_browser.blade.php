<!DOCTYPE html>
<html lang="es">
<head>
    <title>{{ getenv('APP_TITLE') }}</title>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0, minimum-scale=1.0">

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-reset.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"/>

    <link href="{{ asset('css/workarea/vendor/jquery.fancybox.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/workarea/vendor/gallery.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('css/workarea/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/workarea/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('css/workarea/vendor/slidebars.css') }}" rel="stylesheet">


<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/workarea/html5shiv.js') }}"></script>
    <script src="{{ asset('js/workarea/respond.min.js') }}"></script>
    <![endif]-->


    <script>
        function getUrlParam( paramName ) {
            var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' );
            var match = window.location.search.match( reParam );
            return ( match && match.length > 1 ) ? match[1] : null;
        }
        function returnFileUrl(url) {
            var funcNum = getUrlParam('CKEditorFuncNum');
            window.opener.CKEDITOR.tools.callFunction( funcNum, url);
            window.close();
        }
    </script>
</head>
<body>
<section id="container">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Galería de Imágenes
            </header>
            <div class="panel-body">
                <ul class="grid cs-style-3">
                    @foreach($images as $image)
                    <li>
                        <figure>
                            <img src="{{ url($image->path) }}" alt="{{ $image->title }}">
                            <figcaption>
                                <h3>{{ $image->title }}</h3>
                                <a class="fancybox" onclick="returnFileUrl('{{ url($image->path) }}')" rel="group">Seleccionar imagen</a>
                            </figcaption>
                        </figure>
                    </li>
                    @endforeach
                </ul>
                {{ $images->links() }}

            </div>
        </section>
</section>

<script class="include" type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/jquery.dcjqaccordion.2.7.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/jquery.sparkline.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/jquery.nicescroll.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/slidebars.min.js') }}"></script>

<script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/jquery.fancybox.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/jquery.scrollTo.min.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/respond.min.js') }}"></script>

<script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/modernizr.custom.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/workarea/vendor/toucheffects.js') }}"></script>


<script type="text/javascript">
    $(function() {
        jQuery(".fancybox").fancybox();
    });
</script>
</body>
</html>
