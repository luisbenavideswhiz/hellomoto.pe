<label for="">{{ $title }}</label>
<div class="fileupload fileupload-new" data-provides="fileupload">
    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
        @if(isset($path))
            <img src="{{ url($path) }}" alt=""/>
        @else
            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" @if(isset($id)) id="{{ $id }}" @endif />
        @endif
    </div>
    <div class="fileupload-preview fileupload-exists thumbnail"
         style="max-width: 200px; max-height: 150px; line-height: 20px;">
    </div>
    <div>
        <span class="btn btn-white btn-file">
            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Seleccionar imagen</span>
            <span class="fileupload-exists"><i class="fa fa-undo"></i> Cambiar imagen</span>
            <input type="file" class="default" name="{{ $inputName }}"/>
        </span>
        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">
            <i class="fa fa-trash"></i> Eliminar
        </a>
    </div>
</div>