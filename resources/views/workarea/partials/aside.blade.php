<aside>
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            @if(Auth::guard('workarea')->user()->role_id==1)
                <li>
                    <a href="{{ route('workarea.posts.index') }}">
                        <i class="fa fa-file-o"></i>
                        <span>Publicaciones</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('workarea.ambassadors.index') }}">
                        <i class="fa fa-file-o"></i>
                        <span>Embajadores</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('workarea.promotions.index') }}">
                        <i class="fa fa-file-o"></i>
                        <span>Promociones</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('workarea.trivia.index') }}">
                        <i class="fa fa-file-o"></i>
                        <span>Trivia</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('workarea.users.index') }}">
                        <i class="fa fa-file-o"></i>
                        <span>Usuarios</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('workarea.seo.index') }}">
                        <i class="fa fa-file-o"></i>
                        <span>Seo</span>
                    </a>
                </li>
            @endif
            @if(Auth::guard('workarea')->user()->role_id==1 || Auth::guard('workarea')->user()->role_id==2)
                <li>
                    <a href="{{ route('workarea.motovan.index') }}">
                        <i class="fa fa-file-o"></i>
                        <span>Motovan</span>
                    </a>
                </li>
            @endif
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>