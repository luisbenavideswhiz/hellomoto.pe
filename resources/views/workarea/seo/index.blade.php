@extends('workarea.layouts.base')

@section('metas')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/bootstrap-fileupload.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/toastr.min.css') }}" type="text/css"/>
@endsection

@section('content')
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">SEO</header>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="dt-content">
                            <div class="left">
                                <form action="{{ route('workarea.seo.index') }}" method="GET">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-white">
                                                <i class="fa fa-search"></i></button>
                                        </span>
                                        <input type="text" name="query" @if(!empty($query)) value="{{ $query }}"
                                               @endif class="form-control" placeholder="Buscar web">
                                    </div>
                                </form>
                            </div>
                            <div class="right">
                                <a href="#!" class="btn-create btn btn-success">Crear nuevo seo</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="dt-content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Secci&oacute;n de la web</th>
                                    <th></th>
                                    <th>Fecha de creación</th>
                                    <th class="text-right">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($seo as $key => $value)
                                    <tr>
                                        <td>{{ $value->name }}</td>
                                        <td></td>
                                        <td>{{ $value->created_at->format('d/m/Y g:i a') }}</td>
                                        <td class="text-right">
                                            <a href="#!" class="btn btn-info btn-update"
                                               data-action="{{ route('workarea.seo.edit', ['id' => $value->id]) }}">
                                                <i class="fa fa-pencil" aria-hidden="true"></i> </a>

                                            <button data-action="{{ route('workarea.seo.delete', ['id' => $value->id]) }}"
                                                    class="btn btn-danger btn-delete">
                                                <i class="fa fa-times i-action-d" aria-hidden="true"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @if($seo->lastPage()>1)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="dt-content">
                                {!! $seo->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </section>
    </section>

    @component('workarea.modals.delete_item_table')
        @slot('title')
            Eliminar seo
        @endslot
        @slot('message')
            Desea eliminar el seo seleccionado?
        @endslot
    @endcomponent

    @include('workarea.seo.modals.create')
    @include('workarea.seo.modals.update')
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/workarea/vendor/bootstrap-fileupload.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/vendor/jquery.tagsinput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/scripts/form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/scripts/list.js') }}"></script>
    <script>
        $(".tagsinput").tagsInput();
        $(function () {
            List.deleteButton('.btn-delete', '#modal');
            List.createModal('.btn-create', '#mCreate', '#form-create');
            List.updateModal('.btn-update', '#mUpdate', '#form-update', '/workarea/seo/');
        });
    </script>
@endsection