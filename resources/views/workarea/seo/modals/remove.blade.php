<div class="modal fade modal-dialog-center in" id="mRemove" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Remover</h4>
                </div>
                <form action="" id="form-remove">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <input type="hidden" id="id_r" name="id">
                        <label for="name_r">Seguro que desea eliminar el servicio :
                            <strong id="name_r"></strong>
                        </label>
                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" class="btn btn-default" style="width: 30%">Cerrar</a>
                        <button class="btn btn-warning"> Confirmar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>