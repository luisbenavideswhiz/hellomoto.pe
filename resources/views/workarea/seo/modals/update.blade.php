<div class="modal fade modal-dialog-center in" id="mUpdate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Editar</h4>
                </div>
                <form action="{{ route('workarea.seo.edit', ['id'=>null]) }}" id="form-update" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" id="id_u">
                    <input type="hidden" name="_method" value="PATCH">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Secci&oacute;n de la web: </label>
                            <select id="name_u" name="name" class="form-control" disabled>
                                <option value="">Seleccione</option>
                                <option value="home">Home</option>
                                <option value="promociones">Promociones</option>
                                @foreach($category as $key => $value)
                                    <option value="{{ $value->slug }}">{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Título : </label>
                            <input type="text" id="title_u" name="title" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Url : </label>
                            <input type="text" id="url_u" name="url" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Descripci&oacute;n : </label>
                            <input type="text" id="description_u" name="description" class="form-control">
                        </div>
                        <div class="form-group">
                            @component('workarea.partials.upload_image', ['id'=>'image_u'])
                                @slot('title')
                                    Imagen :
                                @endslot
                                @slot('inputName')
                                    image
                                @endslot
                            @endcomponent
                        </div>
                        <div class="form-group">
                            <label>Alt : </label>
                            <input type="text" id="alt_u" name="alt" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>keywords : </label>
                            <input type="text" id="keywords_u" name="keywords" class="form-control tagsinput">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" class="btn btn-default" style="width: 20%">Cerrar</a>
                        <button class="btn btn-success" style="width: 20%"> Confirmar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>