<div class="modal fade modal-dialog-center in" id="mCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Nuevo</h4>
                </div>
                <form action="{{ route('workarea.seo.create') }}" id="form-create" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Secci&oacute;n de la web : </label>
                            <select class="form-control" id="name_c" name="name">
                                <option value="">Seleccione</option>
                                <option value="home">Home</option>
                                <option value="promociones">Promociones</option>
                                @foreach($category as $key => $value)
                                    <option value="{{ $value->slug }}">{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>T&iacute;tulo : </label>
                            <input type="text" id="title_c" name="title" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Url : </label>
                            <input type="text" id="url_c" name="url" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Descripci&oacute;n : </label>
                            <input type="text" id="description_c" name="description" class="form-control">
                        </div>
                        <div class="form-group">
                            @component('workarea.partials.upload_image', ['id'=>'image_c'])
                                @slot('title')
                                    Imagen :
                                @endslot
                                @slot('inputName')
                                    image
                                @endslot
                            @endcomponent
                        </div>
                        <div class="form-group">
                            <label>Alt : </label>
                            <input type="text" id="alt_c" name="alt" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Keywords : </label>
                            <input type="text" id="keywords_c" name="keywords" class="form-control tagsinput">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" class="btn btn-default" style="width: 20%">Cerrar</a>
                        <button class="btn btn-success" style="width: 20%"> Crear</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>