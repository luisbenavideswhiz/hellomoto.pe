@extends('workarea.layouts.base')

@section('content')
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <aside class="profile-nav col-lg-3">
                    <section class="panel">
                        <div class="user-heading round">
                            <h1>{{ Auth::user()->name }}</h1>
                            <p>{{ Auth::user()->email }}</p>
                        </div>

                    </section>
                </aside>
                <aside class="profile-info col-lg-9">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <section class="panel panel-primary">
                        <div class="panel-heading"> Información personal</div>
                        <div class="panel-body bio-graph-info">
                            <form class="form-horizontal" role="form" action="/workarea/profile/update" id="form_info" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Nombres</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Email</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" readonly value="{{ Auth::user()->email }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button type="submit" class="btn btn-success">Guardar</button>
                                        <button type="button" class="btn btn-default">Cancelar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                    <section>
                        <div class="panel panel-primary">
                            <div class="panel-heading"> Cambiar contraseña</div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" action="/workarea/password/update" id="form_password" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" id="id" value="{{ Auth::user()->id }}">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Contraseña actual</label>
                                        <div class="col-lg-6">
                                            <input type="password" class="form-control" name="current_password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Nueva contraseña</label>
                                        <div class="col-lg-6">
                                            <input type="password" class="form-control" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Confirmar contraseña</label>
                                        <div class="col-lg-6">
                                            <input type="password" class="form-control" name="password_confirmation">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button type="submit" class="btn btn-info">Guardar</button>
                                            <button type="button" class="btn btn-default">Cancelar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </aside>
            </div>
            <!-- page end-->
        </section>
    </section>
@endsection
