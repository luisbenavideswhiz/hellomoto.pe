@extends('workarea.layouts.login')

@section('css')
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-reset.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/workarea/vendor/new-style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/workarea/vendor/new-responsive.css') }}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
@endsection

@section('content')
    <form class="form-signin" action="{{ route('workarea.users.password.change') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="user" value="{{ $user->id }}">

        <h2 class="form-signin-heading">Cambiar contraseña Menutres</h2>
        <div class="login-wrap">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="text" placeholder="Correo electrónico" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" placeholder="Contraseña" class="form-control" name="password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input id="password-confirm" type="password" placeholder="Confirma Contraseña" class="form-control" name="password_confirmation" required>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
            <button class="btn btn-lg btn-login btn-block" type="submit">Cambiar contraseña</button>
        </div>
    </form>

@endsection

@section('js')
    <script class="include" type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
@endsection

