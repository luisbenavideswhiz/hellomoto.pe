@extends('workarea.layouts.base')

@section('metas')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/workarea/vendor/toastr.min.css') }}" type="text/css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endsection

@section('content')
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Usuarios</header>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="dt-content">
                            <form action="{{ route('workarea.users.index') }}" method="GET">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-white dropdown-toggle"
                                                data-toggle="dropdown">
                                            @if(is_null($filter))
                                                Todos los campos <span class="caret"></span>
                                            @else
                                                @foreach($filters as $value)
                                                    @if($filter==$value->id)
                                                        {{ $value->name }} <span class="caret"></span>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </button>
                                        <ul class="dropdown-menu">
                                            @foreach($filters as $value)
                                                <li>
                                                    <a href="#" data-id="{{ $value->id }}">{{ $value->name }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <input type="hidden" name="filter" value="{{ is_null($filter)?0:$filter }}">
                                    </div>
                                    <input type="text" class="form-control" name="query" placeholder="Buscar usuarios"
                                           @if(!empty($query)) value="{{ $query }}" @endif>
                                    <span class="input-group-btn">
                                        <button class="btn btn-white" id="date-range" type="button"><i class="fa fa-calendar"></i></button>
                                        <input type="hidden" name="dates" value="">
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="dt-content">
                            <div class="pull-left">
                                Mostrando {{ $users->count() }} de {{ $users->total() }} usuarios
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-primary download-excel"><i class="fa fa-download"></i> Descargar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="dt-content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Nombres y Apellidos</th>
                                    <th>Documento de Identidad</th>
                                    <th>Sexo</th>
                                    <th>Email</th>
                                    <th>Edad</th>
                                    <th>Fecha de Registro</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ ucwords(mb_strtolower($user->name." ".$user->lastname)) }}</td>
                                        <td>{{ $user->document_type." ".$user->document_number }}</td>
                                        <td>{{ $user->sex }}</td>
                                        <td>{{ strtolower($user->email) }}</td>
                                        <td>{{ !is_null($user->birthdate)?$user->birthdate->format('d/m/Y'):'' }}</td>
                                        <td>{{ $user->created_at->format('d/m/Y H:i A') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @if($users->lastPage()>1)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="dt-content">
                                {!! $users->appends($appends)->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </section>
    </section>
@endsection

@section('js')
    <script src="{{ asset('js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/moment-locale-es.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/workarea/vendor/toastr.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/scripts/list.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/workarea/scripts/form.js') }}"></script>
    <script>
        $(function () {
            Form.selectInputGroup('.input-group-btn');
            $('.download-excel').click(function () {
                List.downloadList('/workarea/users/download');
            });
            $('#date-range').daterangepicker({
                "timePickerIncrement": 1,
                "opens": "left",
                "drops": "down",
                "buttonClasses": "btn btn-sm",
                "applyClass": "btn-success",
                "cancelClass": "btn-default"
            }, function(start, end) {
                $('#date-range').removeClass('btn-white');
                $('#date-range').addClass('btn-danger');
                $('#date-range').parent().find('input').val(start.format('YYYY-MM-DD')+' '+end.format('YYYY-MM-DD'));
            });
        });
    </script>
@endsection
