@extends('workarea.layouts.login')

@section('css')
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-reset.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/workarea/vendor/new-style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/workarea/vendor/new-responsive.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
@endsection

@section('content')
    <form class="form-signin" action="{{ route('workarea.users.login') }}" method="POST">
        {{ csrf_field() }}
        <h2 class="form-signin-heading">hellomoto</h2>
        <div class="login-wrap">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="text" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" name="password" class="form-control" placeholder="Contraseña">
                @if ($errors->has('password'))
                    <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif
            </div>
            <label class="checkbox">
                <input type="checkbox" value="remember-me" name="remember">  Recordarme
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Olvidaste tu contraseña?</a>
                </span>
            </label>
            <button class="btn btn-lg btn-login btn-block" type="submit">Iniciar sesión</button>
        </div>
    </form>

    @include('workarea.modals.recover-password')
@endsection

@section('js')
    <script class="include" type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
@endsection