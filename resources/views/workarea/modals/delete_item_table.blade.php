<div class="modal fade modal-dialog-center" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">
                        {{ $title }}
                    </h4>
                </div>
                <div class="modal-body">
                    {{ $message }}
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default modal-btn-cancel" type="button">Cerrar</button>
                    <button class="btn btn-warning modal-btn-confirm" type="button"> Eliminar</button>
                </div>
            </div>
        </div>
    </div>
</div>