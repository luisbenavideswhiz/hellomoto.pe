<!doctype html>
<html lang="es">
<head>
    <link rel="shortcut icon" href="{{ asset('img/mototimeforjoy/favicon.png') }}" rel="SHORTCUT ICON" type="images/icon"/>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>¡Regala #TimeForJoy esta navidad!</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/mototimeforjoy/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mototimeforjoy/presentational-only.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mototimeforjoy/responsive-full-background-image.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mototimeforjoy/selection-sharer.css') }}"/>
    <meta property="fb:app_id" content="460754127662355"/>
    <meta property="og:url" content="https://mototimeforjoy.hellomoto.pe"/>

    <meta name="keywords" content="¡Haz #TimeForJoy esta navidad!"/>
    <meta name="description"
          content="¡Es la temporada de alegría, regalos y celebración! Y la mejor manera de celebrar es conectar con tu seres queridos, incluso si eso significa desconectarse de tu teléfono por un rato. Ya sea estando con tu familia o poniéndote al día con un viejo amigo, ¡solo hace falta una hora de tu tiempo para que alguien se sienta especial! Por eso, haz el compromiso y regala esos 60 preciados minutos a alguien que quieres."/>
    <meta name="image_src" content="https://www.mototimeforjoy.com/images/shareimage.png"/>
    <meta name="image_url" content="https://www.mototimeforjoy.com/images/shareimage.png"/>

    <!-- Facebook Share -->
    <meta property="og:url" content="https://www.mototimeforjoy.com/index.php"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="¡Haz #TimeForJoy esta navidad!"/>
    <meta property="og:description"
          content="¡Es la temporada de alegría, regalos y celebración! Y la mejor manera de celebrar es conectar con tu seres queridos, incluso si eso significa desconectarse de tu teléfono por un rato. Ya sea estando con tu familia o poniéndote al día con un viejo amigo, ¡solo hace falta una hora de tu tiempo para que alguien se sienta especial! Por eso, haz el compromiso y regala esos 60 preciados minutos a alguien que quieres."/>
    <meta property="og:image" content="https://www.mototimeforjoy.com/images/shareimage.png"/>


    <!-- Twitter Share -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="https://www.mototimeforjoy.com/index.php">
    <meta name="twitter:title" content="¡Haz #TimeForJoy esta navidad!">
    <meta name="twitter:description"
          content="¡Es la temporada de alegría, regalos y celebración! Y la mejor manera de celebrar es conectar con tu seres queridos, incluso si eso significa desconectarse de tu teléfono por un rato. Ya sea estando con tu familia o poniéndote al día con un viejo amigo, ¡solo hace falta una hora de tu tiempo para que alguien se sienta especial! Por eso, haz el compromiso y regala esos 60 preciados minutos a alguien que quieres.">
    <meta name="twitter:image:src" content="https://www.mototimeforjoy.com/images/shareimage.png">

    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-5JC6WBV');</script>
    <!-- End Google Tag Manager -->

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5JC6WBV"
            height="0" width="0" style="display:none;visibility:hidden">
    </iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->


<div class="bg-img">
    <header class="container1">
        <section class="content">
            <div class="container">
                <div class="logo"><img src="{{ asset('img/mototimeforjoy/logo.png') }}"/></div>
            </div>
            <h1>¡Regala <span style="color:#fee600;">#TimeForJoy,</span><br/>esta navidad!</h1>
            <div id="countdown">
                <div id="tiles">
                    <span class="number start_value">
                        @foreach(str_split($count) as $digit)
                            <div class="bg-time">{{ $digit }}</div>
                        @endforeach
                    </span>
                </div>
            </div>
            <input type="hidden" name="startnumber" class="startnumber" value="{{ $count }}"/>
            <h5 class="labels">¡Minutos prometidos!</h5>
            <p>
                ¡Es la temporada de alegría, regalos y celebración! Y la mejor manera de celebrar es conectar con tu seres queridos, incluso si eso significa desconectarse de tu teléfono por un rato. Ya sea estando con tu familia o poniéndote al día con un viejo amigo, ¡solo hace falta una hora de tu tiempo para que alguien se sienta especial! Por eso, haz el compromiso y regala esos 60 preciados minutos a alguien que quieres.
            </p>
        </section>
    </header>

    <div class="green-tab">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center"><h4>Hazlo en 3 simples pasos:</h4></div>

                    <div class="col-md-4 text-center"><h2><img src="{{ asset('img/mototimeforjoy/no1.png') }}"/></h2>
                        <p>En tu muro de Facebook o Instagram contándonos cómo vas a regalar #TimeForJoy en esta navidad.</p></div>

                    <div class="col-md-4 text-center"><h2><img src="{{ asset('img/mototimeforjoy/no2.png') }}"/></h2>
                        <p>
                            Asegúrate de etiquetar a ese ser querido con el que quieres compartir este tiempo especial.
                        </p>
                    </div>
                    <div class="col-md-4 text-center"><h2><img src="{{ asset('img/mototimeforjoy/no3.png') }}"/></h2>
                        <p>
                            No olvides usar el hashtag #TimeForJoy en tu publicación. Cada hashtag significa que estás regalando 60 minutos de tu tiempo.
                        </p>
                    </div>
                    <div class="col-12 text-center">
                        <p style="font-size:13px; padding:15px 0px 5px 0px;">¡A más publicaciones, más #TimeForJoy esta navidad!</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="https://www.facebook.com/MotorolaPE/" target="_blank">
                        <img src="{{ asset('img/mototimeforjoy/facebook-icon.png') }}"/>
                    </a>
                    <a href="https://www.instagram.com/motorolape" target="_blank">
                        <img src="{{ asset('img/mototimeforjoy/instagram-icon.png') }}"/>
                    </a>
                </div>
            </div>
        </div>
    </footer>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src='https://code.jquery.com/jquery-3.3.1.js'></script>
    <script src="{{ asset('js/mototimeforjoy/bootstrap.min.js') }}"></script>
    <script src='{{ asset('js/mototimeforjoy/snowfall.jquery.min.js') }}'></script>
    <script src='{{ asset('js/mototimeforjoy/jquery.easy_number_animate.js') }}'></script>


    <script type='text/javascript'>
        $(document).ready(function () {
            $('.collectonme').hide();
            $(document).snowfall({image: "img/mototimeforjoy/flake.png", minSize: 7, maxSize: 16});
        });
    </script>

    <script>
        $(function () {
            var $number = $('.number')
                , $start_input = $('.start_value')
                , $end_input = $('.end_value')
                , $button_up = $('.up')
                , $button_down = $('.down')
            ;

            function up() {
                var options = {
                    start_value: 300
                    , end_value: 6000
                    , duration: 5000
                };
                $number.easy_number_animate(options);
            }
            $button_up.click(up);

            setInterval(function () {
                get_count();
            }, 20000);

            function get_count() {
                $.get({
                    url: "/timeforjoy/count"
                }).done(function (response) {
                    var options = {
                        start_value: parseInt($('.startnumber').val())
                        , end_value: parseInt(response.data)
                        , duration: 4000
                    };
                    $number.easy_number_animate(options);
                    $('.startnumber').val(parseInt(response.data));
                });
            }
        });
    </script>
    <script src="{{ asset('js/mototimeforjoy/selection-sharer.js') }}"></script>
</body>
</html>
