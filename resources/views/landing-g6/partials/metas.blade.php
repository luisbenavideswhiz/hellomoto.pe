<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<meta property="og:url" content="{{ url('moto-g6') }}">
<meta property="og:type" content="website">
<meta property="og:title" content="Lanzamiento Motorola">
<meta property="og:description" content="Conoce a la nueva familia Moto G6">
<meta property="og:image" content="{{ asset('/img/landing-g6/thumb.png') }}">

<meta name="author" content="WHIZ - Agencia Digital">
<meta name="title" content="Lanzamiento Motorola">
<meta name="description" content="Conoce a la nueva familia Moto G6">
<meta name="keywords" content="motog6, familia, smartphone, familia motog6, lanzamiento, motorola, lo último, nuevo">
