<div class="section background" id="section0">
@include('landing-g6.partials.header')    
<div class="container">
       
        <div class="row body-1">
            <div class="col-md-10 offset-1 col-xs-12 mt-5">
                <div class="d-flex justify-content-start">
                    <img src="{{ asset('img/landing-g6/celulares-portada.png')}}" class="img-port">
                </div>
                <div class="moto">
                    <img src="{{ asset('img/landing-g6/tag-portada.png')}}" class="img-fam">
                    <p>
                        Conoce la nueva gama de smartphones inspirada en ti y vive la experiencia <b>helloyou</b> junto a
                        nuestros embajadores.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="actions show-desk">
                <a href="javascript:void(0)" class="btn btn-red" onClick="$.fn.fullpage.moveTo(2,1);">comenzar</a>
                <a href="javascript:void(0)" class="btn btn-red" onClick="$.fn.fullpage.moveTo(6);">conocer familia</a>
            </div>
            <div class="actions show-res">
            <a href="javascript:void(0)" class="btn btn-red" onClick="$.fn.fullpage.moveTo(2,1);">comenzar</a>
                <a href="javascript:void(0)" class="btn btn-red" onClick="$.fn.fullpage.moveTo(6);">conocer familia</a>
            </div>
        </div>
    </div>
</div>