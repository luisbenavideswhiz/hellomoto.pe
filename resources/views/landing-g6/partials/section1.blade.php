<div class="section" id="section1">
    <div class="slide person-1 bg-1" id="slide1">
            <div class="item-data">
                <div class="data">
                    <div class="datum">
                        <div class="imgemb show-res">
                            <img src="{{ asset('img/landing-g6/ramiro_resp.png')}}" alt="">
                        </div>
                        <div class="titulo">
                            Ramiro Porro
                        </div>
                        <p>
                            "Cambia los fondos de tus fotos como desees con la familia Moto g6."
                        </p>
                        <a href="http://www.motorola.com.pe/ " target="_blank">
                            Conoce más
                        </a>
                    </div>
                </div>
            </div>
            <div class="ramiro_one">
                <div class="img iramiro">
                    <img src="{{ asset('img/landing-g6/ramiro_porro_1.png') }}" >
                </div>
            </div>
    </div>
    <div class="slide person-1 bg-2" id="slide2">
            <div class="item-data">
                <div class="data">
                    <div class="datum">
                        <div class="imgemb show-res">
                            <img src="{{ asset('img/landing-g6/ramiro_resp.png')}}" alt="">
                        </div>
                        <div class="titulo">
                            Ramiro Porro
                        </div>
                        <p>
                            "Cambia los fondos de tus fotos como desees con la familia Moto g6."
                        </p>
                        <a href="http://www.motorola.com.pe/ " target="_blank">
                            Conoce más
                        </a>
                    </div>
                </div>
            </div>
            <div class="ramiro_one">
                <div class="img iramiro">
                    <img src="{{ asset('img/landing-g6/ramiro_porro_1.png') }}" >
                </div>
            </div>
    </div>  
    <div class="slide person-2 bg-1" id="slide3">
            <div class="item-data">
                <div class="data">
                    <div class="datum">
                        <div class="imgemb show-res">
                            <img src="{{ asset('img/landing-g6/alicia_resp.png')}}" alt="">
                        </div>
                        <div class="titulo">
                            Alicia Mercado
                        </div>
                        <p>
                            "Tú eliges dónde va el color con la familia Moto g6."
                        </p>
                        <a href="http://www.motorola.com.pe/ " target="_blank">
                            Conoce más
                        </a>
                    </div>
                </div>
            </div>
            <div class="ramiro_one">
                <div class="img ialicia">
                    <img src="{{ asset('img/landing-g6/ramiro_porro_2.png') }}" >
                </div>
            </div>
    </div>  
    <div class="slide person-2 bg-2" id="slide4">
            <div class="item-data">
                <div class="data">
                    <div class="datum">
                        <div class="imgemb show-res">
                            <img src="{{ asset('img/landing-g6/alicia_resp.png')}}" alt="">
                        </div>
                        <div class="titulo">
                            Alicia Mercado
                        </div>
                        <p>
                            "Tú eliges dónde va el color con la familia Moto g6."
                        </p>
                        <a href="http://www.motorola.com.pe/ " target="_blank">
                            Conoce más
                        </a>
                    </div>
                </div>
            </div>
            <div class="ramiro_one">
                <div class="img ialicia">
                    <img src="{{ asset('img/landing-g6/ramiro_porro_2.png') }}" >
                </div>
            </div>
    </div> 
    <div class="slide person-3 bg-1" id="slide5">
            <div class="item-data">
                <div class="data">
                    <div class="datum">
                        <div class="imgemb show-res">
                            <img src="{{ asset('img/landing-g6/entes_resp.png')}}" alt="">
                        </div>
                        <div class="titulo">
                            Entes
                        </div>
                        <p>
                            "Enfócate en lo que más te gusta con la familia Moto g6."
                        </p>
                        <a href="http://www.motorola.com.pe/ " target="_blank">
                            Conoce más
                        </a>
                    </div>
                </div>
            </div>
            <div class="ramiro_one">
                <div class="img ientes">
                    <img src="{{ asset('img/landing-g6/ramiro_porro_3.png') }}">
                </div>
            </div>
    </div>
    <div class="slide person-3 bg-2" id="slide6">
            <div class="item-data">
                <div class="data">
                    <div class="datum">
                        <div class="imgemb show-res">
                            <img src="{{ asset('img/landing-g6/entes_resp.png')}}" alt="">
                        </div>
                        <div class="titulo">
                            Entes
                        </div>
                        <p>
                            "Enfócate en lo que más te gusta con la familia Moto g6."
                        </p>
                        <a href="http://www.motorola.com.pe/ " target="_blank">
                            Conoce más
                        </a>
                    </div>
                </div>
            </div>
            <div class="ramiro_one">
                <div class="img ientes">
                    <img src="{{ asset('img/landing-g6/ramiro_porro_3.png') }}">
                </div>
            </div>
    </div>  
</div>        