<header class="header header-fixed">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-xs-6 col mt-4">
                <img src="{{ asset('img/landing-g6/logo.svg') }}" class="img-fluid">
            </div>
            <div class="col-xs-6 col mt-4 text-right text-white redes">
                <a href="https://www.facebook.com/MotorolaPE" target="_blank"> <i class="fab fa-facebook-f "></i></a>
                <a href="https://www.instagram.com/motorolape/" target="_blank"> <i class="fab fa-instagram"></i></a>
                <a href="https://www.youtube.com/user/motorolamobilityperu" target="_blank">
                    <i class="fab fa-youtube "></i></a>
            </div>
        </div>  
    </div>
</header>