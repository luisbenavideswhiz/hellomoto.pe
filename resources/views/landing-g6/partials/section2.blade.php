<div class="section" id="section2">
    <section class="container-fluid sinpadding">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <a href="javascript:void(0)" class="imagen-item">
                    <div class="imagen" style="width: 100%;">
                        <img src="{{ asset('img/landing-g6/1.1.png')}}" width="100%" class="img-fluid img-first"/>
                        <img src="{{ asset('img/landing-g6/1.2.png')}}" width="100%" class="img-fluid img-hover"/>
                    </div>
                    <div class="hover">
                        <p>Pantalla Max Vision 5.7 y 5.9” HD/Full HD</p>
                    </div>
                    <div class="this_ico">
                        <span></span>
                        <span></span>
                    </div>
                    <div class="show_hover"></div>
                </a>
            </div>
            <div class="col-md-4 col-xs-12">
                <a href="javascript:void(0)" class="imagen-item">
                    <div class="imagen" style="width: 100%">
                        <img src="{{ asset('img/landing-g6/2.1.png')}}" width="100%" class="img-fluid img-first"/>
                        <img src="{{ asset('img/landing-g6/2.2.png')}}" width="100%" class="img-fluid img-hover"/>
                    </div>
                    <div class="hover">
                        <p>
                            Dual Creative Camera System
                        </p>
                    </div>
                    <div class="this_ico">
                        <span></span>
                        <span></span>
                    </div>
                    <div class="show_hover"></div>
                </a>
            </div>
            <div class="col-md-4 col-xs-12">
                <a href="javascript:void(0)" class="imagen-item">
                    <div class="imagen" style="width: 100%">
                        <img src="{{ asset('img/landing-g6/3.1.png')}}" width="100%" class="img-fluid img-first"/>
                        <img src="{{ asset('img/landing-g6/3.2.png')}}" width="100%" class="img-fluid img-hover"/>
                    </div>
                    <div class="hover">
                        <p>
                            Doble lente para enfoque rápido
                        </p>
                    </div>
                    <div class="this_ico">
                        <span></span>
                        <span></span>
                    </div>
                    <div class="show_hover"></div>
                </a>
            </div>
            <div class="col-md-4 col-xs-12">
                <a href="javascript:void(0)" class="imagen-item">
                    <div class="imagen" style="width: 100%">
                        <img src="{{ asset('img/landing-g6/4.1.png')}}" width="100%" class="img-fluid img-first"/>
                        <img src="{{ asset('img/landing-g6/4.2.png')}}" width="100%" class="img-fluid img-hover"/>
                    </div>
                    <div class="hover">
                        <p>
                            Más batería de 3,000 y 4,000 mAh
                        </p>
                    </div>
                    <div class="this_ico">
                        <span></span>
                        <span></span>
                    </div>
                    <div class="show_hover"></div>
                </a>
            </div>
            <div class="col-md-4 col-xs-12">
                <a href="javascript:void(0)" class="imagen-item">
                    <div class="imagen" style="width: 100%">
                        <img src="{{ asset('img/landing-g6/5.1.png')}}" width="100%" class="img-fluid img-first"/>
                        <img src="{{ asset('img/landing-g6/5.2.png')}}" width="100%" class="img-fluid img-hover"/>
                    </div>
                    <div class="hover">
                        <p>
                            Más elegante. 3D Glass Design
                        </p>
                    </div>
                    <div class="this_ico">
                        <span></span>
                        <span></span>
                    </div>
                    <div class="show_hover"></div>
                </a>
            </div>
            <div class="col-md-4 col-xs-12">
                <a href="javascript:void(0)" class="imagen-item">
                    <div class="imagen" style="width: 100%">
                        <img src="{{ asset('img/landing-g6/6.1.png')}}" width="100%" class="img-fluid img-first"/>
                        <img src="{{ asset('img/landing-g6/6.2.png')}}" width="100%" class="img-fluid img-hover"/>
                    </div>
                    <div class="hover">
                        <p>
                            Pantallas sin límites 5.7 y 5.9” HD/Full HD
                        </p>
                    </div>
                    <div class="this_ico">
                        <span></span>
                        <span></span>
                    </div>
                    <div class="show_hover"></div>
                 </a>                    
            </div>
        </div>
    </section>
</div>