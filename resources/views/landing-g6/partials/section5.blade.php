<div class="section" id="section5">
    <div class="container">
        <h2>¡Elige el que más<strong> te identifica!</strong></h2>
        <div class="row d-xs-none">
            <div class="col-md-4 col-sm-12">
                <div class="preview">
                    <img src="{{ asset('img/landing-g6/motog6play-logo.png')}}">
                    <img src="{{ asset('img/landing-g6/motog6-play.png')}}">
                    <a href="http://www.motorola.com.pe/moto-g6-play/p" class="btn btn-red">VER MÁS</a>
                </div>
                <div class="available">
                    <p>Disponible en: </p>
                    <a href="http://catalogo.claro.com.pe/equipo/37756/celulares/todos/motorola-moto-g6-play">
                        <img src="{{ asset('img/landing-g6/op-claro.png')}}">
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="preview">
                    <img src="{{ asset('img/landing-g6/motog6-logo.png')}}">
                    <img src="{{ asset('img/landing-g6/motog6.png')}}">
                    <a href="http://www.motorola.com.pe/moto-g6/p" class="btn btn-red">VER MÁS</a>
                </div>
                <div class="available">
                    <p>Disponible en: </p>
                    <a href="http://catalogo.movistar.com.pe/motorola-moto-g6?p=plan-elige-mas-s-1299-129.9-26-190">
                    <img src="{{ asset('img/landing-g6/op-movistar.png')}}">
                    </a>
                    <a href="http://www.entel.pe/promo/moto-g6/">
                    <img src="{{ asset('img/landing-g6/op-entel.png')}}">
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="preview">
                     <img src="{{ asset('img/landing-g6/motog6plus-logo.png')}}">
                    <img src="{{ asset('img/landing-g6/motog6 plus.png')}}">
                    <a href="http://www.motorola.com.pe/moto-g6-plus/p" class="btn btn-red">VER MÁS</a>
                </div>
                <div class="available">
                    <p>Disponible en: </p>
                    <a href=" http://catalogo.claro.com.pe/equipo/37528/celulares/todos/motorola-moto-g6-plus--plus-parlante-bluetooth">
                    <img src="{{ asset('img/landing-g6/op-claro.png')}}">
                    </a>
                </div>
            </div>
        </div>
        <div class="d-xl-none">
            <div class="slick_rslider" id="slick_rslider">
                <div class="item">
                    <div class="col-10 offset-1">
                        <img src="{{ asset('img/landing-g6/motog6play-logo.png')}}">
                        <img src="{{ asset('img/landing-g6/motog6-play.png')}}">
                        <a href="http://www.motorola.com.pe/moto-g6-play/p" class="btn btn-red">VER MÁS</a>
                        <div class="available">
                            <p>Disponible en: </p>
                            <a href="http://catalogo.claro.com.pe/equipo/37756/celulares/todos/motorola-moto-g6-play">
                            <img src="{{ asset('img/landing-g6/op-claro.png')}}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="col-10 offset-1">
                        <img src="{{ asset('img/landing-g6/motog6-logo.png')}}">
                        <img src="{{ asset('img/landing-g6/motog6.png')}}">
                        <a href="http://www.motorola.com.pe/moto-g6/p" class="btn btn-red">VER MÁS</a>
                        <div class="available">
                            <p>Disponible en: </p>
                            <a href="http://catalogo.movistar.com.pe/motorola-moto-g6?p=plan-elige-mas-s-1299-129.9-26-190">
                            <img src="{{ asset('img/landing-g6/op-movistar.png')}}">
                            </a>
                            <a href="http://www.entel.pe/promo/moto-g6/">
                            <img src="{{ asset('img/landing-g6/op-entel.png')}}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="col-10 offset-1">
                        <img src="{{ asset('img/landing-g6/motog6plus-logo.png')}}">
                        <img src="{{ asset('img/landing-g6/motog6 plus.png')}}">
                        <a href="http://www.motorola.com.pe/moto-g6-plus/p" class="btn btn-red">VER MÁS</a>
                        <div class="available">
                            <p>Disponible en: </p>
                            <a href=" http://catalogo.claro.com.pe/equipo/37528/celulares/todos/motorola-moto-g6-plus--plus-parlante-bluetooth">
                            <img src="{{ asset('img/landing-g6/op-claro.png')}}">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="container">
            <div class="content">
                <img src="{{ asset('img/landing-g6/hellomoto.pe.png')}}">
                <span>Todos los derechos reservados - Lima, 2018</span>
                <a href="http://whiz.pe">Powered by WHIZ</a>
            </div>
        </div>
    </footer>
</div>