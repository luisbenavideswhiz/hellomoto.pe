<!doctype html>
<html lang="es">
<head>
    <title>Lanzamiento Motorola</title>
    @include('landing-g6.partials.metas')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="{{ asset('css/landing-g6/jquery.fullPage.css') }}" rel="stylesheet">
    @section('css')
    @show
    @include('web.partials.seo.analytics')
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5JC6WBV"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="loading">
        <div class="load">
            <img src="{{ asset ('img/landings/motogift/loading.svg') }}" alt="">
        </div>
    </div>
    
    @yield('content')
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>
    @section('js')
    @show
</body>
</html>