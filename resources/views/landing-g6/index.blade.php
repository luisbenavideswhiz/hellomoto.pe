@extends('landing-g6.layouts.base')

@section('css')
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}" type="text/css">
    <link href="{{ asset('css/landing-g6/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/landing-g6/responsive.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div id="fullpage">
        @include('landing-g6.partials.section0')
        @include('landing-g6.partials.section1')
        @include('landing-g6.partials.section2')
        @include('landing-g6.partials.section3')
        @include('landing-g6.partials.section4')
        @include('landing-g6.partials.section5')
    </div>
    <footer class="show-res">
        <div class="container">
            <div class="content">
                <img src="{{ asset('img/landing-g6/hellomoto.pe.png')}}">
                <span>Todos los derechos reservados - Lima, 2018</span>
                <a href="http://whiz.pe">Powered by Whiz</a>
            </div>
        </div>
    </footer>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/landing-g6/jquery.fullPage.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/landing-g6/youtube.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/landing-g6/app.js') }}"></script>
@endsection
