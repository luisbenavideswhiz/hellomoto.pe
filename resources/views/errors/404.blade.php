@extends('web.layouts.web')

@section('og_tags')
    <meta property="og:url" content="{{ url('/') }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="hellomoto Perú">
    <meta property="og:description" content="Comunidad peruana de HelloMoto donde encontaras información sobre lo que pasa en tu ciudad siempre al lado de un celular motorola ¡Únete a la comunidad hellomoto Perú y descubre el otro lado de la ciudad!">
    <meta property="og:image" content="{{ url('/img/200x200.jpg') }}">
@endsection

@section('content')
    <div class="home">
        <div class="container-fluid">
            <div class="row">
                <div class="container error">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="img">
                                <img src="{{ asset('img/web/error404.png') }}" alt="">
                            </div>
                            <div class="content">
                                <p>
                                    <span>Vaya</span>, parece que esta página ha estado dando una vuelta por la ciudad.
                                </p>
                                <p>
                                    ¿Qué tal si nos reconectamos con la nuestra?
                                </p>
                                <p>
                                    Descubre nuevas maneras de hacerlo en <a href="{{ url('/') }}">hellomoto</a>.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection