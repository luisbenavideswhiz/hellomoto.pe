<!DOCTYPE html>
<html lang="es">
<head>
    @include('motovan.partials.fb.pixel.index')
    @include('motovan.partials.metas')
    @include('web.partials.cannonical')
    @include('motovan.partials.styles')
    @include('web.partials.seo.analytics')
</head>
<body @section('headerclass')@show>

@include('motovan.partials.header')

<div class="loading">
	<div class="load">
		<img src="{{ asset ('img/landings/motovan/loading.svg') }}" alt="">
	</div>
</div>

@yield('content')

@include('motovan.partials.footer')
@include('motovan.partials.scripts')

</body>
</html>