@extends('motovan.layouts.base')

@section('headerclass')
class="home"
@endsection

@section('content')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-5">
                <div class="img">
                    <img src="{{ asset ('img/landings/motovan/img.jpg') }}" alt="">
                </div>
            </div>
            <div class="col-xs-12 col-md-7">
                <div class="form">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Regístrate</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Inicia sesión</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <form action="" method="post" id="landing">
                                {{ csrf_field() }}
                                <div class="col2">
                                    <div class="input-group">
                                        <span>Nombre</span>
                                        <input type="text" id="name" name="name" class="validate">
                                    </div>
                                    <div class="input-group">
                                        <span>Apellido</span>
                                        <input type="text" id="last_name" name="last_name" class="validate">
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="input-group">
                                        <span>DNI</span>
                                        <input type="text" id="document_number" name="document_number" class="validate">
                                    </div>
                                    <div class="input-group">
                                        <span>Fecha de nacimiento (*)</span>
                                        <div class="input-group">
                                            <input type="text" id="birthdate" class="validate" data-format="YYYY-MM-DD" data-template="DD MMMM YYYY" name="birthdate">
                                        </div>
                                    </div>
                                </div>
                                <div class="col1">
                                    <div class="input-group">
                                        <span>Área</span>
                                        <input type="text" id="area" name="area" class="validate">
                                    </div>
                                </div>
                                <div class="col1">
                                    <div class="input-group">
                                        <span>Email</span>
                                        <input type="text" id="email" name="email" class="validate">
                                    </div>
                                </div>
                                <div class="col1">
                                    <div class="input-group">
                                        <span>Número de celular</span>
                                        <input type="text" id="phone" name="phone" class="validate">
                                    </div>
                                </div>
                                <div class="col1">
                                    <div class="input-group">
                                        <label for="accept_emails" class="flexme">
                                    <span>
                                        Acepto los <a href="https://www.motorola.com.pe/institucional/politica-de-privacidad" target="_blank">términos y condiciones</a>
                                    </span>
                                            <input type="checkbox" name="terms_conditions" value="1" id="terms_conditions" class="validate">
                                        </label>
                                    </div>
                                </div>
                                <div class="col1 actions">
                                    <div class="input-group">
                                        <button type="submit" class="btn btn-submit">Registrarse</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <form action="{{ route('motovan.login') }}" method="POST" id="login">
                                {{ csrf_field() }}
                                <div class="col1">
                                    <div class="input-group">
                                        <span>Correo</span>
                                        <input type="text" id="email_l" name="email" class="validate">
                                    </div>
                                </div>
                                <div class="col1">
                                    <div class="input-group">
                                        <span>DNI</span>
                                        <input type="password" id="document_number_l" name="document_number" class="validate">
                                    </div>
                                </div>
                                <div class="col1 actions mod">
                                    <div class="input-group">
                                        <button type="submit" class="btn btn-submit">Login</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection