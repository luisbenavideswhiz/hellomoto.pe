<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="rede">
                    <a href="https://www.facebook.com/MotorolaPE" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="https://www.instagram.com/motorolape/" target="_blank">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="https://www.youtube.com/user/motorolamobilityperu" target="_blank">
                        <i class="fa fa-youtube"></i>
                    </a>
                </div>
                <div class="powered">
                    <span>
                        Powered by <a class="whiz" data-href="http://whiz.pe" data-target="_blank"><img src="{{ asset('img/landings/motovan/logo_whiz.svg') }}" alt="whiz logo"></a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</footer>