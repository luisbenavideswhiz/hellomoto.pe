<header>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="header">
                    <div class="logo">
                        <img src="{{ asset ('img/landings/motovan/logo.svg') }}" alt="">
                    </div>
                    <h2>
                        La mejor experiencia en la Motovan
                    </h2>
                </div>
            </div>
        </div>
    </div>
</header>
