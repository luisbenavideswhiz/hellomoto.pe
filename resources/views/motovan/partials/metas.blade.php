<title>hellomoto.pe</title>

<meta charset="utf-8">
<meta name="viewport"
      content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0, minimum-scale=1.0">
<meta property="og:url" content="{{ url('sorteos/motogift') }}">
<meta property="og:type" content="website">
<meta property="og:title"
      content="Sorteo hellomoto Perú | MotoGitf | Moto G5 Plus">
<meta property="og:description"
      content="El Team Motorola te invita a vivir estas 4 experiencias, compra tu Moto G5 Plus participa y gana, fiorella cayo, luis horna, leilani aguirre, handra, miguel rodriguez, entes">
<meta property="og:image" content="{{ asset('/img/landings/motogift/thumb.png') }}">

<meta name="author" content="WHIZ - Agencia Digital">
<meta name="title" content="Sorteo hellomoto Perú | MotoGitf | Moto G5 Plus">
<meta name="description"
      content="El Team Motorola te invita a vivir estas 4 experiencias, compra tu Moto G5 Plus participa y gana, fiorella cayo, luis horna, leilani aguirre, handra, miguel rodriguez, entes">
<meta name="keywords" content="Regalo, Sorteo, Motorola, hellomoto, fiorella cayo, luis horna, leilani aguirre, handra, miguel  rodriguez, entes">

<meta name="csrf-token" content="{{ csrf_token() }}">