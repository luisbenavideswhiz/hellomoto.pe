@extends('motovan.layouts.base')

@section('headerclass')
    class="home"
@endsection

@section('content')
<div class="wrapper modd">
	<div class="container">
		<div class="row">
            <div class="col-xs-12 col-md-5">
                <div class="img">
                    <img src="{{ asset ('img/landings/motovan/img.jpg') }}" alt="">
                </div>
            </div>
			<div class="col-xs-12 col-md-7">
                <div class="form">
                    <div class="content">
                        <p>
                            <span>Hey,</span> gracias por inscribirte. Si eres de los que se olvidan las fechas, agéndalo <br>para que no se te pase. ¡Nos vemos!
                        </p>
                        <div class="addtogoogle" data-location="{{ $training->location }}" data-date="{{ $training->date->format('Ymd\\THi00\\Z').'/'.$training->date->format('Ymd\\THi00\\Z') }}">
                            <span>Añadir evento a</span>
                            <img src="{{ asset ('img/landings/motovan/googlecalendar.jpg') }}" alt="">
                        </div>
                        <a href="{{ url('/motovan/inscribete') }}">Finalizar</a>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>

@endsection