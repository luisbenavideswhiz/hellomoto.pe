@extends('motovan.layouts.base')

@section('headerclass')
    class="home"
@endsection

@section('content')
<div class="wrapper mod">
	<div class="container">
		<div class="row">
            <div class="col-xs-12">
                <div class="titulo mod">
                    <h3>Horarios</h3>
                </div>
                <div class="title">
                    <div class="filter" style="display: none">
                        <select name="" id="">
                            <option value="">Filtrar por...</option>
                        </select>
                    </div>
                </div>
                <div class="items">
                    @foreach($trainings as $training)
                    <div class="item">
                        <h3>{{ $training->date_formatted }}</h3>
                        <div class="datum">
                            <div class="data">
                                <i class="fa fa-clock-o"></i>
                                {{ $training->time_formatted }}
                            </div>
                            <div class="data">
                                <i class="fa fa-map-marker"></i>
                                {{ $training->location }}
                            </div>
                            <div class="data">
                                <i class="fa fa-check-circle"></i>
                                Disponible: {{ $training->remaining_coupons }}
                            </div>
                            <div class="data">
                                @if($training->remaining_coupons>0)
                                    @if(!Auth::user()->hasTraining($training->id))
                                        <button type="button" class="btn-register" data-training="{{ $training->id }}">Registrarse</button>
                                    @else
                                        <button type="button" class="registrado" disabled>Registrado</button>
                                    @endif
                                @else
                                    <button type="button" class="agotado" disabled>Agotado</button>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                {{ $trainings->links() }}
            </div>
        </div>
	</div>
</div>
@endsection