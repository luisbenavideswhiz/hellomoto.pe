<!DOCTYPE html>
<html lang="es">
<head>

    {{--@component('landings.comunidad.partials.google.analytics.script')--}}
    {{--@endcomponent--}}

    {{--@component('landings.comunidad.partials.google.tag-manager.script')--}}
    {{--@endcomponent--}}

    @component('landings.comunidad.partials.fb.pixel.index')
    @endcomponent

    @component('landings.comunidad.partials.metas')
    @endcomponent

    @component('landings.comunidad.partials.styles')
    @endcomponent

</head>
<body id="home" class="home">

{{--@component('landings.comunidad.partials.google.tag-manager.code')--}}
{{--@endcomponent--}}

@component('landings.comunidad.partials.update-your-browser')
@endcomponent

<div class="motorola">
    <div class="wrapper">
        <div class="container">
            <div class="row">
                @component('landings.comunidad.partials.header')
                @endcomponent
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div id="primera">
                        <form role="form" class="content">
                            {{ csrf_field() }}
                            <div class="navc show-res">
                                <nav>
                                    <span class="right">Síguenos en:</span>
                                    <ul>
                                        <li>
                                            <a href="https://www.facebook.com/MotorolaPE" target="_blank">
                                                <img src="{{ asset('img/landings/comunidad/social_1.svg') }}" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/motorolape/" target="_blank">
                                                <img src="{{ asset('img/landings/comunidad/social_3.svg') }}" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/MotorolaPE" target="_blank">
                                                <img src="{{ asset('img/landings/comunidad/social_2.svg') }}" alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="header show-desk">
                                <h3>Inscríbete y participa del sorteo de un Moto Z play con Motomod JBL</h3>
                                <p>Registra tus datos aquí</p>
                            </div>
                            <div class="header show-res">
                                <h3>
                                    Inscríbete y participa <br>
                                    del sorteo de un  <br>
                                    Moto Z play con  <br>
                                    Motomod JBL
                                </h3>
                                <p>Registra tus datos aquí</p>
                            </div>
                            <div class="form">
                                <div class="form-group col2">
                                    <div class="input-field">
                                        <span>Nombres(*)</span>
                                        <input type="text" class="form-control info validate" id="name" name="name" placeholder="Nombre" >
                                    </div>
                                    <div class="input-field">
                                        <span>Apellidos(*)</span>
                                        <input type="text" class="form-control info validate" id="lastname" name="lastname" placeholder="Apellido" >
                                    </div>
                                </div>
                                <div class="form-group col2">
                                    <div class="input-field">
                                        <span>G&eacute;nero</span>
                                        <div class="form-group col2 gender">
                                            <div class="input-field">
                                                <label for="gender_id_m"><input type="radio" class="validate" id="gender_id_m" name="gender_id" value="1"> Hombre</label>
                                            </div>
                                            <div class="input-field">
                                                <label for="gender_id_f"><input type="radio" class="validate" id="gender_id_f" name="gender_id" value="2"> Mujer</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-field">
                                        <span>DNI(*)</span>
                                        <input type="text" class="form-control info validate" id="document" name="document" placeholder="DNI" data-url="/comunidad/validate/dni">
                                    </div>
                                </div>
                                <div class="form-group col2">
                                    <div class="input-field">
                                        <span>N&deg; de celular(*)</span>
                                        <input type="text" class="form-control info validate" id="phone" name="phone" placeholder="Celular" >
                                    </div>
                                    <div class="input-field">
                                        <span>Operador</span>
                                        <select name="operator_id" id="operator_id" class="info validate" >
                                            <option value="">Operador</option>
                                            @foreach($operator as $key => $value)
                                                <option value="{{ $value->id }}">{{ $value->value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-field">
                                        <span>Email(*)</span>
                                        <input type="email" class="form-control info validate" id="email" name="email" placeholder="Email"  data-url="/comunidad/validate/email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-field">
                                        <strong>Aceptar correos electr&oacute;nicos:</strong>
                                        <label for="accept_email_1"><input type="radio" class="form-check-input validate" id="accept_email_1" name="accept_email" value="1"> Si </label>
                                        <label for="accept_email_0"><input type="radio" class="form-check-input validate" id="accept_email_0" name="accept_email" value="0"> No </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-field">
                                        <p class="fs12">
                                            Deseo recibir informaci&oacute;n de Motorola por email acerca de productos y ofertas
                                            especiales.
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group mb0">
                                    <div class="input-field">
                                        <span>Fecha de nacimiento</span>
                                    </div>
                                </div>
                                <div class="form-group col3">
                                    <div class="input-field">
                                        <select name="day" class="info validate" id="day" >
                                            <option value="">DD</option>
                                            @for($i=1;$i<=31;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="input-field">
                                        <select name="month" class="info validate" id="month" >
                                            <option value="">MM</option>
                                            <option value="01">Enero</option>
                                            <option value="02">Febrero</option>
                                            <option value="03">Marzo</option>
                                            <option value="04">Abril</option>
                                            <option value="05">Mayo</option>
                                            <option value="06">Junio</option>
                                            <option value="07">Julio</option>
                                            <option value="08">Agosto</option>
                                            <option value="09">Setiembre</option>
                                            <option value="10">Octubre</option>
                                            <option value="11">Noviembre</option>
                                            <option value="12">Diciembre</option>
                                        </select>
                                    </div>
                                    <div class="input-field">
                                        <select name="year" class="info validate" id="year" >
                                            <option value="">AAAA</option>
                                            @for($i=2001;$i>=1900;$i--)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-field">
                                        <strong>Verificaci&oacute;n de edad:</strong>
                                        <label for="age_verification_1"><input type="radio" class="form-check-input validate" id="age_verification_1" name="age_verification" value="1"> Si </label>
                                        <label for="age_verification_0"> <input type="radio" class="form-check-input validate" id="age_verification_0" name="age_verification" value="0"> No </label>
                                    </div>
                                </div>
                                <div class="form-group mb0">
                                    <p>
                                        Confirmo que soy mayor de 16 a&ntilde;os.*
                                    </p>
                                </div>
                                <div class="form-group">
                                    <p class="fs12">
                                        Nadie menor de 16 a&ntilde;os podr&aacute; confirmar o presentar ning&uacute;n tipo de informaci&oacute;n en
                                        los sitios web de Motorola. Por favor haz click en el recuadro correspondiente
                                        para confirmar que eres mayor de 16 a&ntilde;os.
                                    </p>
                                </div>
                                <div class="form-group col2">
                                    <div class="input-field">
                                        <div class="links">
                                            <a href="https://www.motorola.com.pe/legal/terms-of-use" target="_blank">> <u>T&eacute;rminos y condiciones</u></a>
                                            <a href="https://www.motorola.com.pe/legal/privacy-policy" target="_blank">> <u>Pol&iacute;tica de privacidad</u></a>
                                        </div>
                                    </div>
                                    <div class="input-field">
                                        <a href="#" id="register-btn" data-url="/landings/comunidad/register" class="btn btn-primary">
                                            <strong>Enviar</strong>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    @component('landings.comunidad.partials.content')
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
    @component('landings.comunidad.partials.footer')
    @endcomponent
</div>

@component('landings.comunidad.partials.scripts')
@endcomponent

</body>
</html>
