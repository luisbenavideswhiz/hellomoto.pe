<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no"/>
    <!--[if !mso]>
            <!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!--
            <![endif]--><!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <title></title>
    <style type="text/css">div, p, a, li, td {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        body {
            min-width: 100% !important;
            Margin: 0;
            padding: 0;
        }
    </style>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css" media="all">
        td[class="outlook-f"] {
            width: 244px !important;
        }

        td[class="outlook-c"] {
            width: 500px !important;
        }

        @media only screen and (max-width: 480px) {
            td[class="outlook-f"] {
                width: 100% !important
            }
        }
    </style>
    <![endif]-->
</head>
<body bgcolor="#ffffff" marginleft="0" marginright="0" margintop="0" style="margin: 0px; padding: 0px; width: 100% !important;">
    <p><span class="hidesumary" style="display: none;"></span></p>
    <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="">
        <tbody style="">
            <tr>
                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="640">
                        <tbody>
                            <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="w100pc">
                                        <tr>
                                            <td valign="top">
                                                <a href="#!" style="line-height: 0px;">
                                                    <img src="{{ asset('img/landings/comunidad/emails/parte1.jpg') }}" alt="" style="display: block;" /></a>
                                            </td>
                                            <td valign="top">
                                                <img src="{{ asset('img/landings/comunidad/emails/parte2.jpg') }}" alt="" style="display: block;" />
                                            </td>
                                            <td valign="top">
                                                <a href="https://www.facebook.com/MotorolaPE" target="_blank" style="line-height: 0px;">
                                                    <img src="{{ asset('img/landings/comunidad/emails/parte3.jpg') }}" alt="" style="display: block;" /></a>
                                            </td>
                                            <td valign="top">
                                                <a href="https://www.instagram.com/motorolape/" target="_blank" style="line-height: 0px;">
                                                    <img src="{{ asset('img/landings/comunidad/emails/parte4.jpg') }}" alt="" style="display: block;" /></a>
                                            </td>
                                            <td valign="top">
                                                <a href="https://twitter.com/MotorolaPE" target="_blank" style="line-height: 0px;">
                                                    <img src="{{ asset('img/landings/comunidad/emails/parte5.jpg') }}" alt="" style="display: block;" /></a>
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="w100pc">
                                        <tr>
                                            <td>
                                                <img src="{{ asset('img/landings/comunidad/emails/parte6.jpg') }}" alt="" style="display: block;" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="w90pc" width="520" style="width: 640px;height: 270px;background-color:#f04187;">
                                        <tbody>
                                            <tr>
                                                <td width="50">&nbsp;</td>
                                                <td style="font-family: Arial, Helvetica, sans-serif;">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td style="font-size: 21px; color:#fff;">
                                                                <p style="margin-top: 6px;"><strong>¡Gracias por participar! Si quieres saber si eres uno de los ganadores de un Moto Z Play + Mod JBL, ingresa aquí:</strong></p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td style="font-size: 14px; line-height: 16px; color:#fff;">
                                                                <a href=""
                                                                   style="background-color:#e24288;border:1px solid #fff;min-height: 45px;width: 240px;font-size: 18px;color: rgb(255, 255, 255);display: block;text-align: center;line-height: 45px;text-decoration: none;"><strong>DESCÚBRELO AQUÍ</strong></a>
                                                            </td>
                                                            <td style="display: flex; justify-content: flex-end;">
                                                                <img src="{{ asset('img/landings/comunidad/emails/celular.png') }}" alt="" />
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="50">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table align="center" bgcolor="#000000" border="0" cellpadding="0" cellspacing="0" class="w100pc" width="100%" style="height: 60px">
                                        <tbody>
                                            <tr>
                                                <td class="hide" style="font-family: Arial, Helvetica, sans-serif;">
                                                    <table align="center" bgcolor="#000" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td colspan="7" height="7"></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <strong style="color:#fff;padding-left: 10px;">#hello<span style="color:#d33878;">moto</span></strong>
                                                            </td>
                                                            <td style="color: #ffffff; font-size: 10px;padding-right: 25px; text-align: right;">
                                                                <span>2017 Motorola Mobility LLC. &copy; Todos los derechos reservados - Lima 2017 Powered by <a href="http://www.whiz.pe" target="_blank">
                                                                    <img src="{{ asset('img/landings/comunidad/emails/whiz_logo_b.png') }}" alt=""
                                                                         style="width: 48px; height: 8px;"/></a>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="7" height="7"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table align="center" bgcolor="#000000" border="0" cellpadding="0" cellspacing="0" class="w100pc" width="100%" style="height: 60px">
                                        <tbody>
                                            <tr>
                                                <td class="hide" style="font-family: Arial, Helvetica, sans-serif;">
                                                    <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td colspan="7" height="7"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p style="font-size: 10px">
                                                                    Motorola se adhiere a lo establecido por el Decreto Ley N° 28493, el cual regula el uso de correo electrónico comercial no solicitado [SPAM]. "Un email no podrá ser considerado SPAM mientras incluya la forma de ser removido. Si este correo electrónico no fuese de su interés, le pedimos disculpas por la molestia. Si no desea recibir información de nuestros productos o promociones, le agradeceremos darse de baja aquí
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="7" height="7"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
</html>