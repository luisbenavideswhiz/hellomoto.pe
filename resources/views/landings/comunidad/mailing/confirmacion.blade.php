<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no"/>
    <!--[if !mso]>
            <!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!--
            <![endif]--><!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <title></title>
    <style type="text/css">div, p, a, li, td {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        body {
            min-width: 100% !important;
            Margin: 0;
            padding: 0;
        }
    </style>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css" media="all">
        td[class="outlook-f"] {
            width: 244px !important;
        }

        td[class="outlook-c"] {
            width: 500px !important;
        }

        @media only screen and (max-width: 480px) {
            td[class="outlook-f"] {
                width: 100% !important
            }
        }
    </style>
    <![endif]-->
</head>
<body bgcolor="#ffffff" marginleft="0" marginright="0" margintop="0" style="margin: 0px; padding: 0px; width: 100% !important;">
    <p><span class="hidesumary" style="display: none;"></span></p>
    <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="">
        <tbody style="">
            <tr>
                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="640">
                        <tbody>
                            <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="w100pc" width="600" style="width: 640px;height: 278px;margin-bottom:-3px;">
                                        <tr>
                                            <td>
                                                <img src="{{ asset('img/landings/comunidad/emails/mailing_confirmar.jpg') }}" alt="" style="display: block; width: 640px;height: 278px;">
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="w90pc" width="520" style="width: 640px;height: 270px;background-color:#f04187;">
                                        <tbody>
                                            <tr>
                                                <td width="30">&nbsp;</td>
                                                <td style="font-family: Arial, Helvetica, sans-serif;">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td style="font-size: 30px; color:#fff;">
                                                                <p style="margin-top: 6px;"><strong>¡Estás a un paso de ganarte un Moto Z Play con motomod JBL!</strong></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 16px; color:#fff;">
                                                                <p style="margin-top: 6px;">Confirma tu correo electr&oacute;nico dándole click al siguiente botón:</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" height="7px"></td>
                                                        </tr>
                                                        <tr style="display: ;">
                                                            <td style="font-size: 14px; line-height: 16px; color:#fff;">
                                                                <a href="{{ url('/comunidad/confirmation/'.base64_encode($user->email)) }}"
                                                                   style="background-color:#71c8e1;border:1px solid #fff;min-height: 45px;width: 240px;font-size: 18px;color: rgb(255, 255, 255);display: block;text-align: center;line-height: 45px;text-decoration: none;"><strong>CONFIRMAR AQUÍ</strong></a>
                                                            </td>
                                                            <td style="width: 100px;display: flex;justify-content: flex-end;padding-right: 30px;margin-top: 20px;">
                                                                <a href="https://www.facebook.com/MotorolaPE" target="_blank" style="text-decoration: none;">
                                                                    <img src="{{ asset('img/landings/comunidad/emails/redes1.png') }}" alt=""
                                                                         style="width: 24px;height: 24px;padding-left: 7px;"/>
                                                                </a>
                                                                <a href="https://www.instagram.com/motorolape/" target="_blank" style="text-decoration: none;">
                                                                    <img src="{{ asset('img/landings/comunidad/emails/redes2.png') }}" alt=""
                                                                         style="width: 24px;height: 24px;padding-left: 7px;"/>
                                                                </a>
                                                                <a href="https://twitter.com/MotorolaPE" target="_blank" style="text-decoration: none;">
                                                                    <img src="{{ asset('img/landings/comunidad/emails/redes3.png') }}" alt=""
                                                                         style="width: 24px;height: 24px;padding-left: 7px;"/>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="30">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table align="center" bgcolor="#000000" border="0" cellpadding="0" cellspacing="0" class="w100pc" width="100%" style="height: 60px">
                                        <tbody>
                                            <tr>
                                                <td class="hide" style="font-family: Arial, Helvetica, sans-serif;">
                                                    <table align="center" bgcolor="#000" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td colspan="7" height="7"></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <strong style="color:#fff;padding-left: 10px;">#hello<span style="color:#71c8e1;">moto</span></strong>
                                                            </td>
                                                            <td style="color: #ffffff; font-size: 10px;padding-right: 25px; text-align: right;">
                                                                <span>2017 Motorola Mobility LLC. &copy; Todos los derechos reservados - Lima 2017 Powered by 
                                                                    <img src="{{ asset('img/landings/comunidad/emails/whiz_logo_b.png') }}" alt=""
                                                                         style="width: 48px; height: 8px;"/>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="7" height="7"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>