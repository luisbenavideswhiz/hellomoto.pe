<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no"/>
    <!--[if !mso]>
            <!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!--
            <![endif]--><!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <title></title>
    <style type="text/css">div, p, a, li, td {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        body {
            min-width: 100% !important;
            Margin: 0;
            padding: 0;
        }
    </style>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css" media="all">
        td[class="outlook-f"] {
            width: 244px !important;
        }

        td[class="outlook-c"] {
            width: 500px !important;
        }

        @media only screen and (max-width: 480px) {
            td[class="outlook-f"] {
                width: 100% !important
            }
        }
    </style>
    <![endif]-->
</head>
<body bgcolor="#ffffff" marginleft="0" marginright="0" margintop="0" style="margin: 0px; padding: 0px; width: 100% !important;">
    <p><span class="hidesumary" style="display: none;"></span></p>
    <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="">
        <tbody>
            <tr>
                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="640">
                        <tbody>
                            <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="w100pc">
                                        <tr>
                                            <td valign="top">
                                                <a href="#!" style="line-height: 0;">
                                                    <img src="{{ asset('img/landings/comunidad/emails/parte1.jpg') }}" alt="" />
                                                </a>
                                            </td>
                                            <td valign="top">
                                                <img src="{{ asset('img/landings/comunidad/emails/parte2.jpg') }}" alt="" />
                                            </td>
                                            <td valign="top">
                                                <a href="#!" style="line-height: 0;">
                                                    <img src="{{ asset('img/landings/comunidad/emails/parte3.jpg') }}" alt="" />
                                                </a>
                                            </td>
                                            <td valign="top">
                                                <a href="#!" style="line-height: 0;">
                                                    <img src="{{ asset('img/landings/comunidad/emails/parte4.jpg') }}" alt="" />
                                                </a>
                                            </td>
                                            <td valign="top">
                                                <a href="#!" style="line-height: 0;">
                                                    <img src="{{ asset('img/landings/comunidad/emails/parte5.jpg') }}" alt="" />
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="w100pc">
                                        <tr>
                                            <td>
                                                <img src="{{ asset('img/landings/comunidad/emails/parte6.jpg') }}" alt="" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="w90pc" style="background-color:#f04187;height: 452px;">
                                        <tbody>
                                            <tr>
                                                <td width="30">&nbsp;</td>
                                                <td style="font-family: Arial, Helvetica, sans-serif;">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td style="font-size: 30px; color:#fff;text-align: center;">
                                                                <p style="margin-top: 40px;"><strong>Hola Victor,</strong></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 25px; color:#fff;text-align: center;">
                                                                <p">¿quieres formar parte de una comunidad
                                                                diferente con beneficios exclusivos?</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" height="20" "></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td style=" color:#fff;text-align:left;" >
                                                                <p style="margin-top: 6px;font-size: 20px;"><strong>Participa del sorteo de 5 Moto Z Play con motomod JBL.</strong><span></span></p>
                                                                <p style="font-size:18px">¡Unéte a la comunidad<strong style="color:#fff;padding-left: 10px;">hello<span style="color:#f7e636;">moto</span></strong>!</p>
                                                                <a href=""
                                                                   style="background-color:#f04187;border:1px solid #fff;min-height: 45px;width: 240px;font-size: 18px;color: rgb(255, 255, 255);display: inherit;text-align: center;line-height: 45px;text-decoration: none; margin:0 auto; "><strong>REGÍSTRATE Y GANA</strong></a>
                                                            </td>
                                                            <td>
                                                                <img src="{{ asset('img/landings/comunidad/emails/celular.png') }}" alt="" style="display: inline-block;"/>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="30">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table align="center" bgcolor="#000000" border="0" cellpadding="0" cellspacing="0" class="w100pc" width="100%" style="height: 60px">
                                        <tbody>
                                            <tr>
                                                <td class="hide" style="font-family: Arial, Helvetica, sans-serif;">
                                                    <table align="center" bgcolor="#000" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td colspan="7" height="7"></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <strong style="color:#fff;padding-left: 10px;">#hello<span style="color:#ff539d;">moto</span></strong>
                                                            </td>
                                                            <td style="color: #ffffff; font-size: 10px;padding-right: 25px;">
                                                                <span style="float:right;">2017 Motorola Mobility LLC. &copy; Todos los derechos reservados - Lima 2017 Powered by 
                                                                    <img src="{{ asset('img/landings/comunidad/emails/whiz_logo_b.png') }}" alt=""
                                                                         style="width: 48px; height: 8px;"/>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="7" height="7"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td bgcolor="#ffffff" height="28">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>