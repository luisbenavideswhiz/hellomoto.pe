<!DOCTYPE html>
<html lang="es">
<head>

    {{--@component('landings.comunidad.partials.google.analytics.script')--}}
    {{--@endcomponent--}}

    {{--@component('landings.comunidad.partials.google.tag-manager.script')--}}
    {{--@endcomponent--}}

    {{--@component('landings.comunidad.partials.fb.pixel.gracias')--}}
    {{--@endcomponent--}}

    @component('landings.comunidad.partials.metas')
    @endcomponent

    @component('landings.comunidad.partials.styles')
    @endcomponent

</head>
<body class="cconfirmation">

{{--@component('landings.comunidad.partials.google.tag-manager.code')--}}
{{--@endcomponent--}}

@component('landings.comunidad.partials.update-your-browser')
@endcomponent

<div class="motorola">
    <div class="wrapper">
        <div class="container">
            <div class="row">
                @component('landings.comunidad.partials.header')
                @endcomponent
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6"></div>
                <div class="col-xs-12 col-md-6">
                    @if($exist == true)
                        @if($user->confirm == 1)
                            <div id="primera">
                                <form role="form" class="confirmation">
                                    <div class="navc show-res">
                                        <nav>
                                            <span class="right">Síguenos en:</span>
                                            <ul>
                                                <li>
                                                    <a href="https://www.facebook.com/MotorolaPE" target="_blank">
                                                        <img src="{{ asset('img/landings/comunidad/social_1.svg') }}" alt="">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.instagram.com/motorolape/" target="_blank">
                                                        <img src="{{ asset('img/landings/comunidad/social_3.svg') }}" alt="">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://twitter.com/MotorolaPE" target="_blank">
                                                        <img src="{{ asset('img/landings/comunidad/social_2.svg') }}" alt="">
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <div class="header">
                                        <h3>&iexcl;Excelente!</h3>
                                    </div>
                                    <div class="form">
                                        <h3>{{ ucwords($user->name) }}, <br> <br> &iexcl;Gracias por registrarte a la comunidad Motorola!</h3>
                                        <a href="https://www.motorola.com.pe/home" target="_blank" class="btn btn-success">
                                            Visita nuestra web
                                        </a>
                                    </div>
                                </form>
                            </div>
                        @elseif($user->confirm == 0)
                            <div id="primera">
                                <form role="form" class="confirmation">
                                    <div class="navc show-res">
                                        <nav>
                                            <span class="right">Síguenos en:</span>
                                            <ul>
                                                <li>
                                                    <a href="https://www.facebook.com/MotorolaPE" target="_blank">
                                                        <img src="{{ asset('img/landings/comunidad/social_1.svg') }}" alt="">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.instagram.com/motorolape/" target="_blank">
                                                        <img src="{{ asset('img/landings/comunidad/social_3.svg') }}" alt="">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://twitter.com/MotorolaPE" target="_blank">
                                                        <img src="{{ asset('img/landings/comunidad/social_2.svg') }}" alt="">
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <div class="header">
                                        <h3>&iexcl;Bienvenido!</h3>
                                    </div>
                                    <div class="form">
                                        <h3>{{ ucwords($user->name) }}, <br> <br>¡Gracias por registrarte a la comunidad Motorola!</h3>

                                        <a href="https://www.facebook.com/MotorolaPE" target="_blank" class="btn btn-success">
                                            Visita nuestro fanpage
                                        </a>
                                    </div>
                                </form>
                            </div>
                        @endif
                    @elseif($exist == false)
                        <div id="primera">
                            <form role="form" class="confirmation">
                                <div class="navc show-res">
                                    <nav>
                                        <span class="right">Síguenos en:</span>
                                        <ul>
                                            <li>
                                                <a href="https://www.facebook.com/MotorolaPE" target="_blank">
                                                    <img src="{{ asset('img/landings/comunidad/social_1.svg') }}" alt="">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://www.instagram.com/motorolape/" target="_blank">
                                                    <img src="{{ asset('img/landings/comunidad/social_3.svg') }}" alt="">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://twitter.com/MotorolaPE" target="_blank">
                                                    <img src="{{ asset('img/landings/comunidad/social_2.svg') }}" alt="">
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                                <div class="header">
                                    <h3>&iexcl;Uy!</h3>
                                </div>
                                <div class="form">
                                    <h3>El usuario no existe</h3>
                                    <h5>No hemos podido procesar esta solicitud.</h5>
                                    <a href="http://www.hellomoto.pe/comunidad" class="btn btn-success">
                                        Inténtalo nuevamente
                                    </a>
                                </div>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @component('landings.comunidad.partials.footer')
    @endcomponent
</div>

@component('landings.comunidad.partials.scripts')
@endcomponent

</body>
</html>
