<title>Motorola</title>

<meta charset="utf-8">
<meta name="viewport"
      content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0, minimum-scale=1.0">
<meta property="og:url" content="">
<meta property="og:type" content="website">
<meta property="og:title"
      content="ENTRA AL SORTEO, DILE HOLA A MOTO">
<meta property="og:description"
      content="¡Únete a la comunidad hellomoto y participa del sorteo de un Moto Z play con motomod JBL!">
<meta property="og:image" content="{{ asset('/img/landings/comunidad/200x200.jpg') }}">

<meta name="author" content="WHIZ - Agencia Digital">
<meta name="title" content="ENTRA AL SORTEO,">
<meta name="description"
      content="¡Únete a la comunidad hellomoto y participa del sorteo de un Moto Z play con motomod JBL!">
<meta name="keywords" content="hellomoto,Motorola,Moto Z play,motomod JBL,Lima,sorteo,comunidad hellomoto">