<header>
    <div class="col-xs-12 col-sm-6 moto_logo">
        <div class="logo">
            <a href="https://www.motorola.com.pe/home" target="_blank">
                <img src="{{ asset('img/landings/comunidad/m_logo.svg') }}" alt="" class="left">
            </a>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 navc show-desk">
        <nav>
            <span class="right">Siguenos en:</span>
            <ul>
                <li>
                    <a href="https://www.facebook.com/MotorolaPE" target="_blank">
                        <img src="{{ asset('img/landings/comunidad/social_1.svg') }}" alt="">
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/motorolape/" target="_blank">
                        <img src="{{ asset('img/landings/comunidad/social_3.svg') }}" alt="">
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/MotorolaPE" target="_blank">
                        <img src="{{ asset('img/landings/comunidad/social_2.svg') }}" alt="">
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</header>