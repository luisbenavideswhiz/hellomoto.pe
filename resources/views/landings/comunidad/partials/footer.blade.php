<div class="container-fluid">
    <div class="row">
        <footer>
            <div class="hmoto">
                <strong>#hello<span>moto</span></strong>
            </div>
            <div class="copyright">
                <p>
                    2017 Motorola Mobility LLC. &copy; Todos los derechos reservados - Lima 2017 Powered by <a
                            href="http://whiz.pe" target="_blank"><img src="{{ asset('img/landings/comunidad/logo_whiz.svg') }}"
                                                                       alt="whiz logo"></a>
                </p>
            </div>
        </footer>
    </div>
</div>