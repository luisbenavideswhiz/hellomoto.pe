<div id="segunda">
    <h3>
        ¡Únete a la comunidad <span>hello</span><span class="rosa">moto</span> y participa del sorteo de 5 Moto Z Play
        con motomod JBL!
    </h3>
    <ul>
        <li>Beneficios exclusivos</li>
        <li>Entérate de los eventos Motorola y obtén entradas anticipadas</li>
        <li>Conoce a la nueva familia de Embajadores y Equipos Motorola</li>
        <li>MotoMods: transforma tu Moto Z Play</li>
    </ul>
</div>