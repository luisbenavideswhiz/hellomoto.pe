<!DOCTYPE html>
<html lang="es">
<head>

    {{--@component('landings.comunidad.partials.google.analytics.script')--}}
    {{--@endcomponent--}}

    {{--@component('landings.comunidad.partials.google.tag-manager.script')--}}
    {{--@endcomponent--}}

    @component('landings.comunidad.partials.fb.pixel.gracias')
    @endcomponent

    @component('landings.comunidad.partials.metas')
    @endcomponent

    @component('landings.comunidad.partials.styles')
    @endcomponent

</head>
<body class="gracias">

{{--@component('landings.comunidad.partials.google.tag-manager.code')--}}
{{--@endcomponent--}}

@component('landings.comunidad.partials.update-your-browser')
@endcomponent

<div class="motorola">
    <div class="wrapper">
        <div class="container">
            <div class="row">
                @component('landings.comunidad.partials.header_g')
                @endcomponent
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div id="primera">
                        <form role="form" class="confirmation">
                            <div class="navc show-res">
                                <nav>
                                    <span class="right">Síguenos en:</span>
                                    <ul>
                                        <li>
                                            <a href="https://www.facebook.com/MotorolaPE" target="_blank">
                                                <img src="{{ asset('img/landings/comunidad/social_1.svg') }}" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/motorolape/" target="_blank">
                                                <img src="{{ asset('img/landings/comunidad/social_3.svg') }}" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/MotorolaPE" target="_blank">
                                                <img src="{{ asset('img/landings/comunidad/social_2.svg') }}" alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="header">
                                <h3>EST&Aacute;S A UN PASO</h3>
                                <h3>DE ENTRAR AL SORTEO</h3>
                            </div>
                            <div class="form">
                                <strong>Perfecto {{ $name }},</strong>
                                <p>
                                    Sólo falta que confirmes tu correo y serás parte de la comunidad hellomoto.
                                </p>
                                <p>
                                    Te llegará un correo a tu bandeja de entrada, dale click al botón “CONFIRMAR AQUÍ” y automáticamente estarás registrad@.
                                </p>
                            </div>
                            <div class="img">
                                <img src="{{ asset('img/landings/comunidad/motolips.png') }}" alt="">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @component('landings.comunidad.partials.footer')
    @endcomponent
</div>

@component('landings.comunidad.partials.scripts')
@endcomponent

</body>
</html>
