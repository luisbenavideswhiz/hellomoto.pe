<!DOCTYPE html>
<html lang="es">
<head>
    @include('landings.copamotorola.partials.fb.pixel.index')
    @include('landings.copamotorola.partials.metas')
    @include('web.partials.cannonical')
    @include('landings.copamotorola.partials.styles')
    @include('web.partials.seo.analytics')
</head>
<body id="home" @section('headerclass')@show>

@include('landings.copamotorola.partials.header')

@yield('content')

@include('landings.copamotorola.partials.footer')
@include('landings.copamotorola.partials.scripts')

@section('js')@show
</body>
</html>