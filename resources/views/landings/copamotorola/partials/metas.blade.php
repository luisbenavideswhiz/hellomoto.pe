<title>Sorteo hellomoto Perú | Copa Motorola</title>

<meta charset="utf-8">
<meta name="viewport"
      content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0, minimum-scale=1.0">
<meta property="og:url" content="{{ url('sorteos/copa-motorola') }}">
<meta property="og:type" content="website">
<meta property="og:title"
      content="Sorteo hellomoto Perú | Copa Motorola">
<meta property="og:description"
      content="hellomoto Perú  te regalan 2 cupos dobles la Copa Motorola de Fútbol Tenis . Únete a la comunidad Motorola, descubre el otro lado de la ciudad y ¡gánate!">
<meta property="og:image" content="{{ asset('/img/landings/copamotorola/thumb-social.jpg') }}">

<meta name="author" content="WHIZ - Agencia Digital">
<meta name="title" content="Sorteo hellomoto Perú | Copa Motorola">
<meta name="description"
      content="hellomoto Perú  te regalan 2 cupos dobles la Copa Motorola de Fútbol Tenis . Únete a la comunidad Motorola, descubre el otro lado de la ciudad y ¡gánate!">
<meta name="keywords" content="Regalo, Sorteo, Motorola, copa motorola, futbol tennis, futbol, hello moto">

