<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="powered">
                        <span>
                            © Todos los derechos reservados - Lima, 2018
                        </span>
                    <span>
                            Powered by <a class="whiz" data-href="http://whiz.pe" data-target="_blank"><img src="{{ asset('img/landings/copamotorola/logo_whiz.svg') }}" alt="whiz logo"></a>
                        </span>
                </div>
            </div>
        </div>
    </div>
</footer>