@extends('landings.copamotorola.layouts.base')

@section('headerclass')
    class="home"
@endsection

@section('content')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="login">
                        <button class="btn-login" data-target="_self" data-href="{{ url('/?modal=login') }}">
                            Ingresar
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <div class="copy">
                        <h2>
                            ¡PARTICIPA POR UNO DE LOS 2 CUPOS DOBLES!
                        </h2>
                        <h3>
                            Avísale a tu partner para jugar en el emocionante campeonato de la COPA MOTOROLA Fútbol-Tenis
                        </h3>
                    </div>
                    <div class="img-r">
                        <img src="{{ asset ('img/landings/copamotorola/copamotorola.png') }}" alt="">
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form">
                        <div class="form-header">
                            <h2>
                                ENTRA A LA COMUNIDAD, DILE HOLA A MOTO
                            </h2>
                            <h3>
                                Registra tus datos aquí
                            </h3>
                        </div>
                        <form action="{{ url('sorteos/copa-motorola') }}" method="POST" id="landing" name="form_l_motogift">
                            {{ csrf_field() }}
                            <div class="col2">
                                <div class="input-group">
                                    <span>Nombre (*)</span>
                                    <input type="text" id="name" name="name" class="validate">
                                </div>
                                <div class="input-group">
                                    <span>Apellido (*)</span>
                                    <input type="text" id="lastname" name="lastname" class="validate">
                                </div>
                            </div>

                            <div class="col2">
                                <div class="input-group">
                                    <span>Género (*)</span>
                                    <label for="sex-m">
                                        <input type="radio" name="sex" id="sex-m" value="mujer" class="validate"> <span>Mujer</span>
                                    </label>
                                    <label for="sex-h">
                                        <input type="radio" name="sex" id="sex-h" value="hombre" class="validate"> <span>Hombre</span>
                                    </label>
                                </div>
                                <div class="input-group">
                                    <span>DNI (*)</span>
                                    <input type="text" id="document_number" name="document_number">
                                </div>
                            </div>

                            <div class="col2">
                                <div class="input-group">
                                    <span>Número de celular (*)</span>
                                    <input type="text" id="phone" name="phone">
                                </div>
                                <div class="input-group">
                                    <span>Operador (*)</span>
                                    <select id="mobile_operator" name="mobile_operator">
                                        <option value="">Seleccionar</option>
                                        <option value="claro">Claro</option>
                                        <option value="movistar">Movistar</option>
                                        <option value="entel">Entel</option>
                                        <option value="bitel">Bitel</option>
                                        <option value="virgin">Virgin</option>
                                        <option value="tuenti">Tuenti</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col1">
                                <div class="input-group">
                                    <span>Email (*)</span>
                                    <input type="text" id="email" name="email">
                                </div>
                            </div>

                            <div class="col1">
                                <div class="input-group">
                                    <label for="accept_emails" class="flexme">
                                        <input type="checkbox" name="accept_emails" value="1" id="accept_emails"> <span>Deseo recibir información sobre productos y ofertas especiales por parte de Motorola.</span>
                                    </label>
                                </div>
                            </div>

                            <div class="col3">
                                <span>Fecha de nacimiento (*)</span>
                                <div class="input-group">
                                    <input type="text" id="birthdate" data-format="YYYY-MM-DD" data-template="DD MMMM YYYY" name="birthdate">
                                </div>
                            </div>

                            <div class="col1">
                                <div class="input-group">
                                    <em>Soy mayor de edad</em>
                                    <label for="full_age_yes">
                                        <span>Si</span> <input type="radio" name="full_age" id="full_age_yes">
                                    </label>
                                    <label for="full_age_no">
                                        <span>No</span> <input type="radio" name="full_age" id="full_age_no">
                                    </label>
                                </div>
                            </div>

                            <div class="col3">
                                <div class="input-group">
                                    <select name="department" id="department">
                                        <option value="">Departamento</option>
                                        @foreach($departments as $department)
                                            <option>{{ $department->department }}</option>
                                        @endforeach
                                    </select>

                                    <select name="province" id="province">
                                        <option value="">Provincia</option>
                                    </select>

                                    <select name="district" id="district">
                                        <option value="">Distrito</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col2">
                                <div class="input-group">
                                    <span>Contraseña (*)</span>
                                    <input type="password" id="password" name="password">
                                </div>
                                <div class="input-group">
                                    <span>Repetir Contraseña (*)</span>
                                    <input type="password" id="repassword" name="repassword">
                                </div>
                            </div>

                            <div class="col1">
                                <div class="input-group">
                                    <div class="acciones">
                                        <label for="terms_condition">
                                            <input type="checkbox" name="terms_conditions">
                                            <div class="tc">
                                                <a target="_blank" href="{{ url('/sorteos/copa-motorola/terminos-condiciones') }}">
                                                    Términos y condiciones</a>
                                                <a target="_blank" href="http://www.motorola.com.pe/institucional/politica-de-privacidad">
                                                    Política de privacidad
                                                </a>
                                            </div>
                                        </label>
                                        <button type="submit" class="btn btn-submit">Registrar y participar</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="hidden-xs col-md-1"></div>
            </div>
        </div>
    </div>
@endsection