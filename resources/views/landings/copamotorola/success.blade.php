@extends('landings.copamotorola.layouts.base')

@section('headerclass')
	class="home"
@endsection

@section('content')
	<div class="wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="login">
						@if(is_null($user))
						<button class="btn-login" data-target="_self" data-href="{{ url('/?modal=login') }}">
							Ingresar
						</button>
						@endif
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-7">
					<div class="copy">
                        <h2>
                            ¡PARTICIPA POR UNO DE LOS 2 CUPOS DOBLES!
                        </h2>
                        <h3>
                            Avísale a tu partner para jugar en el emocionante campeonato de la COPA MOTOROLA Fútbol-Tenis
                        </h3>
                    </div>
					<div class="img-r">
						<img src="{{ asset ('img/landings/copamotorola/copamotorola.png') }}" alt="">
					</div>
				</div>
				<div class="col-xs-12 col-md-4">
					<div class="form success">
						<div class="form-header">
							<h2>
								REGISTRO VALIDADO
							</h2>
						</div>
						<div class="acciones">
							<strong>Ya estas participando del sorteo. <br> ¡Mucha suerte!</strong>
						</div>
					</div>
				</div>
				<div class="hidden-xs col-md-1"></div>
			</div>
		</div>
	</div>

	<script>
        window.setTimeout(function(){
            window.location.href = "/";
        }, 3500);
	</script>
@endsection