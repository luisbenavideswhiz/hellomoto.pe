@extends('landings.copamotorola.layouts.base')

@section('headerclass')
    class="home"
@endsection

@section('content')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="login">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <div class="copy">
                        <h2>
                            ¡PARTICIPA POR UNO DE LOS 2 CUPOS DOBLES!
                        </h2>
                        <h3>
                            Avísale a tu partner para jugar en el emocionante campeonato de la COPA MOTOROLA Fútbol-Tenis
                        </h3>
                    </div>
                    <div class="img-r">
                        <img src="{{ asset ('img/landings/copamotorola/copamotorola.png') }}" alt="">
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form">
                        <div class="form-header">
                            <h2>
                                ENTRA A LA COMUNIDAD, DILE HOLA A MOTO
                            </h2>
                        </div>
                        <form action="{{ url('sorteos/copa-motorola/bienvenido') }}" method="POST" id="form_l_motogift" name="form_l_motogift">
                            {{ csrf_field() }}
                            <div class="col1">
                                <div class="input-group">
                                    <div class="acciones">
                                        <strong>Confirma tus datos y participa del sorteo</strong>
                                        <button type="submit" class="btn">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="hidden-xs col-md-1"></div>
            </div>
        </div>
    </div>
@endsection