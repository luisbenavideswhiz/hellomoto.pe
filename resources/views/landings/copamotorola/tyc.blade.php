@extends('landings.copamotorola.layouts.base')

@section('headerclass')
    class="home"
@endsection

@section('content')
<div class="wrapper terminos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="login">
					<button class="btn-login" data-target="_self" data-href="{{ url('/?modal=login') }}">
						Ingresar
					</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="tyc">
                    <h4>Términos y condiciones</h4>
                    <br>
                    <p>
                        Participarán todos los usuarios que se registren en la web: link <a href="https://hellomoto.pe/sorteos/copa-motorola">https://hellomoto.pe/sorteos/copa-motorola</a>, sean mayores de 18 años y residan en Lima.</p><p>

                        El anuncio de los ganadores se realizará el día martes 13 de Marzo, 2018 mediante una publicación en el Facebook de Motorola.</p><p>

                        Se sortearán 2 cupos dobles para participar en la Copa Motorola Futbol Tenis 2018, que se llevara a cabo el sábado 17 de marzo en Playa Flamencos, Asia a las 11:00 am.</p><p>

                        El concurso no es válido para trabajadores de la agencia Wunderman Phantasia, Whiz o de la empresa de dispositivos móviles Motorola.</p><p>

                        El concurso se realizará desde el 06/03/2018 hasta el 12/03/2018.</p><p>

                        Serán 2 ganadores (cupos dobles) diferentes que podrán participar gratuitamente en la Copa Motorola Futbol Tenis 2018.
                    </p>
                </div>
			</div>
			<div class="hidden-xs col-md-1"></div>
			<div class="col-xs-12 col-md-5">
				<div class="img">
					<img src="{{ asset ('img/landings/copamotorola/copamotorola370x400.png') }}" alt="">
				</div>
			</div>
		</div>
	</div>
</div>
@endsection