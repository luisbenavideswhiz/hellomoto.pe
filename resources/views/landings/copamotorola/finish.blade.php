@extends('landings.copamotorola.layouts.base')

@section('headerclass')
    class="home"
@endsection

@section('content')
    <div class="wrapper terminos">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="login">
                        <button>
                            Ingresar
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="hidden-xs col-md-3"></div>
                <div class="col-xs-12 col-md-6">
                    <div class="finalizado">
                        <div class="img">
                            <img src="{{ asset ('img/landings/copamotorola/copamotorola370x400.png') }}" alt="">
                        </div>
                        <p>
                            El sorteo ya no se encuentra disponible, ingresa a la sección Beneficios de tu perfil y entérate de las nuevas promociones.
                        </p>
                        <div class="links">
                            <a href="{{ url('/usuario/beneficios') }}">Ver beneficios</a>
                            <a href="{{ url('/?modal=register') }}">Crear cuenta</a>
                        </div>
                    </div>
                </div>
                <div class="hidden-xs col-md-3"></div>
            </div>
        </div>
    </div>
@endsection