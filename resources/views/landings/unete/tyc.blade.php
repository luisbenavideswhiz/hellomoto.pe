<!DOCTYPE html>
<html lang="es">
<head>

    {{--@component('landings.unete.partials.google.analytics.script')--}}
    {{--@endcomponent--}}

    {{--@component('landings.unete.partials.google.tag-manager.script')--}}
    {{--@endcomponent--}}

    @component('landings.unete.partials.fb.pixel.index')
    @endcomponent

    @component('landings.unete.partials.metas')
    @endcomponent

    @component('landings.unete.partials.styles')
    @endcomponent

    @include('web.partials.seo.analytics')
</head>
<body id="home" class="home notlogged">

    <header>
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-6">
                    <div class="logo">
                        <a href="{{ url('/') }}" target="_blank">
                            <img src="{{ asset ('img/landings/unete/logo.svg') }}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <nav>
                        <a target="_blank" href="https://www.facebook.com/MotorolaPE">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a target="_blank" href="https://twitter.com/MotorolaPeru">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a target="_blank" href="https://www.instagram.com/motorolape/">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="tyc">
                        <h4>Términos y condiciones</h4>
                        <br>
                        <p>
                            Participarán todos los usuarios que se registren en la web: link <a href="https://hellomoto.pe/sorteos/moto-z-play-motomods-jbl-soundboost">https://hellomoto.pe/sorteos/moto-z-play-motomods-jbl-soundboost</a>, sean mayores de 18 años y residan a nivel nacional.
                        </p>
                        <p>
                            El anuncio de los ganadores se realizará el día jueves 1 de Marzo, 2018 mediante una publicación en el Facebook de Motorola.
                        </p>
                        <p>
                            Se sortearán 3 equipos Moto Z Play, cada uno con su Motomod JBL Soundboost
                        </p>
                        <p>
                            El concurso no es válido para trabajadores de la agencia Wunderman Phantasia, Whiz o de la empresa de dispositivos móviles Motorola.
                        </p>
                        <p>
                            El concurso se realizará desde el 07/02/2018 hasta el 28/02/18.
                        </p>
                        <p>
                            Serán 03 ganadores diferentes, llevándose cada uno un Moto Z Play con un Motomod JBL Soundboost.
                        </p>
                    </div>
                    <a href="{{ url()->previous() }}" class="btn login" onclick="javascript:window.parent.close();">
                        Regresar
                    </a>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="powered">
                        <span>
                            © Todos los derechos reservados - Lima, 2018
                        </span>
                        <span>
                            Powered by <a href="http://whiz.pe" target="_blank"><img src="{{ asset('img/landings/unete/logo_whiz.svg') }}" alt="whiz logo"></a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </footer>

@component('landings.unete.partials.scripts')
@endcomponent

</body>
</html>