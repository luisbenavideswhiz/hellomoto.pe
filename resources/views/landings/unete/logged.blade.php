@extends('landings.unete.layouts.base')

@section('headerclass')
    class="notlogged"
@endsection

@section('content')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-5">
                    <div class="form">
                        <div class="header">
                            <h3>
                                CONFIRMA TUS DATOS Y <br>
                                PARTICIPA DEL SORTEO
                            </h3>

                        </div>
                        <div class="unete">
                            @if($landing->webUserHasLanding($user['id']))
                                <a href="{{ url('/') }}" class="disable">Participando</a>
                            @else
                                <a href="{{ url('sorteos/moto-z-play-motomods-jbl-soundboost/bienvenido') }}" class="btn">Confirmar</a>
                            @endif
                        </div>
                        <form action="">
                            <div class="col1">
                                <div class="input-field">
                                    <div class="acciones">
                                        <a href="{{ url('/sorteos/moto-z-play-motomods-jbl-soundboost/terminos-condiciones') }}">
                                            Términos y condiciones</a>
                                        <a href="https://www.motorola.com.pe/legal/privacy-policy">
                                            Política de privacidad
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12 col-md-7">

                </div>
            </div>
        </div>
    </div>
@endsection
