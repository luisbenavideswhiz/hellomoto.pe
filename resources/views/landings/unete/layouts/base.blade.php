<!DOCTYPE html>
<html lang="es">
<head>
    @include('landings.unete.partials.fb.pixel.index')
    @include('landings.unete.partials.metas')
    @include('web.partials.cannonical')
    @include('landings.unete.partials.styles')
    @include('web.partials.seo.analytics')
</head>
<body id="home" @section('headerclass')@show>

@include('landings.unete.partials.header')

@yield('content')

@include('landings.unete.partials.footer')
@include('landings.unete.partials.scripts')

</body>
</html>