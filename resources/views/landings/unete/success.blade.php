@extends('landings.unete.layouts.base')

@section('headerclass')
    class="notlogged"
@endsection

@section('content')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                @if(is_null($user))
                    <a href="{{ url('/?modal=login') }}" class="btn login">
                        Iniciar sesión
                    </a>
                @endif
                <div class="col-xs-12 col-md-5">
                    <div class="form">
                        <div class="header">
                            <h3>
                                REGISTRO VALIDADO
                            </h3>
                        </div>
                        <div class="unete">
                            <strong>
                                Ya estás participando del sorteo. <br>
                                ¡Mucha suerte!
                            </strong>
                            <img src="{{ asset('img/basicloader.gif') }}" alt="Redireccionando ...">
                            <span>Redireccionando ...</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-7">

                </div>
            </div>
        </div>
    </div>

    <script>
        window.setTimeout(function(){
            window.location.href = "/";
        }, 3500);
    </script>
@endsection