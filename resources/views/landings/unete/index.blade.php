@extends('landings.unete.layouts.base')

@section('headerclass')
    class="home"
@endsection

@section('content')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <a href="{{ url('/?modal=login') }}" class="btn login">
                Iniciar sesión
            </a>
            <div class="col-xs-12 col-md-5">
                <div class="form">
                    <div class="header">
                        <h2>
                            Únete a la comunidad hellomoto.
                        </h2>
                        <h3>
                            Registra tus datos aquí.
                        </h3>
                    </div>
                    <form action="{{ url('/sorteos/moto-z-play-motomods-jbl-soundboost') }}" method="POST" id="landing">
                        {{ csrf_field() }}
                        <div class="col2">
                            <div class="input-field">
                                <span>Nombre (*)</span>
                                <input type="text" id="name" name="name" class="validate">
                            </div>
                            <div class="input-field">
                                <span>Apellido (*)</span>
                                <input type="text" id="lastname" name="lastname" class="validate">
                            </div>
                        </div>

                        <div class="col2">
                            <div class="input-field">
                                <span>Género (*)</span>
                                <label for="sex-m">
                                    <input type="radio" name="sex" id="sex-m" value="mujer" class="validate"> <span>Mujer</span>
                                </label>
                                <label for="sex-h">
                                    <input type="radio" name="sex" id="sex-h" value="hombre" class="validate"> <span>Hombre</span>
                                </label>
                            </div>
                            <div class="input-field">
                                <span>DNI (*)</span>
                                <input type="text" id="document_number" name="document_number">
                            </div>
                        </div>

                        <div class="col2">
                            <div class="input-field">
                                <span>Número de celular (*)</span>
                                <input type="text" id="phone" name="phone">
                            </div>
                            <div class="input-field">
                                <span>Operador (*)</span>
                                <select id="mobile_operator" name="mobile_operator">
                                    <option value="">Seleccionar</option>
                                    <option value="claro">Claro</option>
                                    <option value="movistar">Movistar</option>
                                    <option value="entel">Entel</option>
                                    <option value="bitel">Bitel</option>
                                    <option value="virgin">Virgin</option>
                                    <option value="tuenti">Tuenti</option>
                                </select>
                            </div>
                        </div>

                        <div class="col1">
                            <div class="input-field">
                                <span>Email (*)</span>
                                <input type="text" id="email" name="email">
                            </div>
                        </div>

                        <div class="col1">
                            <div class="input-field">
                                <label for="accept_emails" class="flexme">
                                    <input type="checkbox" name="accept_emails" value="1" id="accept_emails"> <span>Deseo recibir información sobre productos y ofertas especiales por parte de Motorola.</span>
                                </label>
                            </div>
                        </div>

                        <div class="col3">
                            <span>Fecha de nacimiento (*)</span>
                            <div class="input-field">
                                <input type="text" id="birthdate" data-format="YYYY-MM-DD" data-template="DD MMMM YYYY" name="birthdate">
                            </div>
                        </div>

                        <div class="col1">
                            <div class="input-field">
                                <em>Soy mayor de edad</em>
                                <label for="full_age_yes">
                                    <span>Si</span> <input type="radio" name="full_age" id="full_age_yes">
                                </label>
                                <label for="full_age_no">
                                    <span>No</span> <input type="radio" name="full_age" id="full_age_no">
                                </label>
                            </div>
                        </div>

                        <div class="col3">
                            <div class="input-field">
                                <select name="department" id="department">
                                    <option value="">Departamento</option>
                                    @foreach($departments as $department)
                                        <option>{{ $department->department }}</option>
                                    @endforeach
                                </select>

                                <select name="province" id="province">
                                    <option value="">Provincia</option>
                                </select>

                                <select name="district" id="district">
                                    <option value="">Distrito</option>
                                </select>
                            </div>
                        </div>

                        <div class="col2">
                            <div class="input-field">
                                <span>Contraseña (*)</span>
                                <input type="password" id="password" name="password">
                            </div>
                            <div class="input-field">
                                <span>Repetir Contraseña (*)</span>
                                <input type="password" id="repassword" name="repassword">
                            </div>
                        </div>

                        <div class="col1">
                            <div class="input-field">
                                <div class="acciones">
                                    <label for="terms_condition">
                                        <input type="checkbox" name="terms_conditions">
                                        <div class="tc">
                                            <a target="_blank" href="{{ url('/sorteos/moto-z-play-motomods-jbl-soundboost/terminos-condiciones') }}">
                                                Términos y condiciones</a>
                                            <a target="_blank" href="https://www.motorola.com.pe/legal/privacy-policy">
                                                Política de privacidad
                                            </a>
                                        </div>
                                    </label>
                                    <button type="submit" class="btn">Unirme</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-md-7">
                <div class="content">
                    <h2>
                        ¡Gana uno de los 3 moto Z play + motomod JBL Soundboost que tenemos para ti!
                    </h2>
                    <h3>
                        Para participar solo debes registrarte.
                    </h3>
                    <div class="log_hm">
                        <a href="http://www.hellomoto.pe" target="_blank">
                            <img src="{{ asset ('img/landings/unete/logo_hm.svg') }}" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
