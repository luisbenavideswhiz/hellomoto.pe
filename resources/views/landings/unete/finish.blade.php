@extends('landings.unete.layouts.base')

@section('headerclass')
    class="notlogged"
@endsection

@section('content')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-5">
                    <div class="form">
                        <div class="header">
                            <h3>ATENT@ AL SORTEO,</h3>
                            <h3>HOLA A <span class="y">MOTO</span></h3>
                        </div>
                        <div class="unete">
                            <strong>El plazo de participación ya terminó.</strong>
                            <img src="{{ asset('img/basicloader.gif') }}" alt="Redireccionando ...">
                            <span>Redireccionando ...</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-7">

                </div>
            </div>
        </div>
    </div>

    <script>
        window.setTimeout(function(){
            window.location.href = "/";
        }, 3500);
    </script>
@endsection
