<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/landings/intriga/style.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/landings/intriga/responsive.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('js/landings/intriga/app.js') }}" type="text/js">