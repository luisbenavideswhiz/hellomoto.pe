<!doctype html>
<html lang="en">
<head>
    @include('landings.intriga.partials.metas')
    @include('landings.intriga.assets.styles')
    @include('landings.intriga.assets.scripts')
</head>
<body>
@include('landings.intriga.partials.header')
@include('landings.intriga.partials.body')
@include('landings.intriga.partials.footer')
</body>
</html>
