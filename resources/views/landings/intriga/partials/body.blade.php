<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="phoneimage">
                    <img src="{{ asset('img/landings/intriga/phone.png') }}">
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="land_logo">
                    <p>nueva familia</p>
                    <span>moto g<sup>6</sup></span>
                    <div class="phoneimage_resp">
                        <img src="{{ asset('img/landings/intriga/phone.png') }}">
                    </div>
                </div>
                <div class="countdown">
                    <div class="item">
                        <span class="circle days">00</span>
                        <span class="text timeRefDays">Días</span>
                    </div>
                    <div class="item">
                        <span class="circle hours">00</span>
                        <span class="text timeRefHours">Horas</span>
                    </div>
                    <div class="item">
                        <span class="circle minutes">00</span>
                        <span class="text timeRefMinutes">Minutos</span>
                    </div>
                    <div class="item">
                        <span class="circle seconds">00</span>
                        <span class="text timeRefSeconds">Segundos</span>
                    </div>
                </div>
                <div class="content">
                    <p class="default">
                        Sé el primero en enterarte de lo nuevo que tenemos para ti, déjanos tus datos y
                        <span>descúbrelo</span>.
                    </p>
                    <p class="message">
                        <span>¡Hey, gracias por registrarte!</span> Serás el primero en conocer lo nuevo que tenemos para ti.
                    </p>
                </div>
                <form class="form" action="{{ url('/moto-g6/register') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <span id="name" class="error"></span>
                                <input type="text" name="name" class="form-control" placeholder="Escribe tu nombre">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span id="email" class="error"></span>
                                <input type="text" name="email" class="form-control" placeholder="Escribe tu correo">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <button class="btn btn-block sendit" type="submit">Enviar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>