<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="powered">
                    <span>
                        © Todos los derechos reservados - Lima, 2018
                    </span>
                </div>
            </div>
        </div>
    </div>
</footer>