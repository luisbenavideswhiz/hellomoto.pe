<title>Moto g6 hellomoto Perú</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0, minimum-scale=1.0">
<meta property="og:url" content="{{ url('moto-g6') }}">
<meta property="og:type" content="website">
<meta property="og:title"
      content="Moto g6 hellomoto Perú">
<meta property="og:description"
      content="Sé el primero en enterarte de lo nuevo que tenemos para ti.">
<meta property="og:image" content="{{ asset('/img/landings/intriga/thumbs.png') }}">

<meta name="author" content="WHIZ - Agencia Digital">
<meta name="title" content="Sorteo hellomoto Perú | MotoGitf | Moto G5 Plus">
<meta name="description" content="Sé el primero en enterarte de lo nuevo que tenemos para ti.">
<meta name="keywords" content="motog6">