<header>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="logo">
                    <img src="{{ asset('img/landings/intriga/logo_motorola.svg') }}" alt="Motorola">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <nav>
                    <a href="https://www.facebook.com/MotorolaPE" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="https://www.instagram.com/motorolape/" target="_blank">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="https://www.youtube.com/user/motorolamobilityperu" target="_blank">
                        <i class="fa fa-youtube"></i>
                    </a>
                </nav>
            </div>
        </div>
    </div>
</header>