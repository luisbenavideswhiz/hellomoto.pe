@extends('landings.motogift.layouts.base')

@section('headerclass')
    class="home"
@endsection

@section('content')
<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="login">
					<button style="display: none;">
						Ingresar
					</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="land_logo">
					<img src="{{ asset ('img/landings/motogift/logo_motogift.png') }}" alt="">
				</div>
				<div class="the_content">
					<p>
						Por la compra de tu moto g5 plus</span> participa por un <span class="amarillo">Master Class</span> de tu preferencia a cargo de nuestros embajadores. Solo sigue estos simples pasos:
					</p>
					<ul>
						<li>
							<strong>
								<span>1</span>
							</strong>
							<p>Ingresa a tu cuenta hellomoto o registrate.</p>
						</li>
						<li>
							<strong>
								<span>2</span>
							</strong>
							<p>Registra el código de tu voucher de compra y escoge una de las 4 Master Class con nuestros <span class="amarillo">Moto Embajadores.</span></p>
						</li>
					</ul>
				</div>
				<div class="emb_slider">
					<div class="embajadores" id="emb_slider">
						<div class="item">
							<div class="avatar">
								<div class="img">
									<img src="{{ asset ('img/landings/motogift/emb2.jpg') }}" alt="">
								</div>
							</div>
							<div class="content">
								<strong>Master Class de tenis con Lucho Horna</strong>
								<p>Gánate una clase personalizada de tenis, observa de cerca los mejores trucos y consejos que tiene para ti.</p>
							</div>
						</div>

						<div class="item">
							<div class="avatar">
								<div class="img">
									<img src="{{ asset ('img/landings/motogift/emb4.jpg') }}" alt="">
								</div>
							</div>
							<div class="content">
								<strong>Master Class de surf con Miguel Rodriguez y Leilani Aguirre</strong>
								<p>Aprende a correr olas con nuestros campeones de surf y bodyboard en una clase llena de adrenalina.</p>
							</div>
						</div>

						<div class="item">
							<div class="avatar">
								<div class="img">
									<img src="{{ asset ('img/landings/motogift/emb3.jpg') }}" alt="">
								</div>
							</div>
							<div class="content">
								<strong>Master Class de arte con Entes y Handra</strong>
								<p>Plasma toda tu creatividad y pinta un increíble mural, aplicando las técnicas artísticas de Entes y Handra.</p>
							</div>
						</div>

						<div class="item">
							<div class="avatar">
								<div class="img">
									<img src="{{ asset ('img/landings/motogift/emb1.jpg') }}" alt="">
								</div>
							</div>
							<div class="content">
								<strong>Master Class en Dance Studio de Fiorella Cayo</strong>
								<p>Dale ritmo a tu día con los mejores profesores de Dance Studio. Podrás elegir una clase de urban dance.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="hidden-xs col-md-1"></div>
			<div class="col-xs-12 col-md-4">
				<div class="form">
					<div class="form-header">
						<h2>
                            LISTO
                        </h2>
                        <h3>
                            Datos registrado exitosamente.
                        </h3>
					</div>
					<form action="" method="POST" id="form_l_motogift" name="form_l_motogift">
						<div class="col1">
                            <div class="input-group">
                                <div class="acciones">
                                	<strong>Tus datos fueron registrados, verificaremos tu número de boleta para que puedas participar del concurso.</strong>
                                </div>
                            </div>
                        </div>
					</form>
				</div>
			</div>
			<div class="hidden-xs col-md-1"></div>
		</div>
	</div>
</div>

<script>
    window.setTimeout(function(){
        window.location.href = "/";
    }, 3500);
</script>
@endsection