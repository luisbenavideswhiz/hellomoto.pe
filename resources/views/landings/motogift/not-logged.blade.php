@extends('landings.motogift.layouts.base')

@section('headerclass')
    class="home"
@endsection

@section('content')
<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="login">
					<button class="btn-login" data-target="_self" data-href="{{ url('/?modal=login') }}">
						Ingresar
					</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="land_logo">
					<img src="{{ asset ('img/landings/motogift/logo_motogift.png') }}" alt="">
				</div>
				<div class="the_content">
					<p>
						Por la compra de tu <span class="amarillo">moto g5 plus</span> participa por un <span class="amarillo">Master Class</span> de tu preferencia. Solo sigue estos simples pasos:
					</p>
					<ul>
						<li>
							<strong>
								<span>1</span>
							</strong>
							<p>Ingresa a tu cuenta hellomoto o registrate.</p>
						</li>
						<li>
							<strong>
								<span>2</span>
							</strong>
							<p>Registra el código de tu voucher de compra y escoge una de las 4 Master Class</p>
						</li>
					</ul>
				</div>
				<div class="emb_slider">
					<div class="embajadores" id="emb_slider">
						<div class="item">
							<div class="avatar">
								<div class="img">
									<img src="{{ asset ('img/landings/motogift/emb2.jpg') }}" alt="">
								</div>
							</div>
							<div class="content">
								<strong>Master Class de tenis con Lucho Horna</strong>
								<p>Gánate una clase personalizada de tenis, observa de cerca los mejores trucos y consejos que tiene para ti.</p>
							</div>
						</div>

						<div class="item">
							<div class="avatar">
								<div class="img">
									<img src="{{ asset ('img/landings/motogift/emb4.jpg') }}" alt="">
								</div>
							</div>
							<div class="content">
								<strong>Master Class de surf con Miguel Rodriguez y Leilani Aguirre</strong>
								<p>Aprende a correr olas con nuestros campeones de surf y bodyboard en una clase llena de adrenalina.</p>
							</div>
						</div>

						<div class="item">
							<div class="avatar">
								<div class="img">
									<img src="{{ asset ('img/landings/motogift/emb3.jpg') }}" alt="">
								</div>
							</div>
							<div class="content">
								<strong>Master Class de arte con Entes y Handra</strong>
								<p>Plasma toda tu creatividad y pinta un increíble mural, aplicando las técnicas artísticas de Entes y Handra.</p>
							</div>
						</div>

						<div class="item">
							<div class="avatar">
								<div class="img">
									<img src="{{ asset ('img/landings/motogift/emb1.jpg') }}" alt="">
								</div>
							</div>
							<div class="content">
								<strong>Master Class en Dance Studio de Fiorella Cayo</strong>
								<p>Dale ritmo a tu día con los mejores profesores de Dance Studio. Podrás elegir una clase de urban dance.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="hidden-xs col-md-1"></div>
			<div class="col-xs-12 col-md-4">
				<div class="form">
					<div class="form-header">
						<h2>
                            PARTICIPA Y SÉ PARTE DE LA <br>
                            COMUNIDAD HELLOMOTO
                        </h2>
                        <h3>
                            Registra tus datos aquí
                        </h3>
					</div>
					<form action="{{ url('/sorteos/motogift') }}" method="POST" id="landing">
						{{ csrf_field() }}
						<div class="col2">
							<div class="input-group">
								<span>Nombre (*)</span>
                                <input type="text" id="name" name="name" class="validate">
							</div>
							<div class="input-group">
								<span>Apellido (*)</span>
                                <input type="text" id="lastname" name="lastname" class="validate">
							</div>
						</div>

						<div class="col2">
							<div class="input-group">
								<span>Género (*)</span>
                                <label for="sex-m">
                                    <input type="radio" name="sex" id="sex-m" value="mujer" class="validate"> <span>Mujer</span>
                                </label>
                                <label for="sex-h">
                                    <input type="radio" name="sex" id="sex-h" value="hombre" class="validate"> <span>Hombre</span>
                                </label>
							</div>
							<div class="input-group">
								<span>DNI (*)</span>
                                <input type="text" id="document_number" name="document_number">
							</div>
						</div>

						<div class="col2">
							<div class="input-group">
								<span>Número de celular (*)</span>
                                <input type="text" id="phone" name="phone">
							</div>
							<div class="input-group">
								<span>Operador (*)</span>
                                <select id="mobile_operator" name="mobile_operator">
                                    <option value="">Seleccionar</option>
                                    <option value="claro">Claro</option>
                                    <option value="movistar">Movistar</option>
                                    <option value="entel">Entel</option>
                                    <option value="bitel">Bitel</option>
                                    <option value="virgin">Virgin</option>
                                    <option value="tuenti">Tuenti</option>
                                </select>
							</div>
						</div>

						<div class="col1">
                            <div class="input-group">
                                <span>Email (*)</span>
                                <input type="text" id="email" name="email">
                            </div>
                        </div>

                        <div class="col1">
                            <div class="input-group">
                                <label for="accept_emails" class="flexme">
                                    <input type="checkbox" name="accept_emails" value="1" id="accept_emails"> <span>Deseo recibir información sobre productos y ofertas especiales por parte de Motorola.</span>
                                </label>
                            </div>
                        </div>

                        <div class="col3">
                            <span>Fecha de nacimiento (*)</span>
                            <div class="input-group">
                                <input type="text" id="birthdate" data-format="YYYY-MM-DD" data-template="DD MMMM YYYY" name="birthdate">
                            </div>
                        </div>

                        <div class="col1">
                            <div class="input-group">
                                <em>Soy mayor de edad</em>
                                <label for="full_age_yes">
                                    <span>Si</span> <input type="radio" name="full_age" id="full_age_yes">
                                </label>
                                <label for="full_age_no">
                                    <span>No</span> <input type="radio" name="full_age" id="full_age_no">
                                </label>
                            </div>
                        </div>

                        <div class="col3">
                            <div class="input-group">
                                <select name="department" id="department">
                                    	<option value="">Departamento</option>
									@foreach($departments as $department)
										<option>{{ $department->department }}</option>
									@endforeach
                                </select>

                                <select name="province" id="province">
                                    <option value="">Provincia</option>
                                </select>

                                <select name="district" id="district">
                                    <option value="">Distrito</option>
                                </select>
                            </div>
                        </div>

                        <div class="col2">
                            <div class="input-group">
                                <span>Contraseña (*)</span>
                                <input type="password" id="password" name="password">
                            </div>
                            <div class="input-group">
                                <span>Repetir Contraseña (*)</span>
                                <input type="password" id="repassword" name="repassword">
                            </div>
                        </div>

                        <div class="col1">
                            <div class="input-group">
                                <div class="acciones">
                                	<button type="submit" class="btn btn-submit">Registrar y participar</button>
									<label for="terms_condition">
										<input type="checkbox" name="terms_conditions">
										<div class="tc">
											<a target="_blank" href="{{ url('/sorteos/motogift/terminos-condiciones') }}">
												Términos y condiciones</a>
											<a target="_blank" href="http://www.motorola.com.pe/institucional/politica-de-privacidad">
												Política de privacidad
											</a>
										</div>
									</label>
                                </div>
                            </div>
                        </div>

					</form>
				</div>
			</div>
			<div class="hidden-xs col-md-1"></div>
		</div>
	</div>
</div>
@endsection