@extends('landings.motogift.layouts.base')

@section('headerclass')
    class="home"
@endsection

@section('content')
<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="login">
					<button style="display: none;">
						Regresar
					</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-7">
				<div class="land_logo">
					<img src="{{ asset ('img/landings/motogift/logo_motogift.png') }}" alt="">
				</div>
				<div class="the_content success">
					<p>
						Te esperamos todo el tiempo que pudimos :( lamentablemente, el sorteo ya no se encuentra disponible. Sin embargo, puedes ingresar a la sección Beneficios de tu perfil y enterarte de las nuevas promociones.
					</p>
					<p>
						<a href="{{ url('/usuario/beneficios') }}">Ver beneficios</a>
						<a href="{{ url('/?modal=register') }}">Crear cuenta</a>
					</p>
				</div>
			</div>
			<div class="col-xs-12 col-md-5">
				<div class="img specs">
					<img src="{{ asset ('img/landings/motogift/moto_specs.png') }}" alt="">
				</div>
			</div>
		</div>
	</div>
</div>
@endsection