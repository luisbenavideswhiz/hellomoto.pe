@extends('landings.motogift.layouts.base')

@section('headerclass')
    class="home"
@endsection

@section('content')
<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="login">
					<button style="display: none;">
						Regresar
					</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-7">
				<div class="land_logo">
					<img src="{{ asset ('img/landings/motogift/logo_motogift.png') }}" alt="">
				</div>
				<div class="the_content disc">
					<strong>
						Términos y condiciones
					</strong>
					<p>
						Pueden participar todas las personas naturales mayores de 18 años y que residan en Lima que compren un Moto G5 Plus entre el 01/03/2018 y el 26/04/2018.
					</p>
					<p>
						Para participar deberán:
						<ul>
							<li>Registrarse en la web: http://hellomoto.pe/sorteos/motogift  con sus datos y el número de voucher de la compra.</li>
							<li>El registro se podrá realizar desde el 27/03/2018 hasta el 26/04/2018.</li>
							<li>El anuncio de los ganadores se realizará el 27/04/2018 mediante una publicación en el Facebook de Motorola.</li>
						</ul>
					</p>
					<p>
						*Muy importante: Los participantes deben conservar el voucher de compra que deberán mostrar en caso resulten ganadores. 
					</p>
					<p>
						Los premios son cupos para distintas Master Class con embajadores de Motorola de acuerdo al siguiente detalle:
						<ul>
							<li>
								Master class de tenis con Luis Horna: 1 cupo
							</li>
							<li>
								Master class de surf con Leilani Aguirre y Miguel Rodriguez: 2 cupos
							</li>
							<li>
								Master class de arte con Entes y Handra: 4 cupos
							</li>
							<li>
								Master class en Dance Studio de Fiorella Cayo: 3 cupos
							</li>
						</ul>
					</p>
					<p>
						En total: 10 cupos 
					</p>
					<p>
						Las fechas y horas de las Master Class serán coordinadas directamente con los ganadores.
					</p>
					<p>
						El concurso no es válido para trabajadores de la agencia Wunderman Phantasia, Whiz o de la empresa de dispositivos móviles Motorola.
					</p>
				</div>
			</div>
			<div class="col-xs-12 col-md-5">
				<div class="img disc">
					<img src="{{ asset ('img/landings/motogift/moto_specs.png') }}" alt="">
				</div>
			</div>
		</div>
	</div>
</div>
@endsection