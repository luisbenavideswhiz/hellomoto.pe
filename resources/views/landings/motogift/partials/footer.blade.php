<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="powered mod">
                    <a target="_blank" href="{{ url('/sorteos/motogift/terminos-condiciones') }}"> Términos y condiciones</a>
                    <a target="_blank" href="http://www.motorola.com.pe/institucional/politica-de-privacidad"> Política de privacidad</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="powered">
                    <span>
                        © Todos los derechos reservados - Lima, 2018
                    </span>
                    <span>
                        Powered by <a class="whiz" data-href="http://whiz.pe" data-target="_blank"><img src="{{ asset('img/landings/motogift/logo_whiz.svg') }}" alt="whiz logo"></a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</footer>