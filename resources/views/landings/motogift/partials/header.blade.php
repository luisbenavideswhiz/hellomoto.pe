<header>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="logo">
                    <h1>
                        <a id="motorola" data-href="{{ url('/') }}" data-target="_blank">
                            motorola
                        </a>
                    </h1>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <nav>
                    <a class="fb" data-target="_blank" data-href="https://www.facebook.com/MotorolaPE">
                        <i class="fa fa-facebook-square"></i>
                    </a>
                    <a class="inst" data-target="_blank" data-href="https://www.instagram.com/motorolape/">
                        <i class="fa fa-instagram"></i>
                    </a>
                </nav>
            </div>
        </div>
    </div>
</header>
