<!DOCTYPE html>
<html lang="es">
<head>
    @include('landings.motogift.partials.fb.pixel.index')
    @include('landings.motogift.partials.metas')
    @include('web.partials.cannonical')
    @include('landings.motogift.partials.styles')
    @include('web.partials.seo.analytics')
</head>
<body id="home" @section('headerclass')@show>

@include('landings.motogift.partials.header')

<div class="loading">
	<div class="load">
		<img src="{{ asset ('img/landings/motogift/loading.svg') }}" alt="">
	</div>
</div>

@yield('content')

@include('landings.motogift.partials.footer')
@include('landings.motogift.partials.scripts')

</body>
</html>