@extends('landings.tolinchi.layouts.base')

@section('headerclass')
    class="home"
@endsection

@section('content')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="land_logo">
                        <img src="{{ asset ('img/landings/tolinchi/h1.svg') }}" alt="">
                    </div>
                    <div class="img tol visible-xs visible-sm">
                        <img src="{{ asset('img/landings/tolinchi/2.png') }}" alt="">
                    </div>
                    <div class="the_content">
                        <p>
                            ¡Música para todo el día! Escucha tus canciones favoritas sin preocuparte por la batería gracias a la carga TurboPower del moto g<sup>6</sup>: 6 horas de energía en 15 minutos.
                        </p>
                        <p>

                            Por la compra de un motog<sup>6</sup>, llévate unos audífonos motopulse 2 y participa del sorteo para disfrutar una divertida noche de música electrónica con el DJ Tolinchilove. ¡Gánate un box con consumo y llénalo de 9 amigos para que disfruten juntos este espectacular evento!
                        </p>
                        <p>
                            ¿Aún no lo tienes? ¿Qué esperas? Consigue tu pack haciendo clic en comprar.
                        </p>
                    </div>
                    <div class="acciones">
                        <a href="#!" class="out" id="shoot_reg" data-toggle="modal" data-target="#myModal">PARTICIPAR</a>
                        <a href="http://catalogo.movistar.com.pe/motorola-moto-g6?p=plan-elige-mas-s-1299-129.9-26-190"
                           class="in" target="_blank">COMPRAR EN MOVISTAR</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="img tol hidden-xs hidden-sm">
                        <img src="{{ asset('img/landings/tolinchi/2.png') }}" alt="">
                    </div>
                </div>
                <div class="hidden-xs col-md-1"></div>
            </div>
        </div>
    </div>
    @include('landings.tolinchi.partials.modal_login')
@endsection