<header>
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="logo">
                    <h1>
                        <a id="motorola" href="{{ url('/') }}" target="_blank">
                            motorola
                        </a>
                    </h1>
                </div>
            </div>
            <div class="col-xs-6">
                <nav>
                    <a class="fb" target="_blank" href="https://www.facebook.com/MotorolaPE">
                        <i class="fa fa-facebook-square"></i>
                    </a>
                    <a class="inst" target="_blank" href="https://www.instagram.com/motorolape/">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a target="_blank" href="https://www.youtube.com/user/motorolamobilityperu">
                        <i class="fa fa-youtube"></i>
                    </a>
                </nav>
            </div>
        </div>
    </div>
</header>
