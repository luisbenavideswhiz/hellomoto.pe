<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/moment-locale-es.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/landings/copamotorola/combodate.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/landings/tolinchi/app.js') }}"></script>

<!--[if lte IE 10]>
<script type="text/javascript" src="{{ asset('js/appLTIE11.js') }}"></script>
<![endif]-->