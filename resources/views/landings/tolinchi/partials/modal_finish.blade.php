<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form">
                    <div class="form-header">
                        <h2>
                            Ups!
                        </h2>
                        <h3>
                            Llegaste tarde.
                        </h3>
                    </div>
                    <form action="" method="POST" id="form_l_mario" name="form_l_mario">
                        <div class="col1">
                            <div class="input-group">
                                <div class="acciones">
                                    <strong>Esta promoción ya terminó. Regresa a nuestra página de Beneficios para conocer nuestras otras novedades.</strong>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>