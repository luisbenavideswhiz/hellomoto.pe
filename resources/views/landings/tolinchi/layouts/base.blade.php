<!DOCTYPE html>
<html lang="es">
<head>
    @include('landings.tolinchi.partials.fb.pixel.index')
    @include('landings.tolinchi.partials.metas')
    @include('web.partials.cannonical')
    @include('landings.tolinchi.partials.styles')
    @include('web.partials.seo.analytics')
</head>
<body id="home" @section('headerclass')@show>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5JC6WBV"
				  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

@include('landings.tolinchi.partials.header')

<div class="loading">
	<div class="load">
		<img src="{{ asset ('img/landings/tolinchi/loading.svg') }}" alt="">
	</div>
</div>

@yield('content')

@include('landings.tolinchi.partials.footer')
@include('landings.tolinchi.partials.scripts')

@section('js')
@show
</body>
</html>