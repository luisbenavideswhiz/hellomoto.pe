@extends('landings.tolinchi.layouts.base')

@section('headerclass')
class="home"
@endsection

@section('content')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="land_logo">
                    <img src="{{ asset ('img/landings/tolinchi/h1.svg') }}" alt="">
                </div>
                <div class="the_content">
                    <p>
                        Música para todo el día. Escucha tus canciones favoritas sin preocuparte por la batería.
                    </p>
                    <p>
                        Por la compra de un motog6, llévate unos audífonos motopulse y participa del sorteo para disfrutar una divertida noche de electrónica con el Dj Tolinchilove. Además, podrás llevar 9 amigos a este espectacular evento.
                    </p>
                    <p>
                        ¿Aún no lo tienes? ¡Qué esperas! consigue tu pack haciendo clic en comprar.
                    </p>
                </div>
                <div class="acciones">
                    <a href="#!" class="out" id="shoot_reg" data-toggle="modal" data-target="#myModal">PARTICIPAR</a>
                    <a href="#!" class="in">COMPRAR</a>
                </div>
            </div>

            <div class="col-xs-12 col-md-6">
                <div class="img tol">
                    <img src="{{ asset('img/landings/tolinchi/2.png') }}" alt="">
                </div>
            </div>
            <div class="hidden-xs col-md-1"></div>
        </div>
    </div>
</div>
@include('landings.tolinchi.partials.modal_finish')
@endsection

@section('js')
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#myModal').modal('show');
        });
        window.setTimeout(function(){
            window.location.href = "/";
        }, 3500);
    </script>
@endsection