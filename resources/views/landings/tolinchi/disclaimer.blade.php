@extends('landings.tolinchi.layouts.base')

@section('headerclass')
    class="disclaimer"
@endsection

@section('content')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="login">
                        <button style="display: none;">
                            Regresar
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <div class="land_logo"></div>
                    <div class="the_content disc">
                        <strong>
                            Términos y condiciones
                        </strong>
                        <p>
                            Pueden participar todas las personas naturales mayores de 18 años y que residan en Lima que compren el Moto g6  y/o Pack Moto G6 + Headphones Motopulse en Movistar entre el 06/06/2018 y el 30/06/2018.
                        </p>
                        <p>
                            Para participar deberán:
                        <ul>
                            <li>Registrarse en la web: http://hellomoto.pe/sorteos/tolinchi con sus datos y el número de voucher de la compra.</li>
                            <li>El registro se podrá realizar desde el 06/06/2018 hasta el 30/06/2018.</li>
                            <li>El anuncio de los ganadores se realizará el 03/07/2018 mediante una publicación en el Facebook de Motorola.</li>
                        </ul>
                        </p>
                        <p>
                            *Muy importante: Los participantes deben conservar el voucher de compra que deberán mostrar
                            en caso resulten ganadores.
                        </p>
                        <p>
                            El premio es un box en la zona VIP para el ganador y 9 invitados en la fiesta de electrónica
                            Bifröst que se llevará a cabo el 13/07/2018; DJ Tolinchi tocará en dicho evento y acompañará
                            al ganador en esta increible experiencia. Todo el detalle del evento se brindará al ganador
                            mediante un correo electrónico.
                        </p>
                        <p>
                            El ganador deberá confirmar su asistencia mediante correo electrónico y enviar los nombres
                            completos y DNIs de sus 9 invitados a más tardar el 06/07/2018 a las 17 00 horas a
                            melissa.reategui@whiz.pe
                        </p>
                        <p>
                            Para más información del evento ingresar a: https://www.facebook.com/events/641706712830480/
                        </p>
                        <p>
                            El sorteo no es válido para trabajadores de la agencia Wunderman Phantasia, Whiz o de la
                            empresa de dispositivos móviles Motorola.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-5">
                    <div class="img disc"></div>
                </div>
            </div>
        </div>
    </div>
@endsection