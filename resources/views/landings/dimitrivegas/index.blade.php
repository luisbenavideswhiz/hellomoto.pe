<!DOCTYPE html>
<html lang="es">
<head>

    {{--@component('landings.dimitrivegas.partials.google.analytics.script')--}}
    {{--@endcomponent--}}

    {{--@component('landings.dimitrivegas.partials.google.tag-manager.script')--}}
    {{--@endcomponent--}}

    @component('landings.dimitrivegas.partials.fb.pixel.index')
    @endcomponent

    @component('landings.dimitrivegas.partials.metas')
    @endcomponent

    @component('landings.dimitrivegas.partials.styles')
    @endcomponent

</head>
<body id="home" class="home">

{{--@component('landings.dimitrivegas.partials.google.tag-manager.code')--}}
{{--@endcomponent--}}

@component('landings.dimitrivegas.partials.update-your-browser')
@endcomponent

<div class="motorola">
    <div class="wrapper">
        <div class="container">
            <div class="row">

                @component('landings.dimitrivegas.partials.header')
                @endcomponent

            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div id="primera">
                        <form role="form" class="content">
                            {{ csrf_field() }}
                            <img src="{{ asset('img/landings/dimitrivegas/dimitri.jpg') }}" alt="" class="logoform">
                            <div class="navc show-res">
                                <nav>
                                    <span class="right">Siguenos en:</span>
                                    <ul>
                                        <li>
                                            <a href="https://www.facebook.com/MotorolaPE" target="_blank">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/motorolape/" target="_blank">
                                                <i class="fa fa-instagram"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/MotorolaPE" target="_blank">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="header">
                                <h3>Participa ingresando tus datos aqu&iacute;</h3>
                            </div>
                            <div class="form">
                                <div id="success-message" class="validacion hidden">
                                    <img src="{{ asset('img/landings/dimitrivegas/check.png') }}" alt="">
                                    <p>Hola <span id="user-name"></span>, solo falta que confirmes tu correo (<span id="user-email"></span>). Revisa tu bandeja de entrada o tu bandeja de correo no deseado.</p>
                                </div>
                                <div class="form-group col2">
                                    <div class="input-field">
                                        <span>Nombres(*)</span>
                                        <input type="text" class="form-control info validate" id="name" name="name" placeholder="Nombre" >
                                    </div>
                                    <div class="input-field">
                                        <span>Apellidos(*)</span>
                                        <input type="text" class="form-control info validate" id="lastname" name="lastname" placeholder="Apellido" >
                                    </div>
                                </div>
                                <div class="form-group col2">
                                    <div class="input-field">
                                        <span>G&eacute;nero</span>
                                        <div class="form-group col2 gender">
                                            <div class="input-field">
                                                <label for="gender_id"><input type="radio" class="validate" id="gender_id" name="gender_id" value="1"> Hombre</label>
                                            </div>
                                            <div class="input-field">
                                                <label for="gender_id"><input type="radio" class="validate" id="gender_id" name="gender_id" value="2"> Mujer</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-field">
                                        <span>DNI(*)</span>
                                        <input type="text" class="form-control info validate" id="document" name="document" placeholder="DNI" >
                                    </div>  
                                </div>
                                <div class="form-group col2">
                                    <div class="input-field">
                                        <span>N&deg; de celular(*)</span>
                                        <input type="text" class="form-control info validate" id="phone" name="phone" placeholder="Celular" >
                                    </div>
                                    <div class="input-field">
                                        <span>Operador</span>
                                        <select name="operator_id" id="operator_id" class="info validate" >
                                            <option value="">Operador</option>
                                            @foreach($operator as $key => $value)
                                                <option value="{{ $value->id }}">{{ $value->value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-field">
                                        <span>Email(*)</span>
                                        <input type="email" class="form-control info validate" id="email" name="email"
                                               placeholder="Email" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-field">
                                        <strong>Aceptar correos electr&oacute;nicos:</strong>
                                        <br class="visible-xs-block">
                                        <input type="radio" class="form-check-input validate" name="accept_email" id="accept_email" value="1"> Si
                                        <input type="radio" class="form-check-input validate" name="accept_email" id="accept_email" value="0"> No
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-field">
                                        <p class="fs12">
                                            Deseo recibir informaci&oacute;n de Motorola por email acerca de productos y ofertas
                                            especiales.
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group mb0">
                                    <div class="input-field">
                                        <span>Fecha de nacimiento</span>
                                    </div>
                                </div>
                                <div class="form-group col3">
                                    <div class="input-field">
                                        <select name="day" class="info validate" id="day" >
                                            <option value="">D&iacute;a</option>
                                            @for($i=1;$i<=31;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="input-field">
                                        <select name="month" class="info validate" id="month" >
                                            <option value="">Mes</option>
                                            <option value="01">Enero</option>
                                            <option value="02">Febrero</option>
                                            <option value="03">Marzo</option>
                                            <option value="04">Abril</option>
                                            <option value="05">Mayo</option>
                                            <option value="06">Junio</option>
                                            <option value="07">Julio</option>
                                            <option value="08">Agosto</option>
                                            <option value="09">Setiembre</option>
                                            <option value="10">Octubre</option>
                                            <option value="11">Noviembre</option>
                                            <option value="12">Diciembre</option>
                                        </select>
                                    </div>  
                                    <div class="input-field">
                                        <select name="year" class="info validate" id="year" >
                                            <option value="">A&ntilde;o</option>
                                            @for($i=2001;$i>=1900;$i--)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-field">
                                        <strong>Verificaci&oacute;n de edad:</strong>
                                        <input type="radio" class="form-check-input validate" name="age_verification" id="age_verification" value="1"> Si
                                        <input type="radio" class="form-check-input validate" name="age_verification" id="age_verification" value="0"> No
                                    </div>
                                </div>
                                <div class="form-group mb0">
                                    <p>
                                        Confirmo que soy mayor de 16 a&ntilde;os.*
                                    </p>
                                </div>
                                <div class="form-group">
                                    <p class="fs12">
                                        Nadie menor de 16 a&ntilde;os podr&aacute; confirmar o presentar ning&uacute;n tipo de informaci&oacute;n en
                                        los sitios web de Motorola. Por favor haz click en el recuadro correspondiente
                                        para confirmar que eres mayor de 16 a&ntilde;os.
                                    </p>
                                </div>
                                <div class="form-group col2">
                                    <div class="input-field">
                                        <div class="links">
                                            <a href="https://www.motorola.com.pe/legal/terms-of-use" target="_blank">> <u>T&eacute;rminos y condiciones</u></a>
                                            <a href="https://www.motorola.com.pe/legal/privacy-policy" target="_blank">> <u>Pol&iacute;tica de privacidad</u></a>
                                        </div>
                                    </div>
                                    <div class="input-field">
                                        <a href="#" id="register-btn" data-url="/dimitrivegas/register" class="btn btn-primary">
                                            <strong>Participar </strong>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    @component('landings.dimitrivegas.partials.content')
                    @endcomponent

                </div>
            </div>
        </div>

        @component('landings.dimitrivegas.partials.footer')
        @endcomponent

    </div>
</div>

@component('landings.dimitrivegas.partials.scripts')
@endcomponent

</body>
</html>
