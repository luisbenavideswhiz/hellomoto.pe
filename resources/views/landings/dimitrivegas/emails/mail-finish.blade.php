<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no"/>
    <!--[if !mso]>
            <!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!--
            <![endif]--><!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <title></title>
    <style type="text/css">div, p, a, li, td {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        body {
            min-width: 100% !important;
            Margin: 0;
            padding: 0;
        }
    </style>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css" media="all">
        td[class="outlook-f"] {
            width: 244px !important;
        }

        td[class="outlook-c"] {
            width: 500px !important;
        }

        @media only screen and (max-width: 480px) {
            td[class="outlook-f"] {
                width: 100% !important
            }
        }
    </style>
    <![endif]-->
</head>
<body bgcolor="#ffffff" marginleft="0" marginright="0" margintop="0"
      style="margin: 0px; padding: 0px; width: 100% !important;">
<p><span class="hidesumary" style="display: none;"></span></p>
<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="">
    <tbody style="">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" class="w100pc" width="600"
                   style="width: 640px;height: 278px;margin-bottom:-3px;">
                <tr>
                    <td>
                        <img src="{{ asset('img/landings/dimitrivegas/emails/parte1.jpg') }}" alt="" style="display: block;">
                    </td>
                </tr>
            </table>
            <table align="center" border="0" cellpadding="0" cellspacing="0" class="w90pc" width="520"
                   style="width: 640px;height: 200px;background-color:#323f48;">
                <tbody>
                <tr>
                    <td style="font-family: Arial, Helvetica, sans-serif; padding-left: 20px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td style="width: 20px; "></td>
                                <td style="font-size: 16px; color:#fff;">
                                    <p style="margin-top: 6px;">
                                        Gracias por querer ser parte de este gran evento. <br>
                                        Si quieres saber si eres uno de los afortunados ganadores de las entradas dobles VIP y ULTRA VIP,
                                        ingresa aqu&iacute;:
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" height="7px"></td>
                            </tr>
                            <tr style="display: ;">
                                <td style="width: 20px; "></td>
                                <td style="font-size: 14px; line-height: 16px; color:#fff;">
                                    <a href="https://www.facebook.com/MotorolaPE" target="_blank" style="background-color: rgb(230, 107, 0);border-color: rgb(230, 107, 0);min-height: 45px;width: 240px;font-size: 18px;color: rgb(255, 255, 255);display: block;text-align: center;line-height: 45px;text-decoration: none;"><strong>Desc&uacute;brelo aqu&iacute;.</strong></a>
                                </td>
                                <td style="width: 100px;display: flex;justify-content: flex-end;padding-right: 30px;margin-top: 20px;">
                                    <a href="https://www.facebook.com/MotorolaPE" target="_blank" style="text-decoration: none;">
                                        <img src="{{ asset('img/landings/dimitrivegas/emails/facebook.png') }}" alt=""
                                             style="width: 24px;height: 24px;padding-left: 7px;"/>
                                    </a>
                                    <a href="https://www.instagram.com/motorolape/" target="_blank" style="text-decoration: none;">
                                        <img src="{{ asset('img/landings/dimitrivegas/emails/instagram.png') }}" alt=""
                                             style="width: 24px;height: 24px;padding-left: 7px;"/>
                                    </a>
                                    <a href="https://twitter.com/MotorolaPE" target="_blank" style="text-decoration: none;">
                                        <img src="{{ asset('img/landings/dimitrivegas/emails/twitter.png') }}" alt=""
                                             style="width: 24px;height: 24px;padding-left: 7px;"/>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" border="0" cellpadding="0" cellspacing="0" class="w100pc" width="600"
                   style="width: 640px;height: 60px">
                <tbody>
                <tr>
                    <td class="hide" style="font-family: Arial, Helvetica, sans-serif;">
                        <table align="center" bgcolor="#000" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td colspan="7" height="7"></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <strong style="color:#fff;padding-left: 30px;">#hello<span style="color:#e66b00;">moto</span></strong>
                                </td>
                                <td style="color: #ffffff; font-size: 10px;padding-right: 25px;">
												<span style="float:right;	">2017 Motorola Mobility LLC. &copy; Todos los derechos reservados - Lima, 2017 Powered by
													<img src="{{ asset('img/landings/dimitrivegas/emails/whiz_logo_b.png') }}" alt=""
                                                         style="width: 48px; height: 8px;"/>
												</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" height="7"></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" height="7">
                        <br>
                        <small style="font-size: 10px;">
                            Motorola se adhiere a lo establecido por el Decreto Ley N&deg; 28493, el cual regula el uso de correo electr&oacute;nico comercial no solicitado [SPAM]. &quot;Un email no podr&aacute; ser considerado SPAM mientras incluya la forma de ser removido. Si este correo electr&oacute;nico no fuese de su inter&eacute;s, le pedimos disculpas por la molestia. Si no desea recibir informaci&oacute;n de nuestros productos o promociones, le agradeceremos darse de baja aqu&iacute;
                        </small>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

</body>
</html>