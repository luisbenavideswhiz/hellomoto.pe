<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/landings/dimitrivegas/app.js') }}"></script>

<!--[if lte IE 10]>
<script type="text/javascript" src="{{ asset('js/appLTIE11.js') }}"></script>
<![endif]-->