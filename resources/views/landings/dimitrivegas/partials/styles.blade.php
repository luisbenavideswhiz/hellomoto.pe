<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">

<link rel="icon" type="image/x-icon" href="{{ asset('/favicon.ico') }}">
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('/favicon.ico') }}">

<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/landings/dimitrivegas/style.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/landings/dimitrivegas/responsive.css') }}" type="text/css">

<!--[if lte IE 10]>
<link rel="stylesheet" href="{{ asset('css/styleLTIE11.css') }}" type="text/css">
<![endif]-->
