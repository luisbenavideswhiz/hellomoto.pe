<div class="container-fluid" style="background-color: #000000;">
    <div class="row">
        <footer>
            <div class="col-xs-12 col-md-4 hmoto">
                <strong>#hello<span>moto</span></strong>
            </div>
            <div class="col-xs-12 col-md-8 copyright">
                <p class="visible-lg visible-md">
                    &copy; Todos los derechos reservados - Lima 2017 Powered by <a href="http://whiz.pe"
                                                                                   target="_blank"><img
                                src="{{ asset('img/landings/dimitrivegas/logo_whiz.svg') }}" alt="whiz logo"></a>
                </p>

                <p class="visible-lg visible-md">
                    Motorola y la M estilizada son marcas registradas de Motorola Trademark Holdings, LLC.
                </p>

                <p class="visible-xs visible-sm text-center">
                    &copy; Todos los derechos reservados - Lima 2017 <br>
                    Powered by <a href="http://whiz.pe" target="_blank"><img
                                src="{{ asset('img/landings/dimitrivegas/logo_whiz.svg') }}" alt="whiz logo"></a>
                </p>

                <p class="visible-xs visible-sm text-center">
                    Motorola y la M estilizada son marcas registradas de Motorola Trademark Holdings, LLC.
                </p>
            </div>
        </footer>
    </div>
</div>