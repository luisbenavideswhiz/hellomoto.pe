<title>Motorola - Dimitri Vegas & Like Mike</title>

<meta charset="utf-8">
<meta name="viewport"
      content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0, minimum-scale=1.0">
<meta property="og:url" content="">
<meta property="og:type" content="website">
<meta property="og:title"
      content="&iexcl;Motorola presenta: Dimitri Vegas &amp; Like Mike! El 3 de noviembre en el Club Cultural Lima!">
<meta property="og:description"
      content="G&aacute;nate una entrada doble. No te pierdas a los headliners de Tomorrowland #1 del mundo.">
<meta property="og:image" content="{{ asset('/img/landings/dimitrivegas/200x200.jpg') }}">

<meta name="author" content="WHIZ - Agencia Digital">
<meta name="title" content="Motorola presenta: Dimitri Vegas &amp; Like Mike">
<meta name="description"
      content="G&aacute;nate una entrada doble al concierto el 3 de noviembre en el Club Cultural Lima">
<meta name="keywords"
      content="dimitri vegas &amp; like mike, dimitri vegas, like mike, house music, rave lima peru 2017, dimitri vegas &amp; like mike lima 2017, club cultural lima, av. alameda sur chorrillos, teleticket">