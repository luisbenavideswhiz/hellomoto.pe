<!DOCTYPE html>
<html lang="es">
<head>

    {{--@component('landings.dimitrivegas.partials.google.analytics.script')--}}
    {{--@endcomponent--}}

    {{--@component('landings.dimitrivegas.partials.google.tag-manager.script')--}}
    {{--@endcomponent--}}

    @component('landings.dimitrivegas.partials.fb.pixel.gracias')
    @endcomponent

    @component('landings.dimitrivegas.partials.metas')
    @endcomponent

    @component('landings.dimitrivegas.partials.styles')
    @endcomponent

</head>

<body class="cconfirmation">

{{--@component('landings.dimitrivegas.partials.google.tag-manager.code')--}}
{{--@endcomponent--}}

@component('landings.dimitrivegas.partials.update-your-browser')
@endcomponent

<div class="motorola">
    <div class="wrapper">
        <div class="container">
            <div class="row">

                @component('landings.dimitrivegas.partials.header')
                @endcomponent

            </div>
            <div class="row">
                <div class="col-xs-12">
                    @if($exist == true)
                        @if($user->confirm == 1)
                            <div id="primera">
                                <form role="form" class="confirmation">
                                    <img src="{{ asset('img/landings/dimitrivegas/dimitri.jpg') }}" alt="Dimitri Vegas" class="logoform">
                                    <div class="navc show-res">
                                        <nav>
                                            <span class="right">Siguenos en:</span>
                                            <ul>
                                                <li>
                                                    <a href="https://www.facebook.com/MotorolaPE" target="_blank">
                                                        <i class="fa fa-facebook"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.instagram.com/motorolape/" target="_blank">
                                                        <i class="fa fa-instagram"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://twitter.com/MotorolaPE" target="_blank">
                                                        <i class="fa fa-twitter"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <div class="header">
                                        <h3>&iexcl;Excelente!</h3>
                                    </div>
                                    <div class="form">
                                        <h3>{{ ucwords($user->name) }}, <br> <br> &iexcl;Gracias por registrarte al
                                            sorteo de una entrada doble!</h3>
                                        <h5>Estate atento a nuestras redes sociales.</h5>
                                        <a href="https://www.motorola.com.pe/home" target="_blank" class="btn btn-success">
                                            Visita nuestra web
                                        </a>
                                    </div>
                                </form>
                            </div>
                        @elseif($user->confirm == 0)
                            <div id="primera">
                                <form role="form" class="confirmation">
                                    <img src="{{ asset('img/landings/dimitrivegas/dimitri.jpg') }}" alt="Dimitri Vegas" class="logoform">
                                    <div class="navc show-res">
                                        <nav>
                                            <span class="right">Siguenos en:</span>
                                            <ul>
                                                <li>
                                                    <a href="https://www.facebook.com/MotorolaPE" target="_blank">
                                                        <i class="fa fa-facebook"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.instagram.com/motorolape/" target="_blank">
                                                        <i class="fa fa-instagram"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://twitter.com/MotorolaPE" target="_blank">
                                                        <i class="fa fa-twitter"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <div class="header">
                                        <h3>&iexcl;Bienvenido!</h3>
                                    </div>
                                    <div class="form">
                                        <h3>{{ ucwords($user->name) }}, <br> <br> Ya registraste tus datos.</h3>
                                        <h5>Ya eres parte de la comunidad hellomoto.</h5>
                                        <a href="https://www.motorola.com.pe/home" target="_blank" class="btn btn-success">
                                            Visita nuestra web
                                        </a>
                                    </div>
                                </form>
                            </div>
                        @endif
                    @elseif($exist == false)
                        <div id="primera">
                            <form role="form" class="confirmation">
                                <div class="header">
                                    <h3>&iexcl;Uy!</h3>
                                </div>
                                <div class="form">
                                    <h3>El usuario no existe</h3>
                                    <h5>No hemos podido procesar esta solicitud.</h5>
                                    <a href="https://www.motorola.com.pe/home" target="_blank" class="btn btn-success">
                                        Visita nuestra web
                                    </a>
                                </div>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        @component('landings.dimitrivegas.partials.footer')
        @endcomponent

    </div>
</div>

@component('landings.dimitrivegas.partials.scripts')
@endcomponent

</body>
</html>
