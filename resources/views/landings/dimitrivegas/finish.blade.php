<!DOCTYPE html>
<html lang="es">
<head>

    {{--@component('landings.dimitrivegas.partials.google.analytics.script')--}}
    {{--@endcomponent--}}

    {{--@component('landings.dimitrivegas.partials.google.tag-manager.script')--}}
    {{--@endcomponent--}}

    @component('landings.dimitrivegas.partials.fb.pixel.gracias')
    @endcomponent

    @component('landings.dimitrivegas.partials.metas')
    @endcomponent

    @component('landings.dimitrivegas.partials.styles')
    @endcomponent

</head>

<body class="cconfirmation">

{{--@component('landings.dimitrivegas.partials.google.tag-manager.code')--}}
{{--@endcomponent--}}

@component('landings.dimitrivegas.partials.update-your-browser')
@endcomponent

<div class="motorola">
    <div class="wrapper">
        <div class="container">
            <div class="row">

                @component('landings.dimitrivegas.partials.header')
                @endcomponent

            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div id="primera">
                        <form role="form" class="confirmation">
                        <img src="{{ asset('img/landings/dimitrivegas/dimitri.png') }}" alt="Dimitri Vegas"
                             class="logoform">

                            <div class="navc show-res">
                                <nav>
                                    <span class="right">Siguenos en:</span>
                                    <ul>
                                        <li>
                                            <a href="https://www.facebook.com/MotorolaPE" target="_blank">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/motorolape/" target="_blank">
                                                <i class="fa fa-instagram"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/MotorolaPE" target="_blank">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="header">
                                <h3>hellomoto</h3>
                            </div>
                            <div class="form">
                                <h5>El plazo para participar ya termin&oacute;. El sorteo de las entradas se realizar&aacute;
                                    este 27 de octubre por el fanpage de Motorola Per&uacute;.</h5>
                                <a href="https://www.motorola.com.pe/home" target="_blank"
                                   class="btn btn-success">
                                    Visita nuestra web
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @component('landings.dimitrivegas.partials.footer')
        @endcomponent

    </div>
</div>

@component('landings.dimitrivegas.partials.scripts')
@endcomponent

</body>
</html>
