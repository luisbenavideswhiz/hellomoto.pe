@extends('landings.mario.layouts.base')

@section('headerclass')
    class="home"
@endsection

@section('content')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="land_logo">
                        <img src="{{ asset ('img/landings/mario/h1.svg') }}" alt="">
                    </div>
                    <div class="the_content">
                        <p>
                            Pantalla más grande. No solo para ver las mejores fotos sino para aprender cómo tomarlas.
                        </p>
                        <p>
                            Por la compra de un motog6, llévate un trípode y participa del concurso para disfrutar de
                            una tarde con el fotógrafo Mario Arevalo, donde te revelará todos sus tips y trucos para
                            tomar las mejores fotos con tu celular. Además tienes la oportunidad de ganarte el curso
                            online de Mario.
                        </p>
                        <p>
                            ¿Aún no lo tienes?<br>
                            ¡Qué esperas! consigue tu pack haciendo clic en comprar.
                        </p>
                    </div>
                    <div class="acciones">
                        <a href="#!" class="in" id="shoot_reg" data-toggle="modal" data-target="#myModal">PARTICIPAR</a>
                        <a href="http://www.entel.pe/promo/moto-g6/" class="out" target="_blank">COMPRAR</a>
                    </div>
                </div>

                <div class="col-xs-12 col-md-6">
                    <div class="img tol">
                        <img src="{{ asset('img/landings/mario/2.png') }}" alt="">
                    </div>
                </div>
                <div class="hidden-xs col-md-1"></div>
            </div>
            <div class="row">
                <div class="hidden-xs hidden-sm col-md-1 col-lg-1"></div>
                <div class="hidden-xs col-sm-12 col-md-9 col-lg-9">
                    <div class="moto_slider" id="moto_slider">
                        <div class="item">
                            <div class="img">
                                <img src="{{ asset('img/landings/mario/helloyou.png') }}" alt="">
                            </div>
                        </div>
                        <div class="item">
                            <div class="img">
                                <img src="{{ asset('img/landings/mario/helloyou2.png') }}" alt="">
                            </div>
                        </div>
                        <div class="item">
                            <div class="img">
                                <img src="{{ asset('img/landings/mario/helloyou3.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hidden-xs hidden-sm col-md-2 col-lg-2"></div>
            </div>
        </div>
    </div>
    @include('landings.mario.partials.modal_logged')
@endsection

@section('js')
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#myModal').modal('show');
        });
    </script>
@endsection