<!DOCTYPE html>
<html lang="es">
<head>
    @include('landings.mario.partials.fb.pixel.index')
    @include('landings.mario.partials.metas')
    @include('web.partials.cannonical')
    @include('landings.mario.partials.styles')
    @include('web.partials.seo.analytics')
</head>
<body id="home" @section('headerclass')@show>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5JC6WBV"
				  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

@include('landings.mario.partials.header')

<div class="loading">
	<div class="load">
		<img src="{{ asset ('img/landings/mario/loading.svg') }}" alt="">
	</div>
</div>

@yield('content')

@include('landings.mario.partials.footer')
@include('landings.mario.partials.scripts')
@section('js')
@show
</body>
</html>