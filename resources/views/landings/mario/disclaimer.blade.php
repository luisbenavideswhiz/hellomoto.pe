@extends('landings.mario.layouts.base')

@section('headerclass')
    class="home"
@endsection

@section('content')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="login">
                        <button style="display: none;">
                            Regresar
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-7 col-lg-offset-2">
                    <div class="land_logo">
                    </div>
                    <div class="the_content disc">
                        <strong>Términos y condiciones</strong>
                        <p>
                            Pueden participar todas las personas naturales mayores de 18 años y que residan en Lima que compren el Moto G6 y/o Pack Moto G6 + Trípode en Entel entre el 08/06/2018 y el 27/06/2018.
                        </p>
                        <p>
                            Para participar deberán:
                        <ul>
                            <li>Registrarse en la web: http://hellomoto.pe/sorteos/mario con sus datos y el número de voucher de la compra.
                            </li>
                            <li>El registro se podrá realizar desde el 08/06/2018 hasta el 27/06/2018.</li>
                            <li>El anuncio de los ganadores se realizará el 03/07/2018 mediante una publicación en el Facebook de Motorola.
                            </li>
                        </ul>
                        </p>
                        <p>
                            *Muy importante: Los participantes deben conservar el voucher de compra que deberán mostrar en caso resulten ganadores.
                        </p>
                        <p>
                            Los premios son 5 cupos para una Master Class con Mario Arévalo el 06/07/2018 que tendrá una duración aproximada de 2 horas. Todo el detalle de la actividad se brindará a los ganadores mediante un correo electrónico.
                        </p>
                        <p>
                            El concurso no es válido para trabajadores de la agencia Wunderman Phantasia, Whiz o de la
                            empresa de dispositivos móviles Motorola.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-5">
                    <div class="img disc hidden-xs">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection