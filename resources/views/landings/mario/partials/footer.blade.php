<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="powered modd">
                    <div class="visible-xs">
                        <nav>
                            <a class="fb" data-target="_blank" data-href="https://www.facebook.com/MotorolaPE">
                                <i class="fa fa-facebook-square"></i>
                            </a>
                            <a class="inst" data-target="_blank" data-href="https://www.instagram.com/motorolape/">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="https://www.youtube.com/user/motorolamobilityperu" target="_blank">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </nav>
                    </div>
                    <a target="_blank" href="{{ url('/sorteos/mario/terminos-condiciones') }}">
                        hello<span>moto.pe</span></a>
                </div>
                <div class="powered mod">
                    <a target="_blank" href="{{ url('/sorteos/mario/terminos-condiciones') }}">
                        Términos y condiciones</a>
                </div>
                <div class="powered">
                    <div class="left">
                    </div>
                    <div class="right">
                        <span>© Todos los derechos reservados - Lima, 2018 </span>
                        <span>
                            Powered by
                            <a class="whiz" data-href="http://whiz.pe" data-target="_blank">
                                <img src="{{ asset('img/landings/tolinchi/logo_whiz.svg') }}" alt="whiz logo">
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>