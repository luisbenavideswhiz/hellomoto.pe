<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form">
                    <div class="form-header">
                        <h2>
                            CONFIRMA TUS DATOS Y <br>
                            PARTICIPA DEL SORTEO
                        </h2>
                        <h3>
                            Registra tus datos aquí
                        </h3>
                    </div>
                    <form action="{{ url('sorteos/mario/bienvenido') }}" method="POST" id="landing">
                        {{ csrf_field() }}
                        <input type="hidden" name="landing" value="{{ $landing->id }}">
                        <div class="col1 mod">
                            <div class="input-group info">
                                <span>Nro Boleta de compra*</span>
                                <input type="text" id="nro_boleta" name="ticket" autocomplete="off">
                                <div class="show-info" data-html="true" data-trigger="click" data-toggle="tooltip"
                                     title=" <img src='{{ asset ('img/landings/mario/boleta.jpg') }}' /> "
                                     data-placement="bottom"><i class="fa fa-info-circle"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col1">
                            <div class="input-group">
                                <div class="acciones">
                                    <button type="submit" class="btn">Confirmar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>