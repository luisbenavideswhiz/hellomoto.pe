<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form">
                    <div class="form-header">
                        <h2>
                            LISTO
                        </h2>
                        <h3>
                            Datos registrado exitosamente.
                        </h3>
                    </div>
                    <form action="" method="POST" id="form_l_mario" name="form_l_mario">
                        <div class="col1">
                            <div class="input-group">
                                <div class="acciones">
                                    <strong>Tus datos fueron registrados, verificaremos tu número de boleta para que puedas participar del concurso.</strong>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>