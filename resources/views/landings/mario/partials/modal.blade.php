<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form">
                    <div class="form-header">
                        <h2>
                            PARTICIPA Y SÉ PARTE DE LA <br>
                            COMUNIDAD HELLOMOTO
                        </h2>
                        <h3>
                            Registra tus datos aquí
                        </h3>
                    </div>
                    <form action="{{ url('/sorteos/mario') }}" method="POST" id="landing">
                        {{ csrf_field() }}
                        <div class="col2">
                            <div class="input-group">
                                <span>Nombre (*)</span>
                                <input type="text" id="name" name="name" class="validate">
                            </div>
                            <div class="input-group">
                                <span>Apellido (*)</span>
                                <input type="text" id="lastname" name="lastname" class="validate">
                            </div>
                        </div>

                        <div class="col2">
                            <div class="input-group">
                                <span>Género (*)</span>
                                <label for="sex-m">
                                    <input type="radio" name="sex" id="sex-m" value="mujer" class="validate"> <span>Mujer</span>
                                </label>
                                <label for="sex-h">
                                    <input type="radio" name="sex" id="sex-h" value="hombre" class="validate"> <span>Hombre</span>
                                </label>
                            </div>
                            <div class="input-group">
                                <span>DNI (*)</span>
                                <input type="text" id="document_number" name="document_number">
                            </div>
                        </div>

                        <div class="col2">
                            <div class="input-group">
                                <span>Número de celular (*)</span>
                                <input type="text" id="phone" name="phone">
                            </div>
                            <div class="input-group">
                                <span>Operador (*)</span>
                                <select id="mobile_operator" name="mobile_operator">
                                    <option value="">Seleccionar</option>
                                    <option value="entel">Entel</option>
                                </select>
                            </div>
                        </div>

                        <div class="col1">
                            <div class="input-group">
                                <span>Email (*)</span>
                                <input type="text" id="email" name="email">
                            </div>
                        </div>

                        <div class="col1">
                            <div class="input-group">
                                <label for="accept_emails" class="flexme">
                                    <input type="checkbox" name="accept_emails" value="1" id="accept_emails"> <span>Deseo recibir información sobre productos y ofertas especiales por parte de Motorola.</span>
                                </label>
                            </div>
                        </div>

                        <div class="col3">
                            <span>Fecha de nacimiento (*)</span>
                            <div class="input-group">
                                <input type="text" id="birthdate" data-format="YYYY-MM-DD" data-template="DD MMMM YYYY" name="birthdate">
                            </div>
                        </div>

                        <div class="col1">
                            <div class="input-group">
                                <em>Soy mayor de edad</em>
                                <label for="full_age_yes">
                                    <span>Si</span> <input type="radio" name="full_age" id="full_age_yes">
                                </label>
                                <label for="full_age_no">
                                    <span>No</span> <input type="radio" name="full_age" id="full_age_no">
                                </label>
                            </div>
                        </div>

                        <div class="col3">
                            <div class="input-group">
                                <select name="department" id="department">
                                    <option value="">Departamento</option>
                                    @foreach($departments as $department)
                                        <option>{{ $department->department }}</option>
                                    @endforeach
                                </select>

                                <select name="province" id="province">
                                    <option value="">Provincia</option>
                                </select>

                                <select name="district" id="district">
                                    <option value="">Distrito</option>
                                </select>
                            </div>
                        </div>

                        <div class="col2">
                            <div class="input-group">
                                <span>Contraseña (*)</span>
                                <input type="password" id="password" name="password">
                            </div>
                            <div class="input-group">
                                <span>Repetir Contraseña (*)</span>
                                <input type="password" id="repassword" name="repassword">
                            </div>
                        </div>

                        <div class="col1">
                            <div class="input-group">
                                <div class="acciones">
                                    <button type="submit" class="btn btn-submit">Registrar y participar</button>
                                    <label for="terms_condition">
                                        <input type="checkbox" name="terms_conditions">
                                        <div class="tc">
                                            <a target="_blank" href="{{ url('/sorteos/mario/terminos-condiciones') }}">
                                                Términos y condiciones</a>
                                            <a target="_blank" href="http://www.motorola.com.pe/institucional/politica-de-privacidad">
                                                Política de privacidad
                                            </a>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>