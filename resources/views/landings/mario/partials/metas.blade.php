<title>Mario | Nuevo Moto G6 + tripode</title>

<meta charset="utf-8">
<meta name="viewport"
      content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0, minimum-scale=1.0">
<meta property="og:url" content="{{ url('sorteos/mario') }}">
<meta property="og:type" content="website">
<meta property="og:title"
      content="Sorteo hellomoto Perú | Mario | Nuevo Moto G6 + tripode">
<meta property="og:description"
      content="Aprende los mejores trucos de fotografía junto a Mario Arevalo y Motorola. Entérate de más entrando aquí.">
<meta property="og:image" content="{{ asset('/img/landings/mario/thumb.png') }}">

<meta name="author" content="WHIZ - Agencia Digital">
<meta name="title" content="Sorteo hellomoto Perú | Mario | Nuevo Moto G6 + tripode">
<meta name="description"
      content="Aprende los mejores trucos de fotografía junto a Mario Arevalo y Motorola. Entérate de más entrando aquí.">
<meta name="keywords" content="Mario, Arevalo, Trípode, Pack, motog6, fotografía, motorola, sorteo.">