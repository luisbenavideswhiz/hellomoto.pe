@foreach($rows as $key => $value)
    <div class="item">
        <div class="hello">
            <div class="acciones">
                <a href="#" class="unlike" data-referal-id="{{ $value->webPlaylists[0]->id }}" data-url="/ambassadors/playlists/unlike">
                    <div class="give_star">
                        <i class="fa fa-close"></i>
                    </div>
                </a>
            </div>
            <div class="img">
                <img src="{{ $value->webPlaylists[0]->image }}" alt="{{ $value->webPlaylists[0]->title }}">

                <div class="date"></div>
            </div>
        </div>
        <div class="data">
            <span>
                <a href="/motoembajadores/playlist/{{ $value->webPlaylists[0]->slug }}">{{ $value->webPlaylists[0]->title }}</a>
            </span>
            <strong><a href="/motoembajadores/playlist/{{ $value->webPlaylists[0]->slug }}">{{ $value->webPlaylists[0]->webAmbassador->ambassador_fullname }}</a></strong>
        </div>
    </div>
@endforeach