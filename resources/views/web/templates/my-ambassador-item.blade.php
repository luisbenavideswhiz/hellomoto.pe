@foreach($rows as $key => $value)
    <div class="item">
        <div class="hello">
            <div class="acciones">
                <a href="#" class="unfollow" data-referal-id="{{ $value->webAmbassador->id }}" data-url="/ambassadors/unfollow">
                    <div class="give_star">
                        <i class="fa fa-close"></i>
                    </div>
                </a>
            </div>
            <div class="img">
                <img src="{{ $value->webAmbassador->profile_img }}" alt="{{ $value->webAmbassador->ambassador_fullname }}">

                <div class="date"></div>
            </div>
        </div>
        <div class="data">
            <span>
                <a href="/motoembajadores/embajador/{{ $value->webAmbassador->slug }}">{{ $value->webAmbassador->ambassador_fullname }}</a>
            </span>
            <strong><a href="/motoembajadores/embajador/{{ $value->webAmbassador->slug }}">{{ $value->webAmbassador->webCareer->name }}</a></strong>
        </div>
    </div>
@endforeach