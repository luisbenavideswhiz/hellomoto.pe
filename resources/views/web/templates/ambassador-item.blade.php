@foreach($rows as $key => $value)
    <div id="ambassador-id-{{ $value->id }}" class="col-xs-12 col-md-3">
        <div id="{{ $value->id }}" class="item type1">
            <div class="hello">
                <div class="hello_data">
                    <span>
                        moto<span>embajadores</span>
                    </span>
                </div>
                <div class="img">
                    <a href="/motoembajadores/embajador/{{ $value->slug }}">
                        <img src="{{ $value->profile_img }}" alt="{{ $value->ambassador_fullname }}">
                    </a>
                </div>
                <div class="hover">
                    {!! $value->ambassador_socialnetworks !!}
                </div>
            </div>
            <div class="data">
                <a href="/motoembajadores/embajador/{{ $value->slug }}">
                    @if(!is_null($value->ambassador_fullname))
                        <span>{{ $value->ambassador_fullname }}</span>
                    @endif
                    <strong>{{ $value->webCareer->name }}</strong>
                </a>
            </div>
        </div>
    </div>
@endforeach