@foreach($rows as $key => $value)
    <div id="playlist-id-{{ $value->id }}" class="col-xs-12 col-md-3">
        <div class="item type1">
            <a href="/motoembajadores/playlist/{{ $value->slug }}">
                <div class="hello mod">
                    <div class="img">
                        <img src="{{ $value->image }}" alt="{{ $value->webAmbassador->ambassador_fullname }}">
                    </div>
                    <div class="hover">
                    <span>
                        <i class="fa fa-play"></i>
                    </span>
                    </div>
                </div>
                <div class="data">
                    <strong>{{ $value->webAmbassador->ambassador_fullname }}</strong>
                    <span>{{ $value->title }}</span>
                </div>
            </a>
        </div>
    </div>
@endforeach