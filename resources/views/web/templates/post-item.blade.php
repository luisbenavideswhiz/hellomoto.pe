@foreach($rows as $key => $value)
    <div id="post-id-{{ $value->id }}" class="col-xs-12 col-md-4">
        <div class="item {{ $value->type_class }}">
            <a href="/post/{{ $value->slug }}">
                <div class="hello">
                    <div class="hello_data">
                        {!! $value->category_html !!}
                    </div>
                    <div class="img">
                        <img src="{{ url($value->thumb_img) }}" alt="{{ $value->title }}">

                        <div class="date">
                            <span>{{ $value->created_at_readable }}</span>
                        </div>
                    </div>
                </div>
                <div class="data">
                    @if($value->category_slug != 'general')
                        <strong>{{ $value->category_name }}</strong>
                    @endif
                    <span>{{ $value->title }}</span>
                </div>
            </a>
        </div>
    </div>
@endforeach

