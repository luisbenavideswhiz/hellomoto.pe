<!doctype html>
<html amp lang="es">
<head>
    <meta charset="UTF-8">
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <title>{{ getenv('APP_TITLE') }}</title>
    <link rel="canonical" href="http://example.ampproject.org/article-metadata.html">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "NewsArticle",
            "headline": "Open-source framework for publishing content",
            "datePublished": "2015-10-07T12:02:41Z",
            "image": [
                "/img/web/amp/logo.svg"
            ]
        }
    </script>
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    <style amp-custom>
        body {
            background-color: #F8F8F8;
            font-family: 'Montserrat', sans-serif;
        }
        .header {
            align-items: center;
            background-color: #0B3845;
            display: flex;
            height: 51px;
            justify-content: center;
            z-index: 999;
        }
        .header span {
            color: white;
            font-size: 26px;
            font-weight: 700;
        }
        .header span span {
            color: #FEE600;
        }

        .dbanner {
            border-bottom: 10px solid #FF6A00;
            height: 500px;
            position: relative;
        }
        .dbanner .img {
            overflow: hidden;
        }
        .dbanner .img img {
            display: block;
            -o-object-fit: cover;
            object-fit: cover;
            font-family: 'object-fit: cover;';
        }
        .dbanner .data-single {
            background: linear-gradient(to bottom, transparent 0%, rgba(0, 0, 0, 0.65) 66%, rgba(0, 0, 0, 0.65) 100%);
            bottom: 0;
            padding: 20px;
            position: absolute;
            text-align: center;
        }
        .dbanner .data-single .single {
            background: rgba(0, 0, 0, 0.6);
            font-weight: 500;
            font-size: 18px;
            padding: 3px 8px;
        }
        .dbanner .data-single .single span {
            color: white;
        }
        .dbanner .data-single .single span span {
            color: #FF6A00;
        }
        .dbanner .data-single h1 {
            color: #ffffff;
            font-weight: 600;
            font-size: 42px;
            margin: 10px auto;
            text-align: center;
        }

        main {
            margin: 0 auto;
            width: 90%;
        }
        main article header {
            margin-top: 28px;
        }
        main article header .entry-meta {
            display: flex;
            justify-content: center;
            margin-bottom: 7px;
        }
        main article header .entry-meta time {
            color: #818484;
            font-weight: 600;
            font-size: 12px;
            letter-spacing: 1px;
            text-transform: uppercase;
        }
        main article header .author {
            color: #818484;
            display: flex;
            font-weight: 400;
            font-size: 14px;
            justify-content: center;
        }
        main article header .author span {
            font-style: italic;
            margin-bottom: 21px;
            margin-left: 7px;
            text-decoration: underline;
        }
        main article .entry-content p {
            color: #818483;
            font-weight: 400;
            font-size: 16px;
            line-height: 26px;
            margin-bottom: 24px;
        }

        .inst {}
        .inst .hash {
            align-items: center;
            background-color: #0F4D5F;
            display: flex;
            height: 65px;
            justify-content: center;
        }
        .inst .hash span {
            color: white;
            font-size: 35px;
            font-weight: 700;
        }
        .inst .hash span span {
            color: #FEE600;
        }

        .redes {
            background-color: #eeeeee;
            align-items: center;
            display: flex;
            height: 79px;
            justify-content: center;
        }
        .redes a {
            color: #000000;
            font-size: 16px;
            margin-left: 14px;
            margin-right: 14px;
            text-decoration: none;
        }
        .redes a i {}

        footer {
            background-color: #0B3845;
            height: auto;
            padding: 14px 0;
        }
        footer .logo {
            align-items: center;
            display: flex;
            justify-content: center;
            margin-bottom: 14px;
        }
        footer .disclaimer {
            align-items: center;
            color: white;
            display: flex;
            font-size: 14px;
            font-weight: 500;
            justify-content: center;
            margin: 0 auto;
            text-align: center;
            width: 90%;
        }

        .relacionados {
            margin: 0 auto;
            width: 90%;
        }
        .relacionados h4 {
            display: flex;
            justify-content: center;
            margin-bottom: 28px;
            position: relative;
        }
        .relacionados h4::before {
            background-color: #0F4D5F;
            content: "";
            display: block;
            height: 1px;
            position: absolute;
            top: 50%;
            width: 100%;
        }
        .relacionados h4 span {
            background-color: #F8F8F8;
            color: #0F4D5F;
            font-size: 20px;
            padding: 7px 21px;
            position: relative;
            text-transform: uppercase;
            z-index: 9;
        }
        .relacionados .item-wrap {
            margin-bottom: 21px;
        }
        .relacionados .item-wrap .item {
            height: 391px;
            margin-top: 21px;
        }
        .relacionados .item-wrap .item:nth-child(1) {
            margin-top: 0;
        }
        .relacionados .item-wrap .item .hello {
            position: relative;
        }
        .relacionados .item-wrap .item .hello .hello_data {
            align-items: center;
            background-color: rgba(0, 0, 0, 0.6);
            display: flex;
            height: 36px;
            justify-content: center;
            left: 0;
            padding: 0 14px;
            position: absolute;
            top: 0;
            z-index: 9;
        }
        .relacionados .item-wrap .item.type1 .hello .hello_data span {
            color: #3eaae8;
        }
        .relacionados .item-wrap .item.type1 .hello .hello_data span span {
            color: white;
        }
        .relacionados .item-wrap .item.type3 .hello .hello_data span {
            color: white;
        }
        .relacionados .item-wrap .item.type3 .hello .hello_data span span {
            color: #62c546;
        }
        .relacionados .item-wrap .item.type4 .hello .hello_data span {
            color: #f6310f;
        }
        .relacionados .item-wrap .item.type4 .hello .hello_data span span {
            color: white;
        }
        .relacionados .item-wrap .item .hello .hello_data span {
            font-weight: 500;
        }
        .relacionados .item-wrap .item .hello .img .date {
            align-items: center;
            background: linear-gradient(transparent, rgba(0, 0, 0, 0.5));
            bottom: 0;
            display: flex;
            height: 50px;
            justify-content: center;
            position: absolute;
            width: 100%;
        }
        .relacionados .item-wrap .item .hello .img .date span {
            color: white;
        }
        .relacionados .item-wrap .item .data {
            align-items: center;
            background-color: white;
            display: flex;
            flex-direction: column;
            height: calc(100% - 280px);
            justify-content: center;
            padding: 0 14px;
        }
        .relacionados .item-wrap .item .data strong {
            display: block;
            font-size: 12px;
            text-align: center;
            text-transform: uppercase;
        }
        .relacionados .item-wrap .item.type1 .data strong {
            color: #3eaae8;
        }
        .relacionados .item-wrap .item.type3 .data strong {
            color: #62c546;
        }
        .relacionados .item-wrap .item.type4 .data strong {
            color: #f6310f;
        }
        .relacionados .item-wrap .item .data a {
            display: inline-block;
            text-decoration: none;
        }
        .relacionados .item-wrap .item .data a span {
            color: #0F4D5F;
            display: block;
            font-size: 16px;
            font-weight: 600;
            line-height: 19px;
            text-align: center;
        }
    </style>
</head>
<body>
    <header class="header">
        <span>hello<span>moto</span></span>
    </header>
    <div class="wrapper">
        <div class="dbanner">
            <div class="img bk-img">
                <amp-img src="{{ asset('/storage/posts/banners/24.png') }}" alt="Mujeres Maravillosas en la Historia de la Tecnología" height="500" width="414"></amp-img>
            </div>
            <div class="data-single">
                <span class="single">
                    <span>
                        experiencias
                        <span>moto</span>
                    </span>
                </span>
                <h1>Mujeres Maravillosas en la Historia de la Tecnología</h1>
            </div>
        </div>
        <main>
            <article>
                <header>
                    <div class="entry-meta">
                        <time class="entry-date published" datetime="2017-11-30 10:19:00">30 de November del 2017</time>
                    </div>
                    <div class="author">
                        Escrito por
                        <span> Motorola</span>
                    </div>
                </header>
                <div class="entry-content">
                    <p>
                        Parece que la Historia siempre privilegió los hechos masculinos, destacando en sus páginas a héroes, científicos, artistas e inventores hombres. Mucha gente no sabe, pero mientras los libros sólo hablan de los jóvenes, la mujer estaba bien tranquila, estudiando, trabajando y produciendo cosas sensacionales. En fin, ayudando a la Humanidad a evolucionar y tornarse mucho más tecnológica.
                    </p>
                    <p>
                        Para acabar con esa injusticia, en este Día Internacional de la Mujer, nosotros en la Comunidad queremos homenajear y contar la historia de esas mujeres tecnológicas que en sus laboratorios, aulas y talleres, hicieron que la Ciencia y la Tecnología sean lo que son hoy.
                    </p>
                    <p>
                        1. Ada Lovelace
                        <br>
                        Hija del poeta Lord Byron, Ada hacía poesía incluso con los números. Con sus cálculos precisos y la creatividad con los números, la futura Lady Lovelace produjo el primer algoritmo para una máquina realizar una tarea. La máquina analítica de Charles Babage fue conocida como el primer proyecto de una máquina analítica, y el algoritmo de Ada el primer "software" para ella. Con eso, Ada no sólo fue una de las primeras personas en describir, en 1842, lo que en el futuro se convertiría en un ordenador, como también se convirtió en la primera programadora.
                        ¡Gracias, Ada!
                    </p>
                    <p>
                        2. Las criptógrafas de Bletchley Park
                        <br>
                        Durante la Segunda Guerra Mundial, mientras los soldados británicos luchaban en el frente, un grupo enorme de mujeres trabajaba en un asunto crucial para la estrategia de guerra: romper la criptografía del ejército alemán. Si los aliados pudieran romper los códigos usados en las comunicaciones alemanas interceptadas, tendrían una ventaja enorme para vencer la guerra. Tres mujeres en especial, Joan Clarke Murray, Margaret Rock y Mavis Lever Batey, rompieron todas las barreras de género de la época y se convirtieron en criptoanalistas, responsables de Alan Turing por analizar y romper el código de la increíble máquina de criptografía alemana Enigma. Pero, además de las tres estrellas de rock, más de 5.000 mujeres llegaron a trabajar en Bletchley Park - más del 75% de todos los empleados -, ejerciendo un papel fundamental en la victoria aliada.
                    </p>
                    <p>
                        3. Grace Hopper:
                        <br>
                        Piense en una mujer que tenía Ph.D en Matemáticas por Yale, fue creadora del primer compilador de lenguajes de programación del mundo, definió las bases para el primer lenguaje de programación basado en lenguas naturales y fue almirante condecorado de la Marina de los Estados Unidos. Les presentamos a Grace Hopper, que lo hizo todo y se convirtió en una leyenda en la historia de la computación. Como si no bastase, hay informes de que fue ella quien creó el término bug para referirse a errores en el sistema. Parece que un día su máquina no funcionaba y ella, sin pensar, la abrió, ¿qué encontró? Al parecer un insecto muerto que obstaculizaba las conexiones.
                    </p>
                </div>
            </article>
        </main>
    </div>
    <div class="relacionados">
        <h4>
            <span>Últimos posts</span>
        </h4>
        <div class="item-wrap">
            <div class="item type4">
                <div class="hello">
                    <div class="hello_data">
                        <span>experiencias<span>moto</span></span>
                    </div>
                    <div class="img">
                        <amp-img src="{{ asset ('/storage/posts/thumbs/24.png') }}" alt="" height="280" width="384" layout="responsive"></amp-img>

                        <div class="date">
                            <span>30 de November del 2017</span>
                        </div>
                    </div>
                </div>
                <div class="data">
                    <a href="/experienciasmoto/post/mujeres-maravillosas-en-la-historia-de-la-tecnologia"><span>Mujeres Maravillosas en la Historia de la Tecnología</span></a>
                </div>
            </div>
            <div class="item type3">
                <div class="hello">
                    <div class="hello_data">
                        <span>moto<span>style</span></span>
                    </div>
                    <div class="img">
                        <amp-img src="{{ asset ('/storage/posts/thumbs/23.png') }}" alt="" height="280" width="384" layout="responsive"></amp-img>

                        <div class="date">
                            <span>30 de November del 2017</span>
                        </div>
                    </div>
                </div>
                <div class="data">
                    <strong>gastronomía &amp; restaurantes</strong>
                    <a href="/motostyle/post/cocina-de-a-dos"><span>COCINA DE A DOS</span></a>
                </div>
            </div>
            <div class="item type1">
                <div class="hello">
                    <div class="hello_data">
                        <span>hello<span>ciudades</span></span>
                    </div>
                    <div class="img">
                        <amp-img src="{{ asset ('/storage/posts/thumbs/20.jpg') }}" alt="" height="280" width="384" layout="responsive"></amp-img>

                        <div class="date">
                            <span>30 de November del 2017</span>
                        </div>
                    </div>
                </div>
                <div class="data">
                    <strong>hellolima</strong>
                    <a href="/hellociudades/post/motorola-presenta-dimitri-vegas-&amp;-like-mike"><span>Motorola presenta: Dimitri Vegas &amp; Like Mike</span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="inst">
        <div class="hash">
            <span>
                #hello<span>moto</span>
            </span>
        </div>
    </div>
    <div class="redes">
        <a href="https://www.facebook.com/MotorolaPE" target="_blank">
            <i class="fa fa-facebook"></i>Facebook
        </a>
        <a href="https://twitter.com/MotorolaPeru" target="_blank">
            <i class="fa fa-twitter"></i> Twitter
        </a>
        <a href="https://www.instagram.com/motorolape/" target="_blank">
            <i class="fa fa-instagram"></i> Instagram
        </a>
    </div>
    <footer>
        <div class="logo">
            <amp-img src="{{ asset ('img/web/logo_blanco.svg') }}" alt="" height="78" width="300"></amp-img>
        </div>
        <div class="disclaimer">
            © 2018 MOTOROLA MOBILTY LLC. TODOS LOS DERECHOS RESERVADOS
        </div>
    </footer>
</body>
</html>