<div class="navegation">
    <div class="logo">
        <img src="{{asset('img/web/logo_negro.png')}}" alt="">
    </div>
    <nav>
        <ul>
            <li><a href="{{ url('usuario/mis-embajadores') }}"> Mis embajadores</a></li>
            <li><a href="{{ url('usuario/mis-playlist') }}"> Mis playlist</a></li>
            <li><a href="{{ url('usuario/beneficios') }}"> Beneficios</a></li>
            <li><a href="{{ url('usuario/perfil') }}"> Mi perfil</a></li>
        </ul>
    </nav>
</div>