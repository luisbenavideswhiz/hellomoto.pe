<div id="participar" class="modal fade participar" role="dialog">
    <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <p>¿Deseas participar de esta promoción?</p>
                <div class="action">
                    <a href="#" data-dismiss="modal">Sí</a>
                    <a href="#" data-dismiss="modal">No</a>
                </div>
            </div>
        </div>
    </div>
</div>
