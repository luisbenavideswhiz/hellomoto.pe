<div id="myModal" class="modal fade delete" role="dialog">
    <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-group">
                    <img src="{{asset('img/web/logo_plomo.png')}}" alt="">
                    <label for="">¿Desea eliminar este embajador?</label>
                </div>
                <div class="action">
                    <a href="#" data-dismiss="modal">eliminar</a>
                </div>
            </div>
        </div>
    </div>
</div>
