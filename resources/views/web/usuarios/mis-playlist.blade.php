<!DOCTYPE html>
<html lang="es">
<head>
    <title>{{ getenv('APP_TITLE') }}</title>

    @component('web.partials.assets.metas')
    @endcomponent

    @component('web.partials.assets.styles')
    @endcomponent

</head>
<body class="user">

{{ csrf_field() }}

@component('web.partials.header.desktop')
@endcomponent

@component('web.partials.header.responsive')
@endcomponent

<div id="mis-playlist">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-2">

                    @component('web.usuarios.partials.navigation')
                    @endcomponent

                </div>
                <div class="col-xs-12 col-md-10">
                    <div class="row">
                        <div class="col-xs-12 col-md-4">
                            <h1>Mis playlist</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="item-wrap">
                            @if(count($rows) > 0)
                                @component('web.templates.my-playlist-item',
                                [
                                    'rows' => $rows
                                ])
                                @endcomponent
                            @else
                                <div class="nope">
                                    <p>
                                        Escucha los playlists recomendados por los embajadores y guarda tus favoritos.
                                    </p>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>

@component('web.usuarios.modals.eliminar')
@endcomponent

@component('web.partials.footer')
@endcomponent

@component('web.partials.assets.scripts')
@endcomponent

</body>
</html>