<!DOCTYPE html>
<html lang="es">
<head>
    <title>{{ getenv('APP_TITLE') }}</title>

    @component('web.partials.metas')
    @endcomponent

    @component('web.partials.style')
    @endcomponent
</head>
<body class="user">
@component('web.partials.header')
@endcomponent

@component('web.partials.header_r')
@endcomponent
<div id="hellopuntos">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-2">
                    @component('web.usuarios.partials.navigation')
                    @endcomponent
                </div>
                <div class="col-xs-12 col-md-10">
                    <div class="row">
                        <div class="col-xs-12"><h2>Mis hellopuntos</h2></div>
                        <div class="col-xs-12">
                            <div class="sub-title">
                                <h3>Compartidos</h3>
                                <a href="perfil">volver</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">id</th>
                                        <th scope="col">entrada</th>
                                        <th scope="col">canal</th>
                                        <th scope="col">puntos</th>
                                        <th scope="col">fecha - hora <i class="fa fa-caret-down"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php for ($x = 1; $x <= 3; $x++): ?>
                                    <tr>
                                        <th scope="row">01</th>
                                        <td>HelloTalks e o Moto Snap JBL Soundboost 2.0</td>
                                        <td>Facebook</td>
                                        <td>10</td>
                                        <td>10-10-2017 | 3:00pm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">02</th>
                                        <td>Es momento de aprender del arte urbano</td>
                                        <td>Facebook</td>
                                        <td>5</td>
                                        <td>10-10-2017 | 3:00pm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">03</th>
                                        <td>Lanzamiento del nuevo MOTO G5</td>
                                        <td>twitter</td>
                                        <td>1</td>
                                        <td>10-10-2017 | 3:00pm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">04</th>
                                        <td>HelloTalks e o Moto Snap 8.0</td>
                                        <td>Facebook</td>
                                        <td>8</td>
                                        <td>10-10-2017 | 3:00pm</td>
                                    </tr>
                                    <?php endfor;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="cmc"><a href="#">cargar m&aacute;s premios</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@component('web.usuarios.modals.detalle')
@endcomponent

@component('web.partials.footer')
@endcomponent

@component('web.partials.scripts')
@endcomponent

</body>
</html>