<!DOCTYPE html>
<html lang="es">
<head>
    <title>{{ getenv('APP_TITLE') }}</title>

    @component('web.partials.assets.metas')
    @endcomponent

    @component('web.partials.assets.styles')
    @endcomponent
</head>
<body class="user">
@component('web.partials.header.desktop')
@endcomponent

@component('web.partials.header.responsive')
@endcomponent
<div id="canjear">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-2">
                    @component('web.usuarios.partials.navigation')
                    @endcomponent
                </div>
                <div class="col-xs-12 col-md-10">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1>Radio MP3</h1>
                        </div>
                    </div>
                    <div class="content">
                        <div class="item-wrap">
                            <div class="item">
                                <div class="hello">
                                    <div class="img">
                                        <img src="http://via.placeholder.com/300x300" alt="">

                                        <div class="date"></div>
                                    </div>
                                </div>
                                <div class="data">
                                    <strong>Radio-mp3</strong>
                                    <span>300 ptos</span>
                                </div>
                            </div>
                            <div class="form">
                                <h3>Actualiza tus datos</h3>

                                <div class="group">
                                    <div class="form-group">
                                        <label for="">Nombre</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Apellido</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="group">
                                    <div class="form-group">
                                        <label for="">Tipo de Documento</label>
                                        <select class="form-control" name="" id="">
                                            <option value="">Seleccione</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Nro de Documento</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="group three">
                                    <div class="form-group">
                                        <label for="">Pa&iacute;s</label>
                                        <select class="form-control" name="" id="">
                                            <option value="">Seleccione</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Departamento</label>
                                        <select class="form-control" name="" id="">
                                            <option value="">Seleccione</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Ciudad</label>
                                        <select class="form-control" name="" id="">
                                            <option value="">Seleccione</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="group two">
                                    <div class="form-group">
                                        <label for="">Celular</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Dirección</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="group one">
                                    <div class="form-group">
                                        <label for="">Correo electr&oacute;nico</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="group">
                                    <a href="#">cancelar</a>
                                    <a href="#" data-toggle="modal" data-target="#myModal">canjear</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="msj">
                                <img src="{{asset('img/web/logo_plomo.png')}}" alt="">

                                <p>Acabas de canjear : [PRODUCTO], te restan: [PUNTOS TOTALES] puntos.</p>

                                <p>analizar la forma de entrega para crear las vistas de cierre.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@component('web.usuarios.modals.confirmacion')
@endcomponent

@component('web.partials.footer')
@endcomponent

@component('web.partials.assets.scripts')
@endcomponent

</body>
</html>