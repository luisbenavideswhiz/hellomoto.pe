<!DOCTYPE html>
<html lang="es">
<head>
    <title>{{ getenv('APP_TITLE') }}</title>

    @component('web.partials.assets.metas')
    @endcomponent

    @component('web.partials.assets.styles')
    @endcomponent

</head>
<body class="user">

{{ csrf_field() }}

@component('web.partials.header.desktop')
@endcomponent

@component('web.partials.header.responsive')
@endcomponent

<div id="perfil">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-2">

                    @component('web.usuarios.partials.navigation')
                    @endcomponent

                </div>
                <div class="col-xs-12 col-md-10">
                    <div class="row">
                        <div class="col-xs-12"><h1>Mi perfil</h1></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-2">
                            <div class="img-profile">
                                <img id="profile-image"
                                     src="@if(!is_null($user->profile_img)) {{ $user->profile_img }} @else {{ "http://via.placeholder.com/110x110" }} @endif"
                                     width="110"
                                     alt="">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-10">
                            <form id="update-form" data-url="/usuario/actualizar">
                                <div class="group">
                                    <div class="form-group">
                                        <span><i class="fa fa-user-circle-o"></i></span>
                                        <label for="">Nombre:</label>
                                        <input type="text" id="name" name="name"
                                               class="popover-validate"
                                               value="{{ $user->name }}">
                                    </div>
                                    <div class="form-group">
                                        <span><i class="fa fa-user-circle-o"></i></span>
                                        <label for="">Apellido:</label>
                                        <input type="text" id="lastname" name="lastname"
                                               class="popover-validate"
                                               value="{{ $user->lastname }}">
                                    </div>
                                </div>
                                <div class="group">
                                    <div class="form-group">
                                        <span><i class="fa fa-id-card-o"></i></span>
                                        <label for="">{{ strtoupper($user->document_type) }}: </label>
                                        <input type="text" id="document_number" name="document_number" maxlength="8"
                                               class="popover-validate"
                                               value="{{ $user->document_number }}">
                                    </div>
                                    <div class="form-group">
                                        <span><i class="fa fa-envelope-o"></i></span>
                                        <label for="">Email: </label>
                                        <input type="text" id="email" name="email"
                                               class="popover-validate"
                                               value="{{ $user->email }}">
                                    </div>
                                </div>
                                <div class="group">
                                    <div class="form-group">
                                        <span><i class="fa fa-venus-mars"></i></span>
                                        <label for="">Sexo: </label>
                                        <select id="sex" name="sex"
                                                class="popover-validate">
                                            <option value="hombre"
                                                    @if($user->sex=='hombre')selected="selected"@endif>
                                                Hombre
                                            </option>
                                            <option value="mujer"
                                                    @if($user->sex=='mujer')selected="selected"@endif>Mujer
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <span><i class="fa fa-mobile"></i></span>
                                        <label for="">Celular: </label>
                                        <input type="text" id="phone" name="phone" maxlength="15"
                                               class="popover-validate"
                                               value="{{ $user->phone }}">
                                    </div>
                                </div>
                                <div class="group">
                                        <span><i class="fa fa-gift"></i></span>
                                        <label for="">Cumplea&ntilde;os: </label>
                                        <div class="datapicker">
                                            <input type="text" id="birthdate" name="birthdate"
                                                   class="datapicker nbbn popover-validate"
                                                   value="{{ $user->birthdate }}">

                                            <div class="datebtn">
                                            </div>
                                        </div>

                                    <div class="form-group">
                                        <span><i class="fa fa-mobile"></i></span>
                                        <label for="">Operador: </label>
                                        <select id="mobile_operator" name="mobile_operator"
                                                class="popover-validate">
                                            <option value="claro"
                                                    @if($user->mobile_operator=='claro')selected="selected"@endif>
                                                Claro
                                            </option>
                                            <option value="movistar"
                                                    @if($user->mobile_operator=='movistar')selected="selected"@endif>
                                                Movistar
                                            </option>
                                            <option value="entel"
                                                    @if($user->mobile_operator=='entel')selected="selected"@endif>
                                                Entel
                                            </option>
                                            <option value="bitel"
                                                    @if($user->mobile_operator=='bitel')selected="selected"@endif>
                                                Bitel
                                            </option>
                                            <option value="virgin"
                                                    @if($user->mobile_operator=='virgin')selected="selected"@endif>
                                                Virgin Mobile
                                            </option>
                                            <option value="tuenti"
                                                    @if($user->mobile_operator=='tuenti')selected="selected"@endif>
                                                Tuenti
                                            </option>
                                            <option value="cuy"
                                                    @if($user->mobile_operator=='cuy')selected="selected"@endif>
                                                Cuy M&oacute;vil
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                            <form id="upload-form">
                                <div class="group">
                                    <div class="form-group">
                                        <span><i class="fa fa-picture-o"></i></span>
                                        <label for="">Imagen: </label>
                                        <input type="file" id="profile_img" name="profile_img" data-url="/usuario/subir-imagen">
                                    </div>
                                </div>
                            </form>
                            <div class="cmc">
                                <a href="#" id="update-btn">actualizar datos</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@component('web.usuarios.modals.detalle')
@endcomponent

@component('web.partials.footer')
@endcomponent

@component('web.partials.assets.scripts')
@endcomponent

</body>
</html>