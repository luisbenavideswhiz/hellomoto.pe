<!DOCTYPE html>
<html lang="es">
<head>
    <title>{{ getenv('APP_TITLE') }}</title>

    @component('web.partials.assets.metas')
    @endcomponent

    @component('web.partials.assets.styles')
    @endcomponent
</head>
<body class="user">
@component('web.partials.header.desktop')
@endcomponent

@component('web.partials.header.responsive')
@endcomponent
<div id="catalogo-premios">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-2">
                    @component('web.usuarios.partials.navigation')
                    @endcomponent
                </div>
                <div class="col-xs-12 col-md-10">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <h1>Beneficios</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="item-wrap">
                                @foreach($landings as $landing)
                                <div class="item">
                                    <div class="img">
                                        <img src="{{ $landing->image }}" width="215">
                                    </div>
                                    <div class="data">
                                        <div class="canjear">
                                            @if($landing->status==0)
                                                <a href="#" class="disable"> concluído</a>
                                            @else
                                                @if(!$landing->webUserHasLanding($user['id']))
                                                    <a href="{{ $landing->path }}" class="enable" target="_blank">participar</a>
                                                @else
                                                    <a href="#" class="disable"> participando</a>
                                                @endif
                                            @endif
                                        </div>
                                        <a target="_blank" href="{{ $landing->terms_conditions }}">
                                            Términos y condiciones
                                        </a>
                                        <a target="_blank" href="http://www.motorola.com.pe/institucional/politica-de-privacidad">
                                            Política de privacidad
                                        </a>
                                    </div>
                                </div>
                                @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@component('web.usuarios.modals.participar')
@endcomponent

@component('web.usuarios.modals.detalle')
@endcomponent

@component('web.partials.footer')
@endcomponent

@component('web.partials.assets.scripts')
@endcomponent

</body>
</html>