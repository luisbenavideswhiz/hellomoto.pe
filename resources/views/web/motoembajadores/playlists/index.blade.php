@extends('web.layouts.web')

@section('title')
    @component('web.partials.title')
    @endcomponent
@endsection

@section('og_tags')
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@MotorolaPE"/>
    <meta name="twitter:creator" content="@MotorolaPE"/>

    <meta property="og:url" content="{{ url('/') }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Descubre la comunidad hellomoto"/>
    <meta property="og:description"
          content="Acompa&ntilde;a a nuestros embajadores a los mejores eventos de Lima y s&eacute; parte de una comunidad diferente."/>
    <meta property="og:image" content="{{ url('/img/200x200.jpg') }}"/>
@endsection

@section('content')
    <input type="hidden" id="current_category" name="current_category" value="hellotips">
    <div class="motoembajadores">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <h1>
                                Playlists
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="container item-playlist">
                    <div id="section-content" class="row">
                        @if($rows)
                            @component('web.templates.playlist-item',
                            [
                                'rows' => $rows
                            ])
                            @endcomponent
                        @else
                            @component('web.partials.no-rows',
                            [
                                'rows' => $rows
                            ])
                            @endcomponent
                        @endif
                    </div>

                    @if($rows)
                        @component('web.partials.load-more-posts', [
                            'url' => '/ambassadors/playlists'
                        ])
                        @endcomponent
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection