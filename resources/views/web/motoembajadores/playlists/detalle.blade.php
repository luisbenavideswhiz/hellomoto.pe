@extends('web.layouts.web')

@section('og_tags')
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@MotorolaPE"/>
    <meta name="twitter:creator" content="@MotorolaPE"/>

    <meta property="og:url" content="{{ url('/motoembajadores/playlist/'.$row->slug) }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="{{ $row->title }}"/>
    <meta property="og:description"
          content="Acompa&ntilde;a a nuestros embajadores a los mejores eventos de Lima y s&eacute; parte de una comunidad diferente."/>
    <meta property="og:image" content="{{ $row->image }}"/>
@endsection

@section('content')
    <input type="hidden" id="spotify_img"
           value="{{ str_replace('https://open.spotify.com/', '', $row->url) }}">
    <div class="motoembajadores">
        <div class="container-fluid">
            <div class="row">
                <div class="container emb-playd">
                    <div class="row">
                        <div class="col-xs-12 col-md-5">
                            <div class="playlist_cover">
                                <h3>
                                    {{ $row->title }}
                                </h3>

                                <div class="cover">
                                    <div class="img">
                                        <img src="{{ $row->image }}" width="372" height="372" alt="">
                                    </div>
                                </div>
                                <div class="data">
                                    <div class="emb">
                                        <img src="{{ $row->webAmbassador->profile_img }}"
                                             alt="{{ $row->webAmbassador->ambassador_fullname }}" width="51"
                                             height="51">
                                        <span>Por {{ $row->webAmbassador->ambassador_fullname  }}</span>
                                    </div>
                                    <div class="links">
                                        @if($follow == 1)
                                            <a id="ambassador-follow-btn" href="#!" class="follow"
                                               data-url="/ambassadors/unfollow"
                                               data-referal-id="{{ $row->webAmbassador->id }}"
                                               data-logged="@if(session('logged')){{"logged"}}@else{{"no-logged"}}@endif">
                                                Dejar de seguir al embajador
                                            </a>
                                        @else
                                            <a id="ambassador-follow-btn" href="#!" class="follow"
                                               data-url="/ambassadors/follow"
                                               data-referal-id="{{ $row->webAmbassador->id }}"
                                               data-logged="@if(session('logged')){{"logged"}}@else{{"no-logged"}}@endif">
                                                Sigue al embajador
                                            </a>
                                        @endif
                                        @if($like == 1)
                                            <a id="playlist-follow-btn" href="#!" class="sp"
                                               data-url="/ambassadors/playlists/unlike"
                                               data-referal-id="{{ $row->id }}"
                                               data-logged="@if(session('logged')){{"logged"}}@else{{"no-logged"}}@endif">
                                                Eliminar playlist
                                            </a>
                                        @else
                                            <a id="playlist-follow-btn" href="#!" class="sp"
                                               data-url="/ambassadors/playlists/like"
                                               data-referal-id="{{ $row->id }}"
                                               data-logged="@if(session('logged')){{"logged"}}@else{{"no-logged"}}@endif">
                                                Guardar playlist
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <div class="img">
                                <iframe src="https://embed.spotify.com/?uri={{ $row->url }}&theme=white"
                                        width="671" height="832" frameborder="0" allowtransparency="true"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection