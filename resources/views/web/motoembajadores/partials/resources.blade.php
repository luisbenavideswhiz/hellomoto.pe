@if(count($related_ambassador) != 0)
    <div class="recursos">
        <h4>
            <span>Art&iacute;culos relacionados</span>
        </h4>

        <div class="items">
            @foreach($related_ambassador as $key => $value)
                <a href="{{ $value->webPost->url_post }}" class="item">
                    <div class="hello">
                        <div class="img">
                            <img src="{{ url($value->webPost->thumb_img) }}" width="170" height="180" alt="{{ $value->webPost->title }}">

                            <div class="date">
                                <span>{{ $value->webPost->created_at_readable }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="data">
                        <span>{{ $value->webPost->title }}</span>
                    </div>
                </a>
            @endforeach

        </div>

    </div>
@endif