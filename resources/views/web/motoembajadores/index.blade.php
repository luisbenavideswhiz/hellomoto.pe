@extends('web.layouts.web')

@section('content')
    <div class="motoembajadores">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <h1>
                                <span>Moto</span>Embajadores
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="container item-emb">
                    <div id="section-content" class="row">
                        @if($rows)
                            @component('web.templates.ambassador-item',
                            [
                                'rows' => $rows
                            ])
                            @endcomponent
                        @else
                            @component('web.partials.no-rows',
                            [
                                'rows' => $rows
                            ])
                            @endcomponent
                        @endif
                    </div>

                    @if($rows)
                        @component('web.partials.load-more-posts', [
                            'url' => '/ambassadors/rows'
                        ])
                        @endcomponent
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection