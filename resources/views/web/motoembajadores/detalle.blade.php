@extends('web.layouts.web')

@section('og_tags')
    @component('web.partials.title')
        {{ $row->ambassador_fullname }}
    @endcomponent
    @component('web.partials.seo.ambassadors', [
        'ambassador' => $row
    ])
    @endcomponent
@endsection

@section('content')
    <div class="motoembajadores">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="hidden-xs col-md-2"></div>
                        <div class="col-xs-12 col-md-8">
                            <h2>{{ $row->ambassador_fullname }}</h2>
                            <span class="quees">{{ $row->webCareer->name }}</span>
                            <span class="ciudad">{{ ucwords($row->city) }}</span>
                        </div>
                        <div class="hidden-xs col-md-2"></div>
                    </div>
                </div>
                <div class="embajadores">
                    <div class="item">
                        <div class="izq">
                            <div class="embajador">
                                <div class="item">
                                    <div class="hello">
                                        <div class="img">
                                            <img src="{{ $row->profile_img }}"
                                                 alt="{{ $row->ambassador_fullname }}"
                                                 width="270" height="280">
                                        </div>
                                    </div>
                                    <div class="data">
                                        <span>Sígueme:</span>

                                        <div class="acciones">
                                            {!! $row->ambassador_socialnetworks !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="der">
                            <div class="entry-content">
                                {!! $row->bio !!}

                                @if($follow == 1)
                                    <a id="ambassador-follow-btn" href="#!" class="follow"
                                       data-url="/ambassadors/unfollow"
                                       data-referal-id="{{ $row->id }}"
                                       data-logged="@if(session('logged')){{"logged"}}@else{{"no-logged"}}@endif">
                                        Dejar de seguir al embajador
                                    </a>
                                @else
                                    <a id="ambassador-follow-btn" href="#!" class="follow"
                                       data-url="/ambassadors/follow"
                                       data-referal-id="{{ $row->id }}"
                                       data-logged="@if(session('logged')){{"logged"}}@else{{"no-logged"}}@endif">
                                        Sigue al embajador
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="izq">
                            <div class="spotify_c">
                                @if(count($row->webPlaylists) != 0)
                                    <iframe src="https://embed.spotify.com/?uri={{ $row->webPlaylists[0]->url }}"
                                            width="269"
                                            height="298" frameborder="0" allowtransparency="true"></iframe>
                                @endif
                            </div>
                        </div>
                        @if($row->profile_video != '')
                            <div class="der">
                                <div class="emb_video">
                                    <span>Mira lo que {{ $row->ambassador_nickname }} tiene para ti</span>

                                    <div class="video_c">
                                        <iframe width="470" height="264"
                                                src="https://www.youtube.com/embed/{{ $row->profile_video }}"
                                                frameborder="0"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>


                @component('web.motoembajadores.partials.resources',
                    [
                        'related_ambassador' => $related_ambassador
                    ])
                @endcomponent

                @if(count($row->webAmbassadorsHasWebGalleries) != 0)
                    @component('web.partials.gallery',
                    [
                        'gallery' => $row->webAmbassadorsHasWebGalleries
                    ])
                    @endcomponent
                @endif

                @component('web.partials.tags-shared',
                [
                    'tags' => $row->tags_html,
                    'url' => $url
                ])
                @endcomponent

                @component('web.partials.previous-next',
                [
                    'prev' => $post->prev,
                    'next' => $post->next
                ])
                @endcomponent

            </div>

            @component('web.partials.related-posts',
                [
                    'related' => $related
                ])
            @endcomponent

            @component('web.partials.disqus',
            [
                'url' => $url
            ])
            @endcomponent

        </div>
    </div>
@endsection

@section('social_sdk')

    @component('web.partials.sdk.fb')
    @endcomponent

    @component('web.partials.sdk.tw')
    @endcomponent

@endsection