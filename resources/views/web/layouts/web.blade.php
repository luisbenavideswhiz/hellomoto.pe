<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0, minimum-scale=1.0">
    <meta name="author" content="WHIZ - Agencia Digital">

    @if(isset($seo))
        @component('web.partials.seo.index',
        [
            'seo'=> $seo
        ])
        @endcomponent
    @else
        @section('og_tags')
        @show
    @endif

    @include('web.partials.cannonical')
    @include('web.partials.assets.styles')
    @include('web.partials.seo.analytics')
</head>

<body data-device="{{ $device }}">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5JC6WBV"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

{{ csrf_field() }}

@include('web.partials.header.desktop',
[
    'menu' => $menu
])
@include('web.partials.header.responsive',
[
    'menu' => $menu
])

@yield('content')

@include('web.partials.instagram',
[
    'instagram' => $instagram
])

@include('web.partials.footer')
@include('web.modals.login.index')
@include('web.modals.forgot-password')
@include('web.modals.new-pass')
@include('web.modals.rewards')
@include('web.partials.assets.scripts')

@yield('social_sdk')

</body>
</html>
