@extends('web.layouts.web')

@section('content')
    <div class="home">
        <div class="container-fluid">
            <div class="row">
                @component('web.partials.slider',
                [
                    'slider' => $slider
                ])
                @endcomponent
            </div>
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="item-filter" data-url="/posts/rows">
                                <a href="#!" data-category="" class="active">
                                    <span>todos</span>
                                </a>
                                <a href="#!" data-category="2">
                                    <span>experiencias<span>moto</span></span>
                                </a>
                                <a href="#!" data-category="1">
                                    <span>hello<span>ciudades</span></span>
                                </a>
                                <a href="#!" data-category="3">
                                    <span>moto<span>style</span></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="container item-wrap">
                    <div id="section-content" class="row">
                        @if($rows)
                            @component('web.templates.post-item',
                            [
                                'rows' => $rows
                            ])
                            @endcomponent
                        @else
                            @component('web.partials.no-rows',
                            [
                                'rows' => $rows
                            ])
                            @endcomponent
                        @endif
                    </div>
                    @if($rows)
                        @component('web.partials.load-more-posts', [
                            'url' => '/posts/rows'
                        ])
                        @endcomponent
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection