<?= '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc>{{ url('sitemap/'.$category->id.'-'.$subCategory->id.'/web.xml') }}</loc>
        <lastmod>{{ $subCategory->updated_at->format('c') }}</lastmod>
    </sitemap>
    {{--<sitemap>--}}
        {{--<loc>{{ url('sitemap/'.$category->id.'-'.$subCategory->id.'/news.xml') }}</loc>--}}
        {{--<lastmod>{{ $subCategory->updated_at->format('c') }}</lastmod>--}}
    {{--</sitemap>--}}
</sitemapindex>