<?= '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:xhtml="http://www.w3.org/1999/xhtml"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
        xmlns:news="http://www.google.com/schemas/sitemap-news/0.9">
    @foreach($posts as $key => $value)
        <url>
            <loc>{{ url($value->slug) }}</loc>
            <news:news>
                <news:publication>
                    <news:name>{{ $value->title }}</news:name>
                    <news:language>es</news:language>
                </news:publication>
                <news:genres>Blog</news:genres>
                <news:publication_date>{{ $value->created_at->format('Y-m-d') }}</news:publication_date>
                <news:title>{{ $value->title }}</news:title>
                <news:keywords>{{ $value->tags }}</news:keywords>
            </news:news>
            <changefreq>hourly</changefreq>
            <priority>1.0</priority>
        </url>
    @endforeach
</urlset>