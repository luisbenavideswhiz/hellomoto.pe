<?= '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($categories as $category)
        <sitemap>
            <loc>{{ url('sitemap/'.$category->webCategoryParent->id.'-'.$category->id.'.xml') }}</loc>
            <lastmod>{{ $category->updated_at->format('c') }}</lastmod>
        </sitemap>
    @endforeach
</sitemapindex>