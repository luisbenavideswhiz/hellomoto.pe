@extends('web.layouts.web')

@section('title')
    @component('web.partials.title')
    @endcomponent
@endsection


@section('og_tags')
    @component('web.partials.seo.default')
    @endcomponent
@endsection

@section('content')
    <input type="hidden" id="current_category" name="current_category" value="hellociudades">
    <input type="hidden" data-section="1" class="active">
    <div class="ciudades resultados">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <h1>
                                <span>Resultado de: </span> <em>"{{ ucwords($word) }}"</em>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="container item-wrap">
                    <div id="section-content" class="row">

                        @if($rows)
                            @component('web.templates.post-item',
                            [
                                'rows' => $rows
                            ])
                            @endcomponent
                        @else
                            @component('web.partials.no-rows',
                            [
                                'rows' => $rows
                            ])
                            @endcomponent
                        @endif

                    </div>

                    @if($rows)
                        @component('web.partials.load-more-posts', [
                            'url' => '/posts/rows'
                        ])
                        @endcomponent
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection