@extends('web.layouts.web')

@section('content')
    <input type="hidden" id="current_category" name="current_category" value="motostyle">
    <input type="hidden" data-category="3" class="active">

    <div class="motostyle">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <h1>
                                moto<span>style</span>
                            </h1>
                        </div>

                        @component('web.partials.filters',
                        [
                            'rows' => $subcategories,
                            'filter_subcategory' => $filter_subcategory,
                            'filter_orderby' => $filter_orderby
                        ])
                        @endcomponent

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="container item-wrap">
                    <div id="section-content" class="row">
                        @if($rows)
                            @component('web.templates.post-item',
                            [
                                'rows' => $rows
                            ])
                            @endcomponent
                        @else
                            @component('web.partials.no-rows',
                            [
                                'rows' => $rows
                            ])
                            @endcomponent
                        @endif
                    </div>

                    @if($rows)
                        @component('web.partials.load-more-posts', [
                            'url' => '/posts/rows'
                        ])
                        @endcomponent
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection