@extends('web.layouts.web')

@section('og_tags')
    @component('web.partials.seo.default')
    @endcomponent
@endsection

@section('content')
	<div class="promocionesmoto">
		<div class="container-fluid">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="tyc">
								<h4>Términos De Venta De Motorola</h4>
								<p>
								Por favor lea los siguientes términos antes de acceder a esta dirección. El acceder a esta dirección indicará que usted se encuentra de acuerdo con los siguientes términos y condiciones.
								</p>
								
								<p>
								¿Quieres comprar uno de nuestros teléfonos? ¡Estupendo! Nos encanta que seas nuestro cliente. Y definitivamente queremos que estés contento. Por eso es que te comentamos qué puedes esperar y qué no de la transacción. Como a ti, no nos gustan las sorpresas desagradables. Estos Términos te condicionan a ti a Motorola Mobility LLC. y Motorola Comercial S.A. de C.V. (en lo sucesivo, “Motorola”) y Infracommerce Negocios y Soluciones en Internet MX, distribuidor exclusivo para ventas en linea, durante la compra de dispositivos o accesorios (en lo sucesivo, el “Dispositivo”) desde este sitio. Es importante que leas los Términos en detalle. Si haces un pedido por un dispositivo o accesorio desde este sitio, aceptas estos Términos y también los Términos de uso del sitio web. Recuerda que si compras un Dispositivo desde algún sitio que no sea este, podrían aplicarse otros términos de venta a tu compra.
								</p>

								<p>
								Si bien todos nuestros Dispositivos son especiales, algunos lo son tanto que se incluirán términos adicionales en un anexo específico del Dispositivo ubicado en la parte inferior de estos Términos. Si existe algún conflicto, lo redactado en el anexo se impondrá sobre estos Términos.
								</p>

								<p>
								Condiciones de Compra/pago: ¡Muéstranos el Dinero
								</p>

								<p>
								No eres una tienda. Compras este teléfono para ti o para obsequiarlo. No autorizamos la reventa de productos comprados en este sitio con propósitos comerciales. La garantía, el soporte y cualquier otro servicio proporcionado no es transferible si revendes los productos comprados en este sitio.
								Asegúrate de usar una tarjeta de crédito o débito valida o hacer los pagos en efectivo en nuestros canales de pago. Por si hay alguna confusión, tú debes pagar los impuestos, cargos y, por supuesto, el plan mensual. Al hacer un pedido, permites a Motorola y su revendedor INFRACOMMERCE cobrar mediante el método de pago el monto total de tu pedido, incluido cualquier cargo o impuesto por envío y entrega.
								</p>

								<p>
								Recuerda que el precio del Dispositivo que aparece en el sitio no incluye cargos de envío ni de entrega. Estos precios aparecerán al pasar por caja (carrito de compra).
								</p>

								<p>
								En caso de adquirir un producto con meses los costos financieros que se deriven de la transacción serán cobrados directamente al consumidor por un tercero autorizado por Motorola e Infracommerce. Motorola no es responsable de estos cobros o cualquier otro gravamen o costo derivado de la operación transaccional.
								</p>

								<p>
								Errores en Precios
								</p>

								<p>
								Si hay algún error en el precio, aceptas que Motorola no estará obligado a enviar el producto, a menos que ya se te haya cobrado. Si observamos un error y no se te ha cobrado aún, te haremos conocer el precio correcto y podrás decidir si deseas continuar con tu pedido al precio correcto o si prefieres cancelarlo.
								</p>

								<p>
								Plan
								</p>

								<p>
								Debes obtener un plan de servicio para tu teléfono para activarlo. Estos los ofrecen otras empresas, y el plan es entre tú y ellos; Motorola e Infracommerce no forman parte. Se consciente que pueden cobrarte por cancelar con anticipación el plan. Algunos operadores pueden tener un período de prueba sin cargos por cancelación, pero si intentas usarlo para obtener un teléfono sin un plan a un precio reducido, Motorola te cobrará más de lo que habría costado comprar el teléfono sin plan en un comienzo.
								</p>

								¡Nuestro Envío!
								</p>

								<p>
								Si compras uno de nuestros productos a través del sitio de un tercero, quedas sujeto a los términos y condiciones establecidos en su página. Si tienes algún problema con el producto comprado a través de nuestro sitio avísanos en menos de 30 días o de lo contrario asumiremos que no se dañó durante el transporte. Tú pagas el envío y la entrega como parte del pedido. Si pides más de un producto, es posible que lleguen en distintos momentos. No se te cobra por cada elemento hasta que salga de nuestro edificio, pero por supuesto se te cobrará todo lo que hayas comprado. Cuando compras un producto físico, es tuyo tan pronto llega; si lo pierdes después de eso, es tu responsabilidad.
								</p>

								<p>
								Toma en cuenta que si tu pedido está En camino no significa que llegará a tu hogar ese mismo día. Por favor, recuerda revisar la confirmación que enviamos a tu correo electrónico, para conocer la fecha estimada de entrega en la que recibirás tu producto.
								</p>

								<p>
								Antes de finalizar la compra, puedes conocer el tiempo de entrega estándar y express, así como el costo de envío, agregando el producto a tu bolsa de compras e insertando tu código postal.
								</p>

								
								<p>
								Cuando tu producto sea enviado, te enviaremos un correo donde podrás conocer la fecha estimada de entrega y la guía de la paquetería.
								</p>

								<p>
								Eres responsable de administrar cualquier dato almacenado en tu Dispositivo, incluidos por ejemplo, la eliminación, transferencia y creación de copias de seguridad de dichos datos.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection