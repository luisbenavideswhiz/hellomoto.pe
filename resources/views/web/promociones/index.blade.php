@extends('web.layouts.web')

@section('content')
	<div class="promocionesmoto">
		<div class="container-fluid">
			<div class="row">
				<div class="container">
					<div class="col-xs-12">
						<div class="row">
							<div class="prom-slider">
								@foreach($promotions as $promotion)
								@if($promotion->status == 3)
								<a href="{{ $promotion->link }}" target="_blank">
									<img src="{{ url($promotion->image) }}">
								</a>
								@endif
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="container prom-wrap">
					<div class="row" id="section-content">
						@foreach($promotions as $promotion)
						@if($promotion->status == 1)
						<div class="col-xs-12 col-md-4">
							<div class="item">
								<div class="prom">
									<div class="img">
										<a href="{{ $promotion->link }}" target="_blank">
											<img src="{{ url($promotion->image) }}" alt="{{ $promotion->title }}">
										</a>
									</div>
								</div>
								{{--<div class="data">--}}
									{{--<a href="{{ $promotion->link }}" target="_blank">Comprar ahora</a>--}}
								{{--</div>--}}
							</div>
						</div>
						@endif
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
