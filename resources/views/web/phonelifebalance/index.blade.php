<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/x-icon" href="{{ asset ('img/web/plb/favicon.ico') }}">
    <title>¿Cómo está tu equilibrio celular?</title>

    @include('web.phonelifebalance.assets.styles')
</head>
<body>

	@include('web.phonelifebalance.partials.header')
	<div id="root">
		<nav class="c-nav">
			<ul class="intro" id="top-menu">
				<li>
					<a href="#home">
					<div>
						<span>introducción</span>
					</div>
					</a>
				</li>
				<li>
					<a href="#vacations">
					<div>
						<span>¿conectado? ¿o desconectado?</span>
					</div>
					</a>
				</li>
				<li>
					<a href="#data">
					<div>
						<span>haz el test</span>
					</div>
					</a>
				</li>
				<li>
					<a href="#inout">
					<div>
						<span>nuestro punto de vista</span>
					</div>
					</a>
				</li>
				<li>
					<a href="#space">
					<div>
						<span>autoayuda</span>
					</div>
					</a>
				</li>
			</ul>
		</nav>
		<header>
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<a href="https://www.motorola.com.pe/">
							<img src="{{ asset ('img/web/plb/motorola-icon-home.png') }}" alt="">
						</a>
						<div class="back-wrapper">
							<a href="https://hellomoto.pe/">
								Volver a hello<span>moto</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</header>
		<section id="home">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-5">
						<h2>
							¿Tu <span>teléfono te pertenece</span> o le perteneces a tu teléfono?
						</h2>
						<p>
							Para muchos de nosotros, es difícil saberlo. Aunque los teléfonos han enriquecido enormemente nuestras vidas, también la han invadido. Creemos que la clave está en el equilibrio. Participa en la vida a través de tu teléfono y sin él. <span>¿Cómo está el balance entre tu teléfono y tu vida?</span>
						</p>
						<a href="https://phonelifebalance-la.motorola.com/#/question-1" target="_blank">haz el test</a>
					</div>
				</div>
			</div>
		</section>
		<section id="vacations">
			<div class="o-wrapper">
				<h2 class="c-section__title">
					hello
					<span>vacation</span>
				</h2>
				<div class="c-section-vacation__body">
					<p>Recorrimos el mundo para ver cómo los smartphones han cambiado nuestra vida.</p>
				</div>
			</div>
			<div class="c-section-vacation__video">
				<div class="c-youtube">
					<div class="c-youtube__keyframe"></div>
					<iframe id="c-youtube__video" width="640" height="390" src="https://www.youtube.com/embed/oYgUl6FQE2k?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
			</div>
		</section>
		<section id="data">
			<div class="o-wrapper">
				<h2 class="c-section__title">
					phone-life balance: 
					<span>los datos</span>
				</h2>
				<div class="c-section-the-data__body">
					<p>
						Preguntamos a diferentes usuarios de smartphones 10 sencillas preguntas acerca de su relación con su teléfono. ¿Cómo te comparas?
					</p>
				</div>
				<a class="c-button c-button--pink" href="https://phonelifebalance-la.motorola.com/#/question-1" target="_blank">
					haz el test
				</a>
			</div>
			<div class="c-categories">
				<div class="o-wrapper">
					<h2 class="c-section__title">
						categorías 
						<span>de usuario</span>
					</h2>
					<div class="c-section-vacation__body">
						<em>
							¿con cuál te identificas?
						</em>
					</div>
				</div>
				<div class="o-wrapper">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#levelFive" aria-expanded="true" aria-controls="levelFive">
										<span>level 5 </span>
										<span> Phone-Fanatico</span>
										<span class="accordion-toggle-icon">
											<span></span>
											<span></span>
										</span>
									</a>
								</h4>
							</div>
							<div id="levelFive" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="level5">
								<div class="panel-body">
									<div class="the_content">
										<div class="img">
											<img src="{{ asset ('img/web/plb/persona-5.png') }}" alt="">
										</div>
										<div class="p">
											<h5>
												Phone-Fanático
											</h5>
											<p>
												Nunca dejas de utilizar tu teléfono, una realidad que es el doble de negativa en más de un sentido. Es lo primero que revisas en la mañana, antes de dormir y siempre que puedes. A veces, te descubres mirándolo sin saber por qué. Separarte de tu teléfono, incluso durante unos minutos, te hace sentir vulnerable y estresado. Tu relación con tu teléfono está eclipsando tu relación con tus familiares y amigos; y de hecho prefieres enviar un mensaje de texto que hablar con alguien en persona, además de sentir un impulso de contestar los mensajes de manera inmediata, sin importar qué o quién te interrumpa. No será fácil, pero el balance entre el teléfono y la vida aún está a tu alcance.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="level4">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#levelFour" aria-expanded="false" aria-controls="levelFour">
										<span>level 4</span>
										<span>Phone-Enamorado</span>
										<span class="accordion-toggle-icon">
											<span></span>
											<span></span>
										</span>
									</a>
								</h4>
							</div>
							<div id="levelFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="level4">
								<div class="panel-body">
									<div class="the_content">
										<div class="img">
											<img src="{{ asset ('img/web/plb/persona-4.png') }}" alt="">
										</div>
										<div class="p">
											<h5>
												Phone-Enamorado
											</h5>
											<p>
												Tú y tu teléfono. Comenzó como un coqueteo casual y floreció como un romance. Ahora podrás deslizarte por esa pendiente hasta la obsesión. Tu teléfono nunca está apagado. En las noches, lo dejas junto a tu cama. Cuando creas que haces multitareas, en realidad te multi-distraes. A veces revisas tu teléfono para ver la hora o el clima, pero cuando te das cuenta estás haciendo algo más, sin saber la hora o el clima. ¿Pasaron segundos o minutos? Es difícil de decir. Lo que es menos difícil de decir es que este comportamiento hacia el teléfono corre el riesgo de convertirse en automático.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="level3">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#levelThree" aria-expanded="false" aria-controls="levelThree">
										<span>level 3</span>
										<span>Phone-Dependiente</span>
										<span class="accordion-toggle-icon">
											<span></span>
											<span></span>
										</span>
									</a>
								</h4>
							</div>
							<div id="levelThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="level3">
								<div class="panel-body">
									<div class="the_content">
										<div class="img">
											<img src="{{ asset ('img/web/plb/persona-3.png') }}" alt="">
										</div>
										<div class="p">
											<h5>
												Phone-Dependiente
											</h5>
											<p>
												Oooh... tu teléfono es tan tierno. ¿Cómo resistirse? A veces no puedes. Lo usas en momentos de inactividad solo porque está allí. Y también la ansiedad que sientes cuando tu teléfono dice que le queda el 10 % de batería. Los modales personales indican cuándo y dónde utilizar el teléfono, no siempre vives en él. Si bien te molesta que tus amigos usen su teléfono cuando salen, no te molesta tanto como para no hacer lo mismo. Tu balance entre el teléfono y la vida es mejor que el de la mayoría, pero ese zumbido fantasma que imaginas tal vez intenta decirte algo.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="level2">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#levelTwo" aria-expanded="false" aria-controls="levelTwo">
										<span>level 2</span>
										<span>Phone-Consciente</span>
										<span class="accordion-toggle-icon">
											<span></span>
											<span></span>
										</span>
									</a>
								</h4>
							</div>
							<div id="levelTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="level2">
								<div class="panel-body">
									<div class="the_content">
										<div class="img">
											<img src="{{ asset ('img/web/plb/persona-2.png') }}" alt="">
										</div>
										<div class="p">
											<h5>
												Phone-Consciente
											</h5>
											<p>
												Vives la vida con tu teléfono, no en él. Y por eso te saludamos con dos pulgares arriba. Utilizas tu teléfono para ahorrar tiempo y energía que puedes invertir en cosas que te importan. Comprendes el valor de las relaciones y cómo aprovecharlas. Tienes horarios para usar tu teléfono y horarios para vivir el momento. Te diste cuenta que el equilibrio entre el teléfono y la vida no es un estado final, sino algo que necesitas cuidar. Y trabajas para mantenerlo. En resumen, has logrado alcanzar un estado ejemplar del balance entre el teléfono y la vida, sigue así y transmítelo.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="level1">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#levelOne" aria-expanded="false" aria-controls="levelOne">
										<span>level 1</span>
										<span>Phone-sapiens</span>
										<span class="accordion-toggle-icon">
											<span></span>
											<span></span>
										</span>
									</a>
								</h4>
							</div>
							<div id="levelOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="level1">
								<div class="panel-body">
									<div class="the_content">
										<div class="img">
											<img src="{{ asset ('img/web/plb/persona-1.png') }}" alt="">
										</div>
										<div class="p">
											<h5>
												Phone-sapiens
											</h5>
											<p>
												Solo eres un humano con un teléfono. Apenas. Lo utilizas para lo básico: revisar la hora, hacer llamadas telefónicas y... bueno, en realidad eso es todo. Nunca te ha gustado la onda de mandar mensajes; toma demasiado tiempo escribir con el dedo índice. Claro, tienes algunas cuentas en redes sociales, pero no has publicado nada en meses. En el día a día, no molestar es tu modus operandi. Bueno, la buena noticia es que no corres el riesgo de que tu teléfono tome tu vida. La no tan buena noticia es que te podrías estar perdiendo el siglo 21. Ser móvil no solo es conveniente, es esencial en la vida diaria. No acostumbramos a decir esto, pero un poco más de teléfono en tu dieta podría ser bueno para ti.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="inout">
			<div class="o-wrapper">
				<h2 class="c-section__title">
					¿conectado? 
					<span class="c-section-letter__highlight">¿o desconectado?</span>
				</h2>
				<p>
					El año pasado, el smartphone cumplió 10 años. Mejoró nuestra vida de muchas formas. Y no es sólo por los videos de gatos ni los memes. 
				</p>
				<p>
					Cada día, los smartphones cambian la forma en la que vivimos, hacemos transacciones bancarias, compramos, aprendemos, consultamos al doctor, nos conectamos con nuestros seres queridos e iniciamos nuevas relaciones. 
					<span>
						Y al tiempo que los smartphones nos acercan a quienes están lejos, a veces nos alejan de las personas que están justo frente a nosotros. 
					</span>
				</p>
				<p class="c-section-letter__show-more ">
					<a>
						<span>
							Lee mas
						</span>
					</a>
				</p>
				<div class="c-section-letter__more hideme">
					<p>
						¿Puedes resistirte a abrir notificaciones o responder mensajes cuando estas con tu familia o amigos? ¿Podemos estar conectados y aún asi estar presentes? ¿Podemos compartir tanto en la vida real como lo hacemos en Internet? 
					</p>
					<p>
						<span class="c-section-letter__highlight">
							Es hora de que haya un mejor phone-life balance
						</span>
					</p>
					<p>
						Como inventores del primer teléfono móvil hace 44 años, nos interesa mucho saber cómo las personas están utilizando hoy esta tecnología. Queremos crear teléfonos que te ayuden en tu vida sin convertirse en el centro de atención.
					</p>
					<p>
						Después de todo, no solo somos una empresa de ingenieros, también somos personas; padres, amigos e hijos.
						<span class="c-section-letter__highlight">
							Nos importa la evolución de los smartphones y el efecto que tienenen el mundo.
						</span>
					</p>
					<p class="c-section-letter__emphasized">
						<span>
							1/3
						</span>
						<span>
							de los encuestados afirmó que le ha dado prioridad a los smartphones por encima de la gente que le importa y que desea pasar un poco de tiempo con ellos.
						</span>
					</p>
					<p>
						Nos asociamos con la Dra. Nancy Etcoff, especialista en el comportamiento mental / cerebral, y la ciencia de la Felicidad en la Universidad de Harvard y Psicóloga en el departamento de Psiquiatría del Hospital General de Massachusetts, para realizar un estudio sobre "El balance en el uso del smartphone". El estudio muestra que 1 de cada 3 entrevistados ya prioriza el smartphone en vez de las personas que ama, y con quienes desea pasar su tiempo. 
					</p>
					<p>
						También nos estamos asociando con organizaciones como SPACE, una aplicación creada para cambiar comportamientos, que puede ayudar a las personas a tener una mejor relación con sus smartphones.
					</p>
					<p>
						<span class="c-section-letter__highlight">
							En Motorola estamos repensando todo lo que hacemos para crear soluciones que te reconecten con lo que más te importa, y te ayuden a tener un mejor balance en el uso de la tecnología. 
						</span>
						Así crearemos productos que nos devuelvan la alegría de la conexión, en línea y fuera de línea.
					</p>
					<p>Aquí hay un mejor balance entre el teléfono y la vida.</p>
					<img class="c-section-letter__logo" src="{{ asset ('img/web/plb/motorola-logo-orange.png') }}" alt="Motorola">
					<p class="c-section-letter__show-less">
						<a>
							<span>
								Lee menos
							</span>
						</a>
					</p>
				</div>
			</div>
		</section>
		<section id="space">
			<div class="c-section-the-science__apps">
				<div class="o-wrapper">
					<h2 class="c-section__title">
						¿quieres 
						<strong>mejorar </strong>
						el balance entre el uso del teléfono y tu vida?
					</h2>
					<div class="c-section-the-science__apps-body">
						<p class="c-section-the-science__apps-list">
							Descargar la app correcta no es tan contradictorio como parece.
							<br><br>
							SPACE te ayuda a construir una relación saludable con tu teléfono. Respaldado por investigaciones de las universidades más renombradas, este programa personalizado de cambios de comportamiento digital te ayudará a tomar de vuelta el control de tu teléfono para generar más espacio en tu vida. Busca el balance entre el uso del teléfono y tu vida, y deja atras tú adicción al celular.
							<br><br>
							Descarga ahora o encuentra más información en
							<a href="https://findyourphonelifebalance.com/" target="_blank">
								space-app.com
							</a>
						</p>
						<a href="https://findyourphonelifebalance.com/" target="_blank">
							<img alt="Space" src="{{ asset ('img/web/plb/space.png') }}">
						</a>
					</div>
				</div>
			</div>
		</section>
		<footer>
			<div class="o-wrapper">
				<div class="img">
					<img alt="Motorola a Lenovo company" class="footer-img-logo" src="{{ asset ('img/web/plb/motorola_footer_logo.png') }}">
				</div>
				<div class="copyright-block">
					<p>© 2018 Motorola Mobility LLC. Todos los derechos reservados.</p>
					<p>Motorola y la M estilizada son marcas registradas de Motorola Trademark Holdings, LLC.</p>
					<p>Todos los smartphones están diseñados y fabricados por Motorola Mobility LLC, una subsidiaria de propiedad exclusiva de Lenovo.</p>
					<p>Teléfono para pedidos realizados en la tienda online de Motorola (www.motorola.com.pe)</p>
					<p>SAC - Motorola +52 55 4160-7906</p>
					<p>Horario de atención de 9:00 a 18:00 horas.</p>
				</div>
			</div>
		</footer>
	</div>
	@include('web.phonelifebalance.assets.scripts')

</body>
</html>