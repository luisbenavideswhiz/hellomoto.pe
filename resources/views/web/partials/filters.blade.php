<div class="col-xs-12 col-sm-6 col-md-3">
    <div class="orderby">
        <span>Subcategor&iacute;a:</span>

        <div class="input-field">
            <select id="filter_subcategory" name="filter_subcategory" class="filters">
                <option value="todos">Todos</option>
                @foreach($rows as $key => $value)
                    @if($filter_subcategory == $value->slug)
                        <option value="{{ $value->slug }}" selected="selected">{{ $value->name }}</option>
                    @else
                        <option value="{{ $value->slug }}">{{ $value->name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-3">
    <div class="orderby">
        <span>Ordenar por:</span>

        <div class="input-field">
            <select id="filter_orderby" name="filter_orderby" class="filters">
                @if($filter_orderby == 'mas-recientes')
                    <option value="mas-recientes" selected="selected">M&aacute;s recientes</option>
                @else
                    <option value="mas-recientes">M&aacute;s recientes</option>
                @endif
                @if($filter_orderby == 'menos-recientes')
                    <option value="menos-recientes" selected="selected">Menos recientes</option>
                @else
                    <option value="menos-recientes">Menos recientes</option>
                @endif
            </select>
        </div>
    </div>
</div>