<div class="entry-gal">
    <h4>
        <span>Galer&iacute;a</span>
    </h4>

    <div id="gallery" style="display:none;">
        @foreach($gallery as $key => $value)
            @if($value->webGallery->filetype == 'image')
                <img src="{{ url($value->webGallery->path) }}" alt="{{ $value->webGallery->alt }}"
                     data-image="{{ url($value->webGallery->path) }}" data-description="{{ $value->webGallery->alt }}">
            @endif

            @if($value->webGallery->filetype == 'video')
                <img src="https://img.youtube.com/vi/{{ $value->webGallery->path }}/0.jpg"
                     alt="{{ $value->webGallery->alt }}"
                     data-image="https://img.youtube.com/vi/{{ $value->webGallery->path }}/0.jpg" data-description="{{ $value->webGallery->alt }}">
            @endif
        @endforeach
    </div>
</div>