<div class="nope">
    <p>
        Disculpe, pero no hemos encontrado resultados con su t&eacute;rmino de b&uacute;squeda.
        Por favor,
        intenta nuevamente con una palabra clave diferente.
    </p>
</div>