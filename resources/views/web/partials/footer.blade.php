<div class="redes">
    <a href="https://www.facebook.com/MotorolaPE" target="_blank">
        <i class="fa fa-facebook"></i>Facebook
    </a>
    <a href="https://twitter.com/MotorolaPeru" target="_blank">
        <i class="fa fa-twitter"></i> Twitter
    </a>
    <a href="https://www.instagram.com/motorolape/" target="_blank">
        <i class="fa fa-instagram"></i> Instagram
    </a>
</div>
<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="logo">
                            <img src="{{ asset('img/web/logo_blanco.svg') }}" alt="">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="disclaimer">
                            &copy; {{ date('Y') }} MOTOROLA MOBILTY LLC. TODOS LOS DERECHOS RESERVADOS
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>    

<div class="gotoup">
    <a href="#top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>
