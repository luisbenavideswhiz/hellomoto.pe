<div class="row">
    <div class="container dis">
        <div class="col-xs-12">
            <div id="disqus_thread"></div>
            <script>

                var disqus_config = function () {
                    this.page.url = "{{ getenv('APP_URL') }}";
                    this.page.identifier = "{{ $url }}";
                };
                (function () {
                    var d = document, s = d.createElement('script');
                    s.src = 'https://motorola-peru.disqus.com/embed.js';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                })();

            </script>
            <noscript>
                Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments
                    powered by Disqus.</a>
            </noscript>
        </div>
    </div>
</div>
