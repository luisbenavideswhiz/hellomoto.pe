<header class="d">
    <div class="headings">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-3">
                            <a href="https://www.motorola.com.pe/home" class="logo" target="_blank">
                                <img src="{{ asset('img/web/logo_blanco.svg') }}" alt="">
                            </a>
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-9">
                            <nav class="hmenu">
                                <ul>
                                    <li>
                                        <a href="https://www.motorola.com.pe/home" target="_blank">Sitio Web
                                            Motorola</a>
                                    </li>
                                    <li>
                                        <a href="#!">Celulares</a>
                                        <ul>
                                            <li>
                                                <a href="http://www.motorola.com.pe/smartphones/familia-moto-z"
                                                   target="_blank">Familia Moto Z</a>
                                            </li>
                                            <li>
                                                <a href="http://www.motorola.com.pe/smartphones/familia-moto-g"
                                                   target="_blank">Familia Moto G</a>
                                            </li>
                                            <li>
                                                <a href="http://www.motorola.com.pe/moto-c-4g/p" target="_blank">Moto
                                                    C</a>
                                            </li>
                                            <li>
                                                <a href="http://www.motorola.com.pe/moto-e-plus/p"
                                                   target="_blank">Moto E Plus</a>
                                            </li>
                                            <li>
                                                <a href="http://www.motorola.com.pe/smartphones"
                                                   target="_blank">Ver todos los celulares</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#!">Dónde Comprar</a>
                                        <ul>
                                            <li class="text-center">
                                                <a href="http://www.tiendaclaroperu.com/p/motorola.html"
                                                   target="_blank"
                                                   style="display: block;margin: auto;">
                                                    <img src="/img/web/claro.svg" alt="Claro">
                                                </a>
                                            </li>
                                            <li class="text-center">
                                                <a href="http://www.entel.pe/equipos/celulares-postpago/"
                                                   target="_blank"
                                                   style="display: block;margin: auto;">
                                                    <img src="/img/web/entel.svg" alt="Entel">
                                                </a>
                                            </li>
                                            <li class="text-center">
                                                <a href="http://catalogo.movistar.com.pe/smartphones/motorola"
                                                   target="_blank"
                                                   style="display: block;margin: auto;">
                                                    <img src="/img/web/movistar.svg" alt="Movistar">
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    {{--<li>--}}
                                        {{--<a href="{{ url('/promociones') }}">Promociones</a>--}}
                                    {{--</li>--}}
                                    <li>
                                        <a href="#!">
                                            Ayuda <i class="fa fa-chevron-down"></i>
                                        </a>
                                        <ul>
                                            <li>
                                                <a href="https://motorola-global-es-latam.custhelp.com/app/home"
                                                   target="_blank">Para Celulares,Tablets y Accesorios</a>
                                            </li>
                                            <li>
                                                <a href="https://motorola-global-es-latam.custhelp.com/app/software-upgrade-news/g_id/1984"
                                                   target="_blank">Para Software y Aplicaciones</a>
                                            </li>
                                            <li>
                                                <a href="https://motorola-global-es-latam.custhelp.com/app/answers/detail/a_id/89882"
                                                   target="_blank">Controladores</a>
                                            </li>
                                            <li>
                                                <a href="https://motorola-global-es-latam.custhelp.com/app/mcp/trackrepair2"
                                                   target="_blank">Servicio y Reparación</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span>
                                            <i class="fa fa-search"></i>
                                        </span>

                                        <div class="searcher">
                                            <div class="input-field">
                                                <input type="text" class="search-input" placeholder="Digite una palabra">
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="logo">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="/">
                                <h1>hello<span>moto</span></h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mmenu">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <nav>
                                <ul>
                                    <li>
                                        <a href="{{ url('/hellociudades') }}" >
                                            <span>hello</span>ciudades <i class="fa fa-chevron-down"></i>
                                        </a>
                                        <ul>
                                            @foreach($menu['hellociudades'] as $key => $value)
                                                <li>
                                                    <a href="{{ url('/hellociudades/'.$value->slug) }}" >{{ $value->name }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/experienciasmoto') }}" >
                                            experiencias<span>moto</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/motostyle') }}" >
                                            <span>moto</span>style <i class="fa fa-chevron-down"></i>
                                        </a>
                                        <ul>
                                            @foreach($menu['motostyle'] as $key => $value)
                                                <li>
                                                    <a href="{{ url('/motostyle/'.$value->slug) }}" >{{ $value->name }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/motoembajadores') }}" >
                                            <span>moto</span>embajadores <i class="fa fa-chevron-down"></i>
                                        </a>
                                        <ul>
                                            <li>
                                                <a href="{{ url('/motoembajadores') }}" >Embajadores</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/motoembajadores/playlists') }}" >Playlists</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/hellotips') }}" >
                                            <span>hello</span>tips
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/phone-life-balance') }}" >
                                            phonelife<span>balance</span>
                                        </a>
                                    </li>
                                    @if(Auth::check())
                                        @php $user = Auth::user(); @endphp
                                        <li class="user" style="width: 160px">
                                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span><i class="fa fa-user"></i> Hola {{ ucwords($user->name) }}</span>
                                            </a>

                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                <li><a href="/usuario/mis-embajadores"> Mis embajadores</a></li>
                                                <li><a href="/usuario/mis-playlist"> Mis playlist</a></li>
                                                <li><a href="/usuario/beneficios"> Beneficios</a>
                                                </li>
                                                <li><a href="/usuario/perfil"> Mi perfil</a></li>
                                                <li><a href="/usuario/logout"> Cerrar Sesi&oacute;n</a></li>
                                            </ul>
                                        </li>
                                    @else
                                        <li class="bordn">
                                            <a class="register-button" id="login-modal-desktop" href="#!" data-toggle="modal" data-target="#loginModal">
                                                reg&iacute;strate<span> beneficios</span>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
