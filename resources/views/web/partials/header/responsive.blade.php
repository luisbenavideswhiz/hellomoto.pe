<header class="r">
    <div class="headings">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="log">
                                <img src="{{ asset('img/web/iso_blanco.svg') }}" alt="">

                                <div class="menutrigger">
                                    <i class="fa fa-reorder"></i>
                                    <i class="fa fa-close"></i>
                                </div>
                            </div>
                            <div class="logo">
                                <a href="{{ url('/') }}" >
                                    <span>hello<span>moto</span></span>
                                </a>
                            </div>
                            <div class="search" id="r_search">
                                <span>
                                    <i class="fa fa-search"></i>
                                </span>

                                <div class="searcher">
                                    <div class="input-field">
                                        <input type="text" class="search-input" placeholder="Digite una palabra">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="rmenu">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="#rmenu0" data-toggle="collapse" data-parent="#accordion">
                            <span>Menú</span>principal<i class="fa fa-chevron-down"></i>
                        </a>
                    </h4>
                </div>
                <div id="rmenu0" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="panel-group" id="mainaccordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="{{ url('/phone-life-balance') }}" >
                                            phonelife<span>balance</span>
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#rmenu1" data-toggle="collapse" data-parent="#mainaccordion">
                                            <span>hello</span>ciudades <i class="fa fa-chevron-down"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="rmenu1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        @foreach($menu['hellociudades'] as $key => $value)
                                            <a href="{{ url('/hellociudades/'.$value->slug) }}" >{{ $value->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="{{ url('/experienciasmoto') }}" >
                                            experiencias<span>moto</span>
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#rmenu3" data-toggle="collapse" data-parent="#mainaccordion">
                                            <span>moto</span>style <i class="fa fa-chevron-down"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="rmenu3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        @foreach($menu['motostyle'] as $key => $value)
                                            <a href="{{ url('/motostyle/'.$value->slug) }}" >{{ $value->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#rmenu5" data-toggle="collapse" data-parent="#mainaccordion">
                                            <span>moto</span>embajadores <i class="fa fa-chevron-down"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="rmenu5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <a href="{{ url('/motoembajadores') }}" >Embajadores</a>
                                        <a href="{{ url('/motoembajadores/playlists') }}" >Playlists</a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="{{ url('/hellotips') }}" >
                                            <span>hello</span>tips
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            @if(Auth::check())
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#rmenu7" data-toggle="collapse" data-parent="#mainaccordion">
                                            <span><i class="fa fa-user"></i> Hola {{ ucwords(Auth::user()->name) }}</span> <i
                                                    class="fa fa-chevron-down"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="rmenu7" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <a href="/usuario/mis-embajadores"> Mis embajadores</a>
                                        <a href="/usuario/mis-playlist"> Mis playlist</a>
                                        <a href="/usuario/beneficios"> Beneficios</a>
                                        <a href="/usuario/perfil"> Mi perfil</a>
                                        <a href="/usuario/logout"> Cerrar Sesi&oacute;n</a>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a id="login-modal-responsive" href="#!" data-toggle="modal" data-target="#loginModal">
                                            <span>Ingresar</span>
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="#rmenu6" data-toggle="collapse" data-parent="#accordion">
                            <span>M&aacute;s</span>opciones <i class="fa fa-chevron-down"></i>
                        </a>
                    </h4>
                </div>
                <div id="rmenu6" class="panel-collapse collapse">
                    <div class="panel-body">
                        <a href="https://www.motorola.com.pe/home" target="_blank">Sitio Web Motorola</a>
                        <div class="panel-group" id="subaccordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#rmenu61" data-toggle="collapse" data-parent="#subaccordion">
                                            <span>Celulares</span> <i class="fa fa-chevron-down"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="rmenu61" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <a href="http://www.motorola.com.pe/smartphones/familia-moto-z"
                                           target="_blank">Familia Moto Z</a>
                                        <a href="http://www.motorola.com.pe/smartphones/familia-moto-g"
                                           target="_blank">Familia Moto G</a>
                                        <a href="http://www.motorola.com.pe/moto-c-4g/p" target="_blank">Moto
                                            C</a>
                                        <a href="http://www.motorola.com.pe/moto-e-plus/p"
                                           target="_blank">Moto E Plus</a>
                                        <a href="http://www.motorola.com.pe/smartphones"
                                           target="_blank">Ver todos los celulares</a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#rmenu62" data-toggle="collapse" data-parent="#subaccordion">
                                            <span>D&oacute;nde Comprar</span> <i class="fa fa-chevron-down"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="rmenu62" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <a href="http://www.tiendaclaroperu.com/p/motorola.html"
                                           target="_blank">
                                            Claro
                                        </a>
                                        <a href="http://www.entel.pe/equipos/celulares-postpago/"
                                           target="_blank">
                                            Entel
                                        </a>
                                        <a href="http://catalogo.movistar.com.pe/smartphones/motorola"
                                           target="_blank">
                                            Movistar
                                        </a>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="panel panel-default">--}}
                                {{--<div class="panel-heading">--}}
                                    {{--<a href="{{ url('/promociones') }}" target="_blank">Promociones</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#rmenu63" data-toggle="collapse" data-parent="#subaccordion">
                                            <span>Ayuda</span> <i class="fa fa-chevron-down"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="rmenu63" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <a href="https://motorola-global-es-latam.custhelp.com/app/home"
                                           target="_blank">Para Celulares, Tablets y Accesorios</a>
                                        <a href="https://motorola-global-es-latam.custhelp.com/app/software-upgrade-news/g_id/1984"
                                           target="_blank">Para Software y Aplicaciones</a>
                                        <a href="https://motorola-global-es-latam.custhelp.com/app/answers/detail/a_id/89882"
                                           target="_blank">Controladores</a>
                                        <a href="https://motorola-global-es-latam.custhelp.com/app/mcp/trackrepair2"
                                           target="_blank">Servicio y Reparaci&oacute;n</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
