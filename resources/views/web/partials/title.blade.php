<title>@if($slot!='')
        {{ $slot }} | hellomoto Perú
        @else
        {{ getenv('APP_TITLE') }}
        @endif
</title>