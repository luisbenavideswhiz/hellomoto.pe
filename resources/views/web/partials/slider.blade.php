<div class="featured-slider">
    <div class="mollie-slider">
        {{--<div id="post-id-1" class="item slick-slide slick-current slick-active slick-center" data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide00"> <img src="http://hellomoto.whiz.pe/img/landing-g6/banner-web-hellomoto.png" alt="Conoce la nueva familia moto g6"> <div class="overlay-slider-home"> <div class="overlay-bk-black"></div> <div class="feat-overlay"> <div class="feat-text"> <h3> <a href="http://hellomoto.whiz.pe/moto-g6" tabindex="0"> La familia moto g6 está inspirada en ti </a> </h3> <a href="http://hellomoto.whiz.pe/moto-g6" style="background-color: #f33421;" tabindex="0"> ¡CONÓCELOS! </a> </div> </div> </div> </div>--}}

        @foreach($slider as $key => $value)
            <div id="post-id-{{ $value->id }}" class="item" style="background-image: url({{ url($value->banner_img) }})" alt="{{ $value->title }}"å>

                <div class="overlay-slider-home">
                    <div class="overlay-bk-black"></div>
                    <div class="feat-overlay">
                        <div class="feat-text {{ $value->type_class }}">

                            {!! $value->category_html !!}

                            <h3>
                                <a href="{{ $value->url_post }}">
                                    {{ $value->title }}
                                </a>
                            </h3>

                            <a href="{{ $value->url_post }}">leer m&aacute;s</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
