<link rel="icon" type="image/x-icon" href="{{ asset('/favicon.ico') }}">
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('/favicon.ico') }}">
<link rel="icon" type="image/png" href="{{ asset('/favicon-16x16.png') }}" sizes="16x16">
<link rel="icon" type="image/png" href="{{ asset('/favicon-32x32.png') }}" sizes="32x32">
<link rel="icon" type="image/png" href="{{ asset('/favicon-96x96.png') }}" sizes="96x96">
<link href="{{ asset('/apple-touch-icon.png') }}" rel="apple-touch-icon" />
<link href="{{ asset('/apple-touch-icon-144x144.png') }}" rel="apple-touch-icon" sizes="152x152" />
<link href="{{ asset('/apple-touch-icon-152x152.png') }}" rel="apple-touch-icon" sizes="167x167" />
<link href="{{ asset('/apple-touch-icon-180x180.png') }}" rel="apple-touch-icon" sizes="180x180" />
<link href="{{ asset('/android-icon-144x144.png') }}" rel="icon" sizes="144x144" />
<link href="{{ asset('/android-icon-192x192.png') }}" rel="icon" sizes="192x192" />

<link rel="stylesheet" href="{{ mix('css/web/bundle.css') }}" type="text/css">
<link rel="stylesheet" href="{{ mix('css/web/all.css') }}" type="text/css">

<!--[if lte IE 10]>
<link rel="stylesheet" href="{{ asset('css/styleLTIE11.css') }}" type="text/css">
<![endif]-->
