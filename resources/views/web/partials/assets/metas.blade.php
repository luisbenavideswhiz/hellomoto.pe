

	@if(!isset($post))
		<meta name="title" content="hellomoto Perú | Descubre el otro lado de la ciudad">
		<meta name="description" content="Comunidad peruana de HelloMoto donde encontaras informacion sobre lo que pasa en tu ciudad siempre al lado de un celular motorola ¡Únete a la comunidad hellomoto Perú y descubre el otro lado de la ciudad!">
		<meta name="keywords" content="Hellomoto, Motorola, Moto G, Moto Z, MotoMods, Smartphone, celulares, celulares motorola">
	@else
		<meta name="title" content="{{ !isset($post->current->meta_title)?'':$post->current->meta_title }} | hellomoto Perú">
		<meta name="description" content="{{ !isset($post->current->meta_description)?'':$post->current->meta_description }}">
		<meta name="keywords" content="{{ !isset($post->current->meta_keywords)?'':$post->current->meta_keywords }}">
	@endif


