<div class="inst">
    <div class="hash">
        <span>
            #hello<span>moto</span>
        </span>
    </div>
    <div id="instagram_content" class="post_content">
        @foreach($instagram as $key => $value)
            <div class="item">
                <img src="{{ $value->images->standard_resolution->url }}" alt="{{ $value->caption->text }}"/>

                <div class="hover">
                    <a href="#"
                       class="instagram-item"
                       @if(isset($value->videos))
                       data-filetype="{{ substr($value->videos->standard_resolution->url, strlen($value->videos->standard_resolution->url)-3) }}"
                       data-filename="{{ $value->videos->standard_resolution->url }}"
                       @else
                       data-filetype="{{ substr($value->images->standard_resolution->url, strlen($value->images->standard_resolution->url)-3) }}"
                       data-filename="{{ $value->images->standard_resolution->url }}"
                       @endif
                       data-text="{{ $value->caption->text }}"
                       data-link="{{ $value->link }}"
                       data-likes="{{ $value->likes->count }}"
                       data-comments="{{ $value->comments->count }}"
                       data-user-image="{{ $value->caption->from->profile_picture }}"
                       data-username="{{ $value->caption->from->full_name }}"
                            >
                        <i class="fa fa-heart"></i> {{ $value->likes->count }}
                        <i class="fa fa-comment-o"></i> {{ $value->comments->count }}
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div>

<div id="instagram-modal" class="modal fade istragramo" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="close" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </div>
                <div class="left">
                    <img id="image" src="" class="hidden" alt="">
                    <iframe id="video" class="hidden" frameborder="0" src="" width="500" height="500"></iframe>
                </div>
                <div class="right">
                    <div class="box">
                        <div class="header">
                            <div class="log">
                                <img id="user-image" src="" alt="" width="50">

                                <div id="username" href="#"></div>
                            </div>
                            <div class="toinst">
                                <a id="link" href="#" target="_blank">
                                    Abrir en Instagram
                                </a>
                            </div>
                        </div>
                        <div class="meta">
                            <div class="likes">
                                <i class="fa fa-heart"></i> <span id="likes"></span>
                            </div>
                            <div class="comments">
                                <i class="fa fa-comment"></i> <span id="comments"></span>
                            </div>
                        </div>
                        <div id="text" class="text"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>