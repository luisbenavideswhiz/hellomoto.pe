
<meta property="og:title" content="{{ !isset($ambassador->ambassador_fullname)?'':$ambassador->ambassador_fullname }} | hellomoto Perú"/>
<meta property="og:description" content="{{ !isset($ambassador->bio)?'':strip_tags($ambassador->bio) }}"/>
<meta property="og:image" content="{{ !isset($ambassador->profile_img)?'':url($ambassador->profile_img) }}"/>
<meta property="og:site_name" content="Hello Moto" />
<meta property="og:url" content="{{ url('motoembajadores/embajador/'.$ambassador->slug) }}"/>
<meta property="og:locale" content="es_ES" />
<meta property="og:type" content="article"/>
<meta property="article:publisher" content="https://www.facebook.com/MotorolaPE/" />

<meta name="twitter:title" content="{{ !isset($ambassador->ambassador_fullname)?'':$ambassador->ambassador_fullname }} | hellomoto Perú">
<meta name="twitter:description" content="{{ !isset($ambassador->bio)?'':strip_tags($ambassador->bio) }}">
<meta name="twitter:image" content="{{ !isset($ambassador->profile_img)?'':url($ambassador->profile_img) }}">
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="@MotorolaPE"/>
<meta name="twitter:creator" content="@MotorolaPE"/>

<meta itemprop="name" content="{{ !isset($ambassador->ambassador_fullname)?'':$ambassador->ambassador_fullname }} | hellomoto Perú">
<meta itemprop="description" content="{{ !isset($ambassador->bio)?'':strip_tags($ambassador->bio) }}">
<meta itemprop="image" content="{{ !isset($ambassador->profile_img)?'':url($ambassador->profile_img) }}">