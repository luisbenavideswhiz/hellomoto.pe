{{--HTML Meta Tags--}}
<title>{{ !isset($seo->title)?:$seo->title }}</title>
<meta name="title" content="{{ !isset($seo->title)?:$seo->title }}">
<meta name="description" content="{{ !isset($seo->description)?:$seo->description }}">
<meta name="keywords" content="{{ !isset($seo->keywords)?:$seo->keywords }}">
<meta name="author" content="WHIZ - Agencia Digital">


{{--Open graph data--}}
<meta property="og:title" content="{{ !isset($seo->title)?:$seo->title }}"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="{{ !isset($seo->url)?:$seo->url }}"/>
<meta property="og:image" content="{{ !isset($seo->image)?:$seo->image }}"/>
<meta property="og:description" content="{{ !isset($seo->description)?:$seo->description }}"/>
<meta property="og:site_name" content="hellomoto Perú"/>


{{--Twitter Card data--}}
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@MotorolaPE">
<meta name="twitter:title" content="{{ !isset($seo->title)?:$seo->title }}">
<meta name="twitter:description" content="{{ !isset($seo->description)?:$seo->description }}">
<meta name="twitter:creator" content="@MotorolaPE">
<meta name="twitter:image:alt" content="{{ !isset($seo->alt)?:$seo->alt }}">
<meta name="twitter:image" content="{{ !isset($seo->image)?:$seo->image }}">