<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="@MotorolaPE"/>
<meta name="twitter:creator" content="@MotorolaPE"/>

<meta property="og:site_name" content="Hello Moto" />
<meta property="og:url" content="{{ url('/') }}"/>
<meta property="og:locale" content="es_ES"/>
<meta property="og:type" content="article"/>
<meta property="article:publisher" content="https://www.facebook.com/MotorolaPE/"/>
<meta property="og:title" content="Descubre la comunidad hellomoto"/>
<meta property="og:description" content="&iexcl;&Uacute;nete a una comunidad diferente donde podr&aacute;s encontrar un lado diferente de tu ciudad!"/>
<meta property="og:image" content="{{ url('/img/200x200.jpg') }}"/>