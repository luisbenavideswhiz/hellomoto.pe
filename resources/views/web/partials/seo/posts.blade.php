<meta property="og:title" content="{{ !isset($post->current->meta_title)?'':$post->current->meta_title }} | hellomoto Perú"/>
<meta property="og:description" content="{{ !isset($post->current->meta_description)?'':$post->current->meta_description }}"/>
<meta property="og:image" content="{{ !isset($post->current->banner_img)?'':url($post->current->banner_img) }}"/>
<meta property="og:site_name" content="Hello Moto" />
<meta property="og:url" content="{{ !isset($post->current->url_post)?'':url($post->current->url_post) }}"/>
<meta property="og:locale" content="es_ES" />
<meta property="og:type" content="article"/>
<meta property="article:publisher" content="https://www.facebook.com/MotorolaPE/" />

<meta name="twitter:title" content="{{ !isset($post->current->meta_title)?'':$post->current->meta_title }} | hellomoto Perú">
<meta name="twitter:description" content="{{ !isset($post->current->meta_description)?'':$post->current->meta_description }}">
<meta name="twitter:image" content="{{ !isset($post->current->banner_img)?'':url($post->current->banner_img) }}">
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="@MotorolaPE"/>
<meta name="twitter:creator" content="@MotorolaPE"/>

<meta itemprop="name" content="{{ !isset($post->current->meta_title)?'':$post->current->meta_title }} | hellomoto Perú">
<meta itemprop="description" content="{{ !isset($post->current->meta_description)?'':$post->current->meta_description }}">
<meta itemprop="image" content="{{ !isset($post->current->banner_img)?'':url($post->current->banner_img) }}">

<meta property="og:title” content=“{{ !isset($post->current->meta_title)?'':$post->current->meta_title }} | hellomoto Perú"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="{{ !isset($post->current->url_post)?'':url($post->current->url_post) }}"/>
<meta property="og:image" content="{{ !isset($post->current->banner_img)?'':url($post->current->banner_img) }}"/>
<meta property="og:description" content="{{ !isset($post->current->meta_description)?'':$post->current->meta_description }}"/>
<meta property=“og:site_name" content="hellomoto Perú"/>