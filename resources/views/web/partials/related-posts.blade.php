<div class="row">
    <div class="relacionados">
        <h4>
            <span>&Uacute;ltimos Posts</span>
        </h4>
    </div>
    <div class="container item-wrap">
        <div class="row">
            @foreach($related as $key => $value)
                <div id="post-id-{{ $value->id }}" class="col-xs-12 col-md-4">
                    <div class="item {{ $value->type_class }}">
                        <div class="hello">
                            <div class="hello_data">
                                {!! $value->category_html !!}
                            </div>
                            <div class="img">
                                <a href="{{ $value->url_post }}">
                                    <img src="{{ url($value->thumb_img) }}" alt="">
                                </a>

                                <div class="date">
                                    <span>{{ $value->created_at_readable }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="data">
                            @if($value->category_slug != 'general')
                                <strong>{{ $value->category_name }}</strong>
                            @endif
                            <a href="{{ $value->url_post }}"><span>{{ $value->title }}</span></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>