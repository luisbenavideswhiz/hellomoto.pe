<div class="cont-pg">
    <div class="post-pagination">
        <div class="prev-post">
            <a href="{{ $prev->url_post }}">
                <span class="arrow">
                    <i class="fa fa-angle-left"></i>
                </span>
                <span class="pagination-text">
                    <span>Anterior Post</span>
                    <h4>{{ $prev->title }}</h4>
                </span>
            </a>
        </div>
        <div class="next-post">
            <a href="{{ $next->url_post }}">
                <span class="pagination-text">
                    <span>Siguiente Post</span>
                    <h4>{{ $next->title }}</h4>
                </span>
                <span class="arrow">
                    <i class="fa fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
</div>