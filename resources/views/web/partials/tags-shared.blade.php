<div class="tags-shared">
    <div class="post-tags">
        <span>
            <i class="fa fa-tag"></i> Tags:
        </span>
        {!! $tags !!}
    </div>
    <div class="post-shared">
        {{--<a href="#!">--}}
            {{--<i class="fa fa-heart-o"></i>--}}
        {{--</a>--}}
        <a href="#disqus_thread">
            <i class="fa fa-comment"></i>
        </a>
        <a href="https://www.facebook.com/sharer/sharer.php?u={{ url($url) }}"
           class="share"
           target="_blank">
            <i class="fa fa-facebook"></i>
        </a>
        <a href="https://twitter.com/intent/tweet?url={{ url($url) }}"
           class="share"
           data-network="twitter"
           data-url="{{ url($url) }}">
            <i class="fa fa-twitter"></i>
        </a>
        <a href="https://plus.google.com/share?url={{ url($url) }}"
           class="share"
           data-network="google"
           data-url="{{ url($url) }}"
           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
            <i class="fa fa-google-plus"></i>
        </a>
        <a href="https://pinterest.com/pin/create/button/?url={{ url($url) }}" target="_blank"
           data-network="pinterest"
           data-url="{{ url($url) }}">
            <i class="fa fa-pinterest"></i>
        </a>
    </div>
</div>