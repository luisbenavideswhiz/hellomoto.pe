<div class="row">
    <div class="col-xs-12">

        @if($device === 'responsive')
            <div class="cmc">
                <a href="#!" id="get-more-rows" data-url="{{ $url }}" data-page="2">Cargar m&aacute;s contenido</a>
            </div>
        @else
            <input type="hidden" id="get-more-rows" data-url="{{ $url }}" data-page="2">
        @endif

    </div>
</div>