@extends('web.layouts.web')

@section('og_tags')
    @component('web.partials.title')
        {{ $post->current->title }}
    @endcomponent

    @component('web.partials.seo.posts', [
        'post' => $post
    ])
    @endcomponent
@endsection

@section('content')
    <div class="{{ $post->current->parent_category->slug }}">
        <div class="container-fluid">
            <div class="row">
               {{-- <div class="dbanner">
                    <div class="img bk-img">
                        <img src="{{ url($post->current->banner_img) }}" alt="{{ $post->current->title }}">
                    </div>
                    <div class="data-single">
					<span class="single">
                        <a href="{{ url($post->current->parent_category->slug) }}"> {!! $post->current->category_html !!} </a>
                        @if($post->current->category_slug!='general')
                            <span class="subcat"><a href="{{ url($post->current->parent_category->slug.'/'.$post->current->category_slug) }}">{{ $post->current->category_name }}</a></span>
                        @endif
                    </span>

                        <h1>{{ $post->current->title }}</h1>
                    </div>
                </div>--}}

                <main>
                    <article>
                        <header>
                            <div class="data-single">
                                <span class="single">
                                    <a href="{{ url($post->current->parent_category->slug) }}"> {!! $post->current->category_html !!} </a>
                                    @if($post->current->category_slug!='general')
                                        <span class="subcat"><a href="{{ url($post->current->parent_category->slug.'/'.$post->current->category_slug) }}">{{ $post->current->category_name }}</a></span>
                                    @endif
                                </span>
                                <h1>{{ $post->current->title }}</h1>
                            </div>
                            <div class="entry-meta">
                                <time class="entry-date published" datetime="{{ $post->current->created_at }}">{{ $post->current->created_at_readable }}</time>
                            </div>
                            <div class="author">
                                Escrito por
                                <span> {{ $post->current->ambassador_fullname }}</span>
                            </div>
                        </header>
                        <div class="entry-content">
                            <p>
                                {!! $post->current->description !!}
                            </p>
                        </div>
                    </article>
                </main>

                @if(count($post->current->webPostsHasWebGalleries) != 0)
                    @component('web.partials.gallery',
                    [
                        'gallery' => $post->current->webPostsHasWebGalleries
                    ])
                    @endcomponent
                @endif

                <main>
                    <article>
                        <div class="entry-content">
                            <p>
                                {!! $post->current->after_description !!}
                            </p>
                        </div>
                    </article>
                </main>

                @component('web.partials.tags-shared',
                [
                    'tags' => $post->current->tags_html,
                    'url' => $post->current->url_post
                ])
                @endcomponent

                @component('web.partials.previous-next',
                [
                    'prev' => $post->prev,
                    'next' => $post->next
                ])
                @endcomponent

            </div>

            @component('web.partials.related-posts',
            [
                'related' => $related
            ])
            @endcomponent

            {{--@component('web.partials.disqus',--}}
            {{--[--}}
                {{--'url' => $post->current->url_post--}}
            {{--])--}}
            {{--@endcomponent--}}

        </div>
    </div>
@endsection

@section('social_sdk')

    @component('web.partials.sdk.fb')
    @endcomponent

    @component('web.partials.sdk.tw')
    @endcomponent

@endsection
