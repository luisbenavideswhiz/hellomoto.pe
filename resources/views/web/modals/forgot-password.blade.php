<div class="modal fade" id="iforgotModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Restaura tu contrase&ntilde;a</h4>
            </div>
            <div class="modal-body">
                <form id="recovery-form" data-url="/usuario/recuperar-password">
                    <div class="iforgot">
                        <span>Ingresa tu correo electr&oacute;nico y te enviaremos un c&oacute;digo para restaurar tu contrase&ntilde;a.</span>

                        <div class="item">
                            <div class="input-field">
                                <span>Email</span>
                                <input type="text" id="recovery_email" name="email" class="popup-validate" data-placement="top">
                            </div>
                        </div>
                        <div class="item">
                            <a class="boton" href="#" id="recovery-btn">
                                Restablecer contrase&ntilde;a
                            </a>
                        </div>
                        <div class="item">
                            <span>Recuerda revisar tu bandeja de spam.</span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>