<div class="modal fade" id="ivalModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4>Validar Código</h4>
            </div>
            <div class="modal-body">
                <div class="iforgot">
                    <span>Ingresa el código enviado a correo@correo.com</span>
                    <div class="item">
                        <div class="input-field">
                            <span>Código</span>
                            <input type="text">
                        </div>
                    </div>
                    <div class="item">
                        <a href="#!">Enviar de código de nuevo</a>
                    </div>
                    <div class="item">
                         <a class="boton" href="#" data-toggle="modal" data-target="#newpassModal">Validar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>