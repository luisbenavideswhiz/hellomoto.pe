<div class="modal fade" id="loginModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="login">
                    <div class="tabs-wrapper">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="tab active" role="presentation">
                                <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">
                                    <span>Tengo una cuenta</span>
                                </a>
                            </li>
                            <li class="tab" role="presentation">
                                <a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">
                                    <span>Registrate</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            @include('web.modals.login.partials.login')
                            @include('web.modals.login.partials.register')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>