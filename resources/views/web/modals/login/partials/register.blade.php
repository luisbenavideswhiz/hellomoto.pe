<div class="tab-pane" id="tab2" role="tabpanel">
    <div class="tabs-content">
        <div class="logo">
            <img src="{{ asset('img/web/login/logo.svg') }}" alt="">
        </div>
        <form id="register-form" data-url="/usuario/registro">
            <div class="col2">
                <div class="item">
                    <span>Nombre (*)</span>

                    <div class="input-field">
                        <input type="text" id="r_name" name="name" class="popover-validate"
                               value="{{ session('firstname') }}">
                    </div>
                </div>
                <div class="item">
                    <span>Apellido (*)</span>

                    <div class="input-field">
                        <input type="text" id="r_lastname" name="lastname" class="popover-validate"
                               value="{{ session('lastname') }}">
                    </div>
                </div>
            </div>
            <div class="col2">
                <div class="item">
                    <span>Tipo de documento (*)</span>

                    <div class="input-field ssolo">
                        <select id="r_document_type" name="document_type" class="popover-validate">
                            <option value="dni">DNI</option>
                            <option value="pasaporte">Pasaporte</option>
                            <option value="extranjeria">Carnet de Extranjer&iacute;a</option>
                        </select>
                    </div>
                </div>
                <div class="item">
                    <span>N&deg; de documento (*)</span>

                    <div class="input-field">
                        <input type="text" id="r_document_number" name="document_number" class="popover-validate"
                               maxlength="8">
                    </div>
                </div>
            </div>
            <div class="col1">
                <div class="item">
                    <span id="r_sex" class="popover-validate">Sexo</span>

                    <div class="input-field">
                        <label>
                            <input type="radio" id="r_sex_hombre" name="sex" value="hombre"
                                   @if(session('gender') == 'male') checked="checked" @endif
                                   class="popover-validate"> Hombre
                        </label>
                        <label>
                            <input type="radio" id="r_sex_mujer" name="sex" value="mujer"
                                   @if(session('gender') == 'female') checked="checked" @endif
                                   class="popover-validate"> Mujer
                        </label>
                    </div>
                </div>
            </div>
            <div class="col1">
                <div class="item">
                    <span>Email</span>

                    <div class="input-field">
                        <input type="text" id="r_email" name="email" class="popover-validate"
                               value="{{ session('email') }}">
                    </div>
                </div>
                <div class="item ver">
                    <span id="r_accept_emails" class="popover-validate">Aceptar correos electr&oacute;nicos:</span>
                    <label>
                        <input type="radio" id="r_accept_emails_si" name="accept_emails" value="1"
                               class="popover-validate"> Si
                    </label>
                    <label>
                        <input type="radio" id="r_accept_emails_no" name="accept_emails" value="0"
                               class="popover-validate"> No
                    </label>
                    <span class="alert" data-toggle="tooltip"
                          title="Deseo recibir informaci&oacute;n de Motorola por email acerca de productos y ofertas especiales.">
                        <i class="fa fa-exclamation-triangle"></i>
                    </span>
                </div>
            </div>
            <div class="col1">
                <div class="item">
                    <span id="r_birthdate" class="popover-validate">Fecha de Nacimiento</span>
                    <div class="input-field date">
                        <input type="text" name="birthdate" class="form-control" id="birthdate">
                    </div>
                </div>
            </div>
            <div class="col2">
                <div class="item">
                    <span>N&deg; de celular (*)</span>
                    <div class="input-field">
                        <input type="text" id="r_phone" name="phone" class="popover-validate" maxlength="15">
                    </div>
                </div>
                <div class="item">
                    <span>Operador (*)</span>

                    <div class="input-field ssolo">
                        <select id="r_mobile_operator" name="mobile_operator" class="popover-validate">
                            <option value="claro">Claro</option>
                            <option value="movistar">Movistar</option>
                            <option value="entel">Entel</option>
                            <option value="bitel">Bitel</option>
                            <option value="virgin">Virgin Mobile</option>
                            <option value="tuenti">Tuenti</option>
                            <option value="cuy">Cuy M&oacute;vil</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col2">
                <div class="item">
                    <label>Marca de Celular (*)</label>
                    <div class="input-field">
                        <input type="text" class="popover-validate" name="brand_phone" id="brand_phone">
                    </div>
                </div>
                <div class="item">
                    <label>Modelo de Celular (*)</label>
                    <div class="input-field">
                        <input type="text" class="popover-validate" name="model_phone" id="model_phone">
                    </div>
                </div>
            </div>
            <div class="col2">
                <div class="item">
                    <span>Contrase&ntilde;a (*)</span>
                    <div class="input-field">
                        <input type="password" id="r_password" name="password" class="popover-validate">
                    </div>
                </div>
                <div class="item">
                    <span>Confirmar Contrase&ntilde;a (*)</span>
                    <div class="input-field">
                        <input type="password" id="r_password_confirmation" name="password_confirmation"
                               class="popover-validate">
                    </div>
                </div>
            </div>
            <div class="item">
                <a id="register-btn" class="boton" href="#">Registrarme</a>
            </div>
        </form>
        <div class="redes">
            <div class="sec">
                <span>o registrate con</span>
            </div>
            <div class="item">
                <a href="/facebook?action_type=register">
                    <i class="fa fa-facebook"></i>
                </a>
                <a href="/google?action_type=register">
                    <i class="fa fa-google-plus"></i>
                </a>
            </div>
            <p>
                Al continuar, aceptas los
                <a href="#" target="_blank">t&eacute;rminos, condiciones</a> y la
                <a href="#" target="_blank">Pol&iacute;tica de privacidad</a> de Motorola.
            </p>
        </div>
    </div>
</div>