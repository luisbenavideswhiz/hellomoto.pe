<div class="tab-pane active" id="tab1" role="tabpanel">
    <div class="tabs-content">
        <div class="logo">
            <img src="{{ asset('img/web/login/logo.svg') }}" alt="">
        </div>
        <form id="login-form" data-url="/usuario/login">
            <div class="item">
                <span>Usuario</span>

                <div class="input-field">
                    <input type="text" id="l_username" name="email" class="popover-validate">
                </div>
            </div>
            <div class="item">
                <span>Contrase&ntilde;a</span>

                <div class="input-field">
                    <input type="password" id="l_password" name="password" class="popover-validate">
                </div>
            </div>
            <div class="item">
                <a id="login-btn" class="boton" href="#">Ingresar</a>
            </div>
            <div class="item">
                <div class="actions">
                    <div class="input-field">
                        <label>
                            <input type="checkbox"> Recordar contrase&ntilde;a
                        </label>
                    </div>
                    <div class="input-field">
                        <a href="#" data-toggle="modal" data-target="#iforgotModal">
                            Olvid&eacute; mi contrase&ntilde;a
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <div class="redes">
            <div class="prim">
                <span>o</span>
            </div>
            <div class="item">
                <a href="/facebook?action_type=login">
                    <i class="fa fa-facebook"></i>
                </a>
                <a href="/google?action_type=login">
                    <i class="fa fa-google-plus"></i>
                </a>
            </div>
        </div>
    </div>
</div>