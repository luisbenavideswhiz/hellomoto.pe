<div class="modal fade" id="newpassModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4>Nueva contraseña</h4>
            </div>
            <div class="modal-body">
                <form id="change-pass-form" data-url="/usuario/cambiar-password">
                    <div class="iforgot">
                        <p id="confirm-pass" class="text-info" style="width: 80%;margin: auto;text-align: center;padding: 15px;"></p>

                        <div class="item mb">
                            <div class="input-field">
                                <span>Nueva contraseña</span>
                                <input type="password" id="recovery_password" name="password" data-placement="top">
                            </div>
                        </div>
                        <div class="item mb">
                            <div class="input-field">
                                <span>Confirma contraseña</span>
                                <input type="password" id="recovery_password_confirmation" name="password_confirmation"
                                       data-placement="top">
                            </div>
                        </div>
                        <div class="item mb">
                            <div class="input-field">
                                <span>Código secreto</span>
                                <input type="text" id="code" name="code" data-placement="top">
                            </div>
                        </div>
                        <div class="item">
                            <a class="boton" href="#" id="change-pass-btn">Restablecer contraseña</a>
                        </div>
                        <div class="item">
                            <span>Te hemos enviado un email con tu código secreto, Recuerda revisar tu bandeja de spam.</span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>