<div class="modal fade" id="seguirModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body">
                <div class="sc">
                    <div class="logo">
                        <img src="{{ asset('img/web/login/logo.svg') }}" alt="">
                    </div>
                    <h4 id="title">¡Felicitaciones!</h4>
                    <p id="text">Obtuviste 700 ptos por seguir el playlist</p>
                </div>
            </div>
        </div>
    </div>
</div>