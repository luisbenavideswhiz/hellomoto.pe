<!DOCTYPE html>
<html lang="es">
<head>
    @component('web.partials.seo.index',
            [
                'seo'=> $seo
            ])
    @endcomponent

    @include('web.partials.cannonical')
    @include('web.partials.assets.metas', ['post'=> isset($post)?$post:null])
    @include('web.partials.assets.styles')
    @include('web.partials.seo.analytics')
</head>
<body id="home">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5JC6WBV"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


@include('web.partials.header.desktop',
[
    'menu' => $menu
])


@include('web.partials.header.responsive',
[
    'menu' => $menu
])


<div class="ciudades">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <h1>
                            Resultado de búsqueda
                        </h1>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="orderby">
                            <span>Ordenar por:</span>

                            <div class="input-field">
                                <select>
                                    <option value="">Seleccionar</option>
                                    <option value="1">Opción 1</option>
                                    <option value="2">Opción 2</option>
                                    <option value="3">Opción 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container item-wrap">
                <!-- poner filtro -->
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <div class="item type1">
                            <div class="hello">
                                <div class="hello_data">
                                    <span>hello<span>ciudades</span></span>
                                </div>
                                <div class="img">
                                    <img src="http://via.placeholder.com/360x280" alt="">

                                    <div class="date">
                                        <span>11 de setiembre del 2017</span>
                                    </div>
                                </div>
                            </div>
                            <div class="data">
                                <strong>Lima</strong>
                                <span>HelloTalks eo Moto...</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="item type4">
                            <div class="hello">
                                <div class="hello_data">
                                    <span>hello<span>embajadores</span></span>
                                </div>
                                <div class="img">
                                    <img src="http://via.placeholder.com/360x280" alt="">

                                    <div class="date">
                                        <span>11 de setiembre del 2017</span>
                                    </div>
                                </div>
                            </div>
                            <div class="data">
                                <strong>Lima</strong>
                                <span>HelloTalks eo Moto...</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="cmc">
                            <a href="#!">Cargar más contenido</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@component('web.partials.instagram',
[
    'instagram' => $instagram
])
@endcomponent

@include('web.partials.footer')
@include('web.modals.login.index')
@include('web.modals.forgot-password')
@include('web.modals.new-pass')
@include('web.modals.rewards')
@include('web.partials.assets.scripts')

@yield('social_sdk')
</body>
</html>