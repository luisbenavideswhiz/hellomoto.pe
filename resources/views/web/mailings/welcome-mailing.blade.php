<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:o="urn:schemas-microsoft-com:office:office"
	xmlns:v="urn:schemas-microsoft-com:vml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<meta name="format-detection" content="telephone=no"/>
		<!--[if !mso]>
		<!-->
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<!--
		<![endif]-->
		<!--[if gte mso 9]>
		<xml>
			<o:OfficeDocumentSettings>
				<o:AllowPNG/>
				<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
		<title></title>
		<style type="text/css">div, p, a, li, td {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
        body {min-width:100% !important; Margin:0; padding:0;}
		.hidesumary {display: none;}
        #outlook a {padding:0;}
        .ReadMsgBody {width:100%;}
        .ExternalClass {width:100%;}
        .bgtd{background: #ffffff;}
        .ExternalClass * {line-height:110%;}
        img {display:block; line-height:100%; border:0; outline:none; text-decoration:none; -ms-interpolation-mode:bicubic;}
        table {border-collapse:collapse !important; border-spacing:0; mso-table-lspace:0pt; mso-table-rspace:0pt;}
        table td {border-collapse:collapse;}
        @media only screen and (min-device-width:320px) and (max-device-width:1024px) {
            a[href^="tel"], a[href^="sms"], a {color:inherit !important; cursor:default !important; text-decoration:none !important;}
        }
        @media only screen and (max-width:480px) {
            img[class="img100pc"] {width:100%; height:auto;}
			img[class="img70pc"] {width:70%; height:auto;}
            img[class="pt30"] {padding-top:30px !important;}
            img[class="pt10"] {padding-top:10px !important;}
            table[class="hide"], td[class="hide"], span[class="hide"], br[class="hide"] {display:none !important;}
            table[class="w100pc"] {width:100%; margin-bottom: 10px;}
			table[class="w90pc"] {width:90%; margin-bottom: 10px;}
            .margin_top{margin-top: 20px!important;}
            td[class="w10"] {width:10px !important;}
            td[class="h30"] {height:30px !important;}
            td[class="h20"] {height:20px !important;}
            td[class="h15"] {height:15px !important;}
            td[class="h10"] {height:10px !important;}
            td[class="txtcenter"] {text-align:center !important;}
            span[class="block"] {display:block !important;}
            .bgtd{background: none;}
			.nave span {display: block;}
			.nave span a {border-bottom: 1px solid #333; display: block; padding: 8px 0;}
			.nave .ocultar {display: none;}
			.hidesumary {display:block;}
        }
        div[style*="margin: 16px 0"] {margin:0 !important; font-size:100% !important;}
	</style>
		<!--[if (gte mso 9)|(IE)]>
		<style type="text/css" media="all">
        	td[class="outlook-f"] {width:244px !important;}
            td[class="outlook-c"] {width:620px !important;}
            @media only screen and (max-width:480px) {
            	td[class="outlook-f"] {width:100% !important}
            }
    	</style>
		<![endif]-->
	</head>
	<body bgcolor="#ffffff" marginleft="0" marginright="0" margintop="0" style="width:100% !important; Margin:0; padding:0;">
		<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody>
				<tr>
					<!-- Emailer Starts Here-->
					<td align="center">
						<table align="center" border="0" cellpadding="0" cellspacing="0" class="w100pc" width="620">
							<tbody>                                
                                <tr>
                                	<td>
                                    	<table align="left" border="0" cellpadding="00" cellspacing="0" width="100%" class="w100pc">
                                        	<tbody>
                                            	<tr>
                                                    <td align="center" valign="top" style="line-height:0px;">
                                                    	<img src="{{ url('img/web/mailing/bkg_bienvenida.jpg') }}" alt="" width="100%" style="width:100%; display:block;" border="0" class="img100pc" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>                                
                                <tr>
                                	<td height="20"></td>                                
                                </tr>
                                
                                <tr>
                                	<td>
                                    	<table align="left" border="0" cellpadding="00" cellspacing="0" width="100%" class="w100pc">
                                        	<tbody>
                                            	<tr>
                                                	<td>
                                                    	<div style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 18px; font-weight: bold; line-height: 22px; text-align:center;">
                                                        	Bienvenido a Hellomoto Perú
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                	<td height="20"></td>                                
                                </tr>
                                
                                <tr>
                                	<td>
                                    	<table align="center" border="0" cellpadding="00" cellspacing="0" width="90%">
                                        	<tbody>
                                            	<tr>
                                                	<td>
                                                    	<div style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 17px; text-align:left;">
                                                        	<p>
																{{ $user->name }}, Te damos la bienvenida a la comunidad Hellomoto, donde podrás vivir un mundo de beneficios exclusivos y experiencias únicas en un sólo lugar.
                                                        	</p>
                                                        	<p>
                                                        		Aquí podrás compartir, disfrutar y sonreír. <!-- Haz <a href="#!" style="color: #3b5999" target="_blank">clic aquí</a> para confirmar tu cuenta y disfrutar de nuestros beneficios. -->
                                                        	</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                	<td height="20"></td>                                
                                </tr>
                                
                                <tr>
                                	<td>
                                    	<table align="center" border="0" cellpadding="00" cellspacing="0" width="100">
                                        	<tbody>
                                            	<tr>
                                                	<td align="center" valign="top" style="line-height: 0px;">
                                                    	<a href="https://www.facebook.com/MotorolaPE" target="_blank" style="line-height: 0px; text-align: center;"><img src="{{ url('img/web/mailing/ico_fb.jpg') }}" alt="" style="display: block; margin: 0 auto 0 auto;"></a>
                                                    </td>
                                                    <td width="10"></td>
                                                    <td align="center" valign="top" style="line-height: 0px;">
                                                    	<a href="http://hellomoto.pe" target="_blank" style="line-height: 0px; text-align: center;"><img src="{{ url('img/web/mailing/ico_link.jpg') }}" alt="" style="display: block; margin: 0 auto 0 auto;"></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                	<td height="20"></td>                                
                                </tr>
                                
                                <tr>
                                	<td>
                                    	<table align="left" border="0" cellpadding="00" cellspacing="0" width="100%" class="w100pc">
                                        	<tbody>
                                            	<tr>
                                                	<td>
                                                    	<div style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 17px; text-align:center;">
                                                        	© 2017 Motorola Mobility LLC. Todos los derechos reservados.
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                	<td height="20"></td>                                
                                </tr>
							</tbody>
						</table>
					</td>
                    <!-- Emailer Ends Here //-->
				</tr>				
			</tbody>
		</table>
	</body>
</html>
