$(window).on('load', function () {
    app.init();
});

var popover = {
    enable: function () {
        var validate = $('.validate');
        if (validate.length) {
            validate.each(function (index, value) {
                $(value).attr('data-toggle', 'popover')
                    .attr('data-html', 'true')
                    .attr('data-content', 'Campo requerido')
                    .attr('data-container', 'body')
                    .attr('data-trigger', 'focus')
                    .attr('data-placement', 'top');
            });
        }
    },
    remove: function () {
        $('input, select').on('focus', function () {
            $('#' + $(this).attr('id')).popover('destroy').removeClass('form-error');
            $('#' + $(this).attr('id')).removeAttr('style');
            $('.day').popover('destroy').removeClass('form-error');
            $('.day').removeAttr('style');
            $('.month').removeAttr('style');
            $('.year').removeAttr('style');
        });
    },
    destroy: function () {
        $('input, select').each(function () {
            $('#' + $(this).attr('id')).popover('destroy').removeClass('form-error');
            $('#' + $(this).attr('id')).removeAttr('style');
        });
        $('#birthdate').css('display','none');
        popover.enable();
    },
    init: function () {
        popover.enable();
        popover.remove();
    }
};

var app = {
    birthdate: function () {
        $('#birthdate').combodate({
            firstItem: 'name',
            minYear: 1900,
            maxYear: 2008
        });
        $('.day').css('width', '32%');
        $('.month').css('width', '33%');
        $('.year').css('width', '32%');
    },
    formSubmit: function () {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            popover.destroy();
        });
        $('#landing').submit(function (event) {
            event.preventDefault();
            var _this = $(this);
            var form = $(this).serialize();
            var url = $(this).attr('action');
            $.ajax({
                url: url,
                data: form,
                method: 'POST',
                success: function (response) {
                    if (response.status) {
                        gtag('event', 'sorteo', {'event_category': 'body', 'event_action': 'click_button', 'event_label': 'register'});
                        window.location.href = response.data.redirect;
                    } else {
                        for (key in response.data.errors) {
                            if ($('#' + key).length) {
                                $("html, body").stop().animate({scrollTop: 0}, 280, 'swing');
                            }
                            switch (key){
                                case 'terms_conditions':
                                    $('[name=' + key + ']').parent().find('a').css('color', 'red');
                                    break;
                                case  'birthdate':
                                    $('.day').css('border-color', 'red');
                                    $('.month').css('border-color', 'red');
                                    $('.year').css('border-color', 'red');
                                    $('.day').addClass('form-error')
                                        .attr('data-placement', 'top')
                                        .attr('data-content', response.data.errors[key]);
                                    break;
                                default:
                                    $('#' + key).css('border-color', 'red');
                                    $('[name=' + key + ']').addClass('form-error')
                                        .attr('data-content', response.data.errors[key]);
                                    break;
                            }
                        }
                        _this.find('.form-error').first().popover('show');
                    }
                }
            });
        });

        $('#login').submit(function (event) {
            event.preventDefault();
            var _this = $(this);
            var form = $(this).serialize();
            var url = $(this).attr('action');
            $.ajax({
                url: url,
                data: form,
                method: 'POST',
                success: function (response) {
                    if (response.status) {
                        window.location.href = response.redirect;
                    } else {
                        for (key in response.errors) {
                            if ($('#' + key).length) {
                                $("html, body").stop().animate({scrollTop: 0}, 280, 'swing');
                            }
                            $('#' + key+'_l').css('border-color', 'red');
                            $('[name=' + key + ']').addClass('form-error').attr('data-content', response.errors[key]);

                        }
                        _this.find('.form-error').first().popover('show');
                    }
                }
            });
        });
        
        $('.addtogoogle>img').click(function () {
            var data = {
                location: $(this).parent().data('location'),
                date: $(this).parent().data('date')
            };
            var win = window.open('https://www.google.com/calendar/render?action=TEMPLATE&text=Motovan en '+
                data.location+'&dates='+data.date+'&sf=true&output=xml', '_blank');
            win.focus();
        });
    },
    trackGA: function () {
        $('#motorola').click(function () {
            gtag('event', 'sorteo', {'event_category': 'header', 'event_action': 'click_button', 'event_label': 'motorola'});
            var url = $(this).data('href');
            window.open(url, $(this).data('target'));
        });
        $('.fb').click(function () {
            gtag('event', 'sorteo', {'event_category': 'header', 'event_action': 'click_button', 'event_label': 'facebook'});
            var url = $(this).data('href');
            window.open(url, $(this).data('target'));
        });
        $('.tw').click(function () {
            gtag('event', 'sorteo', {'event_category': 'header', 'event_action': 'click_button', 'event_label': 'twitter'});
            var url = $(this).data('href');
            window.open(url, $(this).data('target'));
        });
        $('.inst').click(function () {
            gtag('event', 'sorteo', {'event_category': 'header', 'event_action': 'click_button', 'event_label': 'instagram'});
            var url = $(this).data('href');
            window.open(url, $(this).data('target'));
        });
        $('.btn-login').click(function () {
            gtag('event', 'sorteo', {'event_category': 'body', 'event_action': 'click_button', 'event_label': 'login'});
            var url = $(this).data('href');
            window.open(url, $(this).data('target'));
        });
        $('.whiz').click(function () {
            gtag('event', 'sorteo', {'event_category': 'footer', 'event_action': 'click_button', 'event_label': 'whiz'});
            var url = $(this).data('href');
            window.open(url, $(this).data('target'));
        });
    },
    trainingsMotovan: function () {
        $('.btn-register').click(function () {
           $.post('/motovan/inscribete/'+$(this).data('training'), {_token: $('meta[name="csrf-token"]').attr('content') }, function (response) {
                if(typeof response.redirect !== 'undefined'){
                    window.location.href = response.redirect;
                }
           });
        });
    },
    init: function () {
        popover.init();
        this.birthdate();
        this.formSubmit();
        this.trackGA();
        this.trainingsMotovan();
    }
};

$(window).load(function() {
    $('.loading').addClass("loaded");
    $('body').addClass("loaded");

    $('#emb_slider').slick({
        autoplay: true,
        arrows: false,
        dots: true,
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    $(function () {
        $('.show-info').tooltip()
    });
});