(function (e) {
    e.fn.countdown = function (t, n) {
        function i() {
            eventDate = Date.parse(r.date) / 1e3;
            currentDate = Math.floor(e.now() / 1e3);
            if (eventDate <= currentDate) {
                n.call(this);
                clearInterval(interval)
            }
            seconds = eventDate - currentDate;
            days = Math.floor(seconds / 86400);
            seconds -= days * 60 * 60 * 24;
            hours = Math.floor(seconds / 3600);
            seconds -= hours * 60 * 60;
            minutes = Math.floor(seconds / 60);
            seconds -= minutes * 60;
            days == 1 ? thisEl.find(".timeRefDays").text("Dias") : thisEl.find(".timeRefDays").text("Dias");
            hours == 1 ? thisEl.find(".timeRefHours").text("Horas") : thisEl.find(".timeRefHours").text("Horas");
            minutes == 1 ? thisEl.find(".timeRefMinutes").text("Mins") : thisEl.find(".timeRefMinutes").text("Mins");
            seconds == 1 ? thisEl.find(".timeRefSeconds").text("Segs") : thisEl.find(".timeRefSeconds").text("Segs");
            if (r["format"] == "on") {
                days = String(days).length >= 2 ? days : "0" + days;
                hours = String(hours).length >= 2 ? hours : "0" + hours;
                minutes = String(minutes).length >= 2 ? minutes : "0" + minutes;
                seconds = String(seconds).length >= 2 ? seconds : "0" + seconds
            }
            if (!isNaN(eventDate)) {
                thisEl.find(".days").text(days);
                thisEl.find(".hours").text(hours);
                thisEl.find(".minutes").text(minutes);
                thisEl.find(".seconds").text(seconds)
            } else {
                console.log("Invalid date. Example: 30 Tuesday 2013 15:50:00");
                clearInterval(interval)
            }
        }
        var thisEl = e(this);
        var r = {
            date: null,
            format: null
        };
        t && e.extend(r, t);
        i();
        interval = setInterval(i, 1e3)
    }
})(jQuery);

function setImageOne(image) {
    $(image).fadeIn(500).html('<img src="/img/landings/intriga/moto_phone.png"/>').delay(2000).fadeOut(500, function () { setImageTwo(image); });
}

function setImageTwo(image) {
    $(image).fadeIn(500).html('<img src="/img/landings/intriga/moto_back.png"/>').delay(2000).fadeOut(500, function () { setImageOne(image); });
}

function submit() {
    $('.form').submit(function (event) {
        event.preventDefault();
        var form = $(this);
        $.ajax({
            url: form.attr('action'),
            data: form.serialize(),
            method: 'POST',
            beforeSend: function () {
                $('.error').html(' ');
            },
            success: function (response) {
                if (response.status) {
                    $('.content>p').hide();
                    $('.content>p.message').fadeIn();
                    form[0].reset();
                    form.hide();
                    window.setTimeout(function(){
                        window.location.href = "/";
                    }, 3500);
                } else {
                    for (key in response.errors) {
                        $('#'+key).html(response.errors[key]);
                    }
                }
            }
        });
    });
}

$(document).ready(function () {
    // setImageOne('.phoneimage');
    // setImageOne('.phoneimage_resp');
    submit();
    $(".countdown").countdown({
        date: "30 May 2018 13:00:00",
        format: "on"
    });
});