var app = {
    util: {
        scrollTop: function () {
            $("html, body").stop().animate({scrollTop: 0}, 280, 'swing');
        },
        btns: {
            enable: function (element) {
                element.removeAttr('disabled');
            },
            disable: function (element) {
                element.attr('disabled', 'disabled');
            }
        },
        post: function (url, data) {
            app.util.btns.disable($('#register-btn'));
            $.post(url, data, function (response) {
                if (response.success === false) {
                    $.each(response, function (item, value) {
                        $('[name=' + item + ']').addClass('form-error').attr('data-content', value);
                        $('.form-error').first().popover('show');
                        if ($('#' + item).length) {
                            app.util.scrollTop();
                        }
                        return true;
                    });
                } else {
                    if (typeof response.redirect === 'undefined') {
                        var content = $('.validacion');
                        content.removeClass('hidden').find('#user-name').html(response.name);
                        content.find('#user-email').html(response.email);
                        $('form')[0].reset();
                        app.util.scrollTop();
                    } else {
                        window.location.href = response.redirect;
                    }
                }
                app.util.btns.enable($('#register-btn'))
            }, 'json');
        }
    },
    home: {
        popover: {
            enable: function () {
                var validate = $('.validate');
                if (validate.length) {
                    validate.each(function (index, value) {
                        $(value).attr('data-toggle', 'popover')
                            .attr('data-html', 'true')
                            .attr('data-content', 'Campo requerido')
                            .attr('data-container', 'body')
                            .attr('data-trigger', 'focus')
                            .attr('data-placement', 'top');
                    });
                }
            },
            remove: function () {
                $('input, select').on('focus', function () {
                    $('#' + $(this).attr('id')).popover('destroy').removeClass('form-error');
                });
            },
            init: function () {
                app.home.popover.enable();
                app.home.popover.remove();
            }
        },
        btns: {
            register: function () {
                var register = $('#register-btn');
                if (register.length) {
                    register.on('click', function (e) {
                        e.preventDefault();
                        $('.validacion').addClass('hidden');
                        app.util.post($(this).data('url'), $('form').serializeArray());
                    });
                }
            },
            init: function () {
                app.home.btns.register();
            }
        },
        validate: {
            dni: function(){
                var document = $('#document');
                if (document.length) {
                    document.on('focusout', function (e) {
                        e.preventDefault();
                        $('.validacion').addClass('hidden');
                        var element = $(this);
                        var url = element.data('url');
                        var data = {
                            _token: $("[name=_token]").val(),
                            dni: document.val()
                        };
                        $.post(url, data, function (response) {
                            if (response.success === false) {
                                document
                                    .addClass('form-error')
                                    .attr('data-content', response.error.document)
                                    .popover('show');
                            }
                        }, 'json');
                    });
                }
            },
            email: function(){
                var email = $('#email');
                if (email.length) {
                    email.on('focusout', function (e) {
                        e.preventDefault();
                        $('.validacion').addClass('hidden');
                        var element = $(this);
                        var url = element.data('url');
                        var data = {
                            _token: $("[name=_token]").val(),
                            email: email.val()
                        };
                        $.post(url, data, function (response) {
                            if (response.success === false) {
                                email
                                    .addClass('form-error')
                                    .attr('data-content', response.error.email)
                                    .popover('show');
                            }
                        }, 'json');
                    });
                }
            },
            init: function(){
                app.home.validate.dni();
                app.home.validate.email();
            }
        },
        init: function () {
            app.home.popover.init();
            app.home.btns.init();
            app.home.validate.init();
        }
    }
};

$(window).on('load', function () {
    var id = $('body').attr('id');
    if (typeof id !== 'undefined') {
        app[id].init();
    }
});