$(window).on('load', function () {
    app.init();
});

var popover = {
    enable: function () {
        var validate = $('.validate');
        if (validate.length) {
            validate.each(function (index, value) {
                $(value).attr('data-toggle', 'popover')
                    .attr('data-html', 'true')
                    .attr('data-content', 'Campo requerido')
                    .attr('data-container', 'body')
                    .attr('data-trigger', 'focus')
                    .attr('data-placement', 'top');
            });
        }
    },
    remove: function () {
        $('input, select').on('focus', function () {
            $('#' + $(this).attr('id')).popover('destroy').removeClass('form-error');
        });
    },
    destroy: function () {
        $('input, select').each(function () {
            $('#' + $(this).attr('id')).popover('destroy').removeClass('form-error');
            $('#' + $(this).attr('id')).removeAttr('style');
        });
        $('#birthdate').css('display','none');
        popover.enable();
    },
    init: function () {
        popover.enable();
        popover.remove();
    }
};

var app = {
    birthdate: function () {
        $('#birthdate').combodate({
            firstItem: 'name',
            minYear: 1900,
            maxYear: 2008
        });
        $('.day').css('width', '32%');
        $('.month').css('width', '33%');
        $('.year').css('width', '32%');
    },
    formSubmit: function () {
        $('#myModal').on('hidden.bs.modal', function (e) {
            popover.destroy();
            $('.show-info').tooltip('hide');
        });
        $('#landing').submit(function () {
            event.preventDefault();
            var form = $(this).serialize();
            var url = $(this).attr('action');
            $.ajax({
                url: url,
                data: form,
                method: 'POST',
                success: function (response) {
                    gtag('event', 'sorteo', {'event_category': 'body', 'event_action': 'click_button', 'event_label': 'register'});
                    if (response.status) {
                        window.location.href = response.data.redirect;
                    } else {
                        for (key in response.data.errors) {
                            $('#' + key).css('border-color', 'red');
                            $('[name=' + key + ']').addClass('form-error')
                                .attr('data-content', response.data.errors[key])
                                .attr('data-placement', 'top');
                            $('.form-error').first().popover('show');
                            if ($('#' + key).length) {
                                $("html, body").stop().animate({scrollTop: 0}, 280, 'swing');
                            }
                            if (key == 'sex') {
                                $('#' + key + '-h').parent().find('span').css('color', 'red');
                                $('#' + key + '-m').parent().find('span').css('color', 'red');
                            }
                            if (key == 'terms_conditions') {
                                $('[name=' + key + ']').parent().find('a').css('color', 'red');
                            }
                            if (key == 'birthdate') {
                                $('.day').css('border-color', 'red');
                                $('.month').css('border-color', 'red');
                                $('.year').css('border-color', 'red');
                            }
                        }
                    }
                }
            });
        });
    },
    ubigeo: function () {
        $('#department').change(function () {
            var department = $(this).val();
            $.ajax({
                url: '/sorteos/moto-z-play-motomods-jbl-soundboost/ubigeo',
                data: {department: department},
                method: 'GET',
                success: function (response) {
                    var data = '<option>Provincia</option>';
                    $(response.data).each(function (i, val) {
                        data += '<option>' + val + '</option>';
                    });
                    $('#province').html(data);
                }
            });
        });

        $('#province').change(function () {
            var department = $(this).val();
            $.ajax({
                url: '/sorteos/moto-z-play-motomods-jbl-soundboost/ubigeo',
                data: {province: department},
                method: 'GET',
                success: function (response) {
                    var data = '<option>Distrito</option>';
                    $(response.data).each(function (i, val) {
                        data += '<option>' + val + '</option>';
                    });
                    $('#district').html(data);
                }
            });
        });
    },
    trackGA: function () {
        $('#motorola').click(function () {
            gtag('event', 'sorteo', {'event_category': 'header', 'event_action': 'click_button', 'event_label': 'motorola'});
            var url = $(this).data('href');
            window.open(url, $(this).data('target'));
        });
        $('.fb').click(function () {
            gtag('event', 'sorteo', {'event_category': 'header', 'event_action': 'click_button', 'event_label': 'facebook'});
            var url = $(this).data('href');
            window.open(url, $(this).data('target'));
        });
        $('.tw').click(function () {
            gtag('event', 'sorteo', {'event_category': 'header', 'event_action': 'click_button', 'event_label': 'twitter'});
            var url = $(this).data('href');
            window.open(url, $(this).data('target'));
        });
        $('.inst').click(function () {
            gtag('event', 'sorteo', {'event_category': 'header', 'event_action': 'click_button', 'event_label': 'instagram'});
            var url = $(this).data('href');
            window.open(url, $(this).data('target'));
        });
        $('.btn-login').click(function () {
            gtag('event', 'sorteo', {'event_category': 'body', 'event_action': 'click_button', 'event_label': 'login'});
            var url = $(this).data('href');
            window.open(url, $(this).data('target'));
        });
        $('.whiz').click(function () {
            gtag('event', 'sorteo', {'event_category': 'footer', 'event_action': 'click_button', 'event_label': 'whiz'});
            var url = $(this).data('href');
            window.open(url, $(this).data('target'));
        });
    },
    init: function () {
        popover.init();
        this.birthdate();
        this.formSubmit();
        this.ubigeo();
        this.trackGA();
    }
};

$(window).load(function() {
    $('.loading').addClass("loaded");
    $('body').addClass("loaded");

    $('#emb_slider').slick({
        autoplay: true,
        arrows: false,
        dots: true,
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    $('#shoot_reg').on("click", function(e){
       e.preventDefault();
    });

    $("#shoot_reg").on("hidden.bs.modal", function () {

    });

    $(function () {
        $('.show-info').tooltip()
    });
});