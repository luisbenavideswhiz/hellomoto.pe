var app = {
    util: {
        scrollTop: function () {
            $("html, body").stop().animate({scrollTop: 0}, 280, 'swing');
        },
        btns: {
            enable: function (element) {
                element.removeAttr('disabled');
            },
            disable: function (element) {
                element.attr('disabled', 'disabled');
            }
        },
        post: function (url, data) {
            app.util.btns.disable($('#register-btn'));
            $.post(url, data, function (response) {
                if (response.success === false) {
                    $.each(response, function (item, value) {
                        $('#' + item).addClass('form-error').attr('data-content', value);
                        $('.form-error').first().popover('show');
                        if ($('#' + item).length) {
                            app.util.scrollTop();
                        }
                        return true;
                    });
                } else {
                    if (typeof response.redirect === 'undefined') {
                        var content = $('.validacion');
                        content.removeClass('hidden').find('#user-name').html(response.name);
                        content.find('#user-email').html(response.email);
                        $('form')[0].reset();
                        app.util.scrollTop();
                    } else {
                        window.location.href = response.redirect;
                    }
                }
                app.util.btns.enable($('#register-btn'))
            }, 'json');
        }
    },
    home: {
        popover: {
            enable: function () {
                var validate = $('.validate');
                if (validate.length) {
                    validate.each(function (index, value) {
                        $(value).attr('data-toggle', 'popover')
                            .attr('data-html', 'true')
                            .attr('data-content', 'Campo requerido')
                            .attr('data-container', 'body')
                            .attr('data-trigger', 'focus')
                            .attr('data-placement', 'top');
                    });
                }
            },
            remove: function () {
                $('input, select').on('focus', function () {
                    $('#' + $(this).attr('id')).popover('destroy').removeClass('form-error');
                });
            },
            init: function () {
                app.home.popover.enable();
                app.home.popover.remove();
            }
        },
        btns: {
            register: function () {
                var register = $('#register-btn');
                if (register.length) {
                    register.on('click', function (e) {
                        e.preventDefault();
                        $('.validacion').addClass('hidden');
                        app.util.post($(this).data('url'), $('form').serializeArray());
                    });
                }
            },
            init: function () {
                app.home.btns.register();
            }
        },
        init: function () {
            app.home.popover.init();
            app.home.btns.init();
        }
    }
};

$(window).on('load', function () {
    var id = $('body').attr('id');
    if (typeof id !== 'undefined') {
        app[id].init();
    }
});