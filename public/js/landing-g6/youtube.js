var Youtube = function(){
    var video = {
        player: null,
        playerId: null,
        playlist: [],
        current: 0
    };
    var app = {
        init: function(playerId, playlist){
            video.playerId = playerId;
            video.playlist = playlist;
            app.newPlayer();
        },
        loadLibrary: function(){
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        },
        newPlayer: function(){
            video.player = new YT.Player(video.playerId, {
                width: utils.getWidth(),
                height: utils.getHeight(),
                videoId: video.playlist[video.current],
                playerVars: { controls:0, rel:0, showinfo:0, modestbranding:1 },
                events: {
                    'onStateChange': app.nextVideo
                }
            });
        },
        play: function(){
            if(video.player.getPlayerState() === 1) return;
            video.player.playVideo();
        },
        pause: function(){
            if(video.player.getPlayerState() === 2) return;
            video.player.pauseVideo();
        },
        scrollControl: function(){
            if(utils.isElementInViewport($('#'+video.playerId))) {
                app.play();
            }else{
                app.pause();
            }
        },
        nextVideo: function (event) {
            if(event.data === 0){
                if(video.current+1<video.playlist.length){
                    video.current++;
                }else{
                    video.current = 0;
                }
                utils.updateProgressBar(video.current);
                video.player.loadVideoById(video.playlist[video.current]);
            }
        }
    };

    var utils = {
        isElementInViewport: function(el){
            if (typeof jQuery === "function" && el instanceof jQuery) el = el[0];
            var rect = el.getBoundingClientRect();
            return (
                rect.top >= 0 &&
                rect.left >= 0 &&
                rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        },
        getHeight: function () {
            var height;
            if(window.mobileAndTabletcheck){
                height = '50%';
            }else{
                height = $(window).height();
            }
            return height;
        },
        getWidth: function () {
            var width;
            if(window.mobileAndTabletcheck){
                width = '100%';
            }else{
                width = '90%';
            }
            return width;
        },
        updateProgressBar: function (value) {
            var percent = (100/3)*(value+1);
            $('.progress-bar').css('width', percent +'%').attr('aria-valuenow', value);
        }
    };

    return {
        init : app.init,
        loadLibrary: app.loadLibrary,
        scrollControl: app.scrollControl
    }
}();