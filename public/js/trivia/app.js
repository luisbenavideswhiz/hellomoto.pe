$(document).ready(function () {
    // Clean Errors Popovers
    var validate = $('.popover-validate');
    if (validate.length) {
        validate.each(function (index, value) {
            $(value).attr('data-toggle', 'popover')
                .attr('data-html', 'true')
                .attr('data-content', 'Campo requerido')
                .attr('data-trigger', 'focus')
                .attr('data-placement', 'top')
                .attr('autocomplete', 'off');
        });
        $('input.popover-validate, select.popover-validate').on('focus', function () {
            var element = $(this).attr('name');
            $('form').each(function (index, value) {
                if ($(value).find('[name=' + element + ']').length) {
                    $(value).find('[name=' + element + ']').popover('dispose');
                    return false;
                }
            });
        });
        $('.nav-pills a').on('click', function () {
            $('.popover').popover('dispose')
        });
    }
    //Combodate
    $('#birthdate').combodate({minYear: 1938, maxYear: 2001, format: 'YYYY-MM-DD', template: 'D MMMM YYYY'});
    $('.day, .month, .year').css({
        'width': '31%',
        'height': '36px',
        'background-color': '#fff',
        'border-color': '#ced4da'
    });
    $('#btn-start').on('click', function (e) {
        e.preventDefault();
        if ($('#user').val() == 0) {
            $('#mod-log').modal('show');
        } else {
            window.location.href = '/trivia/pregunta/1';
        }
    });
    //Forms
    $('form').on('submit', function (e) {
        e.preventDefault();
        var form = $(this).prop('id');
        $.post($(this).prop('action'), $(this).serializeArray(), function (response) {
            if (!response.status) {
                $.each(response.data.errors, function (i, v) {
                    if (i === 'birthdate') {
                        $('.month').attr('data-placement', 'top').attr('data-content', v).popover('show');
                    } else {
                        $('#' + form).find('input[name=' + i + ']').first().attr('data-content', v).popover('show');
                    }
                    return false;
                });
            } else {
                window.location.href = response.data.redirect;
            }
        }, 'json');
    })

});

function footerFixed(){
    if ( $('.wrapper').height() + 150 > $(window).height()) {
        $('footer').removeClass('fixed');
    }
    else{
        $('footer').addClass('fixed');
    }
}

$(window).on('load resize', function(){
    footerFixed();
});


function resp(status) {
    if (status === true) {
        $('.text-r-icon').addClass('fa fa-check-circle-o naranja');
        $('.text-r').removeClass('rojo').addClass('naranja');
        $('.resp-1').html('¡Respuesta');
        $('.resp-2').addClass('naranja').html(' Correcta!');
    } else {
        $('.text-r-icon').addClass('fa fa-times-circle-o rojo');
        $('.text-r').removeClass('rojo').addClass('naranja');
        $('.resp-1').html('¡Respuesta');
        $('.resp-2').addClass('rojo').html(' Incorrecta!');
    }
}

function splitWords(str) {
    var res = str.split(' ');
    var length = res.length;
    var left = [], right = [];
    for (i = 0; i < length; i++) {
        if (i < length / 2) {
            left.push(res[i]);
        }
        else {
            right.push(res[i]);
        }
    }
    return left.join(' ') + '<span>' + ' ' + right.join(' ') + '</span>';
}

$('.questions button').click(function () {
    var btn = $(this);
    $('html, body').animate({
        scrollTop: $('#this_answer').offset().top
    }, 2000);
    $(document.body).addClass('bg-dark');
    $('.wrapper').css('height', '100%');
    $('.text-a').addClass('orange');
    $('.footer-pre').removeClass('footer-pre').addClass('footer');
    $('.progress-bar').removeClass('bg-warning').css('background-color', '#fc6a21');
    $('.progress').css('background-color', 'transparent').css('border', '1px solid #ffffff');
    $('.section-answer').removeClass('d-none').addClass('animated slideInUp').show();
    var data = {};
    data['resp'] = btn.find('input').val();
    $.get('/trivia/corregir', data, function (response) {
        $('.rpt').html(response.answer.answer);
        if (response.status === true) {
            $('#rpt-1').html(splitWords(response.answer.correct_description));
        } else {
            $('#rpt-1').html(splitWords(response.answer.error_description));
        }
        $('button').prop('disabled', true);
        $(btn).removeClass(['btn-outline-secondary', 'border-white', 'border', 'hvr-underline-from-left'])
            .addClass('bg-orange').prop('disabled', false);
        resp(response.status);
    });
});