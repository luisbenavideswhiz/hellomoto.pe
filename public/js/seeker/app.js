var app = {
    util: {
        btns: {
            enable: function (element) {
                element.removeClass('disabled');
                element.removeAttr('disabled');
            },
            disable: function (element) {
                element.addClass('disabled');
                element.attr('disabled', 'disabled');
            }
        },
        popover: {
            enable: function () {
                var validate = $('.validate');
                if (validate.length) {
                    validate.each(function (index, value) {
                        $(value).attr('data-toggle', 'popover')
                            .attr('data-html', 'true')
                            .attr('data-content', 'Campo requerido')
                            .attr('data-container', 'body')
                            .attr('data-trigger', 'focus')
                            .attr('data-placement', 'top');
                    });
                }
            },
            remove: function () {
                $('input, select').on('focus', function () {
                    $('#' + $(this).attr('id')).popover('destroy').removeClass('form-error');
                });
            },
        },
        post: function (url, data) {
            app.util.btns.disable($('#register-btn'));
            $.post(url, data, function (response) {
                if (response.success === false) {
                    $.each(response, function (item, value) {
                        $('[name=' + item + ']').addClass('form-error').attr('data-content', value);
                        $('.form-error').first().popover('show');
                        return true;
                    });
                } else {
                    $('#response').html(response.html);
                }
                app.util.btns.enable($('#register-btn'));
            }, 'json');
        }
    },
};

$(document).on('ready', function () {

    app.util.popover.enable();

    app.util.popover.remove();

    $('#register-btn').on('click', function (e) {
        e.preventDefault();
        var element = $(this);
        if (!element.hasClass('disabled')) {
            app.util.post(element.data('url'), $('form').serializeArray());
        }

    });
});