$(document).on('ready', function () {
});

$(window).on('load', function () {
    instagram();
});

$(window).scroll(function () {
    var items = $('#section-content').find('.item').length;
    if (items >= 6) {
        var fromTop = $(this).scrollTop();
        if (fromTop > 205) {
            $('header.d').addClass('rev');
        } else {
            $('header.d').removeClass('rev');
        }
        if (fromTop > 210) {
            $('header.d').addClass('trev');
        } else {
            $('header.d').removeClass('trev');
        }
    }
});