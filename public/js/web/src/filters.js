(function () {
    var filter = $('.filters');
    if (filter.length) {
        filter.on('change', function (e) {
            e.preventDefault();
            var category = $('#current_category').val();
            var subcategory = $('#filter_subcategory').val();
            var orderby = $('#filter_orderby').val();
            window.location.href = '/' + category + '/' + subcategory + '/' + orderby;
        });
    }
})();