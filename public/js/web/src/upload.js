(function () {
    var file = $('#profile_img');
    if (file.length) {
        file.on('change', function (e) {
            e.preventDefault();
            var element = $(this);
            var image = element[0].files[0];
            var form = new FormData();
            form.append('profile_img', image);
            form.append('_token', $('input[name="_token"]').val());
            $.ajax({
                url: element.data('url'),
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (response) {
                    if (response.status) {
                        window.location.href = response.data.redirect;
                    }
                }
            });
        });
    }
})();