(function () {
    var recovery = $('#recovery-btn');
    if (recovery.length) {
        recovery.on('click', function (e) {
            e.preventDefault();
            var form = $('form#recovery-form');
            var url = form.data('url');
            var data = form.serializeArray();
            data.push({
                name: '_token',
                value: $('input[name="_token"]').val()
            });
            $('input.popover-validate, select.popover-validate').popover('destroy');
            $.post(url, data, function (response) {
                if (response.status) {
                    $("#newpassModal").modal('show');
                } else {
                    show_errors({
                        'errors': response.data.errors,
                        'form': form,
                        'extra': 'recovery_'
                    });
                }
            });
        });
    }
    var change = $('#change-pass-btn');
    if (change.length) {
        change.on('click', function (e) {
            e.preventDefault();
            var form = $('form#change-pass-form');
            var url = form.data('url');
            var data = form.serializeArray();
            data.push({
                name: '_token',
                value: $('input[name="_token"]').val()
            });
            $('input.popover-validate, select.popover-validate').popover('destroy');
            $.post(url, data, function (response) {
                if (response.status) {
                    $('#recovery-form')[0].reset();
                    $('#change-pass-form')[0].reset();
                    $("#confirm-pass").html(response.msg);
                    setTimeout(function () {
                        $("#confirm-pass").html('');
                        $("#iforgotModal").modal('hide');
                        $("#newpassModal").modal('hide');
                    }, 5000)
                } else {
                    show_errors({
                        'errors': response.errors,
                        'form': form,
                    });
                }
            });
        });
    }
})();