(function () {
    var btn = $('#login-btn');
    if (btn.length) {
        btn.on('click', function (e) {
            e.preventDefault();
            var form = $('form#login-form');
            var url = form.data('url');
            var data = form.serializeArray();
            data.push({
                name: '_token',
                value: $('input[name="_token"]').val()
            });
            post({
                'url': url,
                'form': form,
                'data': data,
                'extra': 'l_'
            });
        });
    }
})();