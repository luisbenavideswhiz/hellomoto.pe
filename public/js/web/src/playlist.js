(function () {
    var btn = $('#playlist-follow-btn');
    if (btn.length) {
        btn.on('click', function (e) {
            e.preventDefault();
            var element = $(this);
            var logged = element.data('logged');
            if (logged == 'logged') {
                var url = element.data('url');
                var id = element.data('referal-id');
                var data = {
                    '_token': $('input[name="_token"]').val(),
                    'referal_id': id,
                };
                $.post(url, data, function (response) {
                    if (response.status) {
                        element.html(response.text);
                        element.data('url', response.url);
                    }
                }, 'json');
            } else {
                var device = $('body').attr('data-device');
                $('#login-modal-' + device + '[data-target="#loginModal"]').trigger('click');
            }
        });
    }
    var unlike = $('.unlike');
    if (unlike.length) {
        unlike.on('click', function (e) {
            e.preventDefault();
            var element = $(this);
            var logged = element.data('logged');
            var url = element.data('url');
            var id = element.data('referal-id');
            var data = {
                '_token': $('input[name="_token"]').val(),
                'referal_id': id,
            };
            $.post(url, data, function (response) {
                if (response.status) {
                    window.location.href = '/usuario/mis-playlist';
                }
            }, 'json');
        });
    }
})();