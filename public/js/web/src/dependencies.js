(function () {
    var device = $('body').data('device');
    var suggester = Cookies.get('suggester');
    if (suggester != 'omitted') {
        $('.msuggester').removeClass("unactive");
    }

    if (getParam('modal') == 'login') {
        $('#login-modal-' + device).trigger('click');
    }

    if (getParam('error-login') == 'facebook' ||
        getParam('error-login') == 'twitter' ||
        getParam('error-login') == 'google') {
        $('#login-modal-' + device).trigger('click');
        $('#login_username')
            .attr('data-content', 'Usuario no registrado')
            .popover('show');
    }

    if (getParam('modal') == 'register') {
        $('#login-modal-' + device).trigger('click');
        $('.nav-tabs').find('a[href="#tab2"]').trigger('click');
    }

    $('[data-toggle="tooltip"]').tooltip();

    var slider = $('.mollie-slider');
    if (slider.length) {
        slider.slick({
            centerMode: true,
            variableWidth: true,
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            draggable: true,
            adaptiveHeight: true,
            nextArrow: '<button type="button" class="slick-next slick-nav"><i class="fa fa-angle-right"></i></button>',
            prevArrow: '<button type="button" class="slick-prev slick-nav"><i class="fa fa-angle-left"></i></button>'
        });
    }

    var ultimas = $('.ultimas_slider');
    if (ultimas.length) {
        ultimas.slick({
            autoplay: true,
            dots: false,
            infinite: true,
            speed: 600,
            slidesToShow: 2,
            slidesToScroll: 2,
            draggable: true,
            adaptiveHeight: true,
            nextArrow: '<button type="button" class="slick-next slick-nav"><i class="fa fa-angle-right"></i></button>',
            prevArrow: '<button type="button" class="slick-prev slick-nav"><i class="fa fa-angle-left"></i></button>'
        });
    }

    $('.menutrigger').on('click', function () {
        $(this).toggleClass("active");
        $('.rmenu').toggleClass("active");
    });

    $('.search-input').keyup(function (e) {
        e.preventDefault();
        if (e.keyCode == 13) {
            window.location.href = '/buscar/' + $(this).val();
        }
    });

    $('#close_suggester').on('click', function () {
        $('.msuggester').addClass("unactive");
        Cookies.set('suggester', 'omitted');
    });

    $('.gotoup a').on('click', function () {
        $('html, body').animate({
            scrollTop: $('body').offset().top
        }, 500);
        return false;
    });

    $('#birthdate').datetimepicker({
        debug: true,
        locale: 'es',
        format: 'YYYY-MM-DD',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-crosshairs',
            clear: 'fa fa-trash',
            close: 'fa fa-times'
        },
    });

    $('#r_search span').on("click", function () {
        $('#r_search .searcher').toggleClass("ractive");
    });

    $("#gallery").unitegallery({
        gallery_theme: "slider",
        slider_scale_mode: "fit",
    });
})();