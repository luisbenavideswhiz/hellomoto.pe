(function () {
    var getMore = $('#get-more-rows');
    if (getMore.prop('tagName') === 'INPUT') {
        $(window).scroll(function () {
            if (Math.ceil($(window).scrollTop()) === $(document).height() - $(window).height()) {
                var category = $('[data-category].active').data('category');
                var page = parseInt(getMore.attr('data-page'));
                var options = {
                    'category': category,
                    'page': page,
                    'url': getMore.data('url')
                };
                get_rows(options);
            }
        });
    } else {
        if (getMore.length) {
            getMore.on('click', function (e) {
                e.preventDefault();
                var category = $('[data-category].active').data('category');
                var page = parseInt(getMore.attr('data-page'));
                var options = {
                    'category': category,
                    'page': page,
                    'url': getMore.data('url')
                };
                get_rows(options);
            });
        }
    }
})();