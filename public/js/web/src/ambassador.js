(function () {
    var btn = $('#ambassador-follow-btn');
    if (btn.length) {
        btn.on('click', function (e) {
            e.preventDefault();
            var element = $(this);
            var logged = element.data('logged');
            if (logged == 'logged') {
                var url = element.data('url');
                var id = element.data('referal-id');
                var data = {
                    '_token': $('input[name="_token"]').val(),
                    'referal_id': id,
                };
                $.post(url, data, function (response) {
                    if (response.status) {
                        element.html(response.data.text);
                        element.data('url', response.data.url);
                    }
                }, 'json');
            } else {
                var device = $('body').attr('data-device');
                $('#login-modal-' + device + '[data-target="#loginModal"]').trigger('click');
            }
        });
    }
    var unfollow = $('.unfollow');
    if (unfollow.length) {
        unfollow.on('click', function (e) {
            e.preventDefault();
            var element = $(this);
            var url = element.data('url');
            var id = element.data('referal-id');
            var data = {
                '_token': $('input[name="_token"]').val(),
                'referal_id': id,
            };
            $.post(url, data, function (response) {
                if (response.status) {
                    window.location.href = '/usuario/mis-embajadores';
                }
            }, 'json');
        });
    }
})();