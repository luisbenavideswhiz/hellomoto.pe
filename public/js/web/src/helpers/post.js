var post = function (options) {
    $('popover-validate').popover('destroy');
    $.post(options.url, options.data, function (response) {
        if (response.status) {
            window.location.href = response.data.redirect;
        } else {
            show_errors({
                'errors': response.data.errors,
                'form': options.form,
                'extra': options.extra
            });
        }
    });
};