(function () {
    var validate = $('.popover-validate');
    if (validate.length) {
        validate.each(function (index, value) {
            $(value).attr('data-toggle', 'popover')
                .attr('data-html', 'true')
                .attr('data-content', 'Campo requerido')
                .attr('data-trigger', 'focus')
                .attr('data-placement', 'top')
                .attr('autocomplete', 'off');
        });
        $('.popover-validate').on('focus', function () {
            var element = $(this).attr('name');
            $('form').each(function (index, value) {
                if ($(value).find('[name=' + element + ']').length) {
                    $(value).find('[name=' + element + ']').popover('destroy');
                    return false;
                }
            });
        });
        $('input[type=radio]').on('click', function () {
           var element = $(this).attr('name');
           if(element=='sex' || element=='accept_emails' || element=='age_verify'){
               $('#r_'+element).popover('destroy');
           }
        });
        $(document).on('change', 'select', function () {
            var element = $(this).attr('class').trim();
            if(element=='day' || element=='month' || element=='year'){
                $('#r_birthdate').popover('destroy');
            }
        });
    }
})();