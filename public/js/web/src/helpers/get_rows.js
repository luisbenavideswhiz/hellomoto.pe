var get_rows = function (options) {
    if (typeof request !== 'undefined') {
        window.request.abort();
        window.request = null;
        $('#loader').remove();
    }
    $('.cmc').addClass('hidden');
    var loading = '<div id="loader" class="text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span><br><br></div>';
    var section_content = $('#section-content');
    if (options.page == 1) {
        section_content.html('');
    }
    section_content.append(loading);
    var data = {
        response_type: 'html',
        category: options.category,
        page: options.page,
        _token: $('[name=_token]').val(),
    };
    var subcategory = $('#filter_subcategory');
    if (subcategory.length) {
        data.subcategory = subcategory.val();
    }
    var orderby = $('#filter_orderby');
    if (orderby.length) {
        data.orderby = orderby.val();
    }


    window.request = $.post(options.url, data, function (response) {
        section_content.find('#loader').remove();
        if (response.status) {
            $('.cmc').removeClass('hidden');
            if (options.page == 1) {
                section_content.html(response.data.html);
            } else {
                section_content.append(response.data.html);
            }
        } else {
            $('.cmc').addClass('hidden');
        }
        $('#get-more-rows').attr('data-page', (options.page + 1));
        $('.item-filter').removeClass('in-progress');
        $('#loader').addClass('hidden');
    });
};