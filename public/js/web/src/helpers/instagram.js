var instagram = function () {
    var instagram = $('.instagram-item');
    if (instagram.length) {
        instagram.on('click', function (e) {
            e.preventDefault();

            var element = $(this);
            var modal = $('#instagram-modal');

            modal.find('#image').addClass('hidden');
            modal.find('#video').addClass('hidden');

            modal.find('#text').html(element.data('text'));
            modal.find('#link').attr('href', element.data('link'));
            modal.find('#likes').text(element.data('likes'));
            modal.find('#comments').text(element.data('comments'));
            modal.find('#user-image').attr('src', element.data('user-image'));
            modal.find('#username').html(element.data('username'));

            if (element.data('filetype') == 'mp4') {
                modal.find('#video').attr('src', element.data('filename'));
                modal.find('#video').removeClass('hidden');
            } else {
                modal.find('#image').attr('src', element.data('filename'));
                modal.find('#image').removeClass('hidden');
            }

            modal.promise().done(function () {
                $('#instagram-modal').modal('show');
            });
        });
    }
};