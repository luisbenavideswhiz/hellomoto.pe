var show_errors = function (options) {
    $.each(options.errors, function (item, value) {
        options.form
            .find('#' + options.extra + item)
            .first()
            .attr('data-content', value)
            .popover('show');
        return false;
    });
};