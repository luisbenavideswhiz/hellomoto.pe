(function () {
    $('#birthdate').combodate({minYear: 1938, maxYear: 2000, format: 'YYYY-MM-DD', template: 'DD MMMM YYYY'});
    $('.day, .month, .year').css({
        'width': '28%',
        'background-color': '#fff'
    });
    var btn = $('#register-btn');
    if (btn.length) {
        btn.on('click', function (e) {
            e.preventDefault();
            var form = $('form#register-form');
            var url = form.data('url');
            var data = form.serializeArray();
            data.push({
                name: '_token',
                value: $('input[name="_token"]').val()
            });
            post({
                'url': url,
                'form': form,
                'data': data,
                'extra': 'r_'
            });
        });
    }
})();