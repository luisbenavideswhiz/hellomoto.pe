(function () {
    var section = $('[data-category]');
    if (section.length) {
        section.on('click', function (e) {
            e.preventDefault();
            var element = $(this);
            var item_filter = $('.item-filter');
            if (!item_filter.hasClass('in-progress')) {
                item_filter.addClass('in-progress');
                item_filter.find('a').removeClass('active');
                element.addClass('active');
                var options = {
                    'url': item_filter.data('url'),
                    'category': element.data('category'),
                    'orderby': element.data('orderby'),
                    'page': 1
                };
                get_rows(options);
            }
        });
    }
})();