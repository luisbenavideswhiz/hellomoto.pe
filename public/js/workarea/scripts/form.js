var Form = function () {

    var ajaxMultipart = function (form) {
        return {
            url: form.prop('action'),
            type: form.prop('method'),
            data: new FormData(form[0]),
            cache: false,
            contentType: false,
            processData: false,
            success: function (response){
                if(!response.status){
                    $.each(response.data.errors, function (i, val) {
                        toastr.error(val,'Error');
                    });
                }else{
                    if(typeof response.data.redirect !=='undefined'){
                        window.location.href = response.data.redirect;
                    }
                    toastr.success(response.message,'Correcto');
                }
            }
        };
    };

    var ajaxSimple = function (form) {
        return {
            url: form.prop('action'),
            type: form.prop('method'),
            data: form.serialize(),
            success: function (response){
                if(!response.status){
                    $.each(response.data.errors, function (i, val) {
                        toastr.error(val,'Error');
                    });
                }else{
                    if(typeof response.data.redirect !=='undefined'){
                        window.location.href = response.data.redirect;
                    }
                    toastr.success(response.message,'Correcto');
                }
            }
        };
    };

    var submitForm = function (id, multipart) {
        $(id).submit(function (event) {
            event.preventDefault();
            CKEditorUpdate();
            var form = $(this);
            var callback;
            multipart = multipart || false;
            if(multipart){
                callback = ajaxMultipart(form);
            }else{
                callback = ajaxSimple(form);
            }
            $.ajax(callback);
        });
    };

    var selectInputGroup = function (id) {
        $(id).on("click.bs.dropdown", function(event){
            event.preventDefault();
            var content = ' <span class="caret"></span>';
            if($(event.target).prop('tagName')!=='BUTTON' && $(event.target).prop('tagName')!=='SPAN'){
                if($(event.target).text().trim()!==''){
                    content = $(event.target).html() + content;
                    $(this).find('.dropdown-toggle').html(content);
                    $(this).find('input').val($(event.target).data('id'));
                }else if($(event.target).prop('tagName')==='I' && $(event.target).parent().prop('tagName')!=='BUTTON'){
                    content = $(event.target).parent().html() + content;
                    $(this).find('.dropdown-toggle').html(content);
                    $(this).find('input').val($(event.target).data('id'));
                }
            }
        });
    };

    var addNetwork = function (id, content, clone) {
      $(id).click(function () {
          if($(content+'>'+clone).length<5){
              $(content).append($('#template-social-network').html());
              Form.selectInputGroup('.input-group-btn');
          }
      });
    };

    var addPlaylist = function (id, content, clone) {
        $(id).click(function () {
            if($(content+'>'+clone).length<5){
                $(content).append($('#template-playlist').html());
                Form.selectInputGroup('.input-group-btn');
            }
        });
    };

    var addQuestion = function (id, content, clone) {
        $(id).click(function () {
            if($(content+'>'+clone).length<4){
                $(content).append($(content+'>'+clone+':last').clone());
                $(content+'>'+clone+':last').find('input').empty();
                $(content+'>'+clone+':last').find('input[type=hidden]').remove();
                $(content+'>'+clone+':last').find('.check-correct').val($(content+'>'+clone).length);
            }else{
                toastr.error('No se puede agregar mas de 4 alternativas','Error');
            }
        });
    };

    var deleteNetwork = function (className, dropdown) {
      $(document).on('click', className,function () {
        $(this).parent().parent().remove();
      });
    };

    var deleteQuestion = function (className, dropdown) {
        $(document).on('click', className,function () {
            if($(className).length!==1){
                $(this).parent().parent().remove();
            }else{
                $(this).parent().parent().find('input[type=text]').val('');
                $(this).parent().parent().find('input[type=hidden]').val('');
                $(this).parent().parent().find('input[type=checkbox]').prop('checked', false);
                $(this).parent().parent().find(dropdown).html('Seleccionar <span class="caret"></span>');
            }
        });
    };

    var deletePlaylist = function (className) {
        $(document).on('click', className,function () {
            var button = $(this);
            var id = button.data('playlist');
            if(typeof id!=='undefined'){
                button.html('<i class="glyphicon glyphicon-hourglass"></i><span>Eliminando ...</span>');
                $.post('/workarea/playlists/'+id, {_method:"DELETE",_token:$('meta[name="csrf-token"]').attr('content')},function(response){
                    if(response.status){
                        $(this).parent().parent().parent().remove();
                    }
                });
            }else{
                $(this).parent().parent().parent().remove();
            }
        });
    };

    var CKEditorUpdate = function () {
        if(typeof CKEDITOR !=='undefined'){
            for (instance in CKEDITOR.instances){
                CKEDITOR.instances[instance].updateElement();
            }
        }
    };

    var getData = function (id, callback) {
        $.get($(id).data('action'), callback);
    };

    return {
        form: submitForm,
        get: getData,
        selectInputGroup: selectInputGroup,
        addNetwork: addNetwork,
        deleteNetwork: deleteNetwork,
        deletePlaylist: deletePlaylist,
        addPlaylist: addPlaylist,
        addQuestion: addQuestion,
        deleteQuestion: deleteQuestion
    };
}();
