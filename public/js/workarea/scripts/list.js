var List = function () {

    var Modal = {
        button: null,
        setButton: function (button) {
            Modal.button = button;
        },
        confirm: function (modal, callback) {
            $(".modal-btn-confirm").on("click", function () {
                callback(true, Modal.button);
                $(modal).modal('hide');
            });

            $(".modal-btn-cancel").on("click", function () {
                callback(false, Modal.button);
                $(modal).modal('hide');
            });
        },
        init: function (modal) {
            $(modal).modal('show');
        },
        action: function (confirm, btn) {
            if (confirm) {
                $.ajax({
                    url: btn.data('action'),
                    type: 'POST',
                    data: {
                        _method: 'DELETE',
                        _token: $('meta[name="csrf-token"]').prop('content')
                    },
                    success: function (response) {
                        if (response.status) {
                            btn.parents('tr').remove();
                            toastr.success(response.message, 'Bien');
                        } else {
                            toastr.error(response.message, 'Error');
                        }
                    }
                });
            }
        }
    };

    function getUrlVars()
    {
        var vars = {}, hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        if(hashes[0]!==window.location.href){
            for(var i = 0; i < hashes.length; i++)
            {
                hash = hashes[i].split('=');
                vars[hash[0]] = hash[1];
            }
        }
        return vars;
    }

    return {
        deleteButton: function (id, modal) {
            Modal.confirm(modal, Modal.action);

            $(id).click(function (event) {
                event.preventDefault();
                Modal.setButton($(this));
                Modal.init(modal);
            });
        },
        createModal: function (id, modal, form) {
            Form.form(form, true);
            $(id).click(function (event) {
                event.preventDefault();
                Modal.setButton($(this));
                Modal.init(modal);
            });
        },
        updateModal: function (id, modal, form, base_url) {
            Form.form(form, true);
            $(id).click(function (event) {
                event.preventDefault();
                Modal.setButton($(this));
                Form.get($(this), function (response) {
                    if (response.status) {
                        var data = response.data;
                        for (var key in data) {
                            if (key === 'position'){
                                if(data[key] !== null){
                                    $('#'+ key + '_u').parents('.form-group').show();
                                }else{
                                    $('#'+ key + '_u').prop('disable', true);
                                    $('#'+ key + '_u').parents('.form-group').hide();
                                }
                            }
                            if ($('#' + key + '_u').prop("tagName") === 'IMG') {
                                $('#' + key + '_u').prop('src', data[key]);
                            } else {
                                $('#' + key + '_u').val(data[key]);
                            }
                            if (key === 'keywords') {
                                $('#' + key + '_u').importTags(data[key]);
                            }
                        }
                        $(form).prop('action', base_url + $('#id_u').val());
                        Modal.init(modal);
                    }
                });
            });
        },
        downloadList: function (url) {

            var urlQuery = window.location.search;
            //urlQuery.dates = urlQuery.dates.replace('+', '%252B');
            window.open(url +''+ urlQuery, '_blank');
        }
    };
}();
