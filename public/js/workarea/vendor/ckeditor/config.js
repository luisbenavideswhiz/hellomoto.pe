/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
    config.language = 'es';
	// config.uiColor = '#AADC6E';

    /*config.toolbarGroups = [
        { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
        { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
        { name: 'links' },
        { name: 'insert' },
        { name: 'forms' },
        { name: 'tools' },
        { name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'others' },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
        { name: 'styles' },
        { name: 'colors' }
    ];*/

    config.toolbarGroups = [
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'insert', groups: [ 'insert' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'document', groups: [ 'document', 'doctools', 'mode' ] },
        { name: 'others', groups: [ 'others' ] },
        { name: 'colors', groups: [ 'colors' ] },
        '/',
        { name: 'forms', groups: [ 'forms' ] },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'align', 'blocks', 'bidi', 'paragraph' ] },
        { name: 'styles', groups: [ 'styles' ] }
    ];


    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';

    // Simplify the dialog windows.
    config.removeDialogTabs = 'image:advanced;link:advanced';
    config.removeButtons = 'Flash,Smiley,PageBreak,Iframe,CreateDiv,Language,Form,Radio,Checkbox,TextField,Textarea,Select,Button,ImageButton,HiddenField,Replace,SelectAll,Find,Templates,Save,NewPage,Preview,BidiLtr,BidiRtl,Font,FontSize,ShowBlocks,About';

    config.extraPlugins = 'widgetselection,lineutils,widget,notificationaggregator,embed,embedbase,autolink,autoembed,colorbutton,colordialog,html5video';
    config.embed_provider = '//iframe.ly/api/oembed?url={url}&callback={callback}&api_key=dc6a88f27675d0a144044f';

    config.filebrowserImageBrowseUrl =  '/workarea/images/browser';
    config.filebrowserImageUploadUrl = '/workarea/images/uploader?command=QuickUpload';

    //config.filebrowserBrowseUrl = '/workarea/videos/browser';
    config.filebrowserUploadUrl = '/workarea/videos/uploader?command=QuickUpload';
};

