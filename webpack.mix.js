const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
    'public/js/jquery.min.js',
    'public/js/bootstrap.min.js',
    'public/js/jquery.cookie.js',
    'public/js/moment.min.js',
    'public/js/moment-locale-es.js',
    'public/js/bootstrap-datetimepicker.min.js',
    'public/js/jquery.fancybox.min.js',
    'public/js/slick.min.js',
    'public/js/combodate.js',
    'public/js/unitegallery.min.js',
    'public/js/ug-theme-slider.js',
], 'public/js/web/bundle.js');

mix.scripts([
    'public/js/web/src/helpers/get_rows.js',
    'public/js/web/src/helpers/instagram.js',
    'public/js/web/src/helpers/popover.js',
    'public/js/web/src/helpers/post.js',
    'public/js/web/src/helpers/errors.js',
    'public/js/web/src/helpers/url_param.js',

    'public/js/web/src/dependencies.js',
    'public/js/web/src/filters.js',
    'public/js/web/src/get_more_rows.js',
    'public/js/web/src/sections.js',

    'public/js/web/src/login.js',
    'public/js/web/src/register.js',
    'public/js/web/src/update.js',
    'public/js/web/src/recovery.js',
    'public/js/web/src/upload.js',

    'public/js/web/src/ambassador.js',
    'public/js/web/src/playlist.js',

    'public/js/web/src/jquery.js',

], 'public/js/web/all.js');

mix.styles([
    './public/css/bootstrap.min.css',
    './public/css/unite-gallery.css',
    './public/css/ug-theme-default.css',
    './public/css/web/bootstrap-datetimepicker.min.css',
    './public/css/jquery.fancybox.min.css',
    './public/css/font-awesome.min.css'
], 'public/css/web/bundle.css').version();

mix.styles([
    './public/css/web/style.css',
    './public/css/web/responsive.css',
    './public/css/web/popover.css',
], 'public/css/web/all.css').version();

//mix.js('./resources/assets/js/app.js', './public/js')
//mix.sass('./resources/assets/sass/app.scss', './public/css')

/*mix.sass('./resources/assets/sass/landings/dimitrivegas/style.scss', './public/css/landings/dimitrivegas');
 mix.sass('./resources/assets/sass/landings/dimitrivegas/responsive.scss', './public/css/landings/dimitrivegas');*/

//mix.styles([
//    './public/css/landings/dimitrivegas/style.css',
//    './public/css/landings/dimitrivegas/responsive.scss'
//], 'public/css/landings/dimitrivegas/build.css').version();

mix.sass('./resources/assets/sass/landings/comunidad/style.scss', './public/css/landings/comunidad');
mix.sass('./resources/assets/sass/landings/comunidad/responsive.scss', './public/css/landings/comunidad');

mix.sass('./resources/assets/sass/landings/unete/style.scss', './public/css/landings/unete');
mix.sass('./resources/assets/sass/landings/unete/responsive.scss', './public/css/landings/unete');

mix.sass('./resources/assets/sass/landings/motogift/style.scss', './public/css/landings/motogift');
mix.sass('./resources/assets/sass/landings/motogift/responsive.scss', './public/css/landings/motogift');

mix.sass('./resources/assets/sass/landings/copamotorola/style.scss', './public/css/landings/copamotorola');
mix.sass('./resources/assets/sass/landings/copamotorola/responsive.scss', './public/css/landings/copamotorola');

mix.sass('./resources/assets/sass/landings/motovan/style.scss', './public/css/landings/motovan');
mix.sass('./resources/assets/sass/landings/motovan/responsive.scss', './public/css/landings/motovan');

mix.sass('./resources/assets/sass/landings/intriga/style.scss', './public/css/landings/intriga');
mix.sass('./resources/assets/sass/landings/intriga/responsive.scss', './public/css/landings/intriga');

mix.sass('./resources/assets/sass/landings/tolinchi/style.scss', './public/css/landings/tolinchi');
mix.sass('./resources/assets/sass/landings/tolinchi/responsive.scss', './public/css/landings/tolinchi');

mix.sass('./resources/assets/sass/landings/mario/style.scss', './public/css/landings/mario');
mix.sass('./resources/assets/sass/landings/mario/responsive.scss', './public/css/landings/mario');

mix.sass('./resources/assets/sass/workarea/style.scss', './public/css/workarea/');
mix.sass('./resources/assets/sass/workarea/responsive.scss', './public/css/workarea/');

mix.sass('./resources/assets/sass/web/style.scss', './public/css/web/');
mix.sass('./resources/assets/sass/web/responsive.scss', './public/css/web/');

mix.sass('./resources/assets/sass/web/plb/style.scss', './public/css/web/plb/');
mix.sass('./resources/assets/sass/web/plb/responsive.scss', './public/css/web/plb/');

mix.sass('./resources/assets/sass/landing-g6/style.scss', './public/css/landing-g6/');
mix.sass('./resources/assets/sass/landing-g6/responsive.scss', './public/css/landing-g6/');

mix.sass('./resources/assets/sass/trivia/style.scss', './public/css/trivia/');
mix.sass('./resources/assets/sass/trivia/responsive.scss', './public/css/trivia/');

mix.browserSync('hellomoto.fake');

// Full API
// mix.js(src, output);
// mix.react(src, output); <-- Identical to mix.js(), but registers React Babel compilation.
// mix.extract(vendorLibs);
// mix.sass(src, output);
// mix.standaloneSass('src', output); <-- Faster, but isolated from Webpack.
// mix.less(src, output);
// mix.stylus(src, output);
// mix.browserSync('my-site.dev');
// mix.combine(files, destination);
// mix.babel(files, destination); <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
// mix.minify(file);
// mix.sourceMaps(); // Enable sourcemaps
// mix.version(); // Enable versioning.
// mix.disableNotifications();
// mix.setPublicPath('path/to/public');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// mix.options({
//   extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
//   processCssUrls: true, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
//   purifyCss: false, // Remove unused CSS selectors.
//   uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
//   postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
// });
